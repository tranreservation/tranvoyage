-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : sam. 12 mars 2022 à 23:40
-- Version du serveur : 10.3.31-MariaDB-0+deb10u1
-- Version de PHP : 7.3.31-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `tranv1550213`
--

-- --------------------------------------------------------

--
-- Structure de la table `affectations`
--

CREATE TABLE `affectations` (
  `id_affectations` int(12) NOT NULL,
  `utilisateurs_affectation_id` int(12) DEFAULT NULL,
  `gare_affectation_id` int(12) DEFAULT NULL,
  `date_creat_affectation` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `affectations`
--

INSERT INTO `affectations` (`id_affectations`, `utilisateurs_affectation_id`, `gare_affectation_id`, `date_creat_affectation`) VALUES
(3, 4, 3, '2021-04-26 14:34:59'),
(5, 89, 5, '2021-04-28 18:38:33');

-- --------------------------------------------------------

--
-- Structure de la table `annonces`
--

CREATE TABLE `annonces` (
  `id_annonces` int(12) NOT NULL,
  `libelle_annonces` text DEFAULT NULL,
  `date_creat_annonces` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `annonces`
--

INSERT INTO `annonces` (`id_annonces`, `libelle_annonces`, `date_creat_annonces`) VALUES
(4, 'LA LISTE DES GARES SELON LES DESTINATIONS: Plus besoin de vous dÃ©placer si vous pouvez acheter vos tickets en toute sÃ©cutitÃ© ici', '2021-05-02 15:51:49');

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

CREATE TABLE `compagnie` (
  `id_compagnie` int(11) NOT NULL,
  `nom_compagnie` varchar(255) NOT NULL,
  `email_comp` varchar(200) DEFAULT NULL,
  `telephone_comp` varchar(200) DEFAULT NULL,
  `secur_ajout` varchar(255) DEFAULT NULL,
  `id_destination` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compagnie`
--

INSERT INTO `compagnie` (`id_compagnie`, `nom_compagnie`, `email_comp`, `telephone_comp`, `secur_ajout`, `id_destination`, `created_at`, `updated_at`) VALUES
(3, 'CTE TRANSPORT ', 'ctecompagniedetransportexpress@gmail.com', '0505489532', NULL, 0, '2021-04-25 08:42:36', '2021-04-25 08:42:36'),
(4, 'SABE TRANSPORT ', 'tran.reservation@gmail.com', '22504375', NULL, 0, '2021-04-25 08:45:45', '2021-04-25 08:45:45'),
(5, 'SITTI TRANSPORT ', 'sitti@gmail.com', '0777251106', NULL, 0, '2021-04-25 08:51:02', '2021-04-25 08:51:02'),
(6, 'GAMON TRANSPORT ', 'gamon@gmail.com', '0103339813', NULL, 0, '2021-04-25 08:52:22', '2021-04-25 08:52:22'),
(7, 'TTF TRANSPORT ', 'ttf@gmail.com', '0709044555', NULL, 0, '2021-04-25 08:54:06', '2021-04-25 08:54:06'),
(8, 'COMPAGNIE DE TRANSPORT TCHOLOGO OFFICIEL ', 'ctto@gmail.com', '0575112078/0747707778', NULL, 0, '2021-04-25 08:59:17', '2021-04-25 08:59:17'),
(9, 'GTT TRANSPORT ', 'gtt@gmail.com', '0140796023', NULL, 0, '2021-04-25 09:01:56', '2021-04-25 09:01:56'),
(10, 'STBA TRANSPORT ', 'stba@gmail.com', '0707673454', NULL, 0, '2021-04-25 09:04:16', '2021-04-25 09:04:16'),
(11, 'STC TRANSPORT ', 'stc@gmail.com', '0707853060', NULL, 0, '2021-04-25 09:06:04', '2021-04-25 09:06:04'),
(12, 'UTS TRANSPORT ', 'uts@gmail.com', '0777601570', NULL, 0, '2021-04-25 09:09:16', '2021-04-25 09:09:16'),
(13, 'TRANSPORT AVS', 'transportavs@gmail.com', '0102956063', NULL, 0, '2021-04-25 09:11:25', '2021-04-25 09:11:25'),
(14, 'GDF TRANSPORT ', 'gdftransport@gmail.com', '0779801963', NULL, 0, '2021-04-25 09:12:47', '2021-04-25 09:12:47'),
(15, 'TCF EXPRESS MISTRAL ', 'tcf@gmail.com', '0140511652', NULL, 0, '2021-04-25 09:15:22', '2021-04-25 09:15:22'),
(16, 'GDF TRANSPORT *', 'gdftransport@cmail.com', '20372037/ 0758609881', NULL, 0, '2021-04-25 09:19:58', '2021-04-25 09:19:58'),
(17, 'GANA TRANSPORT CI', 'ganatrans@yahoo.fr', '0777872347', NULL, 0, '2021-04-25 10:09:20', '2021-04-25 10:09:20'),
(18, 'ETS SAMA TRANSPORT ', 'samatransport@gmail.com', '0708028942/0574585827', NULL, 0, '2021-04-25 10:14:20', '2021-04-25 10:14:20'),
(19, 'TILEMSI TRANSPORT ABIDJAN ', 'tilemstransport@gmail.com', '+22344904212', NULL, 0, '2021-04-25 10:33:24', '2021-04-25 10:33:24'),
(20, 'SONEF TRANSPORT VOYAGEURS ', 'sonef_niger@yahoo.fr', '+22720734358/+22790901239/22720752174', NULL, 0, '2021-04-25 10:56:17', '2021-04-25 10:56:17'),
(21, 'CHONCO TRANSPORT ', 'dsilue7@gmail.com', '0586909092', NULL, 0, '2021-04-25 10:59:54', '2021-04-25 10:59:54'),
(22, 'UTB', 'contacts@utb.ci', '0564616360', NULL, 0, '2021-04-25 19:14:17', '2021-04-25 19:14:17'),
(23, 'OUEST TRANSACTIONS CI OT TRANSPORT ', 'ouesttransactionci@gmail.com', '20375186', NULL, 0, '2021-04-26 06:45:10', '2021-04-26 06:45:10');

-- --------------------------------------------------------

--
-- Structure de la table `destination`
--

CREATE TABLE `destination` (
  `id_destination` int(11) NOT NULL,
  `compagnie_destination_id` varchar(255) NOT NULL,
  `depart_gare_id` int(12) DEFAULT NULL,
  `arrive_gare_id` int(12) DEFAULT NULL,
  `date_depart` date DEFAULT NULL,
  `heure_depart` varchar(100) DEFAULT NULL,
  `heure_arrivee` varchar(200) NOT NULL,
  `tarif_prix` int(12) NOT NULL DEFAULT 0,
  `tarif_comp` int(12) NOT NULL DEFAULT 0,
  `destination_created_at` datetime DEFAULT current_timestamp(),
  `destination_updated_at` datetime DEFAULT current_timestamp(),
  `secur_ajout` varchar(255) DEFAULT NULL,
  `statut_destination` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `destination`
--

INSERT INTO `destination` (`id_destination`, `compagnie_destination_id`, `depart_gare_id`, `arrive_gare_id`, `date_depart`, `heure_depart`, `heure_arrivee`, `tarif_prix`, `tarif_comp`, `destination_created_at`, `destination_updated_at`, `secur_ajout`, `statut_destination`) VALUES
(11, '22', 6, 8, '2021-07-19', '12:30', '12:30', 8000, 0, '2021-04-25 19:16:31', '2021-04-25 19:16:31', NULL, 0),
(12, '22', 6, 8, '2021-04-29', '14:00', '14:00', 8000, 0, '2021-04-25 19:18:01', '2021-04-25 19:18:01', NULL, 0),
(13, '22', 6, 8, '2021-04-29', '19:30', '19:30', 8000, 0, '2021-04-25 19:19:44', '2021-04-25 19:19:44', NULL, 0),
(14, '22', 6, 7, '2021-08-26', '10:30', '10:30', 5500, 0, '2021-04-25 19:26:49', '2021-04-25 19:26:49', NULL, 0),
(15, '22', 6, 7, '2021-06-16', '12:00', '12:00', 5500, 0, '2021-04-25 19:28:03', '2021-04-25 19:28:03', NULL, 0),
(16, '22', 6, 7, '2021-08-23', '15:00', '15:00', 5500, 0, '2021-04-25 19:29:11', '2021-04-25 19:29:11', NULL, 0),
(18, '22', 6, 11, '2021-04-29', '13:30', '13:30', 9000, 0, '2021-04-25 19:32:47', '2021-04-25 19:32:47', NULL, 0),
(19, '22', 6, 11, '2021-05-03', '15:00', '15:00', 0, 0, '2021-04-25 19:34:13', '2021-04-25 19:34:13', NULL, 0),
(20, '22', 6, 40, '2021-06-13', '16:00', '16:00', 4725, 4500, '2021-04-25 19:37:16', '2021-04-25 19:37:16', NULL, 0),
(21, '22', 6, 13, '2021-06-03', '12:00', '12:00', 5250, 5000, '2021-04-25 19:39:39', '2021-04-25 19:39:39', NULL, 0),
(22, '22', 6, 14, '2021-05-03', '08:30', '14:30', 8400, 8000, '2021-04-25 19:41:19', '2021-04-25 19:41:19', NULL, 0),
(23, '3', 5, 15, '2021-04-26', '07:00', '12:00', 3000, 0, '2021-04-25 20:01:00', '2021-04-25 20:01:00', NULL, 1),
(24, '23', 38, 48, '2021-04-26', '07:30', '12:30', 6500, 0, '2021-04-26 06:47:35', '2021-04-26 06:47:35', NULL, 1),
(25, '23', 48, 36, '2021-04-26', '12:40', '17:40', 7475, 6500, '2021-04-26 06:50:34', '2021-04-26 06:50:34', NULL, 1),
(26, '23', 5, 17, '2021-04-26', '07:30', '12:30', 6000, 0, '2021-04-26 06:55:33', '2021-04-26 06:55:33', NULL, 1),
(27, '23', 5, 27, '2021-07-08', '06:30', '15:30', 9315, 8100, '2021-04-29 12:49:25', '2021-04-29 12:49:25', NULL, 0),
(29, '21', 5, 7, '2021-04-27', '23:21', '20:21', 11500, 10000, '2021-04-29 20:22:05', '2021-04-29 20:22:05', NULL, 1),
(30, '3', 5, 6, '2021-05-06', '17:08', '14:08', 9200, 8000, '2021-05-03 13:09:08', '2021-05-03 13:09:08', NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `gare`
--

CREATE TABLE `gare` (
  `id_gare` int(12) NOT NULL,
  `email_gare` varchar(200) DEFAULT NULL,
  `telephone_gare` varchar(120) DEFAULT NULL,
  `nom_gare` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `secur_ajout` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `gare`
--

INSERT INTO `gare` (`id_gare`, `email_gare`, `telephone_gare`, `nom_gare`, `created_at`, `updated_at`, `secur_ajout`) VALUES
(5, '', '', 'ADJAMÃ‰ ', '2021-04-25 16:52:15', '2021-04-25 16:52:15', NULL),
(6, '', '', 'ABOBO ', '2021-04-25 16:52:35', '2021-04-25 16:52:35', NULL),
(7, '', '', 'YAMOUSSOUKRO ', '2021-04-25 16:53:00', '2021-04-25 16:53:00', NULL),
(8, '', '', 'BOUAKÃ‰', '2021-04-25 16:53:26', '2021-04-25 16:53:26', NULL),
(9, '', '', 'KOROGHO', '2021-04-25 16:53:56', '2021-04-25 16:53:56', NULL),
(10, '', '', 'FERKÃ‰', '2021-04-25 16:54:21', '2021-04-25 16:54:21', NULL),
(11, '', '', 'BEOUMI', '2021-04-25 16:54:52', '2021-04-25 16:54:52', NULL),
(12, '', '', 'BONON', '2021-04-25 16:55:26', '2021-04-25 16:55:26', NULL),
(13, '', '', 'BOUAFLÃ‰', '2021-04-25 16:55:53', '2021-04-25 16:55:53', NULL),
(14, '', '', 'DALOA', '2021-04-25 16:56:44', '2021-04-25 16:56:44', NULL),
(15, '', '', 'DIMBOKRO', '2021-04-25 16:57:11', '2021-04-25 16:57:11', NULL),
(16, '', '', 'DIVO', '2021-04-25 16:57:30', '2021-04-25 16:57:30', NULL),
(17, '', '', 'DUEKOUE', '2021-04-25 16:58:03', '2021-04-25 16:58:03', NULL),
(18, '', '', 'GABIADJI', '2021-04-25 16:58:38', '2021-04-25 16:58:38', NULL),
(19, '', '', 'GABIADJI', '2021-04-25 16:59:14', '2021-04-25 16:59:14', NULL),
(20, '', '', 'GAGNOA', '2021-04-25 16:59:33', '2021-04-25 16:59:33', NULL),
(21, '', '', 'GESCO', '2021-04-25 16:59:58', '2021-04-25 16:59:58', NULL),
(22, '', '', 'GONATE', '2021-04-25 17:00:32', '2021-04-25 17:00:32', NULL),
(23, '', '', 'GUESSABO', '2021-04-25 17:01:05', '2021-04-25 17:01:05', NULL),
(24, '', '', 'HIRÃ‰', '2021-04-25 17:01:35', '2021-04-25 17:01:35', NULL),
(25, '', '', 'ISSIA', '2021-04-25 17:02:18', '2021-04-25 17:02:18', NULL),
(26, '', '', 'KOUMASSI', '2021-04-25 17:02:42', '2021-04-25 17:02:42', NULL),
(27, '', '', 'MAN', '2021-04-25 17:03:10', '2021-04-25 17:03:10', NULL),
(28, '', '', 'MEAGUI', '2021-04-25 17:03:35', '2021-04-25 17:03:35', NULL),
(29, '', '', 'SAN PEDRO', '2021-04-25 17:04:16', '2021-04-25 17:04:16', NULL),
(30, '', '', 'SINFRA', '2021-04-25 17:04:53', '2021-04-25 17:04:53', NULL),
(31, '', '', 'SOUBRE', '2021-04-25 17:05:18', '2021-04-25 17:05:18', NULL),
(32, '', '', 'TIEBISSOU', '2021-04-25 17:05:44', '2021-04-25 17:05:44', NULL),
(33, '', '', 'TOUMODI', '2021-04-25 17:06:04', '2021-04-25 17:06:04', NULL),
(34, '', '', 'YABAYO', '2021-04-25 17:06:27', '2021-04-25 17:06:27', NULL),
(35, '', '', 'ODIENE', '2021-04-25 17:06:57', '2021-04-25 17:06:57', NULL),
(36, '', '', 'TOUBA', '2021-04-25 17:07:24', '2021-04-25 17:07:24', NULL),
(37, '', '', 'BONDOUKOU', '2021-04-25 17:08:17', '2021-04-25 17:08:17', NULL),
(38, '', '', 'TOUBA', '2021-04-25 17:08:38', '2021-04-25 17:08:38', NULL),
(39, '', '', 'VAVOUA', '2021-04-25 17:08:58', '2021-04-25 17:08:58', NULL),
(40, '', '', 'BANGOLO', '2021-04-25 17:35:49', '2021-04-25 17:35:49', NULL),
(41, '', '', 'TOUBA', '2021-04-26 04:26:04', '2021-04-26 04:26:04', NULL),
(42, '', '', 'DANANÃ‰', '2021-04-26 04:26:30', '2021-04-26 04:26:30', NULL),
(43, '', '', 'BIANKOUMA', '2021-04-26 04:27:36', '2021-04-26 04:27:36', NULL),
(44, '', '', 'ZOAN-HOUNIEN', '2021-04-26 04:28:32', '2021-04-26 04:28:32', NULL),
(45, '', '', 'BLÃ‰NIMÃ‰OUIN', '2021-04-26 04:30:52', '2021-04-26 04:30:52', NULL),
(46, '', '', 'TEAPLEU', '2021-04-26 04:31:13', '2021-04-26 04:31:13', NULL),
(47, '', '', 'MAHAPLEU', '2021-04-26 04:31:59', '2021-04-26 04:31:59', NULL),
(48, '', '', 'LAKOTA', '2021-04-26 04:40:03', '2021-04-26 04:40:03', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `guichet`
--

CREATE TABLE `guichet` (
  `id_guichet` int(12) NOT NULL,
  `code_guichet` int(12) DEFAULT 0,
  `created_at` timestamp(6) NULL DEFAULT current_timestamp(6),
  `updated_at` timestamp(6) NULL DEFAULT current_timestamp(6),
  `secur_ajout` varchar(100) DEFAULT NULL,
  `compagnie_id` int(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `historique_app`
--

CREATE TABLE `historique_app` (
  `id_historique` int(12) NOT NULL,
  `libelle_historique` varchar(400) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_07_06_115223_create_users_standard_table', 1),
(4, '2021_07_06_143140_create_t_users_table', 1),
(5, '2021_07_06_143656_create_t_admins_table', 1),
(6, '2021_07_06_143741_create_t_admins_password_resets_table', 1),
(7, '2021_07_06_143842_create_t_params_status_paiement_table', 1),
(8, '2021_07_06_144022_create_t_params_status_convois_table', 1),
(9, '2021_07_06_145537_create_t_params_type_lieu_table', 1),
(10, '2021_07_06_145615_create_t_params_lieu_table', 1),
(11, '2021_07_06_145804_create_t_params_groupes_table', 1),
(12, '2021_07_06_145845_create_t_params_status_retrait_table', 1),
(13, '2021_07_06_145946_create_t_retraits_table', 1),
(14, '2021_07_06_150013_create_t_annonces_table', 1),
(15, '2021_07_06_150038_create_t_compagnies_table', 1),
(16, '2021_07_06_150105_create_t_voyages_table', 1),
(17, '2021_07_06_150133_create_t_paiements_table', 1),
(18, '2021_07_06_150158_create_t_recus_table', 1),
(19, '2021_07_06_150220_create_t_gares_table', 1),
(20, '2021_07_06_150243_create_t_convois_table', 1),
(21, '2021_07_06_150312_create_t_wallets_table', 1),
(22, '2021_07_06_150351_create_t_users_standarts_table', 1),
(23, '2021_07_06_150425_create_t_transactions_table', 1),
(24, '2021_07_17_094839_create_t_convois_temp_table', 1),
(25, '2021_07_20_170248_create_t_tickets_table', 1),
(26, '2021_07_20_170516_create_t_params_status_tickets_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `paiement`
--

CREATE TABLE `paiement` (
  `id_paiement` int(11) NOT NULL,
  `montant_total_ticket` int(12) DEFAULT NULL,
  `code_ticket` varchar(200) DEFAULT NULL,
  `nombre_ticket` int(12) DEFAULT NULL,
  `mont_ticket_unitaire` int(12) DEFAULT NULL,
  `mode` varchar(255) NOT NULL,
  `date_paiement` datetime NOT NULL,
  `heure_paiement` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `utilisateur_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `depart_id` int(12) DEFAULT NULL,
  `arrive_id` int(12) DEFAULT NULL,
  `telephone_paiement` varchar(200) DEFAULT NULL,
  `email_paiement` varchar(200) DEFAULT NULL,
  `status_paiement` int(12) NOT NULL DEFAULT 0,
  `compagnie_paiement_id` int(12) DEFAULT NULL,
  `destination_paiement_ticket_id` int(12) DEFAULT NULL,
  `id_transaction` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `paiement`
--

INSERT INTO `paiement` (`id_paiement`, `montant_total_ticket`, `code_ticket`, `nombre_ticket`, `mont_ticket_unitaire`, `mode`, `date_paiement`, `heure_paiement`, `utilisateur_id`, `created_at`, `updated_at`, `depart_id`, `arrive_id`, `telephone_paiement`, `email_paiement`, `status_paiement`, `compagnie_paiement_id`, `destination_paiement_ticket_id`, `id_transaction`) VALUES
(1, 9000, '5Y5CH9C', 1, 9000, '', '2021-04-27 18:53:25', '2021-07-02 00:27:18', 0, '2021-04-27 20:53:25', '2021-04-27 20:53:25', 6, 11, '0778059869', 'bootnetcrasher@gmail.com', 1, 22, NULL, '5884419527'),
(2, 9000, 'GR8DZ9I', 1, 9000, '', '2021-04-27 18:55:15', '2021-04-27 18:55:15', 0, '2021-04-27 20:55:15', '2021-04-27 20:55:15', 6, 11, '456789876', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(3, 18000, 'VTX87ZZ', 2, 9000, '', '2021-04-27 18:58:20', '2021-04-27 18:58:20', 0, '2021-04-27 20:58:20', '2021-04-27 20:58:20', 6, 11, '0778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(4, 18000, '3O4D5W2', 2, 9000, '', '2021-04-27 18:59:38', '2021-04-27 18:59:38', 0, '2021-04-27 20:59:38', '2021-04-27 20:59:38', 6, 11, '0778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(5, 5500, 'HYG6NQ3', 1, 5500, '', '2021-04-27 21:26:16', '2021-04-27 21:26:16', 0, '2021-04-27 23:26:16', '2021-04-27 23:26:16', 6, 7, '79481690', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(6, 8000, '2G1ARNO', 1, 8000, '', '2021-04-28 09:12:01', '2021-04-28 09:12:01', 0, '2021-04-28 11:12:01', '2021-04-28 11:12:01', 6, 8, '2345678998', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(7, 18000, 'J023YLD', 2, 9000, '', '2021-04-28 09:33:05', '2021-04-28 09:33:05', 0, '2021-04-28 11:33:05', '2021-04-28 11:33:05', 6, 11, '2345678909', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(8, 18000, 'YC7EVCL', 2, 9000, '', '2021-04-28 09:37:07', '2021-04-28 09:37:07', 0, '2021-04-28 11:37:07', '2021-04-28 11:37:07', 6, 11, '0778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(9, 9000, 'UG8IS5X', 1, 9000, '', '2021-04-28 11:40:43', '2021-04-28 11:40:43', 0, '2021-04-28 13:40:43', '2021-04-28 13:40:43', 6, 11, '0778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(10, 8000, 'QSOLO3H', 1, 8000, '', '2021-04-28 15:33:59', '2021-04-28 15:33:59', 0, '2021-04-28 17:33:59', '2021-04-28 17:33:59', 6, 8, '58454115', '', 0, 22, NULL, NULL),
(11, 18000, '4Z1YPYV', 2, 9000, '', '2021-04-28 15:50:31', '2021-04-28 15:50:31', 0, '2021-04-28 17:50:31', '2021-04-28 17:50:31', 6, 11, '58786407', '', 0, 22, NULL, NULL),
(12, 11000, 'KHSRKFZ', 2, 5500, '', '2021-04-29 07:38:59', '2021-04-29 07:38:59', 0, '2021-04-29 09:38:59', '2021-04-29 09:38:59', 6, 7, '04397161', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(13, 11000, 'WC7MZJ9', 2, 5500, '', '2021-04-29 10:41:46', '2021-04-29 10:41:46', 0, '2021-04-29 12:41:46', '2021-04-29 12:41:46', 6, 7, '0748499366', 'abou.otci@gmail.com', 0, 22, NULL, NULL),
(14, 33000, '7HAFQ10', 1, 5500, '', '2021-04-29 14:32:43', '2021-04-29 14:32:43', 0, '2021-04-29 16:32:43', '2021-04-29 16:32:43', 6, 7, '04397161', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(15, 55890, 'U968RP0', 6, 9315, '', '2021-04-29 20:09:56', '2021-04-29 20:09:56', 0, '2021-04-29 22:09:56', '2021-04-29 22:09:56', 5, 27, '0778059869', 'cherubin0225@gmail.com', 0, 23, NULL, NULL),
(16, 11000, 'WVS9FN4', 2, 5500, '', '2021-04-30 12:29:40', '2021-04-30 12:29:40', 0, '2021-04-30 14:29:40', '2021-04-30 14:29:40', 6, 7, '2250778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(17, 18630, 'RRRPOKY', 2, 9315, '', '2021-04-30 12:30:42', '2021-04-30 12:30:42', 0, '2021-04-30 14:30:42', '2021-04-30 14:30:42', 5, 27, '234567890', 'cherubin0225@gmail.com', 0, 23, NULL, NULL),
(18, 18630, 'ERSYEBC', 2, 9315, '', '2021-04-30 12:34:21', '2021-04-30 12:34:21', 0, '2021-04-30 14:34:21', '2021-04-30 14:34:21', 5, 27, '234567899', 'cherubin0225@gmail.com', 0, 23, NULL, NULL),
(19, 9000, '2ID4W3U', 1, 9000, '', '2021-04-30 12:35:16', '2021-04-30 12:35:16', 0, '2021-04-30 14:35:16', '2021-04-30 14:35:16', 6, 11, '345677890', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(20, 9000, '6TKVDFP', 1, 9000, '', '2021-04-30 12:39:49', '2021-04-30 12:39:49', 0, '2021-04-30 14:39:49', '2021-04-30 14:39:49', 6, 11, '23456789', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(21, NULL, 'OQ2OU8N', NULL, NULL, '', '2021-04-30 12:39:54', '2021-04-30 12:39:54', 0, '2021-04-30 14:39:54', '2021-04-30 14:39:54', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL),
(22, 5500, 'P5DD1U0', 1, 5500, '', '2021-04-30 12:42:54', '2021-04-30 12:42:54', 0, '2021-04-30 14:42:54', '2021-04-30 14:42:54', 6, 7, '2345678909', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(23, 18630, 'ZBQGKJX', 2, 9315, '', '2021-04-30 12:44:32', '2021-04-30 12:44:32', 0, '2021-04-30 14:44:32', '2021-04-30 14:44:32', 5, 27, '2250778059869', 'cherubin0225@gmail.com', 0, 23, NULL, NULL),
(24, 18630, 'IWBFU9G', 2, 9315, '', '2021-04-30 12:46:52', '2021-04-30 12:46:52', 0, '2021-04-30 14:46:52', '2021-04-30 14:46:52', 5, 27, '2345678909', 'cherubin0225@gmail.com', 0, 23, NULL, NULL),
(25, 9315, 'JALSDYB', 1, 9315, '', '2021-04-30 15:49:14', '2021-04-30 15:49:14', 0, '2021-04-30 17:49:14', '2021-04-30 17:49:14', 5, 27, '04397161', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(26, 9315, '6Z4C9FR', 1, 9315, '', '2021-05-01 00:08:10', '2021-05-01 00:08:10', 0, '2021-05-01 02:08:10', '2021-05-01 02:08:10', 5, 27, '04397161', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(27, 9000, 'OO21W99', 1, 9000, '', '2021-05-01 12:14:41', '2021-05-01 12:14:41', 0, '2021-05-01 14:14:41', '2021-05-01 14:14:41', 6, 11, '2250778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(28, 9315, '6BK8H4P', 1, 9315, '', '2021-05-01 12:45:40', '2021-05-01 12:45:40', 0, '2021-05-01 14:45:40', '2021-05-01 14:45:40', 5, 27, '04397161', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(29, 9315, 'VHJGNNL', 1, 9315, '', '2021-05-01 12:49:41', '2021-05-01 12:49:41', 0, '2021-05-01 14:49:41', '2021-05-01 14:49:41', 5, 27, '0504397161', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(30, 9315, 'DSACZZ7', 1, 9315, '', '2021-05-01 12:51:16', '2021-05-01 12:51:16', 0, '2021-05-01 14:51:16', '2021-05-01 14:51:16', 5, 27, '0173838605', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(31, 9315, 'U8ONX64', 1, 9315, '', '2021-05-01 12:59:41', '2021-05-01 12:59:41', 0, '2021-05-01 14:59:41', '2021-05-01 14:59:41', 5, 27, '22500504397161', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(32, 9315, '90GUV7W', 1, 9315, '', '2021-05-01 13:03:17', '2021-05-01 13:03:17', 0, '2021-05-01 15:03:17', '2021-05-01 15:03:17', 5, 27, '2250779481690', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(33, 9315, 'W0QVYZU', 1, 9315, '', '2021-05-02 15:54:43', '2021-05-02 15:54:43', 0, '2021-05-02 17:54:43', '2021-05-02 17:54:43', 5, 27, '2250504397161', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(34, 9315, '25UM01K', 1, 9315, '', '2021-05-03 17:42:19', '2021-05-03 17:42:19', 0, '2021-05-03 19:42:19', '2021-05-03 19:42:19', 5, 27, '2250504397161', 'jojosong00@gmail.com', 0, 23, NULL, NULL),
(35, 0, 'PVGQSLQ', 1, 0, '', '2021-05-04 18:31:10', '2021-05-04 18:31:10', 0, '2021-05-04 20:31:10', '2021-05-04 20:31:10', 6, 40, '0555786878', '', 0, 22, NULL, NULL),
(36, 5500, 'I7I5XW5', 1, 5500, '', '2021-05-08 10:41:37', '2021-05-08 10:41:37', 0, '2021-05-08 12:41:37', '2021-05-08 12:41:37', 6, 7, '2250709487352', 'ousmane.sounka1960@gmail.com', 0, 22, NULL, NULL),
(37, 5500, 'AI8045A', 1, 5500, '', '2021-05-15 14:21:36', '2021-05-15 14:21:36', 0, '2021-05-15 16:21:36', '2021-05-15 16:21:36', 6, 7, '47886905', '', 0, 22, NULL, NULL),
(38, 5500, 'WSUR0FN', 1, 5500, '', '2021-05-15 14:25:01', '2021-05-15 14:25:01', 0, '2021-05-15 16:25:01', '2021-05-15 16:25:01', 6, 7, '2250747886905', '', 0, 22, NULL, NULL),
(39, 5500, 'MQRID95', 1, 5500, '', '2021-05-16 16:21:57', '2021-05-16 16:21:57', 0, '2021-05-16 18:21:57', '2021-05-16 18:21:57', 6, 7, '002250747652650', '', 0, 22, NULL, NULL),
(40, 8000, 'JZOGU2M', 1, 8000, '', '2021-05-16 16:29:00', '2021-05-16 16:29:00', 0, '2021-05-16 18:29:00', '2021-05-16 18:29:00', 6, 8, '002250748292602', 'Kedmondkouassi@gmail.com', 0, 22, NULL, NULL),
(41, 11000, 'ZL9PFT2', 2, 5500, '', '2021-05-19 09:48:15', '2021-05-19 09:48:15', 0, '2021-05-19 11:48:15', '2021-05-19 11:48:15', 6, 7, '2250778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(42, 16000, '3RLWBWK', 2, 8000, '', '2021-05-19 10:39:03', '2021-05-19 10:39:03', 0, '2021-05-19 12:39:03', '2021-05-19 12:39:03', 6, 8, '002250779339973', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(43, 1344000, '08TDWCN', 1, 8000, '', '2021-06-02 09:50:12', '2021-06-02 09:50:12', 0, '2021-06-02 11:50:12', '2021-06-02 11:50:12', 6, 8, '0747886905', '', 0, 22, NULL, NULL),
(44, 11000, 'QZQWM2F', 2, 5500, '', '2021-06-02 10:31:46', '2021-06-02 10:31:46', 0, '2021-06-02 12:31:46', '2021-06-02 12:31:46', 6, 7, '22579481690', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(45, 16000, 'WPLAXES', 2, 8000, '', '2021-06-02 11:22:11', '2021-06-02 11:22:11', 0, '2021-06-02 13:22:11', '2021-06-02 13:22:11', 6, 8, '22504397161', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(46, 16000, 'HBEGVLS', 1, 8000, '', '2021-06-03 17:11:46', '2021-06-03 17:11:46', 0, '2021-06-03 19:11:46', '2021-06-03 19:11:46', 6, 8, '234567898', 'Bootnetcrasher@gmail.com', 0, 22, NULL, NULL),
(47, 8000, 'EYDGBQJ', 1, 8000, '', '2021-06-03 19:12:00', '2021-06-03 19:12:00', 0, '2021-06-03 21:12:00', '2021-06-03 21:12:00', 6, 8, '74852514', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(48, 5250, 'ARDLGSG', 1, 5250, '', '2021-06-03 19:18:21', '2021-06-03 19:18:21', 0, '2021-06-03 21:18:21', '2021-06-03 21:18:21', 6, 13, '4525555', 'cherubin@hhjj.com', 0, 22, NULL, NULL),
(49, 4725, 'N5MHPEJ', 1, 4725, '', '2021-06-04 10:15:19', '2021-06-04 10:15:19', 0, '2021-06-04 12:15:19', '2021-06-04 12:15:19', 6, 40, '2250778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(50, 4725, 'F7HG16R', 1, 4725, '', '2021-06-04 10:31:56', '2021-06-04 10:31:56', 0, '2021-06-04 12:31:56', '2021-06-04 12:31:56', 6, 40, '0778059869', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(51, 11000, 'MXW7VUE', 2, 5500, '', '2021-06-10 10:25:36', '2021-06-10 10:25:36', 0, '2021-06-10 12:25:36', '2021-06-10 12:25:36', 6, 7, '0504397161', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(52, 9450, 'UBBCJQJ', 2, 4725, '', '2021-06-13 10:40:06', '2021-06-13 10:40:06', 0, '2021-06-13 12:40:06', '2021-06-13 12:40:06', 6, 40, '2345678909', 'cherubin0225@gmail.com', 0, 22, NULL, NULL),
(53, 16500, 'O993URR', 3, 5500, '', '2021-06-15 10:07:54', '2021-06-15 10:07:54', 0, '2021-06-15 12:07:54', '2021-06-15 12:07:54', 6, 7, '22504397161', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(54, 5500, 'XJ9CBBX', 1, 5500, '', '2021-06-15 12:35:19', '2021-06-15 12:35:19', 0, '2021-06-15 14:35:19', '2021-06-15 14:35:19', 6, 7, '2250504397161', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(55, 5500, 'ZZOGU3K', 1, 5500, '', '2021-06-19 08:33:12', '2021-06-19 08:33:12', 0, '2021-06-19 10:33:12', '2021-06-19 10:33:12', 6, 7, '0747886905', 'bootnetcrasher@gmail.com', 0, 22, NULL, NULL),
(56, 16000, 'EPSES8U', 2, 8000, '', '2021-06-27 16:00:13', '2021-06-27 16:00:13', 0, '2021-06-27 18:00:13', '2021-06-27 18:00:13', 6, 8, '0504397161', 'jojosong00@gmail.com', 0, 22, NULL, NULL),
(57, 8000, 'U6W6X8B', 1, 8000, '', '2021-07-02 00:16:48', '2021-07-02 00:16:49', 0, '2021-07-02 02:16:48', '2021-07-02 02:16:48', 6, 8, '47886905', 'dks1@yopmail.com', 0, 22, 11, '100932768'),
(58, 8000, 'XT56KAZ', 1, 8000, '', '2021-07-02 00:29:59', '2021-07-02 00:40:13', 0, '2021-07-02 02:29:59', '2021-07-02 02:29:59', 6, 8, '47886905', 'bootnetcrasher@gmail.com', 1, 22, 11, '0180106747'),
(59, 8000, '8AEVNSP', 1, 8000, '', '2021-07-02 00:42:54', '2021-07-02 00:45:36', 0, '2021-07-02 02:42:54', '2021-07-02 02:42:54', 6, 8, '47886905', 'bootnetcrasher@gmail.com', 1, 22, 11, '0257527593'),
(60, 8000, '093P1FT', 1, 8000, '', '2021-07-02 01:34:45', '2021-07-02 01:35:52', 0, '2021-07-02 03:34:45', '2021-07-02 03:34:45', 6, 8, '47886905', 'bootnetcrasher@gmail.com', 1, 22, 11, '0568555369'),
(61, 8000, 'TDCPADR', 1, 8000, '', '2021-07-02 12:25:55', '2021-07-02 12:26:25', 0, '2021-07-02 14:25:55', '2021-07-02 14:25:55', 6, 8, '0504397161', 'jojosong00@gmail.com', 0, 22, 11, '4478574094'),
(62, 5500, 'MHT56JU', 1, 5500, '', '2021-07-07 15:41:19', '2021-07-07 15:46:15', 0, '2021-07-07 17:41:19', '2021-07-07 17:41:19', 6, 7, '0504397161', 'jojosong00@gmail.com', 0, 22, 14, '5677543352'),
(63, 8000, 'S1WRLSI', 1, 8000, '', '2021-07-07 15:49:00', '2021-07-07 15:49:03', 0, '2021-07-07 17:49:00', '2021-07-07 17:49:00', 6, 8, '0504397161', 'jojosong00@gmail.com', 0, 22, 11, '5694335600'),
(64, 5500, 'JW5XQQM', 1, 5500, '', '2021-07-07 22:58:13', '2021-07-07 23:50:24', 0, '2021-07-08 00:58:13', '2021-07-08 00:58:13', 6, 7, '0405397161', 'jojosong00@gmail.com', 0, 22, 16, '8582448579'),
(65, 5500, 'YICS1US', 1, 5500, '', '2021-07-08 00:18:45', '2021-07-08 00:18:46', 0, '2021-07-08 02:18:45', '2021-07-08 02:18:45', 6, 7, '0504397161', 'jojosong00@gmail.com', 0, 22, 14, '0112641261'),
(66, 11000, 'HFH5NPR', 2, 5500, '', '2021-07-08 08:26:00', '2021-07-08 08:26:00', 0, '2021-07-08 10:26:00', '2021-07-08 10:26:00', 6, 7, '0788833060', '', 0, 22, 16, '3036097375'),
(67, 5500, '5L8UESS', 1, 5500, '', '2021-07-08 14:43:27', '2021-07-08 14:43:28', 0, '2021-07-08 16:43:27', '2021-07-08 16:43:27', 6, 7, '0759362429', 'pareabel58@gmail.com', 0, 22, 16, '5300809524'),
(68, 9315, 'BI7V4TG', 1, 9315, '', '2021-07-08 15:18:00', '2021-07-08 15:18:00', 0, '2021-07-08 17:18:00', '2021-07-08 17:18:00', 5, 27, '0747531644', '', 0, 23, 27, '5508079724'),
(69, 8000, 'BZTGK3G', 1, 8000, '', '2021-07-08 16:31:33', '2021-07-08 16:31:33', 0, '2021-07-08 18:31:33', '2021-07-08 18:31:33', 6, 8, '00000000000', '', 0, 22, 11, '5949391026'),
(70, 9315, '9020581', 1, 9315, '', '2021-07-08 16:32:04', '2021-07-08 16:32:04', 0, '2021-07-08 18:32:04', '2021-07-08 18:32:04', 5, 27, '00000000000', '', 0, 23, 27, '5952469045'),
(71, 5500, '3N9THRD', 1, 5500, '', '2021-07-08 17:09:09', '2021-07-08 17:09:09', 0, '2021-07-08 19:09:09', '2021-07-08 19:09:09', 6, 7, '000000000', '', 0, 22, 14, '6174973539'),
(72, 5500, 'Q3DDXPF', 1, 5500, '', '2021-07-08 17:10:40', '2021-07-08 17:10:40', 0, '2021-07-08 19:10:40', '2021-07-08 19:10:40', 6, 7, '00000', '', 0, 22, 14, '6184039741'),
(73, 5500, 'DQADAKA', 1, 5500, '', '2021-07-10 15:39:47', '2021-07-10 16:01:33', 0, '2021-07-10 17:39:47', '2021-07-10 17:39:47', 6, 7, '0504397161', 'jojosong00@gmail.com', 0, 22, 14, '5769336315'),
(74, 5500, 'DE4Q1HL', 1, 5500, '', '2021-07-15 15:19:32', '2021-07-15 15:19:57', 0, '2021-07-15 17:19:32', '2021-07-15 17:19:32', 6, 7, '78059869', 'cherubin0225@gmail.com', 0, 22, 14, '5519732451'),
(75, 16500, 'FERDXCT', 3, 5500, '', '2021-07-16 12:14:11', '2021-07-16 12:22:39', 0, '2021-07-16 14:14:11', '2021-07-16 14:14:11', 6, 7, '225 0758776360', '', 0, 22, 16, '4455997915'),
(76, 38500, '265TT2C', 7, 5500, '', '2021-07-16 15:41:50', '2021-07-16 15:41:50', 0, '2021-07-16 17:41:50', '2021-07-16 17:41:50', 6, 7, '+1 (251) 598-9581', 'sarysasi@mailinator.com', 0, 22, 16, '5651058682'),
(77, 8000, 'NY39I0A', 1, 8000, '', '2021-07-19 12:52:26', '2021-07-19 12:52:27', 0, '2021-07-19 14:52:26', '2021-07-19 14:52:26', 6, 8, '2222', 'dorgelesabihi7@gmail.com', 0, 22, 11, '4634751588'),
(78, 5500, 'F5090CJ', 1, 5500, '', '2021-07-19 14:12:47', '2021-07-19 14:13:17', 0, '2021-07-19 16:12:47', '2021-07-19 16:12:47', 6, 7, '0504397161', 'jojosong00@gmail.com', 0, 22, 16, '5119724567'),
(79, 5500, '30PKLC4', 1, 5500, '', '2021-08-13 16:56:32', '2021-08-13 16:57:27', 0, '2021-08-13 18:56:32', '2021-08-13 18:56:32', 6, 7, '0504397161', 'jojosong00@gmail.com', 0, 22, 16, '6104725367'),
(80, 5500, 'S6TGHMX', 1, 5500, '', '2021-08-23 16:59:52', '2021-08-23 16:59:53', 0, '2021-08-23 18:59:52', '2021-08-23 18:59:52', 6, 7, '0747886905', '', 0, 22, 16, '6119356080'),
(81, 5500, '1S876BN', 1, 5500, '', '2021-08-23 17:01:13', '2021-08-23 17:01:13', 0, '2021-08-23 19:01:13', '2021-08-23 19:01:13', 6, 7, '074788690', '', 0, 22, 16, '6127326407'),
(82, 5500, 'MFKHFXO', 1, 5500, '', '2021-08-23 17:30:39', '2021-08-23 17:30:39', 0, '2021-08-23 19:30:39', '2021-08-23 19:30:39', 6, 7, '0744886005', '', 0, 22, 16, '6303962276'),
(83, 5500, 'JBPSWJO', 1, 5500, '', '2021-08-26 21:55:10', '2021-08-26 21:55:44', 0, '2021-08-26 23:55:10', '2021-08-26 23:55:10', 6, 7, '+1 (475) 558-4653', 'cherubin0225@gmail.co', 0, 22, 14, '7894437394');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `recu`
--

CREATE TABLE `recu` (
  `id_recu` int(11) NOT NULL,
  `lib_recu` varchar(255) NOT NULL,
  `date_recu` varchar(255) NOT NULL,
  `heure_recu` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `secur_ajout` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `id_service` int(11) NOT NULL,
  `type_service` varchar(255) NOT NULL,
  `date_service` date NOT NULL,
  `lib_service` varchar(255) NOT NULL,
  `id_compagnie` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `secur_ajout` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `statut_destination`
--

CREATE TABLE `statut_destination` (
  `id_statut_destination` int(12) NOT NULL,
  `etat_statut` int(12) DEFAULT NULL,
  `destination_id` int(12) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statut_destination`
--

INSERT INTO `statut_destination` (`id_statut_destination`, `etat_statut`, `destination_id`, `created_at`) VALUES
(1, 1, 2, '2021-04-20 15:09:34'),
(2, 0, 2, '2021-04-20 15:11:44'),
(3, 1, 2, '2021-04-20 15:11:51'),
(4, 0, 2, '2021-04-20 15:11:57'),
(5, 1, 2, '2021-04-20 15:12:11'),
(6, 0, 2, '2021-04-20 15:12:26'),
(7, 1, 2, '2021-04-20 15:12:32'),
(8, 1, 1, '2021-04-20 15:12:38'),
(9, 0, 1, '2021-04-20 15:12:45'),
(10, 0, 2, '2021-04-20 15:14:02'),
(11, 1, 1, '2021-04-20 15:14:09'),
(12, 0, 1, '2021-04-20 16:27:43'),
(13, 1, 1, '2021-04-20 16:27:58'),
(14, 0, 1, '2021-04-20 16:40:15'),
(15, 1, 1, '2021-04-20 16:52:08'),
(16, 1, 1, '2021-04-20 16:52:22'),
(17, 0, 1, '2021-04-20 16:59:46'),
(18, 0, 1, '2021-04-21 13:24:05'),
(19, 0, 1, '2021-04-21 13:25:48'),
(20, 0, 1, '2021-04-21 13:26:02'),
(21, 0, NULL, '2021-04-26 09:07:04'),
(22, 0, NULL, '2021-04-26 09:07:33'),
(23, 0, NULL, '2021-04-26 09:55:55'),
(24, 0, NULL, '2021-04-26 09:56:44'),
(25, 1, NULL, '2021-04-26 10:04:44'),
(26, 0, NULL, '2021-04-26 10:05:02'),
(27, 0, NULL, '2021-04-26 10:05:36'),
(28, 0, NULL, '2021-04-26 10:06:01'),
(29, 0, NULL, '2021-04-26 10:51:53'),
(30, 0, NULL, '2021-04-26 10:52:11'),
(31, 0, NULL, '2021-04-26 10:52:26'),
(32, 0, NULL, '2021-04-26 10:52:43'),
(33, 1, NULL, '2021-04-29 12:55:42'),
(34, 0, NULL, '2021-04-29 12:58:22'),
(35, 0, NULL, '2021-04-29 16:11:15'),
(36, 0, NULL, '2021-04-29 16:12:21'),
(37, 0, NULL, '2021-05-03 13:09:38'),
(38, 1, NULL, '2021-05-03 13:13:51');

-- --------------------------------------------------------

--
-- Structure de la table `statut_utilisateurs`
--

CREATE TABLE `statut_utilisateurs` (
  `id_statut_utilisateurs` int(12) NOT NULL,
  `etat_statut` int(12) NOT NULL DEFAULT 0,
  `utilisateurs_id` int(12) DEFAULT NULL,
  `date_creat` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statut_utilisateurs`
--

INSERT INTO `statut_utilisateurs` (`id_statut_utilisateurs`, `etat_statut`, `utilisateurs_id`, `date_creat`) VALUES
(1, 1, 8, NULL),
(2, 1, 7, NULL),
(3, 1, 4, '2021-04-16 10:58:26'),
(4, 1, 2, '2021-04-16 11:02:38'),
(5, 1, 6, '2021-04-16 11:16:57'),
(6, 0, 6, '2021-04-16 11:17:06'),
(7, 0, 7, '2021-04-16 11:18:02'),
(8, 0, 8, '2021-04-16 11:18:11'),
(9, 0, 4, '2021-04-16 11:18:19'),
(10, 1, 5, '2021-04-16 16:44:25'),
(11, 0, 5, '2021-04-16 16:44:32'),
(12, 1, 9, '2021-04-16 16:45:32'),
(13, 0, 2, '2021-04-16 16:47:19'),
(14, 1, 3, '2021-04-17 23:40:02'),
(15, 0, 3, '2021-04-17 23:40:08'),
(16, 1, 14, '2021-04-20 00:09:20'),
(17, 0, 14, '2021-04-20 00:09:28');

-- --------------------------------------------------------

--
-- Structure de la table `trace`
--

CREATE TABLE `trace` (
  `id_trace` int(12) NOT NULL,
  `lib_trace` text DEFAULT NULL,
  `date_trace` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `adresse_ip` varchar(200) DEFAULT NULL,
  `secur` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `trace`
--

INSERT INTO `trace` (`id_trace`, `lib_trace`, `date_trace`, `adresse_ip`, `secur`) VALUES
(1, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin0225@gmail.com ,password : FE6UFSB', '2021-04-15 16:21:44', 'Adresse IP: ::1 Port: 30041', 'eZQpeRiFOd'),
(2, 'Cr&eacute;ation de l\'utilisateur ossey avec email cherubin025@gmail.com ,password : 5F1VqB5', '2021-04-15 16:25:52', 'Adresse IP: ::1 Port: 30238', 'eZQpeRiFOd'),
(3, 'Cr&eacute;ation de l\'utilisateur asseu avec email romeo025@gmail.com ,password : 5bMrm4y', '2021-04-15 16:31:11', 'Adresse IP: ::1 Port: 30488', 'eZQpeRiFOd'),
(4, 'Cr&eacute;ation de l\'utilisateur ossed avec email cherubind0225@gmail.com ,password : 6ENwPYr', '2021-04-15 16:36:13', 'Adresse IP: ::1 Port: 28295', 'eZQpeRiFOd'),
(5, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin0222325@gmail.com ,password : O6K0eTs', '2021-04-15 17:01:43', 'Adresse IP: ::1 Port: 29887', 'eZQpeRiFOd'),
(6, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin02250@gmail.com ,password : opcToTO', '2021-04-15 17:02:17', 'Adresse IP: ::1 Port: 29922', 'eZQpeRiFOd'),
(7, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin0225AZERTY@gmail.com ,password : yrOFcd7', '2021-04-15 17:03:16', 'Adresse IP: ::1 Port: 29962', 'eZQpeRiFOd'),
(8, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin022523@gmail.com ,password : LuTkte8', '2021-04-15 17:05:06', 'Adresse IP: ::1 Port: 30052', 'eZQpeRiFOd'),
(9, 'Cr&eacute;ation de l\'utilisateur osse avec email KAM@KAM.com ,password : ihmp9Xg', '2021-04-15 17:12:58', 'Adresse IP: ::1 Port: 30371', 'eZQpeRiFOd'),
(10, 'Cr&eacute;ation de l\'utilisateur osse asse avec email test@email.com ,password : 9haFQ6e', '2021-04-15 17:17:45', 'Adresse IP: ::1 Port: 30570', 'eZQpeRiFOd'),
(11, 'Cr&eacute;ation de l\'utilisateur osses avec email miche@test.com ,password : MRUV7nc', '2021-04-15 17:37:13', 'Adresse IP: ::1 Port: 31486', 'eZQpeRiFOd'),
(12, 'Cr&eacute;ation de l\'utilisateur osse avec email tesst@email.com ,password : wv5BoNs', '2021-04-15 17:38:02', 'Adresse IP: ::1 Port: 31541', 'eZQpeRiFOd'),
(13, 'Cr&eacute;ation de l\'utilisateur osse avec email KAMy@KAM.com ,password : YH9LIBG', '2021-04-15 17:38:53', 'Adresse IP: ::1 Port: 31574', 'eZQpeRiFOd'),
(14, 'Cr&eacute;ation de l\'utilisateur moubko avec email KAMs@KAM.com ,password : XoRGz9r', '2021-04-15 17:40:13', 'Adresse IP: ::1 Port: 31656', 'eZQpeRiFOd'),
(15, 'Cr&eacute;ation de l\'utilisateur osse avec email biheco4651@mailnest.net ,password : W0czwxk', '2021-04-15 17:40:56', 'Adresse IP: ::1 Port: 31705', 'eZQpeRiFOd'),
(16, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin0225@gmail.come ,password : pqvS85D', '2021-04-15 17:44:09', 'Adresse IP: ::1 Port: 31840', 'eZQpeRiFOd'),
(17, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubina0225@gmail.com ,password : wtsVkZi', '2021-04-15 17:45:31', 'Adresse IP: ::1 Port: 31899', 'eZQpeRiFOd'),
(18, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubina04225@gmail.com ,password : NMRIh52', '2021-04-15 17:46:43', 'Adresse IP: ::1 Port: 31965', 'eZQpeRiFOd'),
(19, 'Cr&eacute;ation de l\'utilisateur ossetre avec email cherubin0225@gmail.coms ,password : cbW8Mx1', '2021-04-15 17:47:13', 'Adresse IP: ::1 Port: 31999', 'eZQpeRiFOd'),
(20, 'Cr&eacute;ation de l\'utilisateur osse asset avec email cherubin0c25@gmail.com ,password : lO0Atsz', '2021-04-15 17:48:00', 'Adresse IP: ::1 Port: 32051', 'eZQpeRiFOd'),
(21, 'Cr&eacute;ation de l\'utilisateur  avec email  ,password : R7bofez', '2021-04-15 17:52:42', 'Adresse IP: ::1 Port: 32613', 'eZQpeRiFOd'),
(22, 'Cr&eacute;ation de l\'utilisateur osse aseu avec email cherubin022e5@gmail.com ,password : tKjaTsb', '2021-04-15 17:54:10', 'Adresse IP: ::1 Port: 32700', 'eZQpeRiFOd'),
(23, 'Cr&eacute;ation de l\'utilisateur osses avec email biheco4651@mailnesst.net ,password : PsJqPQV', '2021-04-15 17:55:18', 'Adresse IP: ::1 Port: 32767', 'eZQpeRiFOd'),
(24, 'Cr&eacute;ation de l\'utilisateur ossey avec email romeo.osset@arobaseprotech.com ,password : qnCPUlI', '2021-04-15 17:59:35', 'Adresse IP: ::1 Port: 32967', 'eZQpeRiFOd'),
(25, 'Cr&eacute;ation de l\'utilisateur ossee avec email romeo.osese@arobaseprotech.com ,password : WX74qjw', '2021-04-15 18:00:40', 'Adresse IP: ::1 Port: 33010', 'eZQpeRiFOd'),
(26, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin0225@gmail.com ,password : WFc8eVk', '2021-04-15 19:14:19', 'Adresse IP: ::1 Port: 36423', 'eZQpeRiFOd'),
(27, 'Cr&eacute;ation de l\'utilisateur ossey avec email romeo.osse@arobaseprotech.com ,password : qTY6QVU', '2021-04-15 19:17:14', 'Adresse IP: ::1 Port: 36534', 'eZQpeRiFOd'),
(28, 'Cr&eacute;ation de l\'utilisateur ossez  avec email miche@test.com ,password : tevMdWB', '2021-04-16 09:06:53', 'Adresse IP: ::1 Port: 41770', 'eZQpeRiFOd'),
(29, 'Cr&eacute;ation de l\'utilisateur ossey  avec email test@email.com ,password : NsN7tAx', '2021-04-16 09:07:39', 'Adresse IP: ::1 Port: 41810', 'eZQpeRiFOd'),
(30, 'Cr&eacute;ation de l\'utilisateur osse avec email KAM@KAM.com ,password : ktGfqF5', '2021-04-16 10:33:08', 'Adresse IP: ::1 Port: 45533', 'eZQpeRiFOd'),
(31, 'Cr&eacute;ation de l\'utilisateur osse avec email cherubin02250@gmail.com ,password : mPADotc', '2021-04-16 10:34:40', 'Adresse IP: ::1 Port: 45607', 'eZQpeRiFOd'),
(32, 'Cr&eacute;ation de l\'utilisateur osset avec email cherubin0225t@gmail.com ,password : iDo672C', '2021-04-16 10:44:22', 'Adresse IP: ::1 Port: 46086', 'eZQpeRiFOd'),
(33, 'Cr&eacute;ation de l\'utilisateur ossee avec email romeo.ossee@arobaseprotech.com ,password : XybMxEh', '2021-04-16 10:45:23', 'Adresse IP: ::1 Port: 46126', 'eZQpeRiFOd'),
(34, 'Suppression de l\'utilisateur ossee Romeoe par ', '2021-04-16 11:51:40', 'Adresse IP: ::1 Port: 50119', 'eZQpeRiFOd'),
(35, 'Suppression de l\'utilisateur osse Romeo assi par ', '2021-04-16 11:52:06', 'Adresse IP: ::1 Port: 50128', 'eZQpeRiFOd'),
(36, 'Suppression de l\'utilisateur osset Romeot par ', '2021-04-16 11:54:56', 'Adresse IP: ::1 Port: 50260', 'eZQpeRiFOd'),
(37, 'Mise Ã  jour de l\'utilisateur   avec email  ,password : C8U69Ju', '2021-04-16 14:44:45', 'Adresse IP: ::1 Port: 57242', '5pqjknWJiX'),
(38, 'Mise Ã  jour de l\'utilisateur ossey Romeo avec email romeo.osse@arobaseprotech.com ,password : w6wFKDa', '2021-04-16 14:49:49', 'Adresse IP: ::1 Port: 57443', '5pqjknWJiX'),
(39, 'Mise Ã  jour de l\'utilisateur osse Romeo asseu avec email KAM@KAM.com ,password : k4azKlS', '2021-04-16 14:50:24', 'Adresse IP: ::1 Port: 57470', '5pqjknWJiX'),
(40, 'Mise Ã  jour de l\'utilisateur osser Romeo asseu er avec email KAM@KAMR.com ,password : MVuhCZe', '2021-04-16 14:51:02', 'Adresse IP: ::1 Port: 57498', '5pqjknWJiX'),
(41, 'Mise Ã  jour de l\'utilisateur oss osse ASSEU avec email test@email4.com ,password : Ejk8mJu', '2021-04-16 14:51:57', 'Adresse IP: ::1 Port: 57537', '5pqjknWJiX'),
(42, 'Mise Ã  jour de l\'utilisateur osse Romeo avec email cherubin0225@gmail.com ,password : mtqpez3', '2021-04-16 14:52:48', 'Adresse IP: ::1 Port: 57575', '5pqjknWJiX'),
(43, 'Mise Ã  jour de l\'utilisateur ossey  osse avec email test@email.com ,password : rEZgdEH', '2021-04-16 15:25:21', 'Adresse IP: ::1 Port: 59038', '5pqjknWJiX'),
(44, 'Mise Ã  jour de l\'utilisateur ossey  osse avec email test@email.com ,password : DSPCjy6', '2021-04-16 15:26:05', 'Adresse IP: ::1 Port: 59084', '5pqjknWJiX'),
(45, 'Mise Ã  jour de l\'utilisateur   avec email  ,password : 5HkRkZE', '2021-04-16 15:35:12', 'Adresse IP: ::1 Port: 59433', '5pqjknWJiX'),
(46, 'Mise Ã  jour de l\'utilisateur osse Romeo avec email cherubin0225@gmail.com ,password : u8KpIfy', '2021-04-16 15:37:52', 'Adresse IP: ::1 Port: 59529', '5pqjknWJiX'),
(47, 'Suppression de l\'utilisateur   par ', '2021-04-16 15:42:30', 'Adresse IP: ::1 Port: 59737', '5pqjknWJiX'),
(48, 'Mise Ã  jour de l\'utilisateur osset Romeo asseut avec email KAM@KAM.comt ,password : wPkLqpk', '2021-04-16 15:43:01', 'Adresse IP: ::1 Port: 59778', '5pqjknWJiX'),
(49, 'Mise Ã  jour de l\'utilisateur ossey  osse avec email test@email.com ,password : bQ81Naj', '2021-04-16 15:43:40', 'Adresse IP: ::1 Port: 59814', '5pqjknWJiX'),
(50, 'Mise Ã  jour de l\'utilisateur ossey Romeo avec email romeo.osse@arobaseprotech.com ,password : 7pgPsDC', '2021-04-16 15:47:04', 'Adresse IP: ::1 Port: 59974', '5pqjknWJiX'),
(51, 'Mise Ã  jour de l\'utilisateur ossey   osse avec email KAM@KAM.comt ,password : o2UnSXu', '2021-04-16 16:02:14', 'Adresse IP: ::1 Port: 60659', '5pqjknWJiX'),
(52, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : bgnq6Ct', '2021-04-16 16:03:07', 'Adresse IP: ::1 Port: 60701', '5pqjknWJiX'),
(53, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : 8Grgfab', '2021-04-16 16:08:50', 'Adresse IP: ::1 Port: 60943', '5pqjknWJiX'),
(54, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : Cb92Qbk', '2021-04-16 16:10:36', 'Adresse IP: ::1 Port: 61016', '5pqjknWJiX'),
(55, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : B3nGH7P', '2021-04-16 16:12:14', 'Adresse IP: ::1 Port: 61085', '5pqjknWJiX'),
(56, 'Mise Ã  jour de l\'utilisateur osse Romeo avec email  ,password : WlKNar3', '2021-04-16 16:22:45', 'Adresse IP: ::1 Port: 61619', '5pqjknWJiX'),
(57, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : GNo852c', '2021-04-16 16:33:17', 'Adresse IP: ::1 Port: 62044', '5pqjknWJiX'),
(58, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : rqJudA8', '2021-04-16 16:34:05', 'Adresse IP: ::1 Port: 62081', '5pqjknWJiX'),
(59, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : wh1nCnM', '2021-04-16 16:34:51', 'Adresse IP: ::1 Port: 62120', '5pqjknWJiX'),
(60, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : zcD1rKZ', '2021-04-16 16:36:02', 'Adresse IP: ::1 Port: 62176', '5pqjknWJiX'),
(61, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : fstiriR', '2021-04-16 16:38:48', 'Adresse IP: ::1 Port: 62291', '5pqjknWJiX'),
(62, 'Mise Ã  jour de l\'utilisateur ossey    avec email KAM@KAM.comt ,password : xOfoNKZ', '2021-04-16 16:39:08', 'Adresse IP: ::1 Port: 62308', '5pqjknWJiX'),
(63, 'Mise Ã  jour de l\'utilisateur osse Romeo avec email KAM@KAM.co ,password : cN3N3Bu', '2021-04-16 16:42:55', 'Adresse IP: ::1 Port: 62493', '5pqjknWJiX'),
(64, 'Mise Ã  jour de l\'utilisateur ossey   rommi avec email KAM@KAM.comt ,password : 2WJuvuF', '2021-04-16 16:43:39', 'Adresse IP: ::1 Port: 62537', '5pqjknWJiX'),
(65, 'Mise Ã  jour de l\'utilisateur ossey   romi avec email KAM@KAM.comt ,password : Da8atc6', '2021-04-16 16:44:17', 'Adresse IP: ::1 Port: 62569', '5pqjknWJiX'),
(66, 'verrouillage de  l\'utilisateur ossey   romi par ', '2021-04-16 16:44:25', 'Adresse IP: ::1 Port: 62581', '5pqjknWJiX'),
(67, 'dÃ©verrouillage de  l\'utilisateur ossey   romi par ', '2021-04-16 16:44:32', 'Adresse IP: ::1 Port: 62588', '5pqjknWJiX'),
(68, 'Cr&eacute;ation de l\'utilisateur patrick assande avec email patrick@pat.com ,password : panoLMn', '2021-04-16 16:45:12', 'Adresse IP: ::1 Port: 62603', '5pqjknWJiX'),
(69, 'Mise Ã  jour de l\'utilisateur patrick assande avec email patrick@pat.com ,password : AzsX32a', '2021-04-16 16:45:25', 'Adresse IP: ::1 Port: 62639', '5pqjknWJiX'),
(70, 'verrouillage de  l\'utilisateur patrick assande par ', '2021-04-16 16:45:32', 'Adresse IP: ::1 Port: 62644', '5pqjknWJiX'),
(71, 'dÃ©verrouillage de  l\'utilisateur ossey Romeo par ', '2021-04-16 16:47:19', 'Adresse IP: ::1 Port: 62726', '5pqjknWJiX'),
(72, 'verrouillage de  l\'utilisateur osse Romeo par ', '2021-04-17 23:40:02', 'Adresse IP: ::1 Port: 29390', '5pqjknWJiX'),
(73, 'dÃ©verrouillage de  l\'utilisateur osse Romeo par ', '2021-04-17 23:40:08', 'Adresse IP: ::1 Port: 29390', '5pqjknWJiX'),
(74, 'Mise Ã  jour de l\'utilisateur osse Romeo avec email cherubin0225@gmail.com ,password : lW8Y6KJ', '2021-04-18 02:13:35', 'Adresse IP: ::1 Port: 36218', NULL),
(75, 'Suppression de l\'utilisateur osse Romeo par ', '2021-04-18 02:15:30', 'Adresse IP: ::1 Port: 36288', NULL),
(76, 'Cr&eacute;ation de l\'utilisateur osse Romeo avec email cherubin0225@gmail.com ,password : mr7XQFY', '2021-04-18 02:15:42', 'Adresse IP: ::1 Port: 36293', NULL),
(77, 'Suppression de l\'utilisateur osse Romeo par ', '2021-04-18 02:18:02', 'Adresse IP: ::1 Port: 36409', NULL),
(78, 'Cr&eacute;ation de l\'utilisateur osse Romeo avec email cherubin0225@gmail.com ,password : YrCzAj9', '2021-04-18 02:18:13', 'Adresse IP: ::1 Port: 36416', NULL),
(79, 'Suppression de l\'utilisateur osse Romeo par ', '2021-04-18 02:20:07', 'Adresse IP: ::1 Port: 36499', NULL),
(80, 'Cr&eacute;ation de l\'utilisateur osse Romeo avec email cherubin0225@gmail.com ,password : aBcp3vc', '2021-04-18 02:20:20', 'Adresse IP: ::1 Port: 36505', NULL),
(81, 'Cr&eacute;ation de l\'utilisateur admin admin avec email admin@admin.com ,password : 92cXg6M', '2021-04-18 03:27:27', 'Adresse IP: ::1 Port: 38855', NULL),
(82, 'Cet utilisateur admin admin vient de changer son mot de passe  , adresse:Adresse IP: ::1 Port: 48580', '0000-00-00 00:00:00', 'Adresse IP: ::1 Port: 48580', '13'),
(83, 'Cet utilisateur admin admin vient de changer son mot de passe  , adresse:Adresse IP: ::1 Port: 48627', '0000-00-00 00:00:00', 'Adresse IP: ::1 Port: 48627', '13'),
(84, 'Cr&eacute;ation de la compagnie avs avec le numero 78059823 et email  par l\'utilisateur admin admin', '2021-04-19 23:34:33', 'Adresse IP: ::1 Port: 2046', '13'),
(85, 'Cr&eacute;ation de la compagnie AVS COMP avec le numero 78059865 et email  par l\'utilisateur admin admin', '2021-04-19 23:35:39', 'Adresse IP: ::1 Port: 2096', '13'),
(86, 'Modification de la compagnie avs avec le numero 78059876 et email avs@compagnie.com par l\'utilisateur admin admin', '2021-04-19 23:58:16', 'Adresse IP: ::1 Port: 2546', '13'),
(87, 'Modification de la compagnie avs1 avec le numero 78059879 et email avs@compagnie1.com par l\'utilisateur admin admin', '2021-04-19 23:59:44', 'Adresse IP: ::1 Port: 2559', '13'),
(88, 'Modification de la compagnie avs2 avec le numero 78059877 et email avs2@compagnie.com par l\'utilisateur admin admin', '2021-04-20 00:00:05', 'Adresse IP: ::1 Port: 2571', '13'),
(89, 'Modification de la compagnie avs3 avec le numero 78059873 et email avs3@compagnie.com par l\'utilisateur admin admin', '2021-04-20 00:00:22', 'Adresse IP: ::1 Port: 2574', '13'),
(90, 'Modification de la compagnie avs4 avec le numero 78059874 et email avs4@compagnie.com par l\'utilisateur admin admin', '2021-04-20 00:00:36', 'Adresse IP: ::1 Port: 2579', '13'),
(91, 'Suppression de l\'compagnie  par admin admin', '2021-04-20 00:06:24', 'Adresse IP: ::1 Port: 2687', '13'),
(92, 'Suppression de l\'compagnie  par admin admin', '2021-04-20 00:06:58', 'Adresse IP: ::1 Port: 2713', '13'),
(93, 'Cr&eacute;ation de l\'utilisateur osseT Romeo T avec email KAMA@KAM.com ,password : 05XURCZ', '2021-04-20 00:09:04', 'Adresse IP: ::1 Port: 2743', '13'),
(94, 'verrouillage de  l\'utilisateur osseT Romeo T par ', '2021-04-20 00:09:20', 'Adresse IP: ::1 Port: 2746', '13'),
(95, 'dÃ©verrouillage de  l\'utilisateur osseT Romeo T par ', '2021-04-20 00:09:28', 'Adresse IP: ::1 Port: 2746', '13'),
(96, 'Mise Ã  jour de l\'utilisateur osseTC Romeo TC avec email KAMA@KAM.com ,password : ID1PWLK', '2021-04-20 00:09:49', 'Adresse IP: ::1 Port: 2761', '13'),
(97, 'Mise Ã  jour de l\'utilisateur osseTC Romeo TC avec email KAMATC@KAM.com ,password : Lsd54Ul', '2021-04-20 00:10:06', 'Adresse IP: ::1 Port: 2768', '13'),
(98, 'Cr&eacute;ation de la gare AdjamÃ© avec le numero 78059869 et email adjame@adjame.co par l\'utilisateur admin admin', '2021-04-20 13:22:51', 'Adresse IP: ::1 Port: 14556', '13'),
(99, 'Cr&eacute;ation de la gare yopougon avec le numero 7805986912 et email adjam@adjame.co par l\'utilisateur admin admin', '2021-04-20 13:25:02', 'Adresse IP: ::1 Port: 14679', '13'),
(100, 'Modification de la compagnie  avec le numero  et email  par l\'utilisateur admin admin', '2021-04-20 13:25:38', 'Adresse IP: ::1 Port: 14705', '13'),
(101, 'Modification de la gare AdjamÃ©2 avec le numero 780598692 et email adjame@adjame.coZ par l\'utilisateur admin admin', '2021-04-20 13:27:24', 'Adresse IP: ::1 Port: 14766', '13'),
(102, 'Suppression de la gare  par admin admin', '2021-04-20 13:34:11', 'Adresse IP: ::1 Port: 15037', '13'),
(103, 'Cr&eacute;ation de la gare BouakÃ© avec le numero 78059869 et email yop@adjame.co par l\'utilisateur admin admin', '2021-04-20 13:38:48', 'Adresse IP: ::1 Port: 15221', '13'),
(104, 'Cr&eacute;ation du voyage avec code VOYAGE-ZECRQD par l\'utilisateur admin admin', '2021-04-20 13:46:20', 'Adresse IP: ::1 Port: 15557', '13'),
(105, 'Cr&eacute;ation du voyage avec code VOYAGE-XT7LSF par l\'utilisateur admin admin', '2021-04-20 13:48:27', 'Adresse IP: ::1 Port: 15623', '13'),
(106, 'Cr&eacute;ation du voyage avec code VOYAGE-H4EQ1Z par l\'utilisateur admin admin', '2021-04-20 14:36:24', 'Adresse IP: ::1 Port: 17463', '13'),
(107, 'verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:09:34', 'Adresse IP: ::1 Port: 18711', '13'),
(108, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:11:44', 'Adresse IP: ::1 Port: 18795', '13'),
(109, 'verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:11:51', 'Adresse IP: ::1 Port: 18801', '13'),
(110, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:11:57', 'Adresse IP: ::1 Port: 18801', '13'),
(111, 'verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:12:11', 'Adresse IP: ::1 Port: 18825', '13'),
(112, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:12:26', 'Adresse IP: ::1 Port: 18836', '13'),
(113, 'verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:12:32', 'Adresse IP: ::1 Port: 18836', '13'),
(114, 'verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:12:37', 'Adresse IP: ::1 Port: 18836', '13'),
(115, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:12:45', 'Adresse IP: ::1 Port: 18836', '13'),
(116, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:14:01', 'Adresse IP: ::1 Port: 18914', '13'),
(117, 'verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 15:14:09', 'Adresse IP: ::1 Port: 18914', '13'),
(118, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 16:27:43', 'Adresse IP: ::1 Port: 19771', '13'),
(119, 'verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 16:27:58', 'Adresse IP: ::1 Port: 19780', '13'),
(120, 'Cr&eacute;ation du voyage <b>BouakÃ©-BouakÃ©</b> avec code VOYAGE-AGJ1RY par l\'utilisateur admin admin', '2021-04-20 16:35:47', 'Adresse IP: ::1 Port: 20051', '13'),
(121, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-20 16:40:15', 'Adresse IP: ::1 Port: 20207', '13'),
(122, 'verrouillage du voyage <b>BouakÃ©-BouakÃ©</b> par admin admin', '2021-04-20 16:52:08', 'Adresse IP: ::1 Port: 20613', '13'),
(123, 'verrouillage du voyage <b>BouakÃ©-yopougon</b> par admin admin', '2021-04-20 16:52:22', 'Adresse IP: ::1 Port: 20622', '13'),
(124, 'Cr&eacute;ation du voyage <b>yopougon-yopougon</b> avec code VOYAGE-IUBEDA par l\'utilisateur admin admin', '2021-04-20 16:54:03', 'Adresse IP: ::1 Port: 20698', '13'),
(125, 'dÃ©verrouillage du voyage <b>BouakÃ©-yopougon</b> par admin admin', '2021-04-20 16:59:46', 'Adresse IP: ::1 Port: 20876', '13'),
(126, 'Modification du voyage <b>BouakÃ©-</b> avec code VOYAGE-S6JLMV par l\'utilisateur admin admin', '2021-04-21 12:27:25', 'Adresse IP: ::1 Port: 38375', '13'),
(127, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-Q2LBKV par l\'utilisateur admin admin', '2021-04-21 12:33:22', 'Adresse IP: ::1 Port: 38636', '13'),
(128, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-6TH2MV par l\'utilisateur admin admin', '2021-04-21 12:34:23', 'Adresse IP: ::1 Port: 38687', '13'),
(129, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-7JQMSQ par l\'utilisateur admin admin', '2021-04-21 12:35:42', 'Adresse IP: ::1 Port: 38789', '13'),
(130, 'Modification du voyage <b>BouakÃ©-yopougon</b> avec code VOYAGE-8AFUMW par l\'utilisateur admin admin', '2021-04-21 12:47:12', 'Adresse IP: ::1 Port: 39332', '13'),
(131, 'Suppression du voyage  <b>yopougon-yopougon</b> par admin admin', '2021-04-21 12:55:45', 'Adresse IP: ::1 Port: 39766', '13'),
(132, 'Cr&eacute;ation du voyage <b>yopougon-yopougon</b> avec code VOYAGE-IWSEGM par l\'utilisateur admin admin', '2021-04-21 12:56:47', 'Adresse IP: ::1 Port: 39820', '13'),
(133, 'Modification du voyage <b>yopougon-yopougon</b> avec code VOYAGE-ZYOGLJ par l\'utilisateur admin admin', '2021-04-21 13:01:18', 'Adresse IP: ::1 Port: 40052', '13'),
(134, 'Modification du voyage <b>yopougon-yopougon</b> avec code VOYAGE-JHWGGF par l\'utilisateur admin admin', '2021-04-21 13:01:41', 'Adresse IP: ::1 Port: 40076', '13'),
(135, 'Modification du voyage <b>yopougon-yopougon</b> avec code VOYAGE-7FZOR9 par l\'utilisateur admin admin', '2021-04-21 13:05:44', 'Adresse IP: ::1 Port: 40266', '13'),
(136, 'Modification du voyage <b>BouakÃ©-BouakÃ©</b> avec code VOYAGE-UR5BDH par l\'utilisateur admin admin', '2021-04-21 13:07:26', 'Adresse IP: ::1 Port: 40352', '13'),
(137, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-RHT6OA par l\'utilisateur admin admin', '2021-04-21 13:07:47', 'Adresse IP: ::1 Port: 40389', '13'),
(138, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-BTRLDG par l\'utilisateur admin admin', '2021-04-21 13:08:05', 'Adresse IP: ::1 Port: 40403', '13'),
(139, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-QDBE7K par l\'utilisateur admin admin', '2021-04-21 13:08:27', 'Adresse IP: ::1 Port: 40430', '13'),
(140, 'Modification du voyage <b>BouakÃ©-yopougon</b> avec code VOYAGE-IV9C8P par l\'utilisateur admin admin', '2021-04-21 13:09:02', 'Adresse IP: ::1 Port: 40456', '13'),
(141, 'Cr&eacute;ation du voyage <b>BouakÃ©-BouakÃ©</b> avec code VOYAGE-LS1HKD par l\'utilisateur admin admin', '2021-04-21 13:09:36', 'Adresse IP: ::1 Port: 40482', '13'),
(142, 'Cr&eacute;ation du voyage <b>yopougon-yopougon</b> avec code VOYAGE-EGQ3PK par l\'utilisateur admin admin', '2021-04-21 13:10:14', 'Adresse IP: ::1 Port: 40492', '13'),
(143, 'Cr&eacute;ation du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-LWEBN8 par l\'utilisateur admin admin', '2021-04-21 13:11:20', 'Adresse IP: ::1 Port: 40565', '13'),
(144, 'Cr&eacute;ation du voyage <b>BouakÃ©-yopougon</b> avec code VOYAGE-1Y8B7N par l\'utilisateur admin admin', '2021-04-21 13:13:53', 'Adresse IP: ::1 Port: 40683', '13'),
(145, 'Modification du voyage <b>BouakÃ©-yopougon</b> avec code VOYAGE-XAF7UJ par l\'utilisateur admin admin', '2021-04-21 13:19:56', 'Adresse IP: ::1 Port: 40958', '13'),
(146, 'Modification du voyage <b>BouakÃ©-yopougon</b> avec code VOYAGE-E3H9LV par l\'utilisateur admin admin', '2021-04-21 13:21:45', 'Adresse IP: ::1 Port: 41057', '13'),
(147, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-M2K813 par l\'utilisateur admin admin', '2021-04-21 13:22:32', 'Adresse IP: ::1 Port: 41099', '13'),
(148, 'Modification du voyage <b>yopougon-yopougon</b> avec code VOYAGE-HEIFVG par l\'utilisateur admin admin', '2021-04-21 13:22:45', 'Adresse IP: ::1 Port: 41113', '13'),
(149, 'Modification du voyage <b>BouakÃ©-BouakÃ©</b> avec code VOYAGE-3QOUPL par l\'utilisateur admin admin', '2021-04-21 13:22:56', 'Adresse IP: ::1 Port: 41113', '13'),
(150, 'Modification du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-AWPUSZ par l\'utilisateur admin admin', '2021-04-21 13:23:25', 'Adresse IP: ::1 Port: 41155', '13'),
(151, 'Cr&eacute;ation du voyage <b>yopougon-BouakÃ©</b> avec code VOYAGE-CIIPOC par l\'utilisateur admin admin', '2021-04-21 13:23:51', 'Adresse IP: ::1 Port: 41178', '13'),
(152, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-21 13:24:05', 'Adresse IP: ::1 Port: 41192', '13'),
(153, 'dÃ©verrouillage du voyage <b>BouakÃ©-yopougon</b> par admin admin', '2021-04-21 13:25:47', 'Adresse IP: ::1 Port: 41270', '13'),
(154, 'dÃ©verrouillage du voyage <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-21 13:26:02', 'Adresse IP: ::1 Port: 41281', '13'),
(155, 'Suppression du voyage  <b>BouakÃ©-yopougon</b> par admin admin', '2021-04-21 14:30:47', 'Adresse IP: ::1 Port: 45174', '13'),
(156, 'Suppression du voyage  <b>yopougon-BouakÃ©</b> par admin admin', '2021-04-21 14:31:06', 'Adresse IP: ::1 Port: 45201', '13'),
(157, 'Mise Ã  jour de l\'utilisateur admin admin avec email admin@admin.com ,password : Ze4AnEb', '2021-04-21 14:37:55', 'Adresse IP: ::1 Port: 45549', '13'),
(158, 'Mise Ã  jour de l\'utilisateur admin admin avec email admin@admin.com ,password : ZuD7bEP', '2021-04-21 14:38:46', 'Adresse IP: ::1 Port: 45604', '13'),
(159, 'Connexion de <b>admin admin ()</b>', '2021-04-22 03:09:35', 'Adresse IP: ::1 Port: 21576', 'admin admin'),
(160, 'Connexion de <b>admin admin ()</b>', '2021-04-22 03:35:51', 'Adresse IP: ::1 Port: 23228', 'admin admin'),
(161, 'Connexion de <b>admin admin ()</b>', '2021-04-22 04:04:16', 'Adresse IP: ::1 Port: 24858', 'admin admin'),
(162, 'Cr&eacute;ation de l\'utilisateur dom dom avec email domainegerance@gmail.com ,password : VO6ENqx', '2021-04-22 16:06:15', 'Adresse IP: ::1 Port: 24953', '13'),
(163, 'Ticket portant le code (4SDYF52) en cours de paiement avec le numero 0102110129 ', '2021-04-22 19:48:21', 'Adresse IP: ::1 Port: 33994', NULL),
(164, 'Ticket portant le code (UV90E49) en cours de paiement avec le numero 0102110129 ', '2021-04-22 19:49:56', 'Adresse IP: ::1 Port: 34051', NULL),
(165, 'Ticket portant le code (H7FK6IJ) en cours de paiement avec le numero 0102110129 ', '2021-04-22 19:54:16', 'Adresse IP: ::1 Port: 34202', NULL),
(166, 'Ticket portant le code (URO0YJC) en cours de paiement avec le numero 0102110129 ', '2021-04-22 19:56:56', 'Adresse IP: ::1 Port: 34300', 'client'),
(167, 'Ticket portant le code (KDQZ967) en cours de paiement avec le numero 0102110129 ', '2021-04-22 20:01:07', 'Adresse IP: ::1 Port: 34451', 'client'),
(168, 'Ticket portant le code (RGT3JKH) en cours de paiement avec le numero 0102110129 ', '2021-04-22 20:03:26', 'Adresse IP: ::1 Port: 34554', 'client'),
(169, 'Ticket portant le code (J5T3ZCH) en cours de paiement avec le numero 0102110129 ', '2021-04-22 20:14:51', 'Adresse IP: ::1 Port: 34942', 'client'),
(170, 'Ticket portant le code (ER1XIRK) en cours de paiement avec le numero 0102110129 ', '2021-04-22 20:15:47', 'Adresse IP: ::1 Port: 34985', 'client'),
(171, 'Ticket portant le code (XY8903Z) en cours de paiement avec le numero 02110129 ', '2021-04-22 20:17:55', 'Adresse IP: ::1 Port: 35072', 'client'),
(172, 'Ticket portant le code (IRS0AJ9) en cours de paiement avec le numero 02110129 ', '2021-04-22 20:18:49', 'Adresse IP: ::1 Port: 35112', 'client'),
(173, 'Connexion de <b>admin admin ()</b>', '2021-04-23 08:44:01', 'Adresse IP: ::1 Port: 35295', 'admin admin'),
(174, 'Ticket portant le code (MFW2IZG) en cours de paiement avec le numero 45656789 ', '2021-04-23 09:16:23', 'Adresse IP: ::1 Port: 36643', '13'),
(175, 'Ticket portant le code (0QJCLUV) en cours de paiement avec le numero 23456789 ', '2021-04-23 09:17:40', 'Adresse IP: ::1 Port: 36698', '13'),
(176, 'Ticket portant le code (1LQNSD7) en cours de paiement avec le numero 23456789 ', '2021-04-23 09:20:39', 'Adresse IP: ::1 Port: 36823', '13'),
(177, 'Ticket portant le code (Y8KHYZ5) en cours de paiement avec le numero 23456890 ', '2021-04-23 09:21:39', 'Adresse IP: ::1 Port: 36866', '13'),
(178, 'Ticket portant le code (Z5D3ZWH) en cours de paiement avec le numero 2345678 ', '2021-04-23 09:22:47', 'Adresse IP: ::1 Port: 36924', '13'),
(179, 'Ticket portant le code (7RSHE85) en cours de paiement avec le numero 22344 ', '2021-04-23 09:23:08', 'Adresse IP: ::1 Port: 36948', '13'),
(180, 'Ticket portant le code (URK6MFW) en cours de paiement avec le numero 2345678909 ', '2021-04-23 09:23:53', 'Adresse IP: ::1 Port: 36989', '13'),
(181, 'Ticket portant le code (XYRGTUB) en cours de paiement avec le numero 23456789 ', '2021-04-23 09:24:04', 'Adresse IP: ::1 Port: 36997', '13'),
(182, 'Ticket portant le code (MJGX38C) en cours de paiement avec le numero 78059869 ', '2021-04-23 10:07:23', 'Adresse IP: ::1 Port: 39216', '13'),
(183, 'Ticket portant le code (2MR9DEV) en cours de paiement avec le numero 0778059869 ', '2021-04-23 10:12:53', 'Adresse IP: ::1 Port: 39485', '13'),
(184, 'Ticket portant le code (T7B56QV) en cours de paiement avec le numero 0778059869 ', '2021-04-23 10:13:32', 'Adresse IP: ::1 Port: 39513', '13'),
(185, 'Ticket portant le code (G678KTE) en cours de paiement avec le numero 7805989869 ', '2021-04-23 10:15:03', 'Adresse IP: ::1 Port: 39582', '13'),
(186, 'Ticket portant le code (9TY49L7) en cours de paiement avec le numero 0778059869 ', '2021-04-23 10:15:50', 'Adresse IP: ::1 Port: 39625', '13'),
(187, 'Ticket portant le code (RKPE45L) en cours de paiement avec le numero 0778059869 ', '2021-04-23 10:28:06', 'Adresse IP: ::1 Port: 40235', '13'),
(188, 'Ticket portant le code (MVKDANW) en cours de paiement avec le numero 78059869 ', '2021-04-23 10:29:03', 'Adresse IP: ::1 Port: 40281', '13'),
(189, 'Ticket portant le code (HUVGLAB) en cours de paiement avec le numero 224567890 ', '2021-04-23 10:29:50', 'Adresse IP: ::1 Port: 40318', '13'),
(190, 'Ticket portant le code (O6I8WT3) en cours de paiement avec le numero 23456789 ', '2021-04-23 10:30:23', 'Adresse IP: ::1 Port: 40337', '13'),
(191, 'Ticket portant le code (JC23JKP) en cours de paiement avec le numero 23456789 ', '2021-04-23 10:30:42', 'Adresse IP: ::1 Port: 40348', '13'),
(192, 'Ticket portant le code (3ZCTYBO) en cours de paiement avec le numero 23456789 ', '2021-04-23 10:31:13', 'Adresse IP: ::1 Port: 40370', '13'),
(193, 'Ticket portant le code (IJ5HQBC) en cours de paiement avec le numero 23456789 ', '2021-04-23 12:23:50', 'Adresse IP: ::1 Port: 45974', '13'),
(194, 'Ticket portant le code (TQFOTUZ) en cours de paiement avec le numero 23456789 ', '2021-04-23 12:24:43', 'Adresse IP: ::1 Port: 46017', '13'),
(195, 'Ticket portant le code (UVK2Y45) en cours de paiement avec le numero 23456789 ', '2021-04-23 12:27:49', 'Adresse IP: ::1 Port: 46173', '13'),
(196, 'Ticket portant le code (LU4WLQN) en cours de paiement avec le numero 23456789 ', '2021-04-23 12:28:34', 'Adresse IP: ::1 Port: 46214', '13'),
(197, 'Ticket portant le code (90YJK6A) en cours de paiement avec le numero  ', '2021-04-23 12:51:39', 'Adresse IP: ::1 Port: 47364', '13'),
(198, 'Ticket portant le code (WPENKDQ) en cours de paiement avec le numero  ', '2021-04-23 12:53:18', 'Adresse IP: ::1 Port: 47466', '13'),
(199, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:01:57', 'Adresse IP: ::1 Port: 47824', '13'),
(200, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:04:10', 'Adresse IP: ::1 Port: 47910', '13'),
(201, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:04:53', 'Adresse IP: ::1 Port: 47938', '13'),
(202, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:06:12', 'Adresse IP: ::1 Port: 47989', '13'),
(203, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:07:04', 'Adresse IP: ::1 Port: 48027', '13'),
(204, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:10:16', 'Adresse IP: ::1 Port: 48181', '13'),
(205, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:10:49', 'Adresse IP: ::1 Port: 48210', '13'),
(206, 'Recherche trouvÃ© du ticket ayant pour code (UVK2Y45) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:12:17', 'Adresse IP: ::1 Port: 48295', '13'),
(207, 'Recherche trouvÃ© du ticket ayant pour code (WPENKDQ) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-23 13:13:51', 'Adresse IP: ::1 Port: 48390', '13'),
(208, 'Recherche non trouvÃ© du ticket ayant pour code (WPENKDQ23) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-23 13:14:16', 'Adresse IP: ::1 Port: 48406', '13'),
(209, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:18:38', 'Adresse IP: ::1 Port: 48626', '13'),
(210, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:18:43', 'Adresse IP: ::1 Port: 48626', '13'),
(211, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:20:38', 'Adresse IP: ::1 Port: 48713', '13'),
(212, 'Recherche trouvÃ© du ticket ayant pour code (UVK2Y45) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 13:20:47', 'Adresse IP: ::1 Port: 48713', '13'),
(213, 'Connexion de <b>admin admin ()</b>', '2021-04-23 03:30:47', 'Adresse IP: ::1 Port: 52131', 'admin admin'),
(214, 'Ticket portant le code (HQ4CP3Z) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-23 16:36:47', 'Adresse IP: ::1 Port: 54928', '13'),
(215, 'Recherche trouvÃ© du ticket ayant pour code (HQ4CP3Z) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-04-23 16:39:13', 'Adresse IP: ::1 Port: 55037', '13'),
(216, 'Ticket portant le code (523FCHM) en cours de paiement avec le numÃ©ro 456789089 ', '2021-04-23 16:48:03', 'Adresse IP: ::1 Port: 55470', '13'),
(217, 'Ticket portant le code (JKHI45D) en cours de paiement avec le numÃ©ro 23456789 ', '2021-04-23 16:55:10', 'Adresse IP: ::1 Port: 55894', '13'),
(218, 'Ticket portant le code (I8O63J5) en cours de paiement avec le numÃ©ro 234567890 ', '2021-04-23 16:56:20', 'Adresse IP: ::1 Port: 55949', '13'),
(219, 'Ticket portant le code (R5LAR1H) en cours de paiement avec le numÃ©ro 234567890 ', '2021-04-23 16:57:43', 'Adresse IP: ::1 Port: 56013', '13'),
(220, 'Ticket portant le code (PEV5HYF) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 16:58:17', 'Adresse IP: ::1 Port: 56056', '13'),
(221, 'Ticket portant le code (S6YZC6U) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 16:58:52', 'Adresse IP: ::1 Port: 56094', '13'),
(222, 'Ticket portant le code (YJ9PMF9) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 16:59:22', 'Adresse IP: ::1 Port: 56128', '13'),
(223, 'Ticket portant le code (2YV5TYN) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 17:00:01', 'Adresse IP: ::1 Port: 56176', '13'),
(224, 'Ticket portant le code (C2389DA) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 17:01:00', 'Adresse IP: ::1 Port: 56232', '13'),
(225, 'Ticket portant le code (WTQBKDE) en cours de paiement avec le numÃ©ro 234567890 ', '2021-04-23 17:02:08', 'Adresse IP: ::1 Port: 56305', '13'),
(226, 'Ticket portant le code (8C6UF1L) en cours de paiement avec le numÃ©ro 212345678 ', '2021-04-23 17:04:59', 'Adresse IP: ::1 Port: 56508', '13'),
(227, 'Ticket portant le code (EBO6ANK) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 17:06:28', 'Adresse IP: ::1 Port: 56582', '13'),
(228, 'Ticket portant le code (XER5TEV) en cours de paiement avec le numÃ©ro 2134567890 ', '2021-04-23 17:09:09', 'Adresse IP: ::1 Port: 56718', '13'),
(229, 'Ticket portant le code (ZOXABGH) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 17:10:30', 'Adresse IP: ::1 Port: 56781', '13'),
(230, 'Ticket portant le code (WLY4K6A) en cours de paiement avec le numÃ©ro 234567890 ', '2021-04-23 17:18:11', 'Adresse IP: ::1 Port: 57771', '13'),
(231, 'Ticket portant le code (TMSBRY2) en cours de paiement avec le numÃ©ro 2345678908 ', '2021-04-23 16:02:51', 'Adresse IP: 160.155.207.11 Port: 44446', '19'),
(232, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 16:03:15', 'Adresse IP: 160.155.207.11 Port: 45948', '19'),
(233, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 16:03:18', 'Adresse IP: 160.155.207.11 Port: 46226', '19'),
(234, 'Recherche trouvÃ© du ticket ayant pour code (HQ4CP3Z) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-04-23 16:03:30', 'Adresse IP: 160.155.207.11 Port: 46860', '19'),
(235, 'Recherche trouvÃ© du ticket ayant pour code (HQ4CP3Z) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-04-23 16:03:30', 'Adresse IP: 160.155.207.11 Port: 46926', '19'),
(236, 'Ticket portant le code (SN0Y61G) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-23 16:14:45', 'Adresse IP: 160.155.207.11 Port: 39096', '19'),
(237, 'Ticket portant le code (MX5DY86) en cours de paiement avec le numÃ©ro 23456789 ', '2021-04-23 16:15:34', 'Adresse IP: 160.155.207.11 Port: 42402', '19'),
(238, 'Ticket portant le code (T7OZ4G0) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-23 17:25:21', 'Adresse IP: 154.0.26.70 Port: 39668', 'client'),
(239, 'Ticket portant le code (P1P8AHA) en cours de paiement avec le numÃ©ro 78059869 ', '2021-04-23 17:26:21', 'Adresse IP: 154.0.26.70 Port: 43364', 'client'),
(240, 'Connexion de <b>osse Romeo ()</b>', '2021-04-23 05:33:23', 'Adresse IP: 154.0.26.70 Port: 36572', 'osse Romeo'),
(241, 'Connexion de <b>osse Romeo ()</b>', '2021-04-23 05:33:23', 'Adresse IP: 154.0.26.70 Port: 36604', 'osse Romeo'),
(242, 'Connexion de <b>osse Romeo ()</b>', '2021-04-23 05:33:24', 'Adresse IP: 154.0.26.70 Port: 36622', 'osse Romeo'),
(243, 'Cet utilisateur osse Romeo vient de changer son mot de passe  , adresse:Adresse IP: 154.0.26.70 Port: 40594', '0000-00-00 00:00:00', 'Adresse IP: 154.0.26.70 Port: 40594', '12'),
(244, 'Connexion de <b>osse Romeo ()</b>', '2021-04-23 05:37:28', 'Adresse IP: 154.0.26.70 Port: 50900', 'osse Romeo'),
(245, 'Connexion de <b>osse Romeo ()</b>', '2021-04-23 05:37:30', 'Adresse IP: 154.0.26.70 Port: 50978', 'osse Romeo'),
(246, 'Ticket portant le code (X68RLI1) en cours de paiement avec le numÃ©ro 0504398161 ', '2021-04-23 19:18:03', 'Adresse IP: 160.154.157.49 Port: 35720', 'client'),
(247, 'Ticket portant le code (1SVRG3O) en cours de paiement avec le numÃ©ro 0779481690 ', '2021-04-23 19:23:03', 'Adresse IP: 160.154.157.49 Port: 47652', 'client'),
(248, 'Connexion de <b>osse Romeo ()</b>', '2021-04-23 07:58:00', 'Adresse IP: 154.0.26.211 Port: 41860', 'osse Romeo'),
(249, 'Suppression de l\'utilisateur   par ', '2021-04-23 19:58:32', 'Adresse IP: 154.0.26.211 Port: 43458', '12'),
(250, 'Suppression de l\'utilisateur   par ', '2021-04-23 19:58:45', 'Adresse IP: 154.0.26.211 Port: 43974', '12'),
(251, 'Suppression de l\'utilisateur dom dom par ', '2021-04-23 19:59:04', 'Adresse IP: 154.0.26.211 Port: 45076', '12'),
(252, 'Cr&eacute;ation de l\'utilisateur tranvoyage super admin avec email admin@tranvoyage.com ,password : 7IPcfoP', '2021-04-23 20:24:28', 'Adresse IP: 154.0.26.211 Port: 39222', '12'),
(253, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-23 08:26:40', 'Adresse IP: 154.0.26.211 Port: 45888', 'tranvoyage super admin'),
(254, 'Cet utilisateur tranvoyage super admin vient de changer son mot de passe  , adresse:Adresse IP: 154.0.26.211 Port: 49360', '0000-00-00 00:00:00', 'Adresse IP: 154.0.26.211 Port: 49360', '74'),
(255, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-23 08:28:26', 'Adresse IP: 154.0.26.211 Port: 51784', 'tranvoyage super admin'),
(256, 'Ticket portant le code (R7GG2Q7) en cours de paiement avec le numÃ©ro 4534567898 ', '2021-04-23 20:30:27', 'Adresse IP: 154.0.26.211 Port: 58232', 'client'),
(257, 'Recherche trouvÃ© du ticket ayant pour code (LU4WLQN) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 20:30:52', 'Adresse IP: 154.0.26.211 Port: 59412', 'client'),
(258, 'Ticket portant le code (1YWX7S8) en cours de paiement avec le numÃ©ro 23456789 ', '2021-04-23 20:32:03', 'Adresse IP: 154.0.26.211 Port: 34374', 'client'),
(259, 'Recherche trouvÃ© du ticket ayant pour code (UVK2Y45) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 20:32:14', 'Adresse IP: 154.0.26.211 Port: 34750', 'client'),
(260, 'Recherche trouvÃ© du ticket ayant pour code (UVK2Y45) par le numÃ©ro de tÃ©tÃ©phone 23456789 ', '2021-04-23 20:32:14', 'Adresse IP: 154.0.26.211 Port: 34762', 'client'),
(261, 'Recherche non trouvÃ© du ticket ayant pour code (LU4WLQN3) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-23 20:32:38', 'Adresse IP: 154.0.26.211 Port: 35926', 'client'),
(262, 'Ticket portant le code (RQNJEWE) en cours de paiement avec le numÃ©ro 0504397161 ', '2021-04-24 08:51:25', 'Adresse IP: 160.154.155.32 Port: 47912', 'client'),
(263, 'Ticket portant le code (H15YLQG) en cours de paiement avec le numÃ©ro 0504397161 ', '2021-04-24 11:38:14', 'Adresse IP: 160.155.65.89 Port: 44756', 'client'),
(264, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-23 23:48:36', 'Adresse IP: 160.155.65.89 Port: 47398', 'tranvoyage super admin'),
(265, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-23 23:49:51', 'Adresse IP: 160.155.65.89 Port: 51130', 'tranvoyage super admin'),
(266, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-24 00:10:59', 'Adresse IP: 160.155.65.89 Port: 37244', 'tranvoyage super admin'),
(267, 'Recherche trouvÃ© du ticket ayant pour code (H15YLQG) par le numÃ©ro de tÃ©tÃ©phone 0504397161 ', '2021-04-24 12:12:29', 'Adresse IP: 160.154.151.248 Port: 42740', '74'),
(268, 'Ticket portant le code (G53E53X) en cours de paiement avec le numÃ©ro 0504397161 ', '2021-04-24 12:16:07', 'Adresse IP: 160.154.151.248 Port: 54832', 'client'),
(269, 'Recherche trouvÃ© du ticket ayant pour code (G53E53X) par le numÃ©ro de tÃ©tÃ©phone 0504397161 ', '2021-04-24 12:17:07', 'Adresse IP: 160.154.151.248 Port: 57896', 'client'),
(270, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-24 01:21:11', 'Adresse IP: 154.0.26.217 Port: 47430', 'tranvoyage super admin'),
(271, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-24 01:38:52', 'Adresse IP: 160.154.156.94 Port: 48182', 'tranvoyage super admin'),
(272, 'Suppression du voyage  <b>yopougon-BouakÃ©</b> par tranvoyage super admin', '2021-04-24 13:39:33', 'Adresse IP: 160.154.156.94 Port: 50172', '74'),
(273, 'Suppression du voyage  <b>BouakÃ©-yopougon</b> par tranvoyage super admin', '2021-04-24 13:41:48', 'Adresse IP: 160.154.156.94 Port: 58410', '74'),
(274, 'Suppression du voyage  <b>BouakÃ©-BouakÃ©</b> par tranvoyage super admin', '2021-04-24 13:42:05', 'Adresse IP: 160.154.156.94 Port: 59570', '74'),
(275, 'Suppression du voyage  <b>yopougon-yopougon</b> par tranvoyage super admin', '2021-04-24 13:42:29', 'Adresse IP: 160.154.156.94 Port: 33116', '74'),
(276, 'Suppression du voyage  <b>BouakÃ©-BouakÃ©</b> par tranvoyage super admin', '2021-04-24 13:42:48', 'Adresse IP: 160.154.156.94 Port: 34540', '74'),
(277, 'Suppression du voyage  <b>yopougon-yopougon</b> par tranvoyage super admin', '2021-04-24 13:43:08', 'Adresse IP: 160.154.156.94 Port: 35584', '74'),
(278, 'Suppression du voyage  <b>yopougon-BouakÃ©</b> par tranvoyage super admin', '2021-04-24 13:43:30', 'Adresse IP: 160.154.156.94 Port: 36946', '74'),
(279, 'Suppression du voyage  <b>BouakÃ©-yopougon</b> par tranvoyage super admin', '2021-04-24 13:43:52', 'Adresse IP: 160.154.156.94 Port: 37954', '74'),
(280, 'Suppression de la gare  par tranvoyage super admin', '2021-04-24 13:58:08', 'Adresse IP: 160.154.156.94 Port: 58720', '74'),
(281, 'Suppression de la gare  par tranvoyage super admin', '2021-04-24 13:58:36', 'Adresse IP: 160.154.156.94 Port: 60522', '74'),
(282, 'Cr&eacute;ation de la gare CTE ADJAMÃ‰ RENAULT  avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-24 14:04:46', 'Adresse IP: 160.154.156.94 Port: 58732', '74'),
(283, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-24 02:21:38', 'Adresse IP: 160.155.65.89 Port: 39178', 'tranvoyage super admin'),
(284, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-24 05:01:00', 'Adresse IP: 160.155.112.163 Port: 60932', 'tranvoyage super admin'),
(285, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-24 05:01:01', 'Adresse IP: 160.155.112.163 Port: 32804', 'tranvoyage super admin'),
(286, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-24 09:03:09', 'Adresse IP: 154.0.26.63 Port: 40488', 'tranvoyage super admin'),
(287, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-25 04:35:57', 'Adresse IP: 160.155.112.163 Port: 36976', 'tranvoyage super admin'),
(288, 'Cr&eacute;ation de la compagnie CTE TRANSPORT  avec le numero 0505489532 et email ctecompagniedetransportexpress@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 04:42:36', 'Adresse IP: 160.155.112.163 Port: 55088', '74'),
(289, 'Suppression de l\'compagnie  par tranvoyage super admin', '2021-04-25 04:42:53', 'Adresse IP: 160.155.112.163 Port: 55826', '74'),
(290, 'Suppression de l\'compagnie  par tranvoyage super admin', '2021-04-25 04:43:03', 'Adresse IP: 160.155.112.163 Port: 56280', '74'),
(291, 'Cr&eacute;ation de la compagnie SABE TRANSPORT  avec le numero 22504375 et email tran.reservation@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 04:45:45', 'Adresse IP: 160.155.112.163 Port: 36658', '74'),
(292, 'Cr&eacute;ation de la compagnie SITTI TRANSPORT  avec le numero 0777251106 et email sitti@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 04:51:02', 'Adresse IP: 160.155.112.163 Port: 51140', '74'),
(293, 'Cr&eacute;ation de la compagnie GAMON TRANSPORT  avec le numero 0103339813 et email gamon@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 04:52:22', 'Adresse IP: 160.155.112.163 Port: 54706', '74'),
(294, 'Cr&eacute;ation de la compagnie TTF TRANSPORT  avec le numero 0709044555 et email ttf@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 04:54:06', 'Adresse IP: 160.155.112.163 Port: 58706', '74'),
(295, 'Cr&eacute;ation de la compagnie COMPAGNIE DE TRANSPORT TCHOLOGO OFFICIEL  avec le numero 0575112078/0747707778 et email ctto@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 04:59:17', 'Adresse IP: 160.155.112.163 Port: 42648', '74'),
(296, 'Cr&eacute;ation de la compagnie GTT TRANSPORT  avec le numero 0140796023 et email gtt@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:01:56', 'Adresse IP: 160.155.112.163 Port: 50120', '74'),
(297, 'Cr&eacute;ation de la compagnie STBA TRANSPORT  avec le numero 0707673454 et email stba@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:04:16', 'Adresse IP: 160.155.112.163 Port: 56402', '74'),
(298, 'Cr&eacute;ation de la compagnie STC TRANSPORT  avec le numero 0707853060 et email stc@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:06:04', 'Adresse IP: 160.155.112.163 Port: 60718', '74'),
(299, 'Cr&eacute;ation de la compagnie UTS TRANSPORT  avec le numero 0777601570 et email uts@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:09:16', 'Adresse IP: 160.155.112.163 Port: 40894', '74'),
(300, 'Cr&eacute;ation de la compagnie TRANSPORT AVS avec le numero 0102956063 et email transportavs@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:11:25', 'Adresse IP: 160.155.112.163 Port: 47540', '74'),
(301, 'Cr&eacute;ation de la compagnie GDF TRANSPORT  avec le numero 0779801963 et email gdftransport@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:12:48', 'Adresse IP: 160.155.112.163 Port: 51470', '74'),
(302, 'Cr&eacute;ation de la compagnie TCF EXPRESS MISTRAL  avec le numero 0140511652 et email tcf@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:15:22', 'Adresse IP: 160.155.112.163 Port: 59114', '74'),
(303, 'Cr&eacute;ation de la compagnie GDF TRANSPORT * avec le numero 20372037/ 0758609881 et email gdftransport@cmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 05:19:58', 'Adresse IP: 160.155.112.163 Port: 46460', '74'),
(304, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-25 06:04:43', 'Adresse IP: 160.155.112.163 Port: 60420', 'tranvoyage super admin'),
(305, 'Cr&eacute;ation de la compagnie GANA TRANSPORT CI avec le numero 0777872347 et email ganatrans@yahoo.fr par l\'utilisateur tranvoyage super admin', '2021-04-25 06:09:20', 'Adresse IP: 160.155.112.163 Port: 46026', '74'),
(306, 'Cr&eacute;ation de la compagnie ETS SAMA TRANSPORT  avec le numero 0708028942/0574585827 et email samatransport@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 06:14:20', 'Adresse IP: 160.155.112.163 Port: 60756', '74'),
(307, 'Cr&eacute;ation de la compagnie TILEMSI TRANSPORT ABIDJAN  avec le numero +22344904212 et email tilemstransport@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 06:33:24', 'Adresse IP: 160.154.155.81 Port: 39852', '74'),
(308, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-25 06:50:22', 'Adresse IP: 160.154.155.81 Port: 36090', 'tranvoyage super admin'),
(309, 'Cr&eacute;ation de la compagnie SONEF TRANSPORT VOYAGEURS  avec le numero +22720734358/+22790901239/22720752174 et email sonef_niger@yahoo.fr par l\'utilisateur tranvoyage super admin', '2021-04-25 06:56:17', 'Adresse IP: 160.154.155.81 Port: 51622', '74'),
(310, 'Cr&eacute;ation de la compagnie CHONCO TRANSPORT  avec le numero 0586909092 et email dsilue7@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-25 06:59:54', 'Adresse IP: 160.154.155.81 Port: 33506', '74'),
(311, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-25 02:33:37', 'Adresse IP: 160.154.137.173 Port: 40134', 'tranvoyage super admin'),
(312, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-25 02:50:17', 'Adresse IP: 160.154.137.173 Port: 44254', 'tranvoyage super admin'),
(313, 'Suppression de la gare  par tranvoyage super admin', '2021-04-25 14:51:43', 'Adresse IP: 160.154.137.173 Port: 49038', '74'),
(314, 'Cr&eacute;ation de la gare ADJAMÃ‰  avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:52:15', 'Adresse IP: 160.154.137.173 Port: 51320', '74');
INSERT INTO `trace` (`id_trace`, `lib_trace`, `date_trace`, `adresse_ip`, `secur`) VALUES
(315, 'Cr&eacute;ation de la gare ABOBO  avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:52:35', 'Adresse IP: 160.154.137.173 Port: 52846', '74'),
(316, 'Cr&eacute;ation de la gare YAMOUSSOUKRO  avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:53:00', 'Adresse IP: 160.154.137.173 Port: 54154', '74'),
(317, 'Cr&eacute;ation de la gare BOUAKÃ‰ avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:53:26', 'Adresse IP: 160.154.137.173 Port: 55760', '74'),
(318, 'Cr&eacute;ation de la gare KOROGHO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:53:56', 'Adresse IP: 160.154.137.173 Port: 57494', '74'),
(319, 'Cr&eacute;ation de la gare FERKÃ‰ avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:54:21', 'Adresse IP: 160.154.137.173 Port: 59292', '74'),
(320, 'Cr&eacute;ation de la gare BEOUMI avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:54:52', 'Adresse IP: 160.154.137.173 Port: 60830', '74'),
(321, 'Cr&eacute;ation de la gare BONON avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:55:26', 'Adresse IP: 160.154.137.173 Port: 34562', '74'),
(322, 'Cr&eacute;ation de la gare BOUAFLÃ‰ avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:55:53', 'Adresse IP: 160.154.137.173 Port: 36270', '74'),
(323, 'Cr&eacute;ation de la gare DALOA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:56:44', 'Adresse IP: 160.154.137.173 Port: 39386', '74'),
(324, 'Cr&eacute;ation de la gare DIMBOKRO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:57:11', 'Adresse IP: 160.154.137.173 Port: 40746', '74'),
(325, 'Cr&eacute;ation de la gare DIVO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:57:30', 'Adresse IP: 160.154.137.173 Port: 42308', '74'),
(326, 'Cr&eacute;ation de la gare DUEKOUE avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:58:03', 'Adresse IP: 160.154.137.173 Port: 44608', '74'),
(327, 'Cr&eacute;ation de la gare GABIADJI avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:58:38', 'Adresse IP: 160.154.137.173 Port: 46816', '74'),
(328, 'Cr&eacute;ation de la gare GABIADJI avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:59:14', 'Adresse IP: 160.154.137.173 Port: 49014', '74'),
(329, 'Cr&eacute;ation de la gare GAGNOA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:59:33', 'Adresse IP: 160.154.137.173 Port: 50216', '74'),
(330, 'Cr&eacute;ation de la gare GESCO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 14:59:58', 'Adresse IP: 160.154.137.173 Port: 51412', '74'),
(331, 'Cr&eacute;ation de la gare GONATE avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:00:32', 'Adresse IP: 160.154.137.173 Port: 54272', '74'),
(332, 'Cr&eacute;ation de la gare GUESSABO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:01:05', 'Adresse IP: 160.154.137.173 Port: 56740', '74'),
(333, 'Cr&eacute;ation de la gare HIRÃ‰ avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:01:35', 'Adresse IP: 160.154.137.173 Port: 58166', '74'),
(334, 'Cr&eacute;ation de la gare ISSIA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:02:18', 'Adresse IP: 160.154.137.173 Port: 60702', '74'),
(335, 'Cr&eacute;ation de la gare KOUMASSI avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:02:42', 'Adresse IP: 160.154.137.173 Port: 33366', '74'),
(336, 'Cr&eacute;ation de la gare MAN avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:03:10', 'Adresse IP: 160.154.137.173 Port: 35248', '74'),
(337, 'Cr&eacute;ation de la gare MEAGUI avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:03:35', 'Adresse IP: 160.154.137.173 Port: 36612', '74'),
(338, 'Cr&eacute;ation de la gare SAN PEDRO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:04:16', 'Adresse IP: 160.154.137.173 Port: 38616', '74'),
(339, 'Cr&eacute;ation de la gare SINFRA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:04:53', 'Adresse IP: 160.154.137.173 Port: 40564', '74'),
(340, 'Cr&eacute;ation de la gare SOUBRE avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:05:18', 'Adresse IP: 160.154.137.173 Port: 41922', '74'),
(341, 'Cr&eacute;ation de la gare TIEBISSOU avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:05:44', 'Adresse IP: 160.154.137.173 Port: 43766', '74'),
(342, 'Cr&eacute;ation de la gare TOUMODI avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:06:04', 'Adresse IP: 160.154.137.173 Port: 45628', '74'),
(343, 'Cr&eacute;ation de la gare YABAYO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:06:27', 'Adresse IP: 160.154.137.173 Port: 46930', '74'),
(344, 'Cr&eacute;ation de la gare ODIENE avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:06:57', 'Adresse IP: 160.154.137.173 Port: 48458', '74'),
(345, 'Cr&eacute;ation de la gare TOUBA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:07:24', 'Adresse IP: 160.154.137.173 Port: 50000', '74'),
(346, 'Cr&eacute;ation de la gare BONDOUKOU avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:08:17', 'Adresse IP: 160.154.137.173 Port: 53610', '74'),
(347, 'Cr&eacute;ation de la gare TOUBA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:08:38', 'Adresse IP: 160.154.137.173 Port: 54630', '74'),
(348, 'Cr&eacute;ation de la gare VAVOUA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:08:58', 'Adresse IP: 160.154.137.173 Port: 55570', '74'),
(349, 'Cr&eacute;ation de la compagnie UTB avec le numero 0564616360 et email contacts@utb.ci par l\'utilisateur tranvoyage super admin', '2021-04-25 15:14:17', 'Adresse IP: 160.154.137.173 Port: 42426', '74'),
(350, 'Cr&eacute;ation du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-OZUJCX par l\'utilisateur tranvoyage super admin', '2021-04-25 15:16:31', 'Adresse IP: 160.154.137.173 Port: 49884', '74'),
(351, 'Cr&eacute;ation du voyage <b>BOUAKÃ‰-BOUAKÃ‰</b> avec code VOYAGE-DIV5JQ par l\'utilisateur tranvoyage super admin', '2021-04-25 15:18:01', 'Adresse IP: 160.154.137.173 Port: 54780', '74'),
(352, 'Modification du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-E9TNQY par l\'utilisateur tranvoyage super admin', '2021-04-25 15:18:35', 'Adresse IP: 160.154.137.173 Port: 56800', '74'),
(353, 'Cr&eacute;ation du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-FBTKYJ par l\'utilisateur tranvoyage super admin', '2021-04-25 15:19:44', 'Adresse IP: 160.154.137.173 Port: 60422', '74'),
(354, 'Cr&eacute;ation du voyage <b>ABOBO -YAMOUSSOUKRO </b> avec code VOYAGE-FH5WEM par l\'utilisateur tranvoyage super admin', '2021-04-25 15:26:49', 'Adresse IP: 160.154.137.173 Port: 54190', '74'),
(355, 'Cr&eacute;ation du voyage <b>ABOBO -YAMOUSSOUKRO </b> avec code VOYAGE-KUC6PQ par l\'utilisateur tranvoyage super admin', '2021-04-25 15:28:03', 'Adresse IP: 160.154.137.173 Port: 59058', '74'),
(356, 'Cr&eacute;ation du voyage <b>ABOBO -YAMOUSSOUKRO </b> avec code VOYAGE-MXUROA par l\'utilisateur tranvoyage super admin', '2021-04-25 15:29:11', 'Adresse IP: 160.154.137.173 Port: 34384', '74'),
(357, 'Cr&eacute;ation du voyage <b>ABOBO -YAMOUSSOUKRO </b> avec code VOYAGE-K8RYQU par l\'utilisateur tranvoyage super admin', '2021-04-25 15:29:59', 'Adresse IP: 160.154.137.173 Port: 37556', '74'),
(358, 'Cr&eacute;ation du voyage <b>ABOBO -BEOUMI</b> avec code VOYAGE-ZRQGGR par l\'utilisateur tranvoyage super admin', '2021-04-25 15:32:47', 'Adresse IP: 160.154.137.173 Port: 48062', '74'),
(359, 'Cr&eacute;ation du voyage <b>ABOBO -BEOUMI</b> avec code VOYAGE-SHKWUX par l\'utilisateur tranvoyage super admin', '2021-04-25 15:34:13', 'Adresse IP: 160.154.137.173 Port: 53206', '74'),
(360, 'Cr&eacute;ation de la gare BANGOLO avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-25 15:35:49', 'Adresse IP: 160.154.137.173 Port: 57848', '74'),
(361, 'Cr&eacute;ation du voyage <b>ABOBO -BANGOLO</b> avec code VOYAGE-IVDQS0 par l\'utilisateur tranvoyage super admin', '2021-04-25 15:37:16', 'Adresse IP: 160.154.137.173 Port: 34136', '74'),
(362, 'Cr&eacute;ation du voyage <b>ABOBO -BOUAFLÃ‰</b> avec code VOYAGE-9J8RQS par l\'utilisateur tranvoyage super admin', '2021-04-25 15:39:39', 'Adresse IP: 160.154.137.173 Port: 40710', '74'),
(363, 'Cr&eacute;ation du voyage <b>ABOBO -DALOA</b> avec code VOYAGE-WGSCPA par l\'utilisateur tranvoyage super admin', '2021-04-25 15:41:19', 'Adresse IP: 160.154.137.173 Port: 46138', '74'),
(364, 'Cr&eacute;ation du voyage <b>ADJAMÃ‰ -DIMBOKRO</b> avec code VOYAGE-GAIYBQ par l\'utilisateur tranvoyage super admin', '2021-04-25 16:01:00', 'Adresse IP: 160.154.137.173 Port: 53922', '74'),
(365, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-25 09:21:38', 'Adresse IP: 160.155.112.163 Port: 35020', 'tranvoyage super admin'),
(366, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 02:25:14', 'Adresse IP: 160.155.112.163 Port: 42062', 'tranvoyage super admin'),
(367, 'Cr&eacute;ation de la gare TOUBA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:26:04', 'Adresse IP: 160.155.112.163 Port: 44044', '74'),
(368, 'Cr&eacute;ation de la gare DANANÃ‰ avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:26:30', 'Adresse IP: 160.155.112.163 Port: 45256', '74'),
(369, 'Cr&eacute;ation de la gare BIANKOUMA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:27:36', 'Adresse IP: 160.155.112.163 Port: 47728', '74'),
(370, 'Cr&eacute;ation de la gare ZOAN-HOUNIEN avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:28:32', 'Adresse IP: 160.155.112.163 Port: 50610', '74'),
(371, 'Cr&eacute;ation de la gare BLÃ‰NIMÃ‰OUIN avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:30:52', 'Adresse IP: 160.155.112.163 Port: 57624', '74'),
(372, 'Cr&eacute;ation de la gare TEAPLEU avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:31:13', 'Adresse IP: 160.155.112.163 Port: 58398', '74'),
(373, 'Cr&eacute;ation de la gare MAHAPLEU avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:31:59', 'Adresse IP: 160.155.112.163 Port: 59912', '74'),
(374, 'Cr&eacute;ation de la gare LAKOTA avec le numero  et email  par l\'utilisateur tranvoyage super admin', '2021-04-26 02:40:03', 'Adresse IP: 160.155.112.163 Port: 48768', '74'),
(375, 'Cr&eacute;ation de la compagnie OUEST TRANSACTIONS CI OT TRANSPORT  avec le numero 20375186 et email ouesttransactionci@gmail.com par l\'utilisateur tranvoyage super admin', '2021-04-26 02:45:10', 'Adresse IP: 160.155.112.163 Port: 60124', '74'),
(376, 'Cr&eacute;ation du voyage <b>TOUBA-LAKOTA</b> avec code VOYAGE-1CH6FA par l\'utilisateur tranvoyage super admin', '2021-04-26 02:47:35', 'Adresse IP: 160.155.112.163 Port: 37478', '74'),
(377, 'Cr&eacute;ation du voyage <b>LAKOTA-TOUBA</b> avec code VOYAGE-QYMNXG par l\'utilisateur tranvoyage super admin', '2021-04-26 02:50:34', 'Adresse IP: 160.155.112.163 Port: 43098', '74'),
(378, 'Cr&eacute;ation du voyage <b>ADJAMÃ‰ -DUEKOUE</b> avec code VOYAGE-H4FZNX par l\'utilisateur tranvoyage super admin', '2021-04-26 02:55:33', 'Adresse IP: 160.155.112.163 Port: 54176', '74'),
(379, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 03:37:14', 'Adresse IP: 160.155.112.163 Port: 33638', 'tranvoyage super admin'),
(380, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 05:06:28', 'Adresse IP: 160.155.65.89 Port: 34244', 'tranvoyage super admin'),
(381, 'dÃ©verrouillage du voyage <b>ABOBO -BOUAKÃ‰</b> par tranvoyage super admin', '2021-04-26 05:07:04', 'Adresse IP: 160.155.65.89 Port: 35854', '74'),
(382, 'dÃ©verrouillage du voyage <b>ABOBO -BOUAKÃ‰</b> par tranvoyage super admin', '2021-04-26 05:07:33', 'Adresse IP: 160.154.137.72 Port: 36812', '74'),
(383, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 05:54:47', 'Adresse IP: 154.0.26.12 Port: 47156', 'tranvoyage super admin'),
(384, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 05:54:47', 'Adresse IP: 154.0.26.12 Port: 47182', 'tranvoyage super admin'),
(385, 'dÃ©verrouillage du voyage <b>ABOBO -BOUAKÃ‰</b> par tranvoyage super admin', '2021-04-26 05:55:55', 'Adresse IP: 154.0.26.12 Port: 52744', '74'),
(386, 'dÃ©verrouillage du voyage <b>ABOBO -YAMOUSSOUKRO </b> par tranvoyage super admin', '2021-04-26 05:56:44', 'Adresse IP: 154.0.26.12 Port: 57474', '74'),
(387, 'Ticket portant le code (VO43FS1) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-26 05:59:22', 'Adresse IP: 154.0.26.12 Port: 38438', '74'),
(388, 'Recherche trouvÃ© du ticket ayant pour code (VO43FS1) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-04-26 06:00:33', 'Adresse IP: 154.0.26.12 Port: 42506', '74'),
(389, 'Recherche trouvÃ© du ticket ayant pour code (VO43FS1) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-04-26 06:02:51', 'Adresse IP: 154.0.26.12 Port: 48822', '74'),
(390, 'Recherche trouvÃ© du ticket ayant pour code (VO43FS1) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-04-26 06:02:54', 'Adresse IP: 154.0.26.12 Port: 49228', '74'),
(391, 'verrouillage du voyage <b>ABOBO -BOUAKÃ‰</b> par tranvoyage super admin', '2021-04-26 06:04:44', 'Adresse IP: 160.154.137.72 Port: 55470', '74'),
(392, 'dÃ©verrouillage du voyage <b>ABOBO -BOUAKÃ‰</b> par tranvoyage super admin', '2021-04-26 06:05:02', 'Adresse IP: 160.154.137.72 Port: 56752', '74'),
(393, 'dÃ©verrouillage du voyage <b>ABOBO -YAMOUSSOUKRO </b> par tranvoyage super admin', '2021-04-26 06:05:36', 'Adresse IP: 160.154.137.72 Port: 58540', '74'),
(394, 'dÃ©verrouillage du voyage <b>ABOBO -YAMOUSSOUKRO </b> par tranvoyage super admin', '2021-04-26 06:06:01', 'Adresse IP: 160.154.137.72 Port: 59968', '74'),
(395, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 06:50:49', 'Adresse IP: 160.155.65.89 Port: 57660', 'tranvoyage super admin'),
(396, 'Suppression du voyage  <b>ABOBO -YAMOUSSOUKRO </b> par tranvoyage super admin', '2021-04-26 06:51:16', 'Adresse IP: 160.155.65.89 Port: 59388', '74'),
(397, 'dÃ©verrouillage du voyage <b>ABOBO -BEOUMI</b> par tranvoyage super admin', '2021-04-26 06:51:53', 'Adresse IP: 160.154.137.72 Port: 33842', '74'),
(398, 'dÃ©verrouillage du voyage <b>ABOBO -BEOUMI</b> par tranvoyage super admin', '2021-04-26 06:52:11', 'Adresse IP: 160.154.137.72 Port: 35116', '74'),
(399, 'dÃ©verrouillage du voyage <b>ABOBO -BANGOLO</b> par tranvoyage super admin', '2021-04-26 06:52:26', 'Adresse IP: 160.154.137.72 Port: 36312', '74'),
(400, 'dÃ©verrouillage du voyage <b>ABOBO -BOUAFLÃ‰</b> par tranvoyage super admin', '2021-04-26 06:52:43', 'Adresse IP: 160.154.137.72 Port: 37462', '74'),
(401, 'Ticket portant le code (GDUY2IS) en cours de paiement avec le numÃ©ro 05279075 ', '2021-04-26 07:47:00', 'Adresse IP: 160.155.65.89 Port: 41428', 'client'),
(402, 'Ticket portant le code (5VL6UWB) en cours de paiement avec le numÃ©ro 04397161 ', '2021-04-26 08:17:36', 'Adresse IP: 160.154.136.184 Port: 51168', 'client'),
(403, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 08:20:52', 'Adresse IP: 160.154.136.184 Port: 35240', 'tranvoyage super admin'),
(404, 'Recherche trouvÃ© du ticket ayant pour code (5VL6UWB) par le numÃ©ro de tÃ©tÃ©phone 04397161 ', '2021-04-26 08:31:07', 'Adresse IP: 160.154.136.184 Port: 48382', '74'),
(405, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 03:17:46', 'Adresse IP: 160.155.65.89 Port: 42142', 'tranvoyage super admin'),
(406, 'Ticket portant le code (D7RWXSX) en cours de paiement avec le numÃ©ro 78059869 ', '2021-04-26 15:46:58', 'Adresse IP: 160.155.207.11 Port: 48454', 'client'),
(407, 'Ticket portant le code (M9GUIH9) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-26 16:47:23', 'Adresse IP: 160.155.207.11 Port: 47732', 'client'),
(408, 'Ticket portant le code (FSYFJ4R) en cours de paiement avec le numÃ©ro 02304549 ', '2021-04-26 17:56:07', 'Adresse IP: 160.155.112.163 Port: 42410', 'client'),
(409, 'Ticket portant le code (QKAFQJV) en cours de paiement avec le numÃ©ro 04397161 ', '2021-04-26 17:59:03', 'Adresse IP: 160.155.112.163 Port: 52624', 'client'),
(410, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-26 06:50:47', 'Adresse IP: 160.155.112.163 Port: 53526', 'tranvoyage super admin'),
(411, 'Ticket portant le code (APTJLMN) en cours de paiement avec le numÃ©ro 7805986932 ', '2021-04-26 19:26:41', 'Adresse IP: 154.0.26.25 Port: 35128', 'client'),
(412, 'Recherche trouvÃ© du ticket ayant pour code (APTJLMN) par le numÃ©ro de tÃ©tÃ©phone 7805986932 ', '2021-04-26 19:32:20', 'Adresse IP: 154.0.26.25 Port: 53612', 'client'),
(413, 'Ticket portant le code (60U2N59) en cours de paiement avec le numÃ©ro 0102110129 ', '2021-04-27 09:37:00', 'Adresse IP: 160.155.207.11 Port: 59364', 'client'),
(414, 'Ticket portant le code (3O4D5W2) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-27 16:59:38', 'Adresse IP: 160.155.207.11 Port: 37036', 'client'),
(415, 'Confirmation trouvÃ© du ticket ayant pour code (3O4D5W2) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-04-27 17:00:25', 'Adresse IP: 160.155.207.11 Port: 39538', 'client'),
(416, 'Ticket portant le code (HYG6NQ3) en cours de paiement avec le numÃ©ro 79481690 ', '2021-04-27 19:26:16', 'Adresse IP: 160.154.157.64 Port: 58802', 'client'),
(417, 'Recherche trouvÃ© du ticket ayant pour code (HYG6NQ3) par le numÃ©ro de tÃ©tÃ©phone 79481690 ', '2021-04-27 19:28:28', 'Adresse IP: 160.154.157.64 Port: 37738', 'client'),
(418, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-27 09:15:29', 'Adresse IP: 160.155.112.163 Port: 50324', 'tranvoyage super admin'),
(419, 'Ticket portant le code (2G1ARNO) en cours de paiement avec le numÃ©ro 2345678998 ', '2021-04-28 07:12:01', 'Adresse IP: 160.155.207.11 Port: 33954', 'client'),
(420, 'Ticket portant le code (J023YLD) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-28 07:33:05', 'Adresse IP: 160.155.207.11 Port: 32830', 'client'),
(421, 'Ticket portant le code (YC7EVCL) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-28 07:37:07', 'Adresse IP: 160.155.207.11 Port: 48926', 'client'),
(422, 'Ticket portant le code (UG8IS5X) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-28 09:40:43', 'Adresse IP: 160.155.207.11 Port: 49412', 'client'),
(423, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-28 10:25:11', 'Adresse IP: 160.155.207.11 Port: 54672', 'tranvoyage super admin'),
(424, 'Mise Ã  jour de l\'utilisateur patrick assande avec email patrick@pat.com ,password : Bu2sa4n', '2021-04-28 10:28:11', 'Adresse IP: 160.155.207.11 Port: 35162', '74'),
(425, 'CrÃ©ation de l\'affectation d\'un utilisateur Ã  une gare par l\'utilisateur tranvoyage super admin', '2021-04-28 10:28:55', 'Adresse IP: 160.155.207.11 Port: 37894', '74'),
(426, 'Modification  de l\'affectation d\'une affectation par l\'utilisateur tranvoyage super admin', '2021-04-28 10:34:46', 'Adresse IP: 160.155.207.11 Port: 55932', '74'),
(427, 'Modification  de l\'affectation d\'une affectation par l\'utilisateur tranvoyage super admin', '2021-04-28 10:35:09', 'Adresse IP: 160.155.207.11 Port: 57110', '74'),
(428, 'Suppression Ã©ffectuÃ©e d\'une affectation par tranvoyage super admin', '2021-04-28 10:35:42', 'Adresse IP: 160.155.207.11 Port: 58798', '74'),
(429, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-28 01:31:29', 'Adresse IP: 160.154.155.151 Port: 58932', 'tranvoyage super admin'),
(430, 'Ticket portant le code (QSOLO3H) en cours de paiement avec le numÃ©ro 58454115 ', '2021-04-28 13:33:59', 'Adresse IP: 154.68.5.107 Port: 39050', 'client'),
(431, 'Ticket portant le code (4Z1YPYV) en cours de paiement avec le numÃ©ro 58786407 ', '2021-04-28 13:50:31', 'Adresse IP: 154.0.26.198 Port: 35772', 'client'),
(432, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-28 02:46:18', 'Adresse IP: 160.155.65.89 Port: 46470', 'tranvoyage super admin'),
(433, 'Cr&eacute;ation de l\'utilisateur jo Jo avec email jo@gmail.com ,password : ZGobM0V', '2021-04-28 14:53:42', 'Adresse IP: 160.155.65.89 Port: 49562', '74'),
(434, 'CrÃ©ation de l\'affectation d\'un utilisateur Ã  une gare par l\'utilisateur tranvoyage super admin', '2021-04-28 16:38:33', 'Adresse IP: 160.155.207.11 Port: 52118', '74'),
(435, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-28 09:21:58', 'Adresse IP: 160.155.112.163 Port: 56086', 'tranvoyage super admin'),
(436, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-28 09:32:04', 'Adresse IP: 160.155.112.163 Port: 49944', 'tranvoyage super admin'),
(437, 'Modification du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-0VJNUV par l\'utilisateur tranvoyage super admin', '2021-04-28 21:33:27', 'Adresse IP: 160.155.112.163 Port: 53014', '74'),
(438, 'Modification du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-ZVSQ0A par l\'utilisateur tranvoyage super admin', '2021-04-28 21:33:59', 'Adresse IP: 160.155.112.163 Port: 54516', '74'),
(439, 'Modification du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-TLER8R par l\'utilisateur tranvoyage super admin', '2021-04-28 21:34:23', 'Adresse IP: 160.155.112.163 Port: 55644', '74'),
(440, 'Modification du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-CA5LUR par l\'utilisateur tranvoyage super admin', '2021-04-28 21:35:02', 'Adresse IP: 160.155.112.163 Port: 57340', '74'),
(441, 'Modification du voyage <b>ABOBO -BOUAKÃ‰</b> avec code VOYAGE-MJTGFP par l\'utilisateur tranvoyage super admin', '2021-04-28 21:35:46', 'Adresse IP: 160.155.112.163 Port: 58732', '74'),
(442, 'Modification du voyage <b>ABOBO -YAMOUSSOUKRO </b> avec code VOYAGE-MK5HGF par l\'utilisateur tranvoyage super admin', '2021-04-28 21:36:14', 'Adresse IP: 160.155.112.163 Port: 59682', '74'),
(443, 'Modification du voyage <b>ABOBO -YAMOUSSOUKRO </b> avec code VOYAGE-MUIWHN par l\'utilisateur tranvoyage super admin', '2021-04-28 21:38:32', 'Adresse IP: 160.155.112.163 Port: 36022', '74'),
(444, 'Modification du voyage <b>ABOBO -YAMOUSSOUKRO </b> avec code VOYAGE-P30RLQ par l\'utilisateur tranvoyage super admin', '2021-04-28 21:39:01', 'Adresse IP: 160.155.112.163 Port: 37470', '74'),
(445, 'Modification du voyage <b>ABOBO -BEOUMI</b> avec code VOYAGE-H1RDBD par l\'utilisateur tranvoyage super admin', '2021-04-28 21:39:46', 'Adresse IP: 160.155.112.163 Port: 39080', '74'),
(446, 'Modification du voyage <b>ABOBO -BEOUMI</b> avec code VOYAGE-DKR9P3 par l\'utilisateur tranvoyage super admin', '2021-04-28 21:40:33', 'Adresse IP: 160.155.112.163 Port: 41586', '74'),
(447, 'Modification du voyage <b>ABOBO -BANGOLO</b> avec code VOYAGE-OXG8QM par l\'utilisateur tranvoyage super admin', '2021-04-28 21:41:16', 'Adresse IP: 160.155.112.163 Port: 44502', '74'),
(448, 'Modification du voyage <b>ABOBO -BOUAFLÃ‰</b> avec code VOYAGE-QYSEDZ par l\'utilisateur tranvoyage super admin', '2021-04-28 21:41:44', 'Adresse IP: 160.155.112.163 Port: 45960', '74'),
(449, 'Ticket portant le code (KHSRKFZ) en cours de paiement avec le numÃ©ro 04397161 ', '2021-04-29 05:38:59', 'Adresse IP: 160.154.138.15 Port: 38812', 'client'),
(450, 'Confirmation trouvÃ© du ticket ayant pour code (KHSRKFZ) par le numÃ©ro de tÃ©tÃ©phone 04397161 ', '2021-04-29 05:40:28', 'Adresse IP: 160.154.138.15 Port: 42466', 'client'),
(451, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-29 06:00:58', 'Adresse IP: 160.154.138.15 Port: 48534', 'tranvoyage super admin'),
(452, 'Ticket portant le code (WC7MZJ9) en cours de paiement avec le numÃ©ro 0748499366 ', '2021-04-29 08:41:46', 'Adresse IP: 160.154.138.15 Port: 56908', '74'),
(453, 'Confirmation non trouvÃ© du ticket ayant pour code (WC7MZJ09) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-29 08:42:56', 'Adresse IP: 160.154.138.15 Port: 32884', '74'),
(454, 'Confirmation non trouvÃ© du ticket ayant pour code (WC7MZJ09) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-29 08:43:11', 'Adresse IP: 160.154.138.15 Port: 33534', '74'),
(455, 'Confirmation trouvÃ© du ticket ayant pour code (WC7MZJ9) par le numÃ©ro de tÃ©tÃ©phone 0748499366 ', '2021-04-29 08:43:41', 'Adresse IP: 160.154.138.15 Port: 35570', '74'),
(456, 'Cr&eacute;ation du voyage <b>ADJAMÃ‰ -MAN</b> avec code VOYAGE-GZ9E0N par l\'utilisateur tranvoyage super admin', '2021-04-29 08:49:25', 'Adresse IP: 160.154.156.85 Port: 56856', '74'),
(457, 'verrouillage du voyage <b>ABOBO -BOUAFLÃ‰</b> par tranvoyage super admin', '2021-04-29 08:55:42', 'Adresse IP: 160.154.156.143 Port: 52524', '74'),
(458, 'dÃ©verrouillage du voyage <b>ABOBO -BOUAFLÃ‰</b> par tranvoyage super admin', '2021-04-29 08:58:22', 'Adresse IP: 160.154.156.143 Port: 33826', '74'),
(459, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-29 09:23:39', 'Adresse IP: 160.155.207.11 Port: 45402', 'tranvoyage super admin'),
(460, 'dÃ©verrouillage du voyage <b>ABOBO -DALOA</b> par tranvoyage super admin', '2021-04-29 12:11:15', 'Adresse IP: 160.154.156.143 Port: 59694', '74'),
(461, 'dÃ©verrouillage du voyage <b>ADJAMÃ‰ -MAN</b> par tranvoyage super admin', '2021-04-29 12:12:21', 'Adresse IP: 160.154.156.143 Port: 36080', '74'),
(462, 'Ticket portant le code (7HAFQ10) en cours de paiement avec le numÃ©ro 04397161 ', '2021-04-29 12:32:43', 'Adresse IP: 160.154.156.143 Port: 60148', 'client'),
(463, 'Confirmation trouvÃ© du ticket ayant pour code (7HAFQ10) par le numÃ©ro de tÃ©tÃ©phone 04397161 ', '2021-04-29 12:34:23', 'Adresse IP: 160.154.156.143 Port: 38348', 'client'),
(464, 'Modification du voyage <b>ABOBO -BEOUMI</b> avec code VOYAGE-BMQU16 par l\'utilisateur tranvoyage super admin', '2021-04-29 12:51:18', 'Adresse IP: 160.155.207.11 Port: 37788', '74'),
(465, 'Modification du voyage <b>ABOBO -BEOUMI</b> avec code VOYAGE-GVMOCZ par l\'utilisateur tranvoyage super admin', '2021-04-29 12:52:18', 'Adresse IP: 160.155.207.11 Port: 41042', '74'),
(466, 'Cr&eacute;ation du voyage <b>ADJAMÃ‰ -TEAPLEU</b> avec code VOYAGE-7CTHPW par l\'utilisateur tranvoyage super admin', '2021-04-29 13:05:17', 'Adresse IP: 160.155.207.11 Port: 55342', '74'),
(467, 'Modification du voyage <b>ADJAMÃ‰ -TEAPLEU</b> avec code VOYAGE-JZKTP8 par l\'utilisateur tranvoyage super admin', '2021-04-29 13:06:02', 'Adresse IP: 160.155.207.11 Port: 58414', '74'),
(468, 'Modification du voyage <b>ADJAMÃ‰ -TEAPLEU</b> avec code VOYAGE-JJMFKD par l\'utilisateur tranvoyage super admin', '2021-04-29 13:06:17', 'Adresse IP: 160.155.207.11 Port: 59310', '74'),
(469, 'Suppression du voyage  <b>ADJAMÃ‰ -TEAPLEU</b> par tranvoyage super admin', '2021-04-29 13:06:28', 'Adresse IP: 160.155.207.11 Port: 59804', '74'),
(470, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-29 04:20:56', 'Adresse IP: 160.155.207.11 Port: 57722', 'tranvoyage super admin'),
(471, 'Cr&eacute;ation du voyage <b>ADJAMÃ‰ -YAMOUSSOUKRO </b> avec code VOYAGE-GMZLHC par l\'utilisateur tranvoyage super admin', '2021-04-29 16:22:05', 'Adresse IP: 160.155.207.11 Port: 32848', '74'),
(472, 'Modification du voyage <b>ADJAMÃ‰ -YAMOUSSOUKRO </b> avec code VOYAGE-FPHWP7 par l\'utilisateur tranvoyage super admin', '2021-04-29 16:23:03', 'Adresse IP: 160.155.207.11 Port: 35500', '74'),
(473, 'Modification du voyage <b>ADJAMÃ‰ -MAN</b> avec code VOYAGE-R85TKB par l\'utilisateur tranvoyage super admin', '2021-04-29 16:23:51', 'Adresse IP: 160.155.207.11 Port: 38280', '74'),
(474, 'Modification du voyage <b>LAKOTA-TOUBA</b> avec code VOYAGE-YXEZK3 par l\'utilisateur tranvoyage super admin', '2021-04-29 16:25:00', 'Adresse IP: 160.155.207.11 Port: 41524', '74'),
(475, 'Ticket portant le code (U968RP0) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-04-29 18:09:56', 'Adresse IP: 154.0.26.68 Port: 58832', 'client'),
(476, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-29 06:56:29', 'Adresse IP: 160.155.112.163 Port: 51536', 'tranvoyage super admin'),
(477, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-30 10:11:46', 'Adresse IP: 160.154.157.33 Port: 44054', 'tranvoyage super admin'),
(478, 'Ticket portant le code (WVS9FN4) en cours de paiement avec le numÃ©ro 2250778059869 ', '2021-04-30 10:29:40', 'Adresse IP: 160.155.207.11 Port: 53212', 'client'),
(479, 'Ticket portant le code (RRRPOKY) en cours de paiement avec le numÃ©ro 234567890 ', '2021-04-30 10:30:42', 'Adresse IP: 160.155.207.11 Port: 57676', 'client'),
(480, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:30', 'Adresse IP: 160.155.207.11 Port: 38394', 'client'),
(481, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:32', 'Adresse IP: 160.155.207.11 Port: 38672', 'client'),
(482, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:32', 'Adresse IP: 160.155.207.11 Port: 38686', 'client'),
(483, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:34', 'Adresse IP: 160.155.207.11 Port: 38736', 'client'),
(484, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:34', 'Adresse IP: 160.155.207.11 Port: 38742', 'client'),
(485, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:34', 'Adresse IP: 160.155.207.11 Port: 38754', 'client'),
(486, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:38', 'Adresse IP: 160.155.207.11 Port: 38910', 'client'),
(487, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:39', 'Adresse IP: 160.155.207.11 Port: 38924', 'client'),
(488, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:39', 'Adresse IP: 160.155.207.11 Port: 38934', 'client'),
(489, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:39', 'Adresse IP: 160.155.207.11 Port: 38942', 'client'),
(490, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:39', 'Adresse IP: 160.155.207.11 Port: 38952', 'client'),
(491, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:45', 'Adresse IP: 160.155.207.11 Port: 39466', 'client'),
(492, 'Recherche non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:45', 'Adresse IP: 160.155.207.11 Port: 39508', 'client'),
(493, 'Confirmation non trouvÃ© du ticket ayant pour code (RWHEB9P) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-04-30 10:33:58', 'Adresse IP: 160.155.207.11 Port: 40738', 'client'),
(494, 'Ticket portant le code (ERSYEBC) en cours de paiement avec le numÃ©ro 234567899 ', '2021-04-30 10:34:21', 'Adresse IP: 160.155.207.11 Port: 42022', 'client'),
(495, 'Ticket portant le code (2ID4W3U) en cours de paiement avec le numÃ©ro 345677890 ', '2021-04-30 10:35:16', 'Adresse IP: 160.155.207.11 Port: 45282', 'client'),
(496, 'Recherche trouvÃ© du ticket ayant pour code (2ID4W3U) par le numÃ©ro de tÃ©tÃ©phone 345677890 ', '2021-04-30 10:37:04', 'Adresse IP: 160.155.207.11 Port: 53196', 'client'),
(497, 'Ticket portant le code (6TKVDFP) en cours de paiement avec le numÃ©ro 23456789 ', '2021-04-30 10:39:49', 'Adresse IP: 160.155.207.11 Port: 35904', 'client'),
(498, 'Ticket portant le code (OQ2OU8N) en cours de paiement avec le numÃ©ro  ', '2021-04-30 10:39:54', 'Adresse IP: 160.155.207.11 Port: 36188', 'client'),
(499, 'Ticket portant le code (P5DD1U0) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-30 10:42:54', 'Adresse IP: 160.155.207.11 Port: 48636', 'client'),
(500, 'Ticket portant le code (ZBQGKJX) en cours de paiement avec le numÃ©ro 2250778059869 ', '2021-04-30 10:44:32', 'Adresse IP: 160.155.207.11 Port: 54478', 'client'),
(501, 'Ticket portant le code (IWBFU9G) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-04-30 10:46:53', 'Adresse IP: 160.155.207.11 Port: 35068', 'client'),
(502, 'Recherche trouvÃ© du ticket ayant pour code (IWBFU9G) par le numÃ©ro de tÃ©tÃ©phone 2345678909 ', '2021-04-30 10:55:12', 'Adresse IP: 160.155.207.11 Port: 37372', 'client'),
(503, 'Ticket portant le code (JALSDYB) en cours de paiement avec le numÃ©ro 04397161 ', '2021-04-30 13:49:14', 'Adresse IP: 160.154.137.188 Port: 49586', 'client'),
(504, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-30 01:58:14', 'Adresse IP: 160.155.207.11 Port: 54784', 'tranvoyage super admin'),
(505, 'CrÃ©ation d\'une annonce par l\'utilisateur tranvoyage super admin', '2021-04-30 15:59:08', 'Adresse IP: 160.155.207.11 Port: 58338', '74'),
(506, 'Modification d\'une annonce par l\'utilisateur tranvoyage super admin', '2021-04-30 13:59:48', 'Adresse IP: 160.155.207.11 Port: 60672', '74'),
(507, 'Suppression d\'une annonce Ã©ffectuÃ©e par tranvoyage super admin', '2021-04-30 14:00:10', 'Adresse IP: 160.155.207.11 Port: 33706', '74'),
(508, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-30 08:48:08', 'Adresse IP: 160.155.112.163 Port: 53772', 'tranvoyage super admin'),
(509, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-04-30 09:54:28', 'Adresse IP: 160.155.112.163 Port: 47470', 'tranvoyage super admin'),
(510, 'Ticket portant le code (6Z4C9FR) en cours de paiement avec le numÃ©ro 04397161 ', '2021-04-30 22:08:10', 'Adresse IP: 160.155.112.163 Port: 39942', '74'),
(511, 'Confirmation trouvÃ© du ticket ayant pour code (6Z4C9FR) par le numÃ©ro de tÃ©tÃ©phone 04397161 ', '2021-04-30 22:10:26', 'Adresse IP: 160.155.112.163 Port: 47690', '74'),
(512, 'Ticket portant le code (OO21W99) en cours de paiement avec le numÃ©ro 2250778059869 ', '2021-05-01 10:14:41', 'Adresse IP: 154.0.26.43 Port: 50678', 'client'),
(513, 'Ticket portant le code (6BK8H4P) en cours de paiement avec le numÃ©ro 04397161 ', '2021-05-01 10:45:41', 'Adresse IP: 160.155.65.89 Port: 57192', 'client'),
(514, 'Ticket portant le code (VHJGNNL) en cours de paiement avec le numÃ©ro 0504397161 ', '2021-05-01 10:49:41', 'Adresse IP: 160.154.156.22 Port: 44692', 'client'),
(515, 'Ticket portant le code (DSACZZ7) en cours de paiement avec le numÃ©ro 0173838605 ', '2021-05-01 10:51:16', 'Adresse IP: 160.154.156.22 Port: 50798', 'client'),
(516, 'Ticket portant le code (U8ONX64) en cours de paiement avec le numÃ©ro 22500504397161 ', '2021-05-01 10:59:41', 'Adresse IP: 160.154.156.22 Port: 54140', 'client'),
(517, 'Ticket portant le code (90GUV7W) en cours de paiement avec le numÃ©ro 2250779481690 ', '2021-05-01 11:03:17', 'Adresse IP: 160.154.156.22 Port: 39360', 'client'),
(518, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-02 01:49:44', 'Adresse IP: 154.0.26.246 Port: 38526', 'tranvoyage super admin'),
(519, 'Modification d\'une annonce par l\'utilisateur tranvoyage super admin', '2021-05-02 13:51:49', 'Adresse IP: 154.0.26.246 Port: 46952', '74'),
(520, 'Modification d\'une annonce par l\'utilisateur tranvoyage super admin', '2021-05-02 13:51:56', 'Adresse IP: 154.0.26.246 Port: 47146', '74'),
(521, 'Ticket portant le code (W0QVYZU) en cours de paiement avec le numÃ©ro 2250504397161 ', '2021-05-02 13:54:43', 'Adresse IP: 154.0.26.246 Port: 56640', '74'),
(522, 'Recherche non trouvÃ© du ticket ayant pour code (Semelle) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:03', 'Adresse IP: 154.0.26.131 Port: 54176', 'client'),
(523, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:16', 'Adresse IP: 154.0.26.131 Port: 55592', 'client'),
(524, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:21', 'Adresse IP: 154.0.26.131 Port: 55980', 'client'),
(525, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:23', 'Adresse IP: 154.0.26.131 Port: 56128', 'client'),
(526, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:26', 'Adresse IP: 154.0.26.131 Port: 56282', 'client'),
(527, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:34', 'Adresse IP: 154.0.26.131 Port: 56562', 'client'),
(528, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:38', 'Adresse IP: 154.0.26.131 Port: 56804', 'client'),
(529, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:38', 'Adresse IP: 154.0.26.131 Port: 56810', 'client'),
(530, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:50', 'Adresse IP: 154.0.26.131 Port: 57596', 'client'),
(531, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:40:51', 'Adresse IP: 154.0.26.131 Port: 57700', 'client'),
(532, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:41:02', 'Adresse IP: 154.0.26.131 Port: 58276', 'client'),
(533, 'Recherche non trouvÃ© du ticket ayant pour code (Qlkjho5) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-02 15:41:07', 'Adresse IP: 154.0.26.131 Port: 58510', 'client'),
(534, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-03 07:43:52', 'Adresse IP: 160.155.65.89 Port: 56446', 'tranvoyage super admin'),
(535, 'Modification du voyage <b>ADJAMÃ‰ -MAN</b> avec code VOYAGE-GZUKSF par l\'utilisateur tranvoyage super admin', '2021-05-03 07:48:11', 'Adresse IP: 160.155.65.89 Port: 45226', '74'),
(536, 'Modification du voyage <b>ABOBO -DALOA</b> avec code VOYAGE-CUVPZO par l\'utilisateur tranvoyage super admin', '2021-05-03 07:48:47', 'Adresse IP: 160.155.65.89 Port: 47878', '74'),
(537, 'Modification du voyage <b>ABOBO -DALOA</b> avec code VOYAGE-GNVDTP par l\'utilisateur tranvoyage super admin', '2021-05-03 07:49:22', 'Adresse IP: 160.155.65.89 Port: 50910', '74'),
(538, 'Modification du voyage <b>ABOBO -BOUAFLÃ‰</b> avec code VOYAGE-8QMEC7 par l\'utilisateur tranvoyage super admin', '2021-05-03 07:50:08', 'Adresse IP: 160.155.65.89 Port: 55356', '74'),
(539, 'Modification du voyage <b>ABOBO -BANGOLO</b> avec code VOYAGE-3EWRZ5 par l\'utilisateur tranvoyage super admin', '2021-05-03 07:50:55', 'Adresse IP: 160.155.65.89 Port: 57810', '74'),
(540, 'Modification du voyage <b>ABOBO -BEOUMI</b> avec code VOYAGE-XKD5Q9 par l\'utilisateur tranvoyage super admin', '2021-05-03 07:52:00', 'Adresse IP: 160.155.65.89 Port: 34800', '74'),
(541, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-03 09:00:31', 'Adresse IP: 160.155.207.11 Port: 39702', 'tranvoyage super admin'),
(542, 'Cr&eacute;ation du voyage <b>ADJAMÃ‰ -ABOBO </b> avec code VOYAGE-RAJ6EP par l\'utilisateur tranvoyage super admin', '2021-05-03 09:09:08', 'Adresse IP: 160.155.207.11 Port: 39250', '74'),
(543, 'dÃ©verrouillage du voyage <b>ADJAMÃ‰ -ABOBO </b> par tranvoyage super admin', '2021-05-03 09:09:38', 'Adresse IP: 160.155.207.11 Port: 40798', '74'),
(544, 'verrouillage du voyage <b>ADJAMÃ‰ -ABOBO </b> par tranvoyage super admin', '2021-05-03 09:13:51', 'Adresse IP: 160.155.207.11 Port: 56300', '74'),
(545, 'Ticket portant le code (25UM01K) en cours de paiement avec le numÃ©ro 2250504397161 ', '2021-05-03 15:42:19', 'Adresse IP: 160.155.65.89 Port: 42908', 'client'),
(546, 'Confirmation trouvÃ© du ticket ayant pour code (25UM01K) par le numÃ©ro de tÃ©tÃ©phone 2250504397161 ', '2021-05-03 15:46:14', 'Adresse IP: 160.155.65.89 Port: 59952', 'client'),
(547, 'Ticket portant le code (PVGQSLQ) en cours de paiement avec le numÃ©ro 0555786878 ', '2021-05-04 16:31:10', 'Adresse IP: 196.47.133.17 Port: 40906', 'client'),
(548, 'Ticket portant le code (I7I5XW5) en cours de paiement avec le numÃ©ro 2250709487352 ', '2021-05-08 08:41:37', 'Adresse IP: 154.0.26.209 Port: 55516', 'client'),
(549, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-08 08:57:38', 'Adresse IP: 160.154.157.203 Port: 44386', 'tranvoyage super admin'),
(550, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-08 08:58:09', 'Adresse IP: 160.154.157.203 Port: 45632', 'tranvoyage super admin'),
(551, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-08 08:58:10', 'Adresse IP: 160.154.157.203 Port: 45640', 'tranvoyage super admin'),
(552, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-10 06:11:11', 'Adresse IP: 160.154.138.15 Port: 33140', 'tranvoyage super admin'),
(553, 'Ticket portant le code (AI8045A) en cours de paiement avec le numÃ©ro 47886905 ', '2021-05-15 12:21:36', 'Adresse IP: 41.215.193.114 Port: 47372', 'client'),
(554, 'Confirmation non trouvÃ© du ticket ayant pour code () par le numÃ©ro de tÃ©tÃ©phone  ', '2021-05-15 12:24:48', 'Adresse IP: 41.215.193.114 Port: 60170', 'client'),
(555, 'Ticket portant le code (WSUR0FN) en cours de paiement avec le numÃ©ro 2250747886905 ', '2021-05-15 12:25:01', 'Adresse IP: 41.215.193.114 Port: 32840', 'client'),
(556, 'Ticket portant le code (MQRID95) en cours de paiement avec le numÃ©ro 002250747652650 ', '2021-05-16 14:21:57', 'Adresse IP: 196.47.133.48 Port: 39784', 'client'),
(557, 'Ticket portant le code (JZOGU2M) en cours de paiement avec le numÃ©ro 002250748292602 ', '2021-05-16 14:29:00', 'Adresse IP: 196.47.133.48 Port: 39662', 'client'),
(558, 'Ticket portant le code (ZL9PFT2) en cours de paiement avec le numÃ©ro 2250778059869 ', '2021-05-19 07:48:15', 'Adresse IP: 160.155.207.11 Port: 33442', 'client'),
(559, 'Ticket portant le code (3RLWBWK) en cours de paiement avec le numÃ©ro 002250779339973 ', '2021-05-19 08:39:04', 'Adresse IP: 160.154.156.215 Port: 44756', 'client'),
(560, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-19 08:57:40', 'Adresse IP: 160.154.156.215 Port: 33708', 'tranvoyage super admin'),
(561, 'Recherche trouvÃ© du ticket ayant pour code (3RLWBWK) par le numÃ©ro de tÃ©tÃ©phone 002250779339973 ', '2021-05-19 09:00:44', 'Adresse IP: 160.154.156.215 Port: 44998', '74'),
(562, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-20 03:07:04', 'Adresse IP: 160.155.65.89 Port: 32836', 'tranvoyage super admin'),
(563, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-21 08:36:28', 'Adresse IP: 160.155.207.11 Port: 48378', 'tranvoyage super admin'),
(564, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-21 08:43:24', 'Adresse IP: 160.155.207.11 Port: 55420', 'tranvoyage super admin'),
(565, 'Modification du voyage <b>ABOBO -DALOA</b> avec code VOYAGE-9RXLVN par l\'utilisateur tranvoyage super admin', '2021-05-21 08:46:32', 'Adresse IP: 160.155.207.11 Port: 44994', '74'),
(566, 'Modification du voyage <b>ABOBO -BOUAFLÃ‰</b> avec code VOYAGE-KDEGCJ par l\'utilisateur tranvoyage super admin', '2021-05-21 08:46:50', 'Adresse IP: 160.155.207.11 Port: 46496', '74'),
(567, 'Modification du voyage <b>ABOBO -BANGOLO</b> avec code VOYAGE-HEXWQ8 par l\'utilisateur tranvoyage super admin', '2021-05-21 08:47:05', 'Adresse IP: 160.155.207.11 Port: 47476', '74'),
(568, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-21 10:55:51', 'Adresse IP: 160.155.65.89 Port: 55608', 'tranvoyage super admin'),
(569, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-05-26 09:07:08', 'Adresse IP: 160.154.48.149 Port: 42086', 'tranvoyage super admin'),
(570, 'Ticket portant le code (08TDWCN) en cours de paiement avec le numÃ©ro 0747886905 ', '2021-06-02 07:50:12', 'Adresse IP: 160.154.48.149 Port: 58786', 'client'),
(571, 'Ticket portant le code (QZQWM2F) en cours de paiement avec le numÃ©ro 22579481690 ', '2021-06-02 08:31:46', 'Adresse IP: 160.154.156.147 Port: 50602', 'client'),
(572, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-02 08:36:51', 'Adresse IP: 160.154.156.147 Port: 40704', 'tranvoyage super admin'),
(573, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-02 08:36:52', 'Adresse IP: 160.154.156.147 Port: 40786', 'tranvoyage super admin'),
(574, 'Ticket portant le code (WPLAXES) en cours de paiement avec le numÃ©ro 22504397161 ', '2021-06-02 09:22:11', 'Adresse IP: 160.154.156.147 Port: 60174', '74'),
(575, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-02 09:24:45', 'Adresse IP: 160.154.156.147 Port: 41712', 'tranvoyage super admin'),
(576, 'Ticket portant le code (HBEGVLS) en cours de paiement avec le numÃ©ro 234567898 ', '2021-06-03 15:11:47', 'Adresse IP: 160.155.207.11 Port: 54266', 'client'),
(577, 'Ticket portant le code (EYDGBQJ) en cours de paiement avec le numÃ©ro 74852514 ', '2021-06-03 17:12:00', 'Adresse IP: 41.66.28.99 Port: 39174', 'client'),
(578, 'Ticket portant le code (ARDLGSG) en cours de paiement avec le numÃ©ro 4525555 ', '2021-06-03 17:18:21', 'Adresse IP: 41.66.28.99 Port: 33624', 'client'),
(579, 'Confirmation non trouvÃ© du ticket ayant pour code (W63JGHM) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-06-03 17:19:42', 'Adresse IP: 41.66.28.99 Port: 38062', 'client'),
(580, 'Confirmation non trouvÃ© du ticket ayant pour code (W63JGHM) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-06-03 17:20:14', 'Adresse IP: 41.66.28.99 Port: 39842', 'client'),
(581, 'Confirmation trouvÃ© du ticket ayant pour code (ARDLGSG) par le numÃ©ro de tÃ©tÃ©phone 4525555 ', '2021-06-03 17:24:25', 'Adresse IP: 41.66.28.99 Port: 52024', 'client'),
(582, 'Ticket portant le code (N5MHPEJ) en cours de paiement avec le numÃ©ro 2250778059869 ', '2021-06-04 08:15:19', 'Adresse IP: 160.155.207.11 Port: 42676', 'client'),
(583, 'Ticket portant le code (F7HG16R) en cours de paiement avec le numÃ©ro 0778059869 ', '2021-06-04 08:31:56', 'Adresse IP: 160.155.207.11 Port: 58852', 'client'),
(584, 'Confirmation non trouvÃ© du ticket ayant pour code (P0YPOUP) par le numÃ©ro de tÃ©tÃ©phone  ', '2021-06-04 08:32:12', 'Adresse IP: 160.155.207.11 Port: 59942', 'client'),
(585, 'Confirmation trouvÃ© du ticket ayant pour code (F7HG16R) par le numÃ©ro de tÃ©tÃ©phone 0778059869 ', '2021-06-04 08:33:19', 'Adresse IP: 160.155.207.11 Port: 36276', 'client'),
(586, 'Ticket portant le code (MXW7VUE) en cours de paiement avec le numÃ©ro 0504397161 ', '2021-06-10 08:25:36', 'Adresse IP: 160.154.137.174 Port: 33468', 'client'),
(587, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-10 08:30:41', 'Adresse IP: 160.154.137.174 Port: 52112', 'tranvoyage super admin'),
(588, 'Ticket portant le code (UBBCJQJ) en cours de paiement avec le numÃ©ro 2345678909 ', '2021-06-13 08:40:06', 'Adresse IP: 105.235.71.157 Port: 48886', 'client'),
(589, 'Connexion de <b>osse Romeo ()</b>', '2021-06-13 08:42:16', 'Adresse IP: 105.235.71.157 Port: 57342', 'osse Romeo'),
(590, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-13 08:56:28', 'Adresse IP: 160.154.136.228 Port: 55758', 'tranvoyage super admin'),
(591, 'Connexion de <b>osse Romeo ()</b>', '2021-06-13 08:59:44', 'Adresse IP: 105.235.71.157 Port: 38618', 'osse Romeo'),
(592, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-14 00:48:46', 'Adresse IP: 160.154.156.245 Port: 42946', 'tranvoyage super admin'),
(593, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-14 05:20:07', 'Adresse IP: 160.154.136.21 Port: 52792', 'tranvoyage super admin'),
(594, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-14 06:03:58', 'Adresse IP: 160.155.112.163 Port: 41832', 'tranvoyage super admin'),
(595, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-14 06:20:09', 'Adresse IP: 160.155.112.163 Port: 39692', 'tranvoyage super admin'),
(596, 'Ticket portant le code (O993URR) en cours de paiement avec le numÃ©ro 22504397161 ', '2021-06-15 08:07:54', 'Adresse IP: 160.154.158.65 Port: 43864', 'client'),
(597, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-15 08:12:01', 'Adresse IP: 160.154.158.65 Port: 34590', 'tranvoyage super admin'),
(598, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-15 08:12:01', 'Adresse IP: 160.154.158.65 Port: 34594', 'tranvoyage super admin'),
(599, 'Ticket portant le code (XJ9CBBX) en cours de paiement avec le numÃ©ro 2250504397161 ', '2021-06-15 10:35:19', 'Adresse IP: 160.154.158.65 Port: 49922', 'client'),
(600, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-15 09:45:53', 'Adresse IP: 160.155.112.163 Port: 43510', 'tranvoyage super admin'),
(601, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-16 09:57:09', 'Adresse IP: 160.154.158.107 Port: 40258', 'tranvoyage super admin'),
(602, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-16 10:13:11', 'Adresse IP: 194.127.28.23 Port: 39516', 'tranvoyage super admin'),
(603, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-16 03:41:23', 'Adresse IP: 160.154.155.226 Port: 42402', 'tranvoyage super admin'),
(604, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-16 03:44:11', 'Adresse IP: 160.154.155.226 Port: 52406', 'tranvoyage super admin'),
(605, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-16 03:48:25', 'Adresse IP: 160.154.155.226 Port: 37220', 'tranvoyage super admin'),
(606, 'Ticket portant le code (ZZOGU3K) en cours de paiement avec le numÃ©ro 0747886905 ', '2021-06-19 06:33:12', 'Adresse IP: 196.47.133.4 Port: 54758', 'client'),
(607, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-26 01:29:45', 'Adresse IP: 160.155.65.89 Port: 42530', 'tranvoyage super admin'),
(608, 'Ticket portant le code (EPSES8U) en cours de paiement avec le numÃ©ro 0504397161 ', '2021-06-27 14:00:14', 'Adresse IP: 160.154.156.59 Port: 43478', 'client'),
(609, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-27 02:02:16', 'Adresse IP: 160.154.156.59 Port: 49054', 'tranvoyage super admin');
INSERT INTO `trace` (`id_trace`, `lib_trace`, `date_trace`, `adresse_ip`, `secur`) VALUES
(610, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-06-27 03:36:05', 'Adresse IP: 160.154.156.59 Port: 57686', 'tranvoyage super admin'),
(611, 'Ticket  en cours de paiement avec le numÃ©ro 47886905 ', '2021-07-01 22:16:48', 'Adresse IP: 160.154.157.124 Port: 32822', 'client'),
(612, 'Ticket  en cours de paiement avec le numÃ©ro 47886905 ', '2021-07-01 22:30:00', 'Adresse IP: 160.154.157.124 Port: 35556', 'client'),
(613, 'Ticket  en cours de paiement avec le numÃ©ro 47886905 ', '2021-07-01 22:42:54', 'Adresse IP: 160.154.157.124 Port: 39104', 'client'),
(614, 'Ticket  en cours de paiement avec le numÃ©ro 47886905 ', '2021-07-01 23:34:45', 'Adresse IP: 160.154.157.124 Port: 46026', 'client'),
(615, 'Ticket  en cours de paiement avec le numÃ©ro 0504397161 ', '2021-07-02 10:25:55', 'Adresse IP: 160.154.157.192 Port: 56930', 'client'),
(616, 'Ticket  en cours de paiement avec le numÃ©ro 0504397161 ', '2021-07-07 13:41:19', 'Adresse IP: 160.155.65.89 Port: 48536', 'client'),
(617, 'Ticket  en cours de paiement avec le numÃ©ro 0504397161 ', '2021-07-07 13:49:00', 'Adresse IP: 160.155.65.89 Port: 44794', 'client'),
(618, 'Ticket  en cours de paiement avec le numÃ©ro 0405397161 ', '2021-07-07 20:58:13', 'Adresse IP: 160.154.155.188 Port: 49868', 'client'),
(619, 'Ticket  en cours de paiement avec le numÃ©ro 0504397161 ', '2021-07-07 22:18:45', 'Adresse IP: 160.154.156.103 Port: 44064', 'client'),
(620, 'Ticket  en cours de paiement avec le numÃ©ro 0788833060 ', '2021-07-08 06:26:00', 'Adresse IP: 160.154.150.183 Port: 50690', 'client'),
(621, 'Ticket  en cours de paiement avec le numÃ©ro 0759362429 ', '2021-07-08 12:43:27', 'Adresse IP: 41.66.28.168 Port: 55522', 'client'),
(622, 'Ticket  en cours de paiement avec le numÃ©ro 0747531644 ', '2021-07-08 13:18:00', 'Adresse IP: 196.183.210.38 Port: 33688', 'client'),
(623, 'Ticket  en cours de paiement avec le numÃ©ro 00000000000 ', '2021-07-08 14:31:33', 'Adresse IP: 176.180.95.242 Port: 37492', 'client'),
(624, 'Ticket  en cours de paiement avec le numÃ©ro 00000000000 ', '2021-07-08 14:32:04', 'Adresse IP: 176.180.95.242 Port: 39768', 'client'),
(625, 'Ticket  en cours de paiement avec le numÃ©ro 000000000 ', '2021-07-08 15:09:09', 'Adresse IP: 176.180.94.238 Port: 36526', 'client'),
(626, 'Ticket  en cours de paiement avec le numÃ©ro 00000 ', '2021-07-08 15:10:40', 'Adresse IP: 176.180.94.238 Port: 42356', 'client'),
(627, 'Ticket  en cours de paiement avec le numÃ©ro 0504397161 ', '2021-07-10 13:39:47', 'Adresse IP: 160.155.19.230 Port: 42624', 'client'),
(628, 'Ticket  en cours de paiement avec le numÃ©ro 78059869 ', '2021-07-15 13:19:32', 'Adresse IP: 160.155.207.11 Port: 51688', 'client'),
(629, 'Ticket  en cours de paiement avec le numÃ©ro 225 0758776360 ', '2021-07-16 10:14:11', 'Adresse IP: 160.120.13.241 Port: 60658', 'client'),
(630, 'Ticket  en cours de paiement avec le numÃ©ro +1 (251) 598-9581 ', '2021-07-16 13:41:50', 'Adresse IP: 194.127.26.76 Port: 42058', 'client'),
(631, 'Ticket  en cours de paiement avec le numÃ©ro 2222 ', '2021-07-19 10:52:26', 'Adresse IP: 160.154.148.145 Port: 44574', 'client'),
(632, 'Ticket  en cours de paiement avec le numÃ©ro 0504397161 ', '2021-07-19 12:12:47', 'Adresse IP: 160.155.240.13 Port: 54902', 'client'),
(633, 'Ticket  en cours de paiement avec le numÃ©ro 0504397161 ', '2021-08-13 14:56:32', 'Adresse IP: 160.155.65.89 Port: 49456', 'client'),
(634, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-08-13 03:00:46', 'Adresse IP: 160.155.65.89 Port: 37370', 'tranvoyage super admin'),
(635, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-08-13 03:00:46', 'Adresse IP: 160.155.65.89 Port: 37364', 'tranvoyage super admin'),
(636, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-08-13 03:00:46', 'Adresse IP: 160.155.65.89 Port: 37366', 'tranvoyage super admin'),
(637, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-08-13 03:00:46', 'Adresse IP: 160.155.65.89 Port: 37368', 'tranvoyage super admin'),
(638, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-08-13 03:02:04', 'Adresse IP: 160.155.65.89 Port: 42096', 'tranvoyage super admin'),
(639, 'Connexion de <b>tranvoyage super admin ()</b>', '2021-08-18 09:00:07', 'Adresse IP: 139.28.219.67 Port: 47496', 'tranvoyage super admin'),
(640, 'Ticket  en cours de paiement avec le numÃ©ro 0747886905 ', '2021-08-23 14:59:53', 'Adresse IP: 160.154.48.149 Port: 42008', 'client'),
(641, 'Ticket  en cours de paiement avec le numÃ©ro 074788690 ', '2021-08-23 15:01:13', 'Adresse IP: 160.154.48.149 Port: 48948', 'client'),
(642, 'Ticket  en cours de paiement avec le numÃ©ro 0744886005 ', '2021-08-23 15:30:39', 'Adresse IP: 160.154.136.26 Port: 42700', 'client'),
(643, 'Ticket  en cours de paiement avec le numÃ©ro +1 (475) 558-4653 ', '2021-08-26 19:55:10', 'Adresse IP: 154.0.26.80 Port: 45662', 'client'),
(644, 'Connexion de <b>osse Romeo ()</b>', '2022-02-22 07:53:30', 'Adresse IP: 160.154.31.241 Port: 58592', 'osse Romeo'),
(645, 'Cet utilisateur osse Romeo vient de changer son mot de passe  , adresse:Adresse IP: 160.154.31.241 Port: 33886', '0000-00-00 00:00:00', 'Adresse IP: 160.154.31.241 Port: 33886', '12'),
(646, 'Connexion de <b>osse Romeo ()</b>', '2022-02-22 07:54:59', 'Adresse IP: 160.154.31.241 Port: 35606', 'osse Romeo'),
(647, 'Cet utilisateur osse Romeo vient de changer son mot de passe  , adresse:Adresse IP: 160.154.31.241 Port: 39156', '0000-00-00 00:00:00', 'Adresse IP: 160.154.31.241 Port: 39156', '12'),
(648, 'Connexion de <b>osse Romeo ()</b>', '2022-02-22 08:03:12', 'Adresse IP: 160.154.31.241 Port: 40292', 'osse Romeo'),
(649, 'Connexion de <b>tranvoyage super admin ()</b>', '2022-03-06 05:38:38', 'Adresse IP: 154.0.26.237 Port: 60622', 'tranvoyage super admin'),
(650, 'Connexion de <b>tranvoyage super admin ()</b>', '2022-03-06 05:42:45', 'Adresse IP: 154.0.26.237 Port: 50092', 'tranvoyage super admin'),
(651, 'Connexion de <b>tranvoyage super admin ()</b>', '2022-03-07 06:52:20', 'Adresse IP: 160.154.48.149 Port: 51780', 'tranvoyage super admin'),
(652, 'Connexion de <b>tranvoyage super admin ()</b>', '2022-03-07 07:30:51', 'Adresse IP: 160.155.221.214 Port: 56026', 'tranvoyage super admin'),
(653, 'Connexion de <b>tranvoyage super admin ()</b>', '2022-03-07 07:43:55', 'Adresse IP: 160.154.31.241 Port: 56252', 'tranvoyage super admin'),
(654, 'ValidÃ© du convois <b>Convois 6 </b> par tranvoyage super admin', '2022-03-07 09:06:39', 'Adresse IP: 160.154.31.241 Port: 43322', '74'),
(655, 'ValidÃ© du convois <b>Convoi Danané </b> par tranvoyage super admin', '2022-03-07 09:08:52', 'Adresse IP: 160.154.31.241 Port: 55370', '74'),
(656, 'ValidÃ© du convois <b>Divertissement sur bassam </b> par tranvoyage super admin', '2022-03-07 09:09:04', 'Adresse IP: 160.154.31.241 Port: 56246', '74'),
(657, 'ValidÃ© du convois <b>Quia consequatur dol </b> par tranvoyage super admin', '2022-03-07 09:09:40', 'Adresse IP: 160.154.31.241 Port: 58890', '74'),
(658, 'Valider le convois du convois <b>dvqsvdgsq </b> par tranvoyage super admin', '2022-03-07 09:21:08', 'Adresse IP: 160.154.31.241 Port: 40192', '74'),
(659, 'Valider le convois du convois <b>Nam aliquip quis alias sint alias corporis laudantium eiusmod tenetur est vitae suscipit sunt </b> par tranvoyage super admin', '2022-03-07 09:26:35', 'Adresse IP: 160.154.31.241 Port: 40922', '74'),
(660, 'Valider le convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-07 09:27:20', 'Adresse IP: 160.154.31.241 Port: 44934', '74'),
(661, 'Valider le convois du convois <b>Divertissement à assini </b> par tranvoyage super admin', '2022-03-07 09:55:07', 'Adresse IP: 160.154.31.241 Port: 53614', '74'),
(662, 'RefusÃ© la validation du convois <b>Divertissement à assini </b> par tranvoyage super admin', '2022-03-07 09:55:22', 'Adresse IP: 160.154.31.241 Port: 55034', '74'),
(663, 'Suppression du convois  <b>Nam aliquip quis alias sint alias corporis laudantium eiusmod tenetur est vitae suscipit sunt </b> par tranvoyage super admin', '2022-03-07 10:14:37', 'Adresse IP: 160.154.31.241 Port: 50706', '74'),
(664, 'Connexion de <b>tranvoyage super admin ()</b>', '2022-03-07 00:36:05', 'Adresse IP: 160.154.48.149 Port: 50534', 'tranvoyage super admin'),
(665, 'RefusÃ© la validation du convois <b>SBP </b> par tranvoyage super admin', '2022-03-07 13:35:44', 'Adresse IP: 160.154.31.241 Port: 53514', '74'),
(666, 'Valider le convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-07 13:36:45', 'Adresse IP: 160.154.31.241 Port: 57298', '74'),
(667, 'Valider le convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-07 14:43:11', 'Adresse IP: 160.154.31.241 Port: 41834', '74'),
(668, 'RefusÃ© la validation du convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-07 14:55:02', 'Adresse IP: 160.154.31.241 Port: 53534', '74'),
(669, 'ValidÃ© le convois du convois <b>Earum sunt qui dicta cumque recusandae Est voluptas qui et eiusmod vel cum ipsum maiores culpa maxime cupidatat quia et </b> par tranvoyage super admin', '2022-03-07 15:10:21', 'Adresse IP: 160.154.31.241 Port: 51148', '74'),
(670, 'ValidÃ© le convois du convois <b>Divertissement à assini </b> par tranvoyage super admin', '2022-03-07 15:15:37', 'Adresse IP: 160.154.31.241 Port: 46906', '74'),
(671, 'Connexion de <b>tranvoyage super admin ()</b>', '2022-03-09 08:28:57', 'Adresse IP: 160.154.48.149 Port: 44738', 'tranvoyage super admin'),
(672, 'ValidÃ© le convois du convois <b>Laborum Et veritatis praesentium illum non odit ratione maxime aut Nam quaerat vitae voluptatem Assumenda nesciunt reprehenderit amet </b> par tranvoyage super admin', '2022-03-09 08:29:48', 'Adresse IP: 160.154.48.149 Port: 48940', '74'),
(673, 'ValidÃ© le convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-09 08:30:24', 'Adresse IP: 160.154.48.149 Port: 52076', '74'),
(674, 'RefusÃ© la validation du convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-09 08:30:34', 'Adresse IP: 160.154.48.149 Port: 53170', '74'),
(675, 'ValidÃ© le convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-09 08:31:19', 'Adresse IP: 160.154.48.149 Port: 56438', '74'),
(676, 'RefusÃ© la validation du convois du convois <b>SBP </b> par tranvoyage super admin', '2022-03-09 08:31:59', 'Adresse IP: 160.154.48.149 Port: 59748', '74'),
(677, 'ValidÃ© le convois du convois <b>Convois 11 </b> par tranvoyage super admin', '2022-03-09 08:32:21', 'Adresse IP: 160.154.48.149 Port: 33398', '74');

-- --------------------------------------------------------

--
-- Structure de la table `type_utilisateurs`
--

CREATE TABLE `type_utilisateurs` (
  `id_type_utilisateurs` int(12) NOT NULL,
  `libelle_type_utilisateurs` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `t_admins`
--

CREATE TABLE `t_admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_admins_password_resets`
--

CREATE TABLE `t_admins_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_annonces`
--

CREATE TABLE `t_annonces` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_compagnies`
--

CREATE TABLE `t_compagnies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_compagnies`
--

INSERT INTO `t_compagnies` (`id`, `nom`, `email`, `telephone`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'CTE TRANSPORT ', 'ctecompagniedetransportexpress@gmail.com', '0505489532', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(2, 'SABE TRANSPORT ', 'tran.reservation@gmail.com', '22504375', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(3, 'SITTI TRANSPORT ', 'sitti@gmail.com', '0777251106', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(4, 'GAMON TRANSPORT ', 'gamon@gmail.com', '0103339813', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(5, 'TTF TRANSPORT ', 'ttf@gmail.com', '0709044555', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(6, 'COMPAGNIE DE TRANSPORT TCHOLOGO OFFICIEL ', 'ctto@gmail.com', '0575112078/0747707778', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(7, 'GTT TRANSPORT ', 'gtt@gmail.com', '0140796023', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(8, 'STBA TRANSPORT ', 'stba@gmail.com', '0707673454', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(9, 'STC TRANSPORT ', 'stc@gmail.com', '0707853060', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(10, 'UTS TRANSPORT ', 'uts@gmail.com', '0777601570', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(11, 'TRANSPORT AVS', 'transportavs@gmail.com', '0102956063', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(12, 'GDF TRANSPORT ', 'gdftransport@gmail.com', '0779801963', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(13, 'TCF EXPRESS MISTRAL ', 'tcf@gmail.com', '0140511652', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(14, 'GDF TRANSPORT *', 'gdftransport@cmail.com', '20372037/ 0758609881', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(15, 'GANA TRANSPORT CI', 'ganatrans@yahoo.fr', '0777872347', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(16, 'ETS SAMA TRANSPORT ', 'samatransport@gmail.com', '0708028942/0574585827', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(17, 'TILEMSI TRANSPORT ABIDJAN ', 'tilemstransport@gmail.com', '+22344904212', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(18, 'SONEF TRANSPORT VOYAGEURS ', 'sonef_niger@yahoo.fr', '+22720734358/+22790901239/22720752174', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(19, 'CHONCO TRANSPORT ', 'dsilue7@gmail.com', '0586909092', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(20, 'UTB', 'contacts@utb.ci', '0564616360', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(21, 'OUEST TRANSACTIONS CI OT TRANSPORT ', 'ouesttransactionci@gmail.com', '20375186', 0, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21');

-- --------------------------------------------------------

--
-- Structure de la table `t_convois`
--

CREATE TABLE `t_convois` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `params_status_convois_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `code` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `libelle` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lieu_depart` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lieu_arrivee` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lieu_rassemblement` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `communaute` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_depart` datetime DEFAULT NULL,
  `date_arrivee` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prix` int(11) NOT NULL,
  `nbre_ticket` int(11) DEFAULT NULL,
  `ticket_disponible` int(11) DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `image_file` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_convois`
--

INSERT INTO `t_convois` (`id`, `params_status_convois_id`, `user_id`, `admin_id`, `code`, `libelle`, `lieu_depart`, `lieu_arrivee`, `lieu_rassemblement`, `communaute`, `date_depart`, `date_arrivee`, `description`, `prix`, `nbre_ticket`, `ticket_disponible`, `actif`, `deleted_at`, `created_at`, `image_file`, `updated_at`) VALUES
(1, 2, 1, NULL, 'LQ20BRBR', 'Convois 1', NULL, 'Autem in voluptate c', 'Nam recusandae Culp', NULL, '2003-07-08 00:00:00', '1976-01-12 00:00:00', 'Excelence voyage oragnise son traditionnel divertissement le Jeudi 10/08/2021. A cet effet , un convois sur bassam est organisé dans le but de prendre un peu d\'air. Le prix de convois est fixé à 2000 FCFA. Le convois aurra lieux le\n Jeudi 10/08/2021 et le départ se ferra à 08h20 et le retour à 16H00. Le lieu de rassemblement est à la rond point de la gare d\'Abobo à 07H30. Pour plus d\'informations , veuillez contactez M. Kouamé au 07-47-88-69-05 pour plus d\'informations. Les conditions de ce convois sont :\n - Il est bon de noté que passé l\'heure de 08h20 , quiquonque viendra ne se fera pas rembourssé\n - Il ticket payé n\'est pas rembourssable \n - Pour ceux qui ont des problèmes de santé , il est préférable de venir avec ces médicamments', 100, 97, NULL, 0, NULL, '2021-07-20 00:13:22', NULL, '2021-07-20 00:13:22'),
(4, 2, 1, NULL, 'V5ZFLYIY', 'Convois 2', NULL, 'Eos nisi dignissimos', 'Magna pariatur Mole', NULL, '2020-08-05 00:00:00', '2011-07-27 00:00:00', 'Maiores perspiciatis', 11, 74, NULL, 0, NULL, '2021-07-20 00:36:56', NULL, '2021-07-20 00:36:56'),
(5, 2, 1, NULL, 'AYI88PWQ', 'Convois 3', NULL, 'Enim nesciunt ipsam', 'Quia natus natus par', NULL, '1992-07-02 00:00:00', '1983-06-09 00:00:00', 'Deserunt ab esse ven', 40, 98, NULL, 0, NULL, '2021-07-20 00:37:04', NULL, '2021-07-20 00:37:04'),
(6, 2, 1, NULL, 'B1Y8HX5Y', 'Convois 4', NULL, 'Ad ea qui fuga Repe', 'Vel eum magni irure', NULL, '1979-06-29 00:00:00', '2012-05-28 00:00:00', 'Aperiam sed recusand', 48, 56, NULL, 0, NULL, '2021-07-20 00:37:14', NULL, '2021-07-20 00:37:14'),
(7, 2, 1, NULL, 'PKJZRT6C', 'Convois 5', NULL, 'Aute cum sint et max', 'Quam in ex iste et N', NULL, '2015-07-21 00:00:00', '1978-08-23 00:00:00', 'Quis nisi sapiente v', 25, 82, NULL, 0, NULL, '2021-07-20 11:42:30', NULL, '2021-07-20 11:42:30'),
(8, 2, 1, NULL, 'Q2ZGLFRN', 'Convois 6', NULL, 'Mollitia corrupti q', 'Quis rerum enim expl', NULL, '1998-03-06 00:00:00', '1970-06-20 00:00:00', 'Excepturi quo commod', 71, 52, NULL, 0, NULL, '2021-07-20 11:49:41', NULL, '2021-07-20 11:49:41'),
(11, 2, 1, NULL, 'PY4A0JW1', 'Quia consequatur dol', 'Quia ipsum magnam v', 'Sunt excepturi omnis', 'Molestiae deserunt e', NULL, '1987-08-03 00:00:00', '2010-05-08 00:00:00', 'Id tempor amet cons', 79, 12, NULL, 0, NULL, '2021-08-24 12:41:42', NULL, '2021-08-24 12:41:42'),
(13, 2, 1, NULL, 'O9IG6UNS', 'Divertissement assini', 'Abobo', 'Assini', 'Abobo gare', NULL, '2021-08-25 00:00:00', '2021-08-25 00:00:00', 'Test', 2120, 50, NULL, 0, NULL, '2021-08-24 13:02:42', NULL, '2021-08-24 13:02:42'),
(25, 1, 10, NULL, '320HWRGV', 'sdfsd', 'dfsdfsd sdfsd', 'fsdfsd', 'Lorem ipsum dolor sit amet co', NULL, '2021-11-11 00:00:00', '2021-11-02 00:00:00', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit voluptatem vero voluptate, accusantium quas architecto quidem tempora veniam, placeat molestiae cumque ipsa asperiores voluptas unde dolorem consequuntur velit voluptatum dignissimos.', 1000, 10, NULL, 0, NULL, '2021-11-04 16:48:28', NULL, '2021-11-04 16:48:28'),
(26, 1, 11, NULL, 'SSKZTP11', 'Error alias praesent', 'Aliqua Magni et et', 'Magni laborum ad con', 'Nam doloremque qui q', NULL, '1986-10-11 00:00:00', '1985-04-19 00:00:00', 'Commodo esse ut quia', 56, 35, NULL, 0, NULL, '2021-11-04 17:58:13', NULL, '2021-11-04 17:58:13'),
(27, 1, 12, NULL, 'K67QLYSP', 'testnova', 'Abidjan', 'San pedro', 'Abobo', NULL, '2021-11-09 00:00:00', '2021-11-10 00:00:00', 'TEST', 1000, 2, NULL, 0, NULL, '2021-11-11 08:44:00', NULL, '2021-11-11 08:44:00'),
(28, 1, 12, NULL, 'WGZ83VKL', 'Consequatur Aut sae', 'Voluptas quo et porr', 'Non quae illum repe', 'Exercitationem omnis', NULL, '1987-11-29 00:00:00', '1990-01-28 00:00:00', 'Nihil error sequi ut', 39, 42, NULL, 0, NULL, '2021-11-11 09:35:52', NULL, '2021-11-11 09:35:52'),
(29, 1, 12, NULL, 'SNHJ6KHU', 'Beatae elit error a', 'Nesciunt illo quis', 'Et fugiat voluptatem', 'Consequuntur rerum u', NULL, '2018-01-10 00:00:00', '1978-04-23 00:00:00', 'Deserunt illo et asp', 34, 18, NULL, 0, NULL, '2021-11-11 10:04:47', NULL, '2021-11-11 10:04:47'),
(30, 1, 12, NULL, '5NMS17UV', 'Amet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod ullam repAmet quod', 'Abidjan', 'San pedro', 'ETEST', NULL, '2021-11-12 00:00:00', '2021-11-13 00:00:00', 'TEST', 1000, 3, NULL, 0, NULL, '2021-11-12 16:00:52', NULL, '2021-11-12 16:00:52'),
(31, 1, 14, NULL, 'YC6FYHGU', 'Hdhheeeheheheeh HdhheeeheheheehHdhheeeheheheehBienvenue dans le presse-papiers Gboard. Le texte que vous copiez est enregistré ici.Appuyez sur un élément pour le coller dans le champ de texte.Bienvenu', 'Abobo gare', 'Bassam - rond de bassam', 'Hdhd', NULL, '2021-11-13 00:00:00', '2021-11-13 00:00:00', 'Gdhdhe', 1284848, 1215, NULL, 0, NULL, '2021-11-13 15:26:51', NULL, '2021-11-13 15:26:51'),
(32, 1, 14, NULL, 'B4M8QTYY', 'Dhhd', 'Dhhd', 'Hdhd Dans le formulaire de création de convois, nous avons une erreur lorsque le nombre de caractere du lieu d\'arrivée est à 250 caractereshdhdhd', 'Hdhfd', NULL, '2021-11-13 00:00:00', '2021-11-13 00:00:00', 'Dvvdd', 1000, 15514, NULL, 0, NULL, '2021-11-13 16:52:56', NULL, '2021-11-13 16:52:56'),
(33, 1, 14, NULL, '01PBVMP6', 'Dans le formulaire de création de convois, nous avons une erreur lorsque le nombre de caractere du libelle est à 250 caracteresDans le formulaire de création de convois, nous avons une erreur lorsque', 'Gdhddgd', 'Dvdvv', 'Dans le formulaire de création de convois,  caractere du lieu de rass', NULL, '2021-11-13 00:00:00', '2021-11-13 00:00:00', 'Hdgd', 1000, 12, NULL, 0, NULL, '2021-11-13 17:10:32', NULL, '2021-11-13 17:10:32'),
(34, 1, 14, NULL, '49WEPVW4', 'Bdvdb', 'Dans le formulaire de création de convois, nous avons une erreur lorsque le nombre de caractere du lieu de départ est à 250 caracteres hdhd hdhhd dhhdhd', 'Hrhf', 'Vdvd', NULL, '2021-11-20 00:00:00', '2021-11-28 00:00:00', 'Fbfd', 1200, 1200, NULL, 0, NULL, '2021-11-13 17:14:25', NULL, '2021-11-13 17:14:25'),
(35, 1, 14, NULL, 'UXFSZFWS', 'Dggdg', 'Gdgd', 'Dans le formulaire de création de convois, nous avons une erreur lorsque le nombre de caractere du lieu de départ est à 250 caracteresDans le formulaire de création de convois, nous avons une erreur l', 'Hwvw', NULL, '2021-11-13 00:00:00', '2021-11-13 00:00:00', 'Gdggs', 1000, 1000, NULL, 0, NULL, '2021-11-13 17:16:22', NULL, '2021-11-13 17:16:22'),
(36, 1, 17, NULL, '1ALINIKA', 'Reprehenderit optio et voluptas quia et ad a aut do', 'Nesciunt commodo quas vel ratione sit quia et perspiciatis veniam quo quo quo veniam', 'Sequi optio reiciendis adipisicing ut perspiciatis fuga Laboriosam in nihil fugiat quibusdam exercitationem nisi qui est officia', 'Quas eos fugiat dolore voluptates asperiores proident\r\nQuas eos fugiat dolore voluptates asperiores proident\r\nQuas eos fugiat dolore voluptates asperiores proident\r\nQuas eos fugiat dolore voluptates aspe', NULL, '2021-11-15 00:00:00', '2021-11-16 00:00:00', 'Molestias cillum et', 1000, 29, NULL, 0, NULL, '2021-11-15 14:20:19', NULL, '2021-11-15 14:20:19'),
(37, 1, 19, NULL, '1W54DVL1', 'Hdhdhd', 'Hdhffh', 'Hdhdd', 'Hehheéehéhrhrheheh HehheéehéhrhrhehehBienvenue dans le presse-papiers Gboard. Le texte que vous copiez est enregistré ici.Bienvenue dans le presse-papiers Gboard. Le texte que vous copiez est enregist', NULL, '2021-11-16 00:00:00', '2021-11-16 00:00:00', 'Vdvdvd', 124554, 54544, NULL, 0, NULL, '2021-11-16 06:44:14', NULL, '2021-11-16 06:44:14'),
(38, 1, 30, NULL, 'CWOIXURF', 'Consequat Hic et temporibus at molestias omnis elit veniam veniam eum qui', 'Distinctio Non rerum consequat Esse ex qui perspiciatis reprehenderit aut laborum Soluta temporibus officia', 'Voluptatum ut similique odio debitis culpa autem in laudantium cum repudiandae itaque et voluptas eu at voluptas eligendi', 'Perferendis labore eum culpa sapiente ipsum eius', 'Sunt qui quis neque', '2021-12-20 00:00:00', '2021-12-21 00:00:00', 'Amet tempore deser', 1000, 85, NULL, 0, NULL, '2021-12-13 14:57:33', NULL, '2021-12-13 14:57:33'),
(39, 1, 30, NULL, 'CZ5DCN4J', 'Fugiat perspiciatis tempora cumque do veniam explicabo Quas quaerat ullamco placeat occaecat esse neque odit voluptas', 'Qui molestiae et nemo eius sunt consequuntur in enim', 'Molestiae dolores qui voluptate et reprehenderit a dignissimos voluptas ea qui dolorem', 'Error amet dolores minim et dolore vero maxime cillum dolorem reprehenderit dicta eu quibusdam ad facere non dolor sunt eos', 'Exercitationem sunt', '2021-12-20 00:00:00', '2021-12-22 00:00:00', 'Cupiditate ea Nam ma', 1000, 58, NULL, 0, NULL, '2021-12-13 15:10:09', NULL, '2021-12-13 15:10:09'),
(40, 1, 31, NULL, 'BD3VGMSM', 'Cocody1', 'Petro', 'Vallon', 'Petro ivoire station', 'Voluptas placeat qu', '2021-12-17 00:00:00', '2021-12-20 00:00:00', 'convois normal', 1500, 10, NULL, 0, NULL, '2021-12-17 10:08:14', NULL, '2021-12-17 10:08:14'),
(41, 1, 32, NULL, 'Z508TPL4', 'Mollit et est esse voluptatem Adipisicing minus', 'Omnis aute nemo rerum animi maxime sint quo et commodo quod sunt', 'Voluptatem aut et sunt maiores sint amet quia dolore nihil cum ut', 'Non quia rem dicta aperiam facilis dolor non quibusdam et recusandae Nemo adipisicing dolor suscipit esse minima aut totam', 'Error quia velit nis', '2022-12-23 00:00:00', '2022-12-24 00:00:00', 'Vero maiores nulla i', 1000, 36, NULL, 0, NULL, '2021-12-18 07:56:47', NULL, '2021-12-18 07:56:47'),
(42, 1, 32, NULL, 'PE0PZQTH', 'Ullamco sed ad cillum sed sint deserunt duis ut', 'Minim eligendi tempore magna corporis', 'A corrupti pariatur Cum unde voluptatem Rerum nisi consequuntur facilis ipsa tempor sit nulla', 'Magni quaerat assumenda veniam dolor harum occaecat est quis qui cillum cum cillum quia deleniti architecto ex quibusdam', 'Excepturi provident', '2022-12-23 00:00:00', '2022-12-24 00:00:00', 'Tempore animi plac', 1000, 78, NULL, 0, NULL, '2021-12-18 07:58:16', NULL, '2021-12-18 07:58:16'),
(44, 1, 32, NULL, 'QGVSL30G', 'Convois 10', 'Nobis est in reiciendis fugit commodi facere velit illum aliqua Exercitation voluptatem Ut eos numquam omnis', 'Aut et Nam harum quia qui qui praesentium', 'Assumenda voluptatum eveniet cupiditate in quia explicabo Similique', 'Placeat suscipit du', '2022-12-23 00:00:00', '2022-12-24 00:00:00', 'Sunt cum aliquip an', 1000, 89, NULL, 0, NULL, '2021-12-18 08:03:22', NULL, '2021-12-18 08:03:22'),
(45, 2, 32, NULL, 'LICPSMI7', 'Convois 11', 'Abobo', 'Adjamé', 'Abobo la gare', 'Duis in aliqua Mole', '2022-12-23 00:00:00', '2022-12-24 00:00:00', 'Vel animi minima ac', 1000, 26, NULL, 0, NULL, '2021-12-18 08:05:57', NULL, '2021-12-18 08:05:57'),
(46, 1, 34, NULL, 'YOW09T1J', 'Ut expedita adipisci sint accusantium reiciendis exercitationem excepturi reprehenderit nulla est ullam dolore aperiam', 'Eum unde sapiente deserunt quia cupiditate repellendus Sunt ea voluptatibus porro exercitation a consequatur nemo dolore deleniti obcaecati', 'Exercitation veniam hic qui suscipit alias hic in consequatur Ducimus placeat praesentium', 'Voluptas dolor dolore sapiente laboriosam ab ut reprehenderit ipsa officia eos ut', 'Quidem consequat Vo', '2022-02-12 00:00:00', '2022-02-12 00:00:00', 'Blanditiis aut sed c', 1000, 9, NULL, 0, NULL, '2022-02-10 03:45:26', NULL, '2022-02-10 03:45:26'),
(50, 1, 17, NULL, 'RO3G8TCY', 'Convois 5', 'Officia dolorum facilis dolorem repellendus Deserunt labore qui molestiae autem ullamco fugiat iste incididunt suscipit cupidatat in quos veniam', 'Perspiciatis nisi quia in quis Nam mollitia ullamco omnis minus atque sint cum', 'Dolor delectus a fuga Ex velit excepteur at omnis incididunt ullam lorem obcaecati beatae debitis', 'Fugiat sit aspernatu', '2022-02-11 00:00:00', '2022-02-12 00:00:00', 'Architecto laudantiu', 1000, 3, NULL, 0, NULL, '2022-02-11 10:01:58', NULL, '2022-02-11 10:01:58'),
(51, 1, 36, NULL, 'L61B3YH8', 'convois 1', 'Qui velit natus nulla a non cupidatat elit sapiente sit sint ea repudiandae molestiae tempora corrupti labore est magna quam', 'Aliquip aut dolores adipisicing fugiat cillum dolores corporis qui eligendi omnis ea', 'Est aute voluptatem magna et sed dolores aliquip', 'Fugit dolor eaque v', '2022-02-12 00:00:00', '2022-03-22 00:00:00', 'Et iure cillum ipsa', 1000, 85, NULL, 0, NULL, '2022-02-12 11:12:13', NULL, '2022-02-12 11:12:13'),
(52, 1, 37, NULL, 'L76U9NJ3', 'Neque quo sint eu odio quibusdam est voluptatibus mollit laboris porro quia blanditiis non', 'Non at magni consequuntur qui quia veniam velit velit', 'Optio tempore sunt iusto modi aspernatur commodi cillum vel odit numquam architecto sed', 'Ut magna fugiat libero quis eveniet quod', 'mardi minitries', '2022-02-15 00:00:00', '2022-02-16 00:00:00', 'Molestiae omnis obca', 1000, 6, NULL, 0, NULL, '2022-02-15 08:30:06', NULL, '2022-02-15 08:30:06'),
(53, 1, 17, NULL, '1KZK8OOC', 'test_image', 'test_image depart', 'test_image arrivée', 'Voluptate eos iste aut est qui voluptatibus non ut et', 'Nobis omnis sit ass', '2022-02-16 00:00:00', '2022-02-17 00:00:00', 'Iure consequuntur ei', 1000, 2, NULL, 0, NULL, '2022-02-16 18:05:24', NULL, '2022-02-16 18:05:24'),
(54, 1, 17, NULL, '34WB1YJS', 'Architecto asperiores laboris nobis amet', 'Ducimus magni qui ab rerum eiusmod doloremque dolore sequi et tempore labore consequatur laboriosam laudantium praesentium eligendi commodo expedita', 'Exercitationem saepe rerum nihil voluptate dolore voluptate alias explicabo Atque itaque ratione iusto consequatur Voluptate reiciendis pariatur Sed ipsum', 'Ut ullam commodo officia est quibusdam', 'Facere est lorem mod', '2022-02-16 00:00:00', '2022-02-17 00:00:00', 'Quia aut quos blandi', 1000, 10, NULL, 0, NULL, '2022-02-16 18:13:03', NULL, '2022-02-16 18:13:03'),
(55, 1, 17, NULL, 'SL7ZT3KC', 'Unde minim sit est consequatur Ut in', 'Voluptatem est quod esse tempore officia explicabo Modi sit quasi atque ad corporis in ut aliquip nesciunt', 'Dolores recusandae Nulla et quia praesentium veniam iste eos harum saepe placeat eiusmod tenetur', 'Atque vitae distinctio Beatae excepteur saepe perferendis quis corrupti culpa', 'Et ut numquam molest', '2022-02-16 00:00:00', '2022-02-16 00:00:00', 'Quo non dolor proide', 558100, 62, NULL, 0, NULL, '2022-02-16 18:15:47', NULL, '2022-02-16 18:15:47'),
(56, 1, 17, NULL, '95C13JFF', 'Perspiciatis aute soluta ab alias tempore minima voluptatum qui rerum illo fugit et perferendis iusto qui aperiam', 'Velit cillum voluptas recusandae Odit officia', 'Tenetur repellendus Et nihil dolor pariatur Commodi perferendis nihil qui cillum', 'Magnam aut aut cupiditate odio error amet eligendi deserunt dolorum quasi quia ut', 'Quasi velit sequi in', '2022-02-16 00:00:00', '2022-02-17 00:00:00', 'Accusamus nobis ut a', 1000, 8, NULL, 0, NULL, '2022-02-16 18:23:30', NULL, '2022-02-16 18:23:30'),
(57, 2, 17, NULL, 'BOTU2L4C', 'Earum sunt qui dicta cumque recusandae Est voluptas qui et eiusmod vel cum ipsum maiores culpa maxime cupidatat quia et', 'Illo nulla error consequatur accusamus voluptatem aliqua', 'Culpa a doloribus exercitationem est consequat Minim', 'Mollit similique quia optio accusantium excepteur beatae et eligendi voluptate elit', 'Minus eius deserunt', '2022-02-16 00:00:00', '2022-02-16 00:00:00', 'Aliquid modi veritat', 1000, 9, NULL, 0, NULL, '2022-02-16 18:30:45', 'photos/images_test.jpg', '2022-02-16 18:30:45'),
(58, 1, 38, NULL, 'GQ05XZ4U', 'Ea consequatur consequatur velit reprehenderit deserunt omnis quasi id laudantium qui dolores amet unde nobis tempora', 'Odit similique cum aut dolor quod animi odit voluptas', 'Qui sunt harum laboriosam laborum molestiae commodi unde voluptate', 'Qui qui ut molestiae dolore sit dolores ab est officia nulla nisi saepe omnis', 'tranvoyage', '2022-02-17 00:00:00', '2022-02-17 00:00:00', 'Autem ut temporibus', 1000, 80, NULL, 0, NULL, '2022-02-17 20:56:18', NULL, '2022-02-17 20:56:18'),
(59, 1, 38, NULL, '34UVPXDD', 'Iste Nam et illo aliquip mollitia enim sit', 'Excepturi ad veniam dolor pariatur Corporis ut rerum illum et', 'Laboriosam cumque dolorum eiusmod unde in id ratione id sint laboriosam ad aliquip vero qui consequatur Dolor consequat Neque', 'Dolor dolore sint quae labore eos alias Nam dolor deserunt quaerat voluptatem veritatis dolore itaque nesciunt error doloribus nisi architecto', 'Veritatis nihil labo', '2022-02-17 00:00:00', '2022-02-17 00:00:00', 'Dicta consequatur O', 1000, 65, NULL, 0, NULL, '2022-02-17 20:57:59', NULL, '2022-02-17 20:57:59'),
(60, 1, 39, NULL, 'STYBXMRX', 'Laborum Et itaque voluptas veritatis dolore quod et in quam rerum distinctio Exercitationem voluptatem cupidatat labore', 'Id officia nisi ea repudiandae adipisicing rerum id quis consequuntur adipisicing aliqua', 'Lorem tenetur accusamus alias iste corrupti dolor rerum reiciendis', 'Ut laboris nihil voluptatum voluptatem Deserunt exercitationem labore provident molestias laboris quo molestiae aut quos saepe id veniam', 'In aliqua Fugiat m', '2022-02-18 00:00:00', '2022-02-18 00:00:00', 'Corporis sed qui bea', 1000, 98, NULL, 0, NULL, '2022-02-18 03:26:23', NULL, '2022-02-18 03:26:23'),
(61, 1, 39, NULL, '6HGR1DPF', 'Quis corrupti consectetur occaecat nobis ea eveniet ut dolorem harum ea in', 'Nemo ipsum fugit voluptas reiciendis quaerat porro labore non sint tenetur expedita ducimus rem', 'Et aute enim qui Nam necessitatibus iure fuga Animi repellendus Nam blanditiis excepteur ut expedita quisquam quasi consequatur exercitation', 'Enim explicabo Commodi vel voluptatem debitis in esse nihil porro cum', 'tranvoyage', '2022-02-18 00:00:00', '2022-02-18 00:00:00', 'Modi et et eum ipsum', 1000, 56, NULL, 0, NULL, '2022-02-18 03:31:06', NULL, '2022-02-18 03:31:06'),
(62, 1, 39, NULL, 'BSZNECJW', 'Quis neque hic voluptas aut dolore', 'Cumque quisquam consectetur doloremque nulla', 'Enim vitae ipsum distinctio Qui deserunt dolor', 'Enim quos natus voluptatem et iusto deleniti', 'Ut harum alias moles', '2022-02-18 00:00:00', '2022-02-18 00:00:00', 'Esse ad duis est Na', 1000, 40, NULL, 0, NULL, '2022-02-18 03:59:02', 'photos/1.PNG', '2022-02-18 03:59:02'),
(63, 1, 24, NULL, 'W3CH7JQV', 'Similique ut non ipsum perspiciatis dolor', 'Reiciendis est quia consequuntur cillum tempor esse cupidatat id corrupti laboriosam', 'Distinctio Voluptates deserunt iste recusandae Iusto aspernatur', 'Molestiae quod magnam sed omnis dignissimos', 'Blanditiis cum autem', '2022-02-18 00:00:00', '2022-02-19 00:00:00', 'Quas sit vel possim', 1000, 1000, NULL, 0, NULL, '2022-02-18 12:42:35', 'photos/images_test.jpg', '2022-02-18 12:42:35'),
(64, 1, 40, NULL, 'O1SHGCUU', 'Ducimus minima dolor ipsa eu sed aute ut non nisi omnis sequi repudiandae necessitatibus expedita consequatur', 'Sequi in ea sint quo beatae aliquam et optio pariatur Ipsa officia eius reiciendis est Nam omnis', 'In eveniet laborum Velit voluptatum omnis eos eos facere incididunt hic', 'Possimus excepteur excepturi earum repudiandae', 'Non dignissimos cumq', '2022-02-22 00:00:00', '2022-02-22 00:00:00', 'Architecto odit id e', 1000, 15, NULL, 0, NULL, '2022-02-22 17:32:37', 'photos/background_transvoyage.png', '2022-02-22 17:32:37'),
(65, 2, 41, NULL, '2B3HIK4O', 'Divertissement à assini', 'Abobo à la gare', 'Assini', 'Abobo à la gare', 'Ayauka', '2022-02-24 00:00:00', '2022-02-24 00:00:00', 'Venez prendre un bon temps', 2000, 10, NULL, 0, NULL, '2022-02-24 12:16:50', 'photos/blank-profile-picture-973460_640.png', '2022-02-24 12:16:50'),
(66, 1, 2, NULL, 'SO5O9L66', 'SBP', 'Abobo', 'Assini', 'Abobo-mairie', 'Juners', '2022-02-27 00:00:00', '2022-02-27 00:00:00', 'Départ à de dossiers', 4000, 26, NULL, 0, NULL, '2022-02-26 01:02:58', 'photos/ABEF8604-E262-4283-B0F4-3B4D96DF1D41.jpeg', '2022-02-26 01:02:58'),
(68, 2, 43, NULL, 'JQG0O9KP', 'Laborum Et veritatis praesentium illum non odit ratione maxime aut Nam quaerat vitae voluptatem Assumenda nesciunt reprehenderit amet', 'Nihil odit laudantium eveniet rerum a eos ipsum ea voluptates aut voluptatem aut cupiditate sint asperiores qui asperiores', 'Et sed laborum consequatur aliquam voluptatem adipisci sunt quam', 'Corporis sapiente voluptatum et in', 'Exercitation reprehe', '2022-03-09 00:00:00', '2022-03-09 00:00:00', 'Accusantium tempora', 1000, 20, NULL, 0, NULL, '2022-03-09 08:28:07', NULL, '2022-03-09 08:28:07');

-- --------------------------------------------------------

--
-- Structure de la table `t_convois_temp`
--

CREATE TABLE `t_convois_temp` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_convois` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prix` int(11) DEFAULT NULL,
  `nom_prenom_utilisateur` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lieu_depart` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lieu_arrivee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_depart` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_arrivee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_checked` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_gares`
--

CREATE TABLE `t_gares` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `compagnie_id` bigint(20) UNSIGNED NOT NULL,
  `params_lieu_id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_gares`
--

INSERT INTO `t_gares` (`id`, `compagnie_id`, `params_lieu_id`, `libelle`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 20, 4, 'ADJAME', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(2, 20, 5, 'ABOBO', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(3, 20, 6, 'YAMOUSSOUKRO', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(4, 20, 6, 'BOUAKE', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(5, 20, 6, 'BEOUMI', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21');

-- --------------------------------------------------------

--
-- Structure de la table `t_paiements`
--

CREATE TABLE `t_paiements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `voyage_id` bigint(20) UNSIGNED DEFAULT NULL,
  `convois_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_standard_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `params_status_paiement_id` bigint(20) UNSIGNED NOT NULL,
  `id_transaction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant_total` int(11) NOT NULL,
  `montant_ticket_unitaire` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone_paiement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre_ticket` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_paiements`
--

INSERT INTO `t_paiements` (`id`, `voyage_id`, `convois_id`, `user_standard_id`, `user_id`, `params_status_paiement_id`, `id_transaction`, `montant_total`, `montant_ticket_unitaire`, `code`, `telephone_paiement`, `nombre_ticket`, `deleted_at`, `created_at`, `updated_at`, `commentaire`) VALUES
(1, NULL, 1, 17, NULL, 1, '4148474257', 100, 100, '12M0I6J', NULL, 1, NULL, '2021-08-24 09:31:24', '2021-08-24 09:31:24', NULL),
(2, NULL, 1, 18, NULL, 1, '4162281264', 100, 100, 'CRGZKA8', NULL, 1, NULL, '2021-08-24 09:33:42', '2021-08-24 09:33:42', NULL),
(3, NULL, 1, 19, NULL, 1, '4176939601', 100, 100, 'C7AM6HT', NULL, 1, NULL, '2021-08-24 09:36:09', '2021-08-24 09:36:09', NULL),
(4, NULL, 1, 20, NULL, 1, '4200286645', 100, 100, 'NM6865B', NULL, 1, NULL, '2021-08-24 09:40:02', '2021-08-24 09:40:02', NULL),
(5, NULL, 1, 21, NULL, 3, '4202156338', 100, 100, 'OM5UWWI', NULL, 1, NULL, '2021-08-24 09:40:21', '2021-08-24 09:41:16', NULL),
(6, NULL, 1, 22, NULL, 1, '476773112', 100, 100, '3VOX4VY', NULL, 1, NULL, '2021-08-24 11:14:37', '2021-08-24 11:14:37', NULL),
(7, NULL, 1, 23, NULL, 1, '4784653404', 100, 100, 'TZXENTE', NULL, 1, NULL, '2021-08-24 11:17:26', '2021-08-24 11:17:26', NULL),
(8, NULL, 1, 24, NULL, 1, '4794235756', 100, 100, 'OEYXNZV', NULL, 1, NULL, '2021-08-24 11:19:02', '2021-08-24 11:19:02', NULL),
(9, NULL, 1, 25, NULL, 1, '4817764291', 100, 100, 'BUSVPOM', NULL, 1, NULL, '2021-08-24 11:22:57', '2021-08-24 11:22:57', NULL),
(10, NULL, 1, 26, NULL, 1, '8109919649', 100, 100, 'PZRRYQV', NULL, 1, NULL, '2021-08-26 20:31:39', '2021-08-26 20:31:39', NULL),
(11, NULL, 1, 33, NULL, 1, '5654204321', 100, 100, 'UQ3T4CY', NULL, 1, NULL, '2021-08-30 13:42:22', '2021-08-30 13:42:22', NULL),
(12, NULL, 1, 34, NULL, 1, '5749482720', 100, 100, '33TOPU8', NULL, 1, NULL, '2021-08-30 13:58:14', '2021-08-30 13:58:14', NULL),
(13, NULL, 1, 35, NULL, 1, '5761394139', 100, 100, 'QSUHL82', NULL, 1, NULL, '2021-08-30 14:00:13', '2021-08-30 14:00:13', NULL),
(14, NULL, 1, 36, NULL, 1, '5767607807', 100, 100, 'X7NBVB3', NULL, 1, NULL, '2021-08-30 14:01:16', '2021-08-30 14:01:16', NULL),
(15, NULL, 1, 37, NULL, 1, '5768549409', 100, 100, 'JWHUPC8', NULL, 1, NULL, '2021-08-30 14:01:25', '2021-08-30 14:01:25', NULL),
(16, NULL, 1, 38, NULL, 1, '4549194693', 100, 100, 'Y1VH00D', NULL, 1, NULL, '2021-08-31 10:38:11', '2021-08-31 10:38:11', NULL),
(17, NULL, 4, 39, NULL, 1, '7213668540', 11, 11, '46YL7OV', NULL, 1, NULL, '2021-09-12 18:02:16', '2021-09-12 18:02:16', NULL),
(18, NULL, 1, 40, NULL, 1, '1652276850', 100, 100, 'VGUTTMI', NULL, 1, NULL, '2021-10-28 02:35:22', '2021-10-28 02:35:22', NULL),
(19, NULL, 1, 41, NULL, 1, '1916247678', 100, 100, '74S7KXJ', NULL, 1, NULL, '2021-10-28 03:19:22', '2021-10-28 03:19:22', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_params_groupes`
--

CREATE TABLE `t_params_groupes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_params_lieu`
--

CREATE TABLE `t_params_lieu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `params_type_lieu_id` bigint(20) UNSIGNED NOT NULL,
  `sup_params_lieu_id` bigint(20) UNSIGNED DEFAULT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_params_lieu`
--

INSERT INTO `t_params_lieu` (`id`, `params_type_lieu_id`, `sup_params_lieu_id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 2, NULL, 'Abidjan', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(4, 3, NULL, 'ADJAME', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(5, 3, NULL, 'ABOBO ', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(6, 2, NULL, 'YAMOUSSOUKRO ', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(7, 2, NULL, 'BOUAKE', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(8, 2, NULL, 'KOROGHO', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(9, 2, NULL, 'BEOUMI', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(10, 2, NULL, 'BONON', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(11, 3, NULL, 'YOPOUGON', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21');

-- --------------------------------------------------------

--
-- Structure de la table `t_params_status_convois`
--

CREATE TABLE `t_params_status_convois` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_params_status_convois`
--

INSERT INTO `t_params_status_convois` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'En attente', 'En attente de validation de la part de l\' administation', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(2, 'Validé', 'Validé part l\' administation', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(3, 'Rejeté', 'Annulé par l\'utilisateur', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(4, 'Expiré', 'Rejeté après analyse', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21');

-- --------------------------------------------------------

--
-- Structure de la table `t_params_status_paiement`
--

CREATE TABLE `t_params_status_paiement` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_params_status_paiement`
--

INSERT INTO `t_params_status_paiement` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Initié', 'Le paiement a été initié', 1, NULL, '2021-07-16 14:24:36', '2021-07-16 14:24:36'),
(2, 'En cours', 'En cours de paiement', 0, NULL, '2021-07-16 14:25:30', '2021-07-16 14:25:30'),
(3, 'Finalisé', 'Le paiement a été finalisé', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48'),
(4, 'Annulé', 'Le paiement a été annulé', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48'),
(5, 'Echoué', 'Le paiement a été echoué', 0, NULL, '2021-08-22 20:43:16', '2021-08-22 20:43:16');

-- --------------------------------------------------------

--
-- Structure de la table `t_params_status_retrait`
--

CREATE TABLE `t_params_status_retrait` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_params_status_retrait`
--

INSERT INTO `t_params_status_retrait` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'En attente', 'La demande est en attente de validation', 1, NULL, '2021-07-16 14:24:36', '2021-07-16 14:24:36'),
(2, 'Validé', 'La demande de retrait a été validé et transféré', 0, NULL, '2021-07-16 14:25:30', '2021-07-16 14:25:30'),
(3, 'Rejeté', 'La demande de retrait a été rejeté', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48'),
(4, 'Annulé', 'La demande de retrait a été annulé ', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48');

-- --------------------------------------------------------

--
-- Structure de la table `t_params_status_tickets`
--

CREATE TABLE `t_params_status_tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_params_status_tickets`
--

INSERT INTO `t_params_status_tickets` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Valide', 'Le ticket est encore valide', 1, NULL, '2021-07-16 14:24:36', '2021-07-16 14:24:36'),
(2, 'Utilisé', 'Le ticket est déja utilisé', 0, NULL, '2021-07-16 14:25:30', '2021-07-16 14:25:30'),
(3, 'Expiré', 'Le ticket a expiré', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48'),
(4, 'Annulé', 'Le ticket est annulé', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48');

-- --------------------------------------------------------

--
-- Structure de la table `t_params_type_lieu`
--

CREATE TABLE `t_params_type_lieu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_params_type_lieu`
--

INSERT INTO `t_params_type_lieu` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Pays', 'Pays', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(2, 'Ville', 'Ville', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(3, 'Commune', 'Commune', 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(4, 'Secteur', NULL, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21');

-- --------------------------------------------------------

--
-- Structure de la table `t_recus`
--

CREATE TABLE `t_recus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `paiement_id` bigint(20) UNSIGNED NOT NULL,
  `date_validation` date DEFAULT NULL,
  `date_refus` date DEFAULT NULL,
  `cause_refus_recu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentaire` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `t_retraits`
--

CREATE TABLE `t_retraits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `params_status_retrait_id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `montant_retrait` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_mooney` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentaire` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_traitement` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_retraits`
--

INSERT INTO `t_retraits` (`id`, `params_status_retrait_id`, `transaction_id`, `user_id`, `montant_retrait`, `code`, `reference_mooney`, `commentaire`, `date_traitement`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 2, 10000, 'KV10UUUF', NULL, NULL, NULL, NULL, '2021-07-26 16:15:40', '2021-07-26 16:15:40'),
(2, 1, NULL, 3, 1200, 'E7YD44ZR', NULL, NULL, NULL, NULL, '2021-08-15 10:11:16', '2021-08-15 10:11:16'),
(3, 1, NULL, 2, 60000, 'TGLXL2RB', NULL, NULL, NULL, NULL, '2021-08-16 15:01:55', '2021-08-16 15:01:55'),
(4, 1, NULL, 2, 200000, 'B1C7LSD3', NULL, NULL, NULL, NULL, '2021-08-18 18:08:39', '2021-08-18 18:08:39'),
(5, 1, NULL, 1, 100000, 'PQSL2K9Z', NULL, NULL, NULL, NULL, '2021-10-27 21:46:48', '2021-10-27 21:46:48'),
(6, 1, NULL, 1, 100, 'IRO3XFWI', NULL, NULL, NULL, NULL, '2021-10-27 21:49:11', '2021-10-27 21:49:11'),
(7, 1, NULL, 1, 100, 'WHGJVBP0', NULL, NULL, NULL, NULL, '2021-10-27 21:49:39', '2021-10-27 21:49:39'),
(8, 1, NULL, 1, 100, 'U8IG89ID', NULL, NULL, NULL, NULL, '2021-10-27 21:49:51', '2021-10-27 21:49:51'),
(9, 1, NULL, 1, 100, 'B4FABVGG', NULL, NULL, NULL, NULL, '2021-10-27 21:50:06', '2021-10-27 21:50:06'),
(10, 1, NULL, 1, 100, 'P4KIAJVZ', NULL, NULL, NULL, NULL, '2021-10-27 21:50:19', '2021-10-27 21:50:19'),
(11, 1, NULL, 1, 100, 'VPP2MI4M', NULL, NULL, NULL, NULL, '2021-10-27 21:50:35', '2021-10-27 21:50:35'),
(12, 1, NULL, 1, 100, 'Z1VPC00U', NULL, NULL, NULL, NULL, '2021-10-27 21:56:29', '2021-10-27 21:56:29'),
(13, 1, NULL, 1, 100, '3UHRB76I', NULL, NULL, NULL, NULL, '2021-10-27 21:56:40', '2021-10-27 21:56:40'),
(14, 1, NULL, 1, 100, 'O5PGZKFO', NULL, NULL, NULL, NULL, '2021-10-27 21:56:51', '2021-10-27 21:56:51'),
(15, 1, NULL, 1, 100, '9OBPB4M6', NULL, NULL, NULL, NULL, '2021-10-27 21:57:00', '2021-10-27 21:57:00');

-- --------------------------------------------------------

--
-- Structure de la table `t_tickets`
--

CREATE TABLE `t_tickets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `paiement_id` bigint(20) UNSIGNED NOT NULL,
  `params_status_ticket_id` bigint(20) UNSIGNED NOT NULL,
  `vendeur_admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `vendeur_id` bigint(20) UNSIGNED DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_tickets`
--

INSERT INTO `t_tickets` (`id`, `paiement_id`, `params_status_ticket_id`, `vendeur_admin_id`, `vendeur_id`, `code`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 5, 1, NULL, 1, 'EK9PJB9', 1, NULL, '2021-08-24 09:41:16', '2021-08-24 09:41:16');

-- --------------------------------------------------------

--
-- Structure de la table `t_transactions`
--

CREATE TABLE `t_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `montant_transaction` int(11) NOT NULL,
  `montant_avant_transaction` int(11) NOT NULL,
  `montant_apres_transaction` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_transactions`
--

INSERT INTO `t_transactions` (`id`, `user_id`, `admin_id`, `montant_transaction`, `montant_avant_transaction`, `montant_apres_transaction`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 100, 0, 100, '2021-08-24 09:41:16', '2021-08-24 09:41:16');

-- --------------------------------------------------------

--
-- Structure de la table `t_users_standards`
--

CREATE TABLE `t_users_standards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_users_standards`
--

INSERT INTO `t_users_standards` (`id`, `telephone`, `email`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '0504397161', 'jojosong00@gmail.com', 1, NULL, '2021-08-18 08:53:50', '2021-08-18 08:53:50'),
(2, '47886905', NULL, 1, NULL, '2021-08-22 20:54:18', '2021-08-22 20:54:18'),
(3, '47886905', NULL, 1, NULL, '2021-08-22 20:57:05', '2021-08-22 20:57:05'),
(4, '47886905', NULL, 1, NULL, '2021-08-22 20:58:25', '2021-08-22 20:58:25'),
(5, '47886905', NULL, 1, NULL, '2021-08-22 21:01:19', '2021-08-22 21:01:19'),
(6, '0447886905', NULL, 1, NULL, '2021-08-22 21:04:02', '2021-08-22 21:04:02'),
(7, '074788690', NULL, 1, NULL, '2021-08-23 13:57:23', '2021-08-23 13:57:23'),
(8, '074788690', NULL, 1, NULL, '2021-08-23 14:00:46', '2021-08-23 14:00:46'),
(9, '074788690', NULL, 1, NULL, '2021-08-23 14:02:06', '2021-08-23 14:02:06'),
(10, '074788690', NULL, 1, NULL, '2021-08-23 14:10:57', '2021-08-23 14:10:57'),
(11, '074788690', NULL, 1, NULL, '2021-08-23 14:11:41', '2021-08-23 14:11:41'),
(12, '074788690', NULL, 1, NULL, '2021-08-23 14:13:53', '2021-08-23 14:13:53'),
(13, '074788690', NULL, 1, NULL, '2021-08-23 14:16:10', '2021-08-23 14:16:10'),
(14, '074788690', NULL, 1, NULL, '2021-08-23 14:16:11', '2021-08-23 14:16:11'),
(15, '074788690', NULL, 1, NULL, '2021-08-23 14:16:31', '2021-08-23 14:16:31'),
(16, '074788690', NULL, 1, NULL, '2021-08-23 14:22:01', '2021-08-23 14:22:01'),
(17, '0747886905', NULL, 1, NULL, '2021-08-24 09:31:24', '2021-08-24 09:31:24'),
(18, '0747886905', NULL, 1, NULL, '2021-08-24 09:33:42', '2021-08-24 09:33:42'),
(19, '0747886905', NULL, 1, NULL, '2021-08-24 09:36:09', '2021-08-24 09:36:09'),
(20, '0747886905', NULL, 1, NULL, '2021-08-24 09:40:02', '2021-08-24 09:40:02'),
(21, '+1 (279) 535-9958', NULL, 1, NULL, '2021-08-24 09:40:20', '2021-08-24 09:40:20'),
(22, '0747886905', NULL, 1, NULL, '2021-08-24 11:14:37', '2021-08-24 11:14:37'),
(23, '0747886905', NULL, 1, NULL, '2021-08-24 11:17:26', '2021-08-24 11:17:26'),
(24, '0747886905', NULL, 1, NULL, '2021-08-24 11:19:02', '2021-08-24 11:19:02'),
(25, '0747886905', NULL, 1, NULL, '2021-08-24 11:22:57', '2021-08-24 11:22:57'),
(26, '47886905', NULL, 1, NULL, '2021-08-26 20:31:39', '2021-08-26 20:31:39'),
(27, '0744886005', NULL, 1, NULL, '2021-08-29 15:12:11', '2021-08-29 15:12:11'),
(28, '0744886005', NULL, 1, NULL, '2021-08-29 15:20:22', '2021-08-29 15:20:22'),
(29, '0744886005', NULL, 1, NULL, '2021-08-29 15:23:57', '2021-08-29 15:23:57'),
(30, '0744886005', NULL, 1, NULL, '2021-08-29 15:29:51', '2021-08-29 15:29:51'),
(31, '+1 (151) 612-6412', NULL, 1, NULL, '2021-08-29 15:31:03', '2021-08-29 15:31:03'),
(32, '+1 (151) 612-6412', NULL, 1, NULL, '2021-08-29 15:35:14', '2021-08-29 15:35:14'),
(33, '0747886905', NULL, 1, NULL, '2021-08-30 13:42:22', '2021-08-30 13:42:22'),
(34, '0747886905', NULL, 1, NULL, '2021-08-30 13:58:14', '2021-08-30 13:58:14'),
(35, '+1 (178) 988-8976', NULL, 1, NULL, '2021-08-30 14:00:13', '2021-08-30 14:00:13'),
(36, '+1 (178) 988-8976', NULL, 1, NULL, '2021-08-30 14:01:16', '2021-08-30 14:01:16'),
(37, '0747886905', NULL, 1, NULL, '2021-08-30 14:01:25', '2021-08-30 14:01:25'),
(38, '8555588', NULL, 1, NULL, '2021-08-31 10:38:11', '2021-08-31 10:38:11'),
(39, '555555555', NULL, 1, NULL, '2021-09-12 18:02:16', '2021-09-12 18:02:16'),
(40, '0747886905', 'dks@yopmail.com', 1, NULL, '2021-10-28 02:35:22', '2021-10-28 02:35:22'),
(41, '0747886905', 'dks@yopmail.com', 1, NULL, '2021-10-28 03:19:22', '2021-10-28 03:19:22'),
(42, '0747886905', 'dks1@yopmail.com', 1, NULL, '2021-11-04 15:40:24', '2021-11-04 15:40:24');

-- --------------------------------------------------------

--
-- Structure de la table `t_voyages`
--

CREATE TABLE `t_voyages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `compagnie_id` bigint(20) UNSIGNED NOT NULL,
  `depart_gare_id` bigint(20) UNSIGNED NOT NULL,
  `arrive_gare_id` bigint(20) UNSIGNED NOT NULL,
  `lieu_depart_id` bigint(20) UNSIGNED NOT NULL,
  `lieu_arrive_id` bigint(20) UNSIGNED NOT NULL,
  `date_depart` datetime NOT NULL,
  `date_arrivee` datetime NOT NULL,
  `prix` int(11) NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_voyages`
--

INSERT INTO `t_voyages` (`id`, `admin_id`, `user_id`, `compagnie_id`, `depart_gare_id`, `arrive_gare_id`, `lieu_depart_id`, `lieu_arrive_id`, `date_depart`, `date_arrivee`, `prix`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(2, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(3, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(4, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(5, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(6, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(7, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(8, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(9, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(10, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(11, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(12, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(14, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21'),
(15, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, '2021-07-26 10:50:21', '2021-07-26 10:50:21');

-- --------------------------------------------------------

--
-- Structure de la table `t_wallets`
--

CREATE TABLE `t_wallets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `solde` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `t_wallets`
--

INSERT INTO `t_wallets` (`id`, `user_id`, `solde`, `created_at`, `updated_at`) VALUES
(1, 1, 100, '2021-07-26 10:50:21', '2021-08-24 09:41:16'),
(2, 2, 0, '2021-07-26 16:13:59', '2021-07-26 16:13:59'),
(3, 3, 0, '2021-08-15 09:49:23', '2021-08-15 09:49:23'),
(4, 4, 0, '2021-08-15 10:39:14', '2021-08-15 10:39:14'),
(5, 5, 0, '2021-08-17 20:02:30', '2021-08-17 20:02:30'),
(6, 6, 0, '2021-08-24 13:32:55', '2021-08-24 13:32:55'),
(7, 7, 0, '2021-10-26 18:24:57', '2021-10-26 18:24:57'),
(8, 8, 0, '2021-10-27 16:12:36', '2021-10-27 16:12:36'),
(9, 9, 0, '2021-10-27 16:15:37', '2021-10-27 16:15:37'),
(10, 10, 0, '2021-11-04 16:35:43', '2021-11-04 16:35:43'),
(11, 11, 0, '2021-11-04 17:33:47', '2021-11-04 17:33:47'),
(12, 12, 0, '2021-11-11 08:42:02', '2021-11-11 08:42:02'),
(13, 13, 0, '2021-11-12 20:47:25', '2021-11-12 20:47:25'),
(14, 14, 0, '2021-11-13 15:25:45', '2021-11-13 15:25:45'),
(15, 15, 0, '2021-11-13 18:32:52', '2021-11-13 18:32:52'),
(16, 16, 0, '2021-11-13 18:40:41', '2021-11-13 18:40:41'),
(17, 17, 0, '2021-11-15 14:14:47', '2021-11-15 14:14:47'),
(18, 18, 0, '2021-11-15 17:47:15', '2021-11-15 17:47:15'),
(19, 19, 0, '2021-11-16 06:41:01', '2021-11-16 06:41:01'),
(20, 20, 0, '2021-12-06 09:52:29', '2021-12-06 09:52:29'),
(21, 22, 0, '2021-12-06 09:57:04', '2021-12-06 09:57:04'),
(22, 23, 0, '2021-12-06 09:57:47', '2021-12-06 09:57:47'),
(23, 24, 0, '2021-12-06 13:33:21', '2021-12-06 13:33:21'),
(24, 25, 0, '2021-12-06 21:59:55', '2021-12-06 21:59:55'),
(25, 26, 0, '2021-12-06 22:04:30', '2021-12-06 22:04:30'),
(26, 27, 0, '2021-12-06 22:04:47', '2021-12-06 22:04:47'),
(27, 28, 0, '2021-12-13 00:10:23', '2021-12-13 00:10:23'),
(28, 29, 0, '2021-12-13 06:43:03', '2021-12-13 06:43:03'),
(29, 30, 0, '2021-12-13 14:56:15', '2021-12-13 14:56:15'),
(30, 31, 0, '2021-12-17 09:58:43', '2021-12-17 09:58:43'),
(31, 32, 0, '2021-12-18 07:50:25', '2021-12-18 07:50:25'),
(32, 33, 0, '2022-02-09 15:09:16', '2022-02-09 15:09:16'),
(33, 34, 0, '2022-02-10 03:41:17', '2022-02-10 03:41:17'),
(34, 35, 0, '2022-02-10 19:58:31', '2022-02-10 19:58:31'),
(35, 36, 0, '2022-02-12 11:11:34', '2022-02-12 11:11:34'),
(36, 37, 0, '2022-02-15 08:22:02', '2022-02-15 08:22:02'),
(37, 38, 0, '2022-02-17 20:54:48', '2022-02-17 20:54:48'),
(38, 39, 0, '2022-02-18 03:25:42', '2022-02-18 03:25:42'),
(39, 40, 0, '2022-02-22 17:31:08', '2022-02-22 17:31:08'),
(40, 41, 0, '2022-02-24 12:15:22', '2022-02-24 12:15:22'),
(41, 42, 0, '2022-03-04 16:00:00', '2022-03-04 16:00:00'),
(42, 43, 0, '2022-03-09 08:27:32', '2022-03-09 08:27:32');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `compagnie_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_verifie` tinyint(1) DEFAULT NULL,
  `see_form_when_first_login` tinyint(1) DEFAULT NULL,
  `email` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `sended_reset_password_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `communaute` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `compagnie_id`, `name`, `telephone`, `telephone_verifie`, `see_form_when_first_login`, `email`, `email_verified_at`, `last_login`, `sended_reset_password_at`, `password`, `actif`, `communaute`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'john doe', '0745858995', NULL, NULL, 'johndoe@yopmail.com', NULL, '2021-10-28 15:54:42', NULL, '$2y$10$ET4hgRIRWve5nKxUB3ccV.3J9yGQu5NVfHUnOKhBFdna6pKKc3a/G', 1, NULL, NULL, NULL, '2021-07-26 10:50:21', '2021-10-28 15:54:42'),
(2, NULL, 'Jojo', NULL, NULL, NULL, 'jojosong00@gmail.com', NULL, '2022-03-07 11:13:44', NULL, '$2y$10$5hCC1l3vw5H2Vtv/yA4XEuZF004RTkCWSMBELunUC0GKVC8t4OD2W', 0, NULL, NULL, NULL, '2021-07-26 16:13:59', '2022-03-07 11:13:44'),
(3, NULL, 'Quinlan Larson', NULL, NULL, NULL, 'nyvebuqoh@mailinator.net', NULL, '2021-08-15 09:49:23', NULL, '$2y$10$I7Mual8DXm5aeg4LjkAl8OgFUeTzkAM/t8.Zq5AmmANAII6c6DFBG', 0, NULL, NULL, NULL, '2021-08-15 09:49:23', '2021-08-15 09:49:23'),
(4, NULL, 'Martin Maxwell', NULL, NULL, NULL, 'godulyku@mailinator.net', NULL, '2021-08-15 10:39:14', NULL, '$2y$10$Db7lEvYDYHAsuBm4KXhiL.nS8SZbKJ084QYcWXc0KmpmSMz85/doy', 0, NULL, NULL, NULL, '2021-08-15 10:39:14', '2021-08-15 10:39:14'),
(5, NULL, 'Hdhdhdd', NULL, NULL, NULL, 'bootnetcrfhhdasher@gmail.com', NULL, '2021-08-17 20:02:30', NULL, '$2y$10$06NDfVBbzx.m4WZJGmzqGOt9WsO7PEQeDsWUElkJRKQeGiSRUWth.', 0, NULL, NULL, NULL, '2021-08-17 20:02:30', '2021-08-17 20:02:30'),
(6, NULL, 'Toto balaise', NULL, NULL, NULL, 'totoazerty@yopmail.com', NULL, '2021-08-24 13:32:55', NULL, '$2y$10$WPtUzX6mTUnp9k3MOtbEg.K94G5tW7xweTZBii1idK1TxiaHQ/H/m', 0, NULL, NULL, NULL, '2021-08-24 13:32:55', '2021-08-24 13:32:55'),
(7, NULL, 'Test', NULL, NULL, NULL, 'test1@gmail.com', NULL, '2021-10-26 18:24:57', NULL, '$2y$10$NIBv1Eb2fisdV30BNxmz3.giQQpj5nNl/tE.k7BP5B3r7OAxBKLLS', 0, NULL, NULL, NULL, '2021-10-26 18:24:57', '2021-10-26 18:24:57'),
(8, NULL, 'Keith Summers', NULL, NULL, NULL, 'xihih@mailinator.com', NULL, '2021-10-27 16:12:36', NULL, '$2y$10$3gfVJDJWtWDVqzTRP9OcAuy.8MMz7CaLW92JLXzN8hdP9rG9K7VjS', 0, NULL, NULL, NULL, '2021-10-27 16:12:36', '2021-10-27 16:12:36'),
(9, NULL, 'Halla Marquez', NULL, NULL, NULL, 'mypegyka@mailinator.com', NULL, '2021-10-27 16:15:37', NULL, '$2y$10$LWLQ4BHGP9.V3RynwSRd/ufnAJtGSEnkEhzQuzOqdXZ9w92st0GOe', 0, NULL, NULL, NULL, '2021-10-27 16:15:37', '2021-10-27 16:15:37'),
(10, NULL, 'Carl Kirkland', NULL, NULL, NULL, 'xenur@mailinator.com', NULL, '2021-11-04 16:35:43', NULL, '$2y$10$PDEWI7nPFXA1NkYpLWqKRuKBLLTUFUbNQHcJ/aZUQ6kO/l31rnJ2a', 0, NULL, NULL, NULL, '2021-11-04 16:35:43', '2021-11-04 16:35:43'),
(11, NULL, 'Michael Hickman', NULL, NULL, NULL, 'xyqenyjo@mailinator.net', NULL, '2021-11-04 17:33:48', NULL, '$2y$10$2Zo2R86LmtgZLeuigTXElOtp7D/4jvfjpiNSjnUR8Nr/h53xqHage', 0, NULL, NULL, NULL, '2021-11-04 17:33:47', '2021-11-04 17:33:48'),
(12, NULL, 'osse asseu romeo', NULL, NULL, NULL, 'cherubin0225@gmail.com', NULL, '2022-02-10 16:00:38', NULL, '$2y$10$1dvoFySa8nYhXonkTR.9e.6XVpz/Am.wIzHQGlKWss2M085xsLJ66', 0, NULL, NULL, NULL, '2021-11-11 08:42:02', '2022-02-10 16:00:38'),
(13, NULL, 'Bertha Nichols', NULL, NULL, NULL, 'wopa@mailinator.net', NULL, '2021-11-12 20:47:25', NULL, '$2y$10$kr.UciIzI3zG3shCKDrC9e/2vmrQaHJwGTIBkLJD8hPJsGY5qXQea', 0, NULL, NULL, NULL, '2021-11-12 20:47:25', '2021-11-12 20:47:25'),
(14, NULL, 'Bxhdhjdh', NULL, NULL, NULL, 'hdhddd@ejhd.com', NULL, '2021-11-13 15:25:45', NULL, '$2y$10$6V2HobVOiLPGea/GVU.KZeNps/kwgIj7VYgX7gXN1scUP6qOXgWa.', 0, NULL, NULL, NULL, '2021-11-13 15:25:45', '2021-11-13 15:25:45'),
(15, NULL, 'Warren Strickland', NULL, NULL, NULL, 'zodira@mailinator.com', NULL, '2021-11-13 18:32:52', NULL, '$2y$10$7/qAiEvachqxdHVI.InNHupQr0RNPpBj1kGA58YgfKTNyJvsu5N6e', 0, NULL, NULL, NULL, '2021-11-13 18:32:51', '2021-11-13 18:32:52'),
(16, NULL, 'Ivor Kirk', NULL, NULL, NULL, 'xano@mailinator.net', NULL, '2021-11-13 18:40:41', NULL, '$2y$10$jt5WxRmVVsPeixx8T4Nj6eCDh5NWf76UfXN98712uxDcVA1/1nCvu', 0, NULL, NULL, NULL, '2021-11-13 18:40:41', '2021-11-13 18:40:41'),
(17, NULL, 'osse asseu romeo', NULL, NULL, NULL, 'romeoasseu0225@gmail.com', NULL, '2022-02-17 09:04:32', NULL, '$2y$10$Pm3/xDh/w2pzqR8CvoCkLuAmKVOxzwzbXGn7/Oc5QF.NdL6BEisT6', 0, NULL, NULL, NULL, '2021-11-15 14:14:47', '2022-02-17 09:04:32'),
(18, NULL, 'Gloria Cochran', NULL, NULL, NULL, 'bojat@mailinator.com', NULL, '2021-11-15 17:47:15', NULL, '$2y$10$RMEffQUAOZmSIVqRcBsPDuz0zP.vgDy85rZFhWd020EZunHTrux0q', 0, NULL, NULL, NULL, '2021-11-15 17:47:15', '2021-11-15 17:47:15'),
(19, NULL, 'Bddh', NULL, NULL, NULL, 'ehhdhe@fbfh.come', NULL, '2021-11-16 06:41:01', NULL, '$2y$10$iEL0RNoM2/kOntwQFWUazeCKFAYJMvzippZhTFajFNIxYh3HoT4q6', 0, NULL, NULL, NULL, '2021-11-16 06:41:01', '2021-11-16 06:41:01'),
(20, NULL, 'osse asseu romeo test', '0778059869', NULL, NULL, 'test2021@test.co', NULL, '2021-12-06 09:52:29', NULL, '$2y$10$ElSPrDIG76MpR4S2V.MKfO5GQ3DdFci1GMCVGQb.NboYX7q9whhLS', 0, NULL, NULL, NULL, '2021-12-06 09:52:29', '2021-12-06 09:52:29'),
(22, NULL, 'Quintessa Chapman', '+1 (716) 248-6959', NULL, NULL, 'qukagituk@mailinator.com', NULL, '2021-12-06 14:14:51', NULL, '$2y$10$nkchKE.cVsVfrVCEs98muuVLtOpQ/fm3KFaSd9nvcy8ANN9igjDRK', 0, NULL, NULL, NULL, '2021-12-06 09:57:04', '2021-12-06 14:14:51'),
(23, NULL, 'Igor Larson', '+1 (879) 765-8898', NULL, NULL, 'bywyhyxiju@mailinator.com', NULL, '2021-12-06 09:57:47', NULL, '$2y$10$O9kmpwMMQoker3k8ojcyReo.40OqlsINu.704BjaWa8t/1OWU3KM6', 0, NULL, NULL, NULL, '2021-12-06 09:57:47', '2021-12-06 09:57:47'),
(24, NULL, 'testa', '122334456789', NULL, NULL, 'testa@testa.co', NULL, '2022-02-18 12:41:44', NULL, '$2y$10$NOEhhquHpCJO2.KJ.kzqQuG/ndMxpBsm7S4iAntgwvOv2MEii2bb2', 0, NULL, NULL, NULL, '2021-12-06 13:33:20', '2022-02-18 12:41:44'),
(25, NULL, 'Jayme Sullivan', '+1 (864) 411-4029', NULL, NULL, 'livoqy@mailinator.com', NULL, '2021-12-06 21:59:55', NULL, '$2y$10$FCKeyzPHmHGZHssNdNVJAef7Mh5rydnSUQA6V47EK1JIcUxw1Skfm', 0, NULL, NULL, NULL, '2021-12-06 21:59:55', '2021-12-06 21:59:55'),
(26, NULL, 'Rhiannon Horne', '0769886905', NULL, NULL, 'gese@mailinator.net', NULL, '2021-12-06 22:04:30', NULL, '$2y$10$5kx5QkoMRqued7S0bhV2C.e5slkpoFtTsTgz/ZUNsDpL.bULweP8q', 0, NULL, NULL, NULL, '2021-12-06 22:04:30', '2021-12-06 22:04:30'),
(27, NULL, 'Valentine Curtis', '0769886905', NULL, NULL, 'botok@mailinator.com', NULL, '2021-12-06 22:04:47', NULL, '$2y$10$c5cTpe5dyYK.qkgc2seiEuNZY.KVRVnez8cuazy0/vMxgN./Fhi2u', 0, NULL, NULL, NULL, '2021-12-06 22:04:47', '2021-12-06 22:04:47'),
(28, NULL, 'osse asseu romeo test', '234567890°', NULL, NULL, 'cherubin0225@gmail.net', NULL, '2021-12-13 00:10:23', NULL, '$2y$10$xOj0LDZtQLj2Fp.lBT6Cd.NCkQ/RVKFD2rp6/iwdtgwP07/i6tF5m', 0, NULL, NULL, NULL, '2021-12-13 00:10:23', '2021-12-13 00:10:23'),
(29, NULL, 'Test', '0747886905', NULL, NULL, 'kouamedhshdavid124@gmail.com', NULL, '2021-12-13 06:43:03', NULL, '$2y$10$cuCxCthQQhUjBqKlJrb1S.ERu5iKgLJip0SFEKSjnHtg1Hg4yEY6e', 0, NULL, NULL, NULL, '2021-12-13 06:43:03', '2021-12-13 06:43:03'),
(30, NULL, 'Vivian Sawyer', '+1 (525) 941-7743', NULL, NULL, 'doworibip@mailinator.com', NULL, '2021-12-13 14:56:15', NULL, '$2y$10$mvCi1iN/UCgUVTLj7j4cIuuIZoFK248YBxfvqbKdlY13h6KbA4IQO', 0, NULL, NULL, NULL, '2021-12-13 14:56:15', '2021-12-13 14:56:15'),
(31, NULL, 'test', '234567890', NULL, NULL, 'test@test.me', NULL, '2021-12-17 09:58:43', NULL, '$2y$10$USomZk2XsvhkjjKJ3SakF.X907h3tVbcc9OMAhkar1XTWIGe.S3ai', 0, NULL, NULL, NULL, '2021-12-17 09:58:43', '2021-12-17 09:58:43'),
(32, NULL, 'Stephen Noel', '+1 (127) 408-6251', NULL, NULL, 'tivicoxa@mailinator.com', NULL, '2021-12-18 07:50:25', NULL, '$2y$10$ws1futRVIyXZ8D0Ywic5GOL9XJ/NQvvIKBqKJkvKVomsvftOjGmQy', 0, NULL, NULL, NULL, '2021-12-18 07:50:25', '2021-12-18 07:50:25'),
(33, NULL, 'Kaye Monroe', '+1 (928) 857-2253', NULL, NULL, 'rugibisy@mailinator.com', NULL, '2022-02-09 15:09:16', NULL, '$2y$10$E8jtbMnBtrQVTrP9VZ2iROgoVdizBxudlMCY17CLPLjs63a5maC..', 0, NULL, NULL, NULL, '2022-02-09 15:09:16', '2022-02-09 15:09:16'),
(34, NULL, 'Lyle Delaney', '+1 (973) 225-2672', NULL, NULL, 'xozorifite@mailinator.com', NULL, '2022-02-10 03:41:17', NULL, '$2y$10$IyeyMMo9FzcEk21R5I0yd.NXiL5cIQAhYdp9VzEv/IFVuhHm8LOMy', 0, NULL, NULL, NULL, '2022-02-10 03:41:17', '2022-02-10 03:41:17'),
(35, NULL, 'Hilda Leblanc', '+1 (358) 658-6074', NULL, NULL, 'dahahi@mailinator.com', NULL, '2022-02-10 19:58:31', NULL, '$2y$10$PIHnk2uLBCtoHogwb2VABu121rMEyhjowsqyPJhX07QI6KnJ/tEUy', 0, NULL, NULL, NULL, '2022-02-10 19:58:31', '2022-02-10 19:58:31'),
(36, NULL, 'Riley Pace', '+1 (922) 778-4129', NULL, NULL, 'nolemu@mailinator.com', NULL, '2022-02-12 11:11:34', NULL, '$2y$10$Gm/4oyTrOpVcpu02e0vZBeR0PrJ2jCzI2bM1Le1.oxkLua/9JHPXi', 0, NULL, NULL, NULL, '2022-02-12 11:11:34', '2022-02-12 11:11:34'),
(37, NULL, 'Christian Baldwin', '+1 (859) 158-6461', NULL, NULL, 'topygehoj@mailinator.com', NULL, '2022-02-15 08:22:02', NULL, '$2y$10$Q4j96WgnEF8vYhsrEuGMaObDZaoa5XmZETsenMROVo4sHt.tOQ5J.', 0, NULL, NULL, NULL, '2022-02-15 08:22:02', '2022-02-15 08:22:02'),
(38, NULL, 'Giacomo Newton', '+1 (135) 875-6034', NULL, NULL, 'vakap@mailinator.com', NULL, '2022-02-17 20:54:48', NULL, '$2y$10$b4kw2FL0n.MLpX2uQr/ZduoKj9NQFAkpnK97TMhHnQ2f48Ny1fCUq', 0, NULL, NULL, NULL, '2022-02-17 20:54:48', '2022-02-17 20:54:48'),
(39, NULL, 'Audra Franks', '+1 (341) 864-6058', NULL, NULL, 'hydurizaxy@mailinator.com', NULL, '2022-02-18 03:25:42', NULL, '$2y$10$ohNPn5SDIyyKRzi0.cUCJ.BjO8UdBHIsuNQKshk8pxVQXbkMwh8eW', 0, NULL, NULL, NULL, '2022-02-18 03:25:42', '2022-02-18 03:25:42'),
(40, NULL, 'Valentine Williams', '+1 (838) 188-2625', NULL, NULL, 'rabumon@mailinator.com', NULL, '2022-02-22 17:31:08', NULL, '$2y$10$AqHjDQm9uWeBH/nwS6og5ug2iMk/1tj5fnhNImz6GkdwbP6x/WO8W', 0, NULL, NULL, NULL, '2022-02-22 17:31:08', '2022-02-22 17:31:08'),
(41, NULL, 'test', '0478968587', NULL, NULL, 'test@yopmail.com', NULL, '2022-02-24 12:15:22', NULL, '$2y$10$q06nWEkiwIbw5g8qrcVBEOOsR8tzTLf4PEY98f8EIu0WjyP4OoLle', 0, NULL, NULL, NULL, '2022-02-24 12:15:21', '2022-02-24 12:15:22'),
(42, NULL, 'Alika Solomon', '+1 (171) 943-6918', NULL, NULL, 'tazagoju@mailinator.com', NULL, '2022-03-04 16:00:00', NULL, '$2y$10$MROGkYJ0N5OMlS2M4cv/OOE.3oZfMuaGm5X7Yvh4/dWReS2AKOlLy', 0, NULL, NULL, NULL, '2022-03-04 16:00:00', '2022-03-04 16:00:00'),
(43, NULL, 'Ruby Sloan', '+1 (316) 241-2764', NULL, NULL, 'waputera@mailinator.com', NULL, '2022-03-09 08:27:33', NULL, '$2y$10$Ei1KvOhGAnrymR.a9zqW/Oynd6vAS.HaRUnwkxt56rOQ9ILlKC6c.', 0, NULL, NULL, NULL, '2022-03-09 08:27:32', '2022-03-09 08:27:33');

-- --------------------------------------------------------

--
-- Structure de la table `users_standard`
--

CREATE TABLE `users_standard` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `telephone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id_utilisateurs` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenoms` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mot_passe` varchar(255) NOT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `compagnie_id` varchar(255) DEFAULT NULL,
  `type_utilisateurs_id` int(12) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `status` int(12) NOT NULL DEFAULT 0,
  `connecte` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id_utilisateurs`, `nom`, `prenoms`, `email`, `mot_passe`, `telephone`, `compagnie_id`, `type_utilisateurs_id`, `created_at`, `updated_at`, `status`, `connecte`) VALUES
(2, 'ossey', 'Romeo', 'romeo.osse@arobaseprotech.com', '6838b32d70437d7c62e6e157cc71d6723903803179e550f5f68b2394ab5e4d4d39595005c6cfc79c8e48c77f1bcd1fac641d7f8e12fca0a19d222e81a87168f9', '78059869', '1', 1, '2021-04-15 19:17:14', '2021-04-15 19:17:14', 0, 'oui'),
(4, 'ossey ', 'osse', 'test@email.com', '4152727bef88af5db30c68d9908dc2d5261aadca58fc0d690e339a037c8de91eb132a3ab51be29c8687fef23bebc0fa3d78b3ea50daf180cb1fbaa576326b749', '78059869', '1', 1, '2021-04-16 09:07:39', '2021-04-16 09:07:39', 0, NULL),
(5, 'ossey ', ' romi', 'KAM@KAM.comt', 'aafa6abcc05052d17e010943260b2d308e0eb97cd375a1ef862d01237b12cede8f8f5b587e449f7e09d5203a7e6d7f57eefdf68430dde8552cfa4c73100b2986', '78059869', '2', 2, '2021-04-16 10:33:08', '2021-04-16 10:33:08', 0, NULL),
(9, 'patrick', 'assande', 'patrick@pat.com', 'c9fa26a7fd4f7558c8ed1bb62db31a70893860d3f9edca92a6df86827c4c68f335840a1ce552e211e6f1ae33b2cd1a945265c9cfe764107ca3a01a9c3d84ae78', '78059869', '23', 1, '2021-04-16 16:45:12', '2021-04-16 16:45:12', 1, NULL),
(12, 'osse', 'Romeo', 'cherubin0225@gmail.com', '1a36f6272b8f681ab5533b350fe0315b8336c9594a553ed668ca849f3b7bae7f49b8fabe5ed44a59f5625ef3913c375a4659065bc5c8f1519c46916a48cdabd5', '78059869', '', 3, '2021-04-18 02:20:20', '2021-04-18 02:20:20', 0, 'oui'),
(13, 'admin', 'admin', 'admin@admin.com', '491756ef99078e2ef469dadd8b1423c842a030a73eec8443cdc12474db17df6d4b27d8bd8da0bbe5d76671464de0725cb41654d0502a8903cab22afdc316dc98', '78059869', '', 3, '2021-04-18 03:27:27', '2021-04-18 03:27:27', 0, 'oui'),
(14, 'osseTC', 'Romeo TC', 'KAMATC@KAM.com', '3cd967197971997364075b9228e66b3b4a24ab1c1fb469763255065c623b36f14676a83d9950445ec42b2f34f45349c2996dbf47c545a6d46750c9af301d2470', '7805986912', '1', 2, '2021-04-20 00:09:04', '2021-04-20 00:09:04', 0, NULL),
(74, 'tranvoyage', 'super admin', 'admin@tranvoyage.com', 'f4593bfef5070aaf85a2a14daf3f6d03933b34dc4fb0460b983c352b1f4d56fa62391b033da3bd5fcb7c644c67196c4fc31826706ad32eeb5ea48be530275811', '78059869', '', 3, '2021-04-24 00:24:28', '2021-04-24 00:24:28', 0, 'oui'),
(75, '', '', NULL, '', '4534567898', NULL, NULL, '2021-04-24 00:30:27', '2021-04-24 00:30:27', 0, NULL),
(76, '', '', NULL, '', '23456789', NULL, NULL, '2021-04-24 00:32:03', '2021-04-24 00:32:03', 0, NULL),
(77, '', '', NULL, '', '0504397161', NULL, NULL, '2021-04-24 12:51:25', '2021-04-24 12:51:25', 0, NULL),
(78, '', '', NULL, '', '0504397161', NULL, NULL, '2021-04-24 15:38:14', '2021-04-24 15:38:14', 0, NULL),
(79, '', '', NULL, '', '0504397161', NULL, NULL, '2021-04-24 16:16:07', '2021-04-24 16:16:07', 0, NULL),
(80, '', '', NULL, '', '0778059869', NULL, NULL, '2021-04-26 09:59:22', '2021-04-26 09:59:22', 0, NULL),
(81, '', '', NULL, '', '05279075', NULL, NULL, '2021-04-26 11:46:59', '2021-04-26 11:46:59', 0, NULL),
(82, '', '', NULL, '', '04397161', NULL, NULL, '2021-04-26 12:17:36', '2021-04-26 12:17:36', 0, NULL),
(83, '', '', NULL, '', '78059869', NULL, NULL, '2021-04-26 19:46:58', '2021-04-26 19:46:58', 0, NULL),
(84, '', '', NULL, '', '0778059869', NULL, NULL, '2021-04-26 20:47:23', '2021-04-26 20:47:23', 0, NULL),
(85, '', '', NULL, '', '02304549', NULL, NULL, '2021-04-26 21:56:07', '2021-04-26 21:56:07', 0, NULL),
(86, '', '', NULL, '', '04397161', NULL, NULL, '2021-04-26 21:59:03', '2021-04-26 21:59:03', 0, NULL),
(87, '', '', NULL, '', '7805986932', NULL, NULL, '2021-04-26 23:26:41', '2021-04-26 23:26:41', 0, NULL),
(88, '', '', NULL, '', '0102110129', NULL, NULL, '2021-04-27 13:37:00', '2021-04-27 13:37:00', 0, NULL),
(89, 'jo', 'Jo', 'jo@gmail.com', '053adbe8f3050151b2b9ce16460c9914d931e57e04a963c7299d6a168cc40df0317dc992b03ed083e3cfdd94eb5807ae4cf6a5eebd3c51696505e25204d62a45', '0102958184', '3', 2, '2021-04-28 18:53:42', '2021-04-28 18:53:42', 0, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `affectations`
--
ALTER TABLE `affectations`
  ADD PRIMARY KEY (`id_affectations`);

--
-- Index pour la table `annonces`
--
ALTER TABLE `annonces`
  ADD PRIMARY KEY (`id_annonces`);

--
-- Index pour la table `compagnie`
--
ALTER TABLE `compagnie`
  ADD PRIMARY KEY (`id_compagnie`);

--
-- Index pour la table `destination`
--
ALTER TABLE `destination`
  ADD PRIMARY KEY (`id_destination`);

--
-- Index pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `gare`
--
ALTER TABLE `gare`
  ADD PRIMARY KEY (`id_gare`);

--
-- Index pour la table `guichet`
--
ALTER TABLE `guichet`
  ADD PRIMARY KEY (`id_guichet`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `paiement`
--
ALTER TABLE `paiement`
  ADD PRIMARY KEY (`id_paiement`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `recu`
--
ALTER TABLE `recu`
  ADD PRIMARY KEY (`id_recu`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id_service`);

--
-- Index pour la table `statut_destination`
--
ALTER TABLE `statut_destination`
  ADD PRIMARY KEY (`id_statut_destination`);

--
-- Index pour la table `statut_utilisateurs`
--
ALTER TABLE `statut_utilisateurs`
  ADD PRIMARY KEY (`id_statut_utilisateurs`);

--
-- Index pour la table `trace`
--
ALTER TABLE `trace`
  ADD PRIMARY KEY (`id_trace`);

--
-- Index pour la table `type_utilisateurs`
--
ALTER TABLE `type_utilisateurs`
  ADD PRIMARY KEY (`id_type_utilisateurs`);

--
-- Index pour la table `t_admins`
--
ALTER TABLE `t_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `t_admins_email_unique` (`email`);

--
-- Index pour la table `t_admins_password_resets`
--
ALTER TABLE `t_admins_password_resets`
  ADD KEY `t_admins_password_resets_email_index` (`email`),
  ADD KEY `t_admins_password_resets_token_index` (`token`);

--
-- Index pour la table `t_annonces`
--
ALTER TABLE `t_annonces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_annonces_admin_id_foreign` (`admin_id`);

--
-- Index pour la table `t_compagnies`
--
ALTER TABLE `t_compagnies`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_convois`
--
ALTER TABLE `t_convois`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `t_convois_code_unique` (`code`),
  ADD KEY `t_convois_params_status_convois_id_foreign` (`params_status_convois_id`),
  ADD KEY `t_convois_user_id_foreign` (`user_id`),
  ADD KEY `t_convois_admin_id_foreign` (`admin_id`);

--
-- Index pour la table `t_convois_temp`
--
ALTER TABLE `t_convois_temp`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `t_convois_temp_code_unique` (`code`);

--
-- Index pour la table `t_gares`
--
ALTER TABLE `t_gares`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_gares_compagnie_id_foreign` (`compagnie_id`),
  ADD KEY `t_gares_params_lieu_id_foreign` (`params_lieu_id`);

--
-- Index pour la table `t_paiements`
--
ALTER TABLE `t_paiements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `t_paiements_id_transaction_unique` (`id_transaction`),
  ADD KEY `t_paiements_voyage_id_foreign` (`voyage_id`),
  ADD KEY `t_paiements_user_id_foreign` (`user_id`),
  ADD KEY `t_paiements_params_status_paiement_id_foreign` (`params_status_paiement_id`),
  ADD KEY `t_paiements_convois_id_foreign` (`convois_id`),
  ADD KEY `t_paiements_user_standard_id_foreign` (`user_standard_id`);

--
-- Index pour la table `t_params_groupes`
--
ALTER TABLE `t_params_groupes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_params_lieu`
--
ALTER TABLE `t_params_lieu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_params_lieu_params_type_lieu_id_foreign` (`params_type_lieu_id`),
  ADD KEY `t_params_lieu_sup_params_lieu_id_foreign` (`sup_params_lieu_id`);

--
-- Index pour la table `t_params_status_convois`
--
ALTER TABLE `t_params_status_convois`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_params_status_paiement`
--
ALTER TABLE `t_params_status_paiement`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_params_status_retrait`
--
ALTER TABLE `t_params_status_retrait`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_params_status_tickets`
--
ALTER TABLE `t_params_status_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_params_type_lieu`
--
ALTER TABLE `t_params_type_lieu`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_recus`
--
ALTER TABLE `t_recus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_recus_paiement_id_foreign` (`paiement_id`);

--
-- Index pour la table `t_retraits`
--
ALTER TABLE `t_retraits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_retraits_params_status_retrait_id_foreign` (`params_status_retrait_id`),
  ADD KEY `t_retraits_user_id_foreign` (`user_id`);

--
-- Index pour la table `t_tickets`
--
ALTER TABLE `t_tickets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `t_tickets_code_unique` (`code`),
  ADD KEY `t_tickets_paiement_id_foreign` (`paiement_id`),
  ADD KEY `t_tickets_vendeur_admin_id_foreign` (`vendeur_admin_id`),
  ADD KEY `t_tickets_vendeur_id_foreign` (`vendeur_id`),
  ADD KEY `t_tickets_params_status_ticket_id_foreign` (`params_status_ticket_id`);

--
-- Index pour la table `t_transactions`
--
ALTER TABLE `t_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_transactions_admin_id_foreign` (`admin_id`),
  ADD KEY `t_transactions_user_id_foreign` (`user_id`);

--
-- Index pour la table `t_users_standards`
--
ALTER TABLE `t_users_standards`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `t_voyages`
--
ALTER TABLE `t_voyages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_voyages_user_id_foreign` (`user_id`),
  ADD KEY `t_voyages_admin_id_foreign` (`admin_id`),
  ADD KEY `t_voyages_compagnie_id_foreign` (`compagnie_id`),
  ADD KEY `t_voyages_lieu_depart_id_foreign` (`lieu_depart_id`),
  ADD KEY `t_voyages_lieu_arrive_id_foreign` (`lieu_arrive_id`),
  ADD KEY `t_voyages_depart_gare_id_foreign` (`depart_gare_id`),
  ADD KEY `t_voyages_arrive_gare_id_foreign` (`arrive_gare_id`);

--
-- Index pour la table `t_wallets`
--
ALTER TABLE `t_wallets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `t_wallets_user_id_foreign` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `users_standard`
--
ALTER TABLE `users_standard`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id_utilisateurs`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `affectations`
--
ALTER TABLE `affectations`
  MODIFY `id_affectations` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `annonces`
--
ALTER TABLE `annonces`
  MODIFY `id_annonces` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `compagnie`
--
ALTER TABLE `compagnie`
  MODIFY `id_compagnie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `destination`
--
ALTER TABLE `destination`
  MODIFY `id_destination` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `gare`
--
ALTER TABLE `gare`
  MODIFY `id_gare` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `guichet`
--
ALTER TABLE `guichet`
  MODIFY `id_guichet` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pour la table `paiement`
--
ALTER TABLE `paiement`
  MODIFY `id_paiement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT pour la table `recu`
--
ALTER TABLE `recu`
  MODIFY `id_recu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `service`
--
ALTER TABLE `service`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `statut_destination`
--
ALTER TABLE `statut_destination`
  MODIFY `id_statut_destination` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `statut_utilisateurs`
--
ALTER TABLE `statut_utilisateurs`
  MODIFY `id_statut_utilisateurs` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `trace`
--
ALTER TABLE `trace`
  MODIFY `id_trace` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=678;

--
-- AUTO_INCREMENT pour la table `type_utilisateurs`
--
ALTER TABLE `type_utilisateurs`
  MODIFY `id_type_utilisateurs` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_admins`
--
ALTER TABLE `t_admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_annonces`
--
ALTER TABLE `t_annonces`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_compagnies`
--
ALTER TABLE `t_compagnies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `t_convois`
--
ALTER TABLE `t_convois`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT pour la table `t_convois_temp`
--
ALTER TABLE `t_convois_temp`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_gares`
--
ALTER TABLE `t_gares`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `t_paiements`
--
ALTER TABLE `t_paiements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `t_params_groupes`
--
ALTER TABLE `t_params_groupes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_params_lieu`
--
ALTER TABLE `t_params_lieu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `t_params_status_convois`
--
ALTER TABLE `t_params_status_convois`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_params_status_paiement`
--
ALTER TABLE `t_params_status_paiement`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `t_params_status_retrait`
--
ALTER TABLE `t_params_status_retrait`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_params_status_tickets`
--
ALTER TABLE `t_params_status_tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_params_type_lieu`
--
ALTER TABLE `t_params_type_lieu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_recus`
--
ALTER TABLE `t_recus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_retraits`
--
ALTER TABLE `t_retraits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `t_tickets`
--
ALTER TABLE `t_tickets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `t_transactions`
--
ALTER TABLE `t_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `t_users_standards`
--
ALTER TABLE `t_users_standards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT pour la table `t_voyages`
--
ALTER TABLE `t_voyages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `t_wallets`
--
ALTER TABLE `t_wallets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT pour la table `users_standard`
--
ALTER TABLE `users_standard`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id_utilisateurs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_annonces`
--
ALTER TABLE `t_annonces`
  ADD CONSTRAINT `t_annonces_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `t_admins` (`id`);

--
-- Contraintes pour la table `t_convois`
--
ALTER TABLE `t_convois`
  ADD CONSTRAINT `t_convois_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `t_admins` (`id`),
  ADD CONSTRAINT `t_convois_params_status_convois_id_foreign` FOREIGN KEY (`params_status_convois_id`) REFERENCES `t_params_status_convois` (`id`),
  ADD CONSTRAINT `t_convois_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `t_gares`
--
ALTER TABLE `t_gares`
  ADD CONSTRAINT `t_gares_compagnie_id_foreign` FOREIGN KEY (`compagnie_id`) REFERENCES `t_compagnies` (`id`),
  ADD CONSTRAINT `t_gares_params_lieu_id_foreign` FOREIGN KEY (`params_lieu_id`) REFERENCES `t_params_lieu` (`id`);

--
-- Contraintes pour la table `t_paiements`
--
ALTER TABLE `t_paiements`
  ADD CONSTRAINT `t_paiements_convois_id_foreign` FOREIGN KEY (`convois_id`) REFERENCES `t_convois` (`id`),
  ADD CONSTRAINT `t_paiements_params_status_paiement_id_foreign` FOREIGN KEY (`params_status_paiement_id`) REFERENCES `t_params_status_paiement` (`id`),
  ADD CONSTRAINT `t_paiements_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `t_paiements_user_standard_id_foreign` FOREIGN KEY (`user_standard_id`) REFERENCES `t_users_standards` (`id`),
  ADD CONSTRAINT `t_paiements_voyage_id_foreign` FOREIGN KEY (`voyage_id`) REFERENCES `t_voyages` (`id`);

--
-- Contraintes pour la table `t_params_lieu`
--
ALTER TABLE `t_params_lieu`
  ADD CONSTRAINT `t_params_lieu_params_type_lieu_id_foreign` FOREIGN KEY (`params_type_lieu_id`) REFERENCES `t_params_type_lieu` (`id`),
  ADD CONSTRAINT `t_params_lieu_sup_params_lieu_id_foreign` FOREIGN KEY (`sup_params_lieu_id`) REFERENCES `t_params_lieu` (`id`);

--
-- Contraintes pour la table `t_recus`
--
ALTER TABLE `t_recus`
  ADD CONSTRAINT `t_recus_paiement_id_foreign` FOREIGN KEY (`paiement_id`) REFERENCES `t_paiements` (`id`);

--
-- Contraintes pour la table `t_retraits`
--
ALTER TABLE `t_retraits`
  ADD CONSTRAINT `t_retraits_params_status_retrait_id_foreign` FOREIGN KEY (`params_status_retrait_id`) REFERENCES `t_params_status_retrait` (`id`),
  ADD CONSTRAINT `t_retraits_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `t_tickets`
--
ALTER TABLE `t_tickets`
  ADD CONSTRAINT `t_tickets_paiement_id_foreign` FOREIGN KEY (`paiement_id`) REFERENCES `t_paiements` (`id`),
  ADD CONSTRAINT `t_tickets_params_status_ticket_id_foreign` FOREIGN KEY (`params_status_ticket_id`) REFERENCES `t_params_status_tickets` (`id`),
  ADD CONSTRAINT `t_tickets_vendeur_admin_id_foreign` FOREIGN KEY (`vendeur_admin_id`) REFERENCES `t_admins` (`id`),
  ADD CONSTRAINT `t_tickets_vendeur_id_foreign` FOREIGN KEY (`vendeur_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `t_transactions`
--
ALTER TABLE `t_transactions`
  ADD CONSTRAINT `t_transactions_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `t_admins` (`id`),
  ADD CONSTRAINT `t_transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `t_voyages`
--
ALTER TABLE `t_voyages`
  ADD CONSTRAINT `t_voyages_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `t_admins` (`id`),
  ADD CONSTRAINT `t_voyages_arrive_gare_id_foreign` FOREIGN KEY (`arrive_gare_id`) REFERENCES `t_gares` (`id`),
  ADD CONSTRAINT `t_voyages_compagnie_id_foreign` FOREIGN KEY (`compagnie_id`) REFERENCES `t_compagnies` (`id`),
  ADD CONSTRAINT `t_voyages_depart_gare_id_foreign` FOREIGN KEY (`depart_gare_id`) REFERENCES `t_gares` (`id`),
  ADD CONSTRAINT `t_voyages_lieu_arrive_id_foreign` FOREIGN KEY (`lieu_arrive_id`) REFERENCES `t_params_lieu` (`id`),
  ADD CONSTRAINT `t_voyages_lieu_depart_id_foreign` FOREIGN KEY (`lieu_depart_id`) REFERENCES `t_params_lieu` (`id`),
  ADD CONSTRAINT `t_voyages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `t_wallets`
--
ALTER TABLE `t_wallets`
  ADD CONSTRAINT `t_wallets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
