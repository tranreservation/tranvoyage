=> Responsive 
- Les pages du frontend (ok)
    - Les partials
        * Le header (ok)
        * Le footer (ok)
    - Page d'accueil
    - Page de détail de convois 
    - Les pages de recherche de tickets
    - Les pages de création de convois
    - La Page qui somme nous
    - La page contact
- Les pages du dashboard
    - La page de connexion
    - La page d'inscription
    - La page du dashboard
    - Les pages de convois
        * afficher la liste de convois
        * création d'un convois
        * afficher le détail d'un convois
    - Les pages de retrait
        * afficher la liste des demandes de retraits
        * création d'une demande de retrait
        * afficher le détail d'un convois
    - Le menu transaction
    - Liste des tickets
    - Mon profil

Lien original : http://demo.harnishdesign.net/html/payyed/index.html



block
@foreach($convois as $convoi)
                        <div class="col-md-6 col-gars_affiche">
                            <div class="gars_affiche">
                                <div id="no-responsive-voyage" class="d-flex flex-row my-flex-container">
                                    {{-- <div class="p-2 mr-auto my-flex-item" id="col-logo">
                                        <span id="logo-7"><i class="fa fa-bus fa-7x"></i></span>
                                    </div>
                                    <div class=" mobile10">
                                        logo
                                    </div>
                                    <div id="no-responsive-voyage" class="p-2 my-flex-item">
                                        text
                                    </div> --}}
                                    <div class="col-3 col-md-3 col-lg-3 mobile10">
                                        <i class="fa fa-bus fa-3x" style="padding:10px;color:#fff;"></i>
                                        <span style="color:white;margin-top:-100px;display:none">Jeu 08 Jui 2021</span>
                                        <a href="reservation.php?res=14&amp;1625760302&amp;date=2021-07-08" data-id="14" class="btn" type="button " style="background-color:#03989E;color:#fff;height:30px;margin-top:-30px;margin-left:40px;border-radius:5px;font-size: 12px;font-weight: bold;display:none">Reserver le ticket</a>
                                    </div>

                                    <div id="no-responsive-voyage" class="col-9 col-md-9 col-lg-9">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul>
                                                    <li class="bull">
                                                        <span class="sous-bull">
                                                            <div class="destination">
                                                                <span class="label">{{ $convoi->lieu_depart }} - {{ $convoi->lieu_arrivee }}.</span>
                                                            </div>
                                                            <span>|</span>
                                                            <div class="depart">
                                                                <span class="label">Départ:</span>
                                                                <span class="value"><?=date('h:i', strtotime($convoi->date_depart));?></span>
                                                            </div>
                                                        </span>
                                                    </li>
                                                    <li class="bull">
                                                        <span class="sous-bull">
                                                            <div class="tarif">
                                                                <span class="label">Tarif:</span>
                                                                <span>{{ $convoi->prix }}XOF</span>
                                                            </div>
                                                            <span>|</span>
                                                            <div class="gare">
                                                                <span class="label">Code : </span><span>{{ $convoi->code }}</span>
                                                            </div>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-3 col-md-12 col-lg-3" id="date-voyage">
                                        <span style="">
                                            <?php
                                                $date = '2021-01-01';
                                            ?>
                                            {{ $helpService->convertDateInFrench($convoi->date_depart->format('Y-m-d')) }}
                                        </span>
                                    </div>
                                    <div class="col-9 col-md-12 col-lg-9">
                                        <a href="{{ route('user.convois.reservation.show', ['code' => $convoi->code ]) }}" data-id="14" class="btn btn_reserver btn-success" >Détail du convois</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
