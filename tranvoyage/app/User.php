<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Models\Compagnie;
use App\Models\Wallet;

class User extends Authenticatable
{

    // protected $table = "t_users";

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'telephone', 'password','compagnie_id','actif','deleted_at','sended_reset_password_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function compagnie(){
        return $this->belongsTo(Compagnie::class);
    }

    public function wallet(){
        return $this->hasOne(Wallet::class);
    }
}
