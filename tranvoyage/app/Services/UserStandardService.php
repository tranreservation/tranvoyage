<?php 

namespace App\Services;

use App\Repositories\UserStandardRepository;
use Illuminate\Support\Facades\Log;

class UserStandardService extends Service{

    public function __construct(UserStandardRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findAll(){
        return $this->repository->all();
    }

    public function findOne(int $id){
        return $this->repository->find($id);
    }

    public function create(array $data){
        return $this->repository->save($data);
    }

    public function update(int $id, array $data){
        return $this->repository->save($data, $id);
    }

}