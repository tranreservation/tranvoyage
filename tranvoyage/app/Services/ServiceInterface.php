<?php

namespace App\Services;

interface ServiceInterface{

    public function all();

    public function findOne($id);

    public function create(array $data);

    public function update(int $id, array $data);

    public function delete($id);
}