<?php 

namespace App\Services\Params;

use App\Repositories\ParamTypeLieuRepository;
use App\Services\Service;

class ParamTypeLieuService extends Service{

    public function __construct(ParamTypeLieuRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findAll(){
        return $this->repository->all();
    }

    public function findOne(int $id){
        return $this->repository->find($id);
    }

    public function create(array $data){
        return $this->repository->save($data);
    }

    public function update(int $id, array $data){
        return $this->repository->save($data, $id);
    }

}