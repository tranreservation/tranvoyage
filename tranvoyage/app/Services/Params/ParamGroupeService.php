<?php 

namespace App\Services\Params;

use App\Repositories\Params\ParamGroupeRepository;
use App\Services\Service;

class ParamGroupeService extends Service{

    public function __construct(ParamGroupeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function all(){
        return $this->repository->all();
    }

    public function findOne(int $id){
        return $this->repository->find($id);
    }

    public function create(array $data){
        return $this->repository->save($data);
    }

    public function update(int $id, array $data){
        return $this->repository->save($data, $id);
    }

}