<?php

namespace App\Services;

class HelpService
{
    public function convertDateInFrench($dat){

        $date_explode = explode("-", $dat);
        $jour=$date_explode[2];
        $mois=$date_explode[1];
        $annee=$date_explode[0];

        $newTimestamp = mktime(12,0,0,$mois,$jour,$annee); // Cr&eacute;&eacute; le timestamp pour ta date (a midi)

        // Ensuite tu fais un tableau avec les jours en Français
        $Jour = array("Dim","Lun","Mar","Mer","Jeu","Ven","Sam");


        // Pareil pour les mois en FR
        $Mois = array("","Jan","Fév","Mar","Avr","Mai","Jui","Jui","Aoû","Sep","Oct","Nov","Déc");


        // Enfin tu affiche le libell&eacute; du jour
        $j= $Jour[date("w", $newTimestamp)];

        // Et le libell&eacute; du mois
        $m= $Mois[date("n", $newTimestamp)];

        $result=$j.' '.$jour.' '.$m.' '.$annee;
        return $result;
    }

    public function convertDateInFrenchComplet($dat){

        $date_explode = explode("-", $dat);
        $jour=$date_explode[2];
        $mois=$date_explode[1];
        $annee=$date_explode[0];

        $newTimestamp = mktime(12,0,0,$mois,$jour,$annee); // Cr&eacute;&eacute; le timestamp pour ta date (a midi)

        // Ensuite tu fais un tableau avec les jours en Français
        $Jour = array("Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi");


        // Pareil pour les mois en FR
        $Mois = array("","Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octôbre","Novembre","Décembre");


        // Enfin tu affiche le libell&eacute; du jour
        $j= $Jour[date("w", $newTimestamp)];

        // Et le libell&eacute; du mois
        $m= $Mois[date("n", $newTimestamp)];

        $result=$j.' '.$jour.' '.$m.' '.$annee;
        return $result;
    }
}
