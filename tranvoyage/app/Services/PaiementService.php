<?php

namespace App\Services;

use App\Cinetpay\Commande;
use App\Models\Paiement;
use App\Repositories\PaiementRepository;
use Illuminate\Support\Facades\Log;

class PaiementService extends Service{

    private $cinetPay = null;
    public function __construct(PaiementRepository $repository)
    {
        $this->repository = $repository;
    }

    public function findAll(){
        return $this->repository->all();
    }

    public function findOne(int $id){
        return $this->repository->find($id);
    }

    public function create(array $data){
        return $this->repository->save($data);
    }

    public function update(int $id, array $data){
        return $this->repository->save($data, $id);
    }

    public function genrateButton($paiement_id){
        $paiement = Paiement::find($paiement_id);

        // Log::debug("--- paiement ---".$paiement);
        // return "dd";

        date_default_timezone_set("Africa/Abidjan");
        $commande = new Commande();
        $id_transaction = $paiement->id_transaction;
        $description_du_paiement = "Mon ticket de ref: $id_transaction";
        $date_transaction = date("Y-m-d H:i:s");
        $identifiant_du_payeur = $paiement->code;

        //Veuillez entrer votre apiKey
        $apiKey = "2038304885ffddd1d536dc0.56779503";
        //Veuillez entrer votre siteId
        $site_id = "179731"; // à remplacer

        //platform ,  utiliser PROD si vous avez cr�� votre compte sur www.cinetpay.com  ou TEST si vous avez cr�� votre compte sur www.sandbox.cinetpay.com
        $plateform = "PROD";

        //la version ,  utilis� V1 si vous voulez utiliser la version 1 de l'api
        $version = "V2";

        // nom du formulaire CinetPay
        $formName = "goCinetPay";
        // Les liens CinetPay
        $notify_url = '';
        // $return_url = "https://www.monconfort.ci/controlleurs/valider_panier_online.php?id_user={$_SESSION['id_user']}&livraison=$livraison";
        // $return_url = "http://tranvoyagetest.ecademy.ci/return.php?id_user={$_GET['code_ticket']}&livraison={$_GET['nombre_ticket']}";
        $return_url = "http://tranvoyagetest.ecademy.ci/return.php?id_transaction={$id_transaction}";
        $cancel_url = "http://tranvoyage.com/";
        // Configuration du bouton
        $btnType = 2; //1-5
        $btnSize = 'large'; // 'small' pour reduire la taille du bouton, 'large' pour une taille moyenne ou 'larger' pour  une taille plus grande

        // Enregistrement de la commande dans notre BD
        $commande->setTransId($id_transaction);
        $montant_a_payer = 100;
        $commande->setMontant($montant_a_payer);
        $commande->create();

        // Param�trage du panier CinetPay et affichage du formulaire
        $cinetPay = new \App\Cinetpay\CinetPay($site_id, $apiKey, $plateform, $version);
        return $cinetPay->setTransId($id_transaction)
            ->setDesignation($description_du_paiement)
            ->setTransDate($date_transaction)
            ->setAmount($montant_a_payer)
            ->setDebug(false) // Valoris� � true, si vous voulez activer le mode debug sur cinetpay afin d'afficher toutes les variables envoy�es chez CinetPay
            ->setCustom($identifiant_du_payeur) // optional
            ->setNotifyUrl($notify_url) // optional
            ->setReturnUrl($return_url) // optional
            ->setCancelUrl($cancel_url) // optional
            ->displayPayButton($formName, $btnType, $btnSize);
    }

}
