<?php

namespace App\Providers;

// require_once "../cinetpay/cinetpay.php";
// require_once "../cinetpay/commande.php";

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\Models\Paiement;
use App\Cinetpay\Commande;
use App\Cinetpay\CinetPay;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);

        Blade::directive('convertDateInFrench', function ($dat) {
            // dd($dat);
            return $dat;
            $date_explode = explode("-", strval($dat));
            $jour=(int)$date_explode[2];
            $mois=(int)$date_explode[1];
            $annee=(int)$date_explode[0];

            $newTimestamp = mktime(12,0,0,$mois,$jour,$annee); // Cr&eacute;&eacute; le timestamp pour ta date (a midi)

            // Ensuite tu fais un tableau avec les jours en Français
            $Jour = array("Dim","Lun","Mar","Mer","Jeu","Ven","Sam");


            // Pareil pour les mois en FR
            $Mois = array("","Jan","F&eacute;v","Mar","Avr","Mai","Jui","Jui","Ao&ucirc;","Sep","Oct","Nov","D&eacute;c");


            // Enfin tu affiche le libell&eacute; du jour
            $j= $Jour[date("w", $newTimestamp)];

            // Et le libell&eacute; du mois
            $m= $Mois[date("n", $newTimestamp)];

            $result=$j.' '.$jour.' '.$m.' '.$annee;
            // return strval($date_explode[0]);
            return $annee;
        });

        // Generate a button
        Blade::directive('generatebtnCinetpay', function($paiement_id){

        });
    }
}
