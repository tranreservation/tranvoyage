<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

use App\User;

class LoginSuccessful
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        Log::info("--- ConvoisController::LoginSuccessful --- START ");
        $user = User::find($event->user->id);
        $user->last_login = new \DateTime;
        $user->save();
        Log::info("--- ConvoisController::LoginSuccessful --- DONE ");
    }
}
