<?php

namespace App\Repositories;

use App\Models\Voyage;
use App\Repositories\Resources\ResourceRepository;

class VoyageRepository extends ResourceRepository
{

    public function __construct(Voyage $voyage)
    {
        $this->model = $voyage;
    }

}
