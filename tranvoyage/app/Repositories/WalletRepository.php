<?php

namespace App\Repositories;
 
use App\Models\Wallet;
use App\Repositories\Resources\ResourceRepository;
 
class WalletRepository extends ResourceRepository
{

    public function __construct(Wallet $wallet)
    {
        $this->model = $wallet;
    }

}