<?php

namespace App\Repositories\Params;
 
use App\Models\Params\ParamStatusRetrait;
use App\Repositories\Resources\ResourceRepository;
 
class ParamStatusRetraitRepository extends ResourceRepository
{

    public function __construct(ParamStatusRetrait $paramStatusRetrait)
    {
        $this->model = $paramStatusRetrait;
    }

}