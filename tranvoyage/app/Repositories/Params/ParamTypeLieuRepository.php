<?php

namespace App\Repositories\Params;
 
use App\Models\Prams\ParamTypeLieu;
use App\Repositories\Resources\ResourceRepository;
 
class ParamTypeLieuRepository extends ResourceRepository
{

    public function __construct(ParamTypeLieu $paramTypeLieu)
    {
        $this->model = $paramTypeLieu;
    }

}