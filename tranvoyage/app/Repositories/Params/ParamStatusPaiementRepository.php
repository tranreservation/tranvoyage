<?php

namespace App\Repositories\Params;
 
use App\Models\Params\ParamStatusPaiement;
use App\Repositories\Resources\ResourceRepository;
 
class ParamStatusPaiementRepository extends ResourceRepository
{

    public function __construct(ParamStatusPaiement $aramStatusPaiement)
    {
        $this->model = $paramStatusPaiement;
    }

}