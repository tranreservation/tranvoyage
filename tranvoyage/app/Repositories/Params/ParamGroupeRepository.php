<?php

namespace App\Repositories\Params;
 
use App\Models\Params\ParamGroupe;
use App\Repositories\Resources\ResourceRepository;
 
class ParamGroupeRepository extends ResourceRepository
{

    public function __construct(ParamGroupe $paramGroupe)
    {
        $this->model = $paramGroupe;
    }

}