<?php

namespace App\Repositories\Params;
 
use App\Models\Params\ParamLieu;
use App\Repositories\Resources\ResourceRepository;
 
class ParamLieuRepository extends ResourceRepository
{

    public function __construct(ParamLieu $paramLieu)
    {
        $this->model = $paramLieu;
    }

}