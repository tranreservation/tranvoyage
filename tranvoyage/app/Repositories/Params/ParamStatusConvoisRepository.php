<?php

namespace App\Repositories\Params;
 
use App\Models\Prams\ParamStatusConvois;
use App\Repositories\Resources\ResourceRepository;
 
class ParamStatusConvoisRepository extends ResourceRepository
{

    public function __construct(ParamStatusConvois $paramStatusConvois)
    {
        $this->model = $paramStatusConvois;
    }

}