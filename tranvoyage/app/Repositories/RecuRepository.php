<?php

namespace App\Repositories;
 
use App\Models\Recu;
use App\Repositories\Resources\ResourceRepository;
 
class RecuRepository extends ResourceRepository
{

    public function __construct(Recu $recu)
    {
        $this->model = $recu;
    }

}