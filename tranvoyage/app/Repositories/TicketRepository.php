<?php

namespace App\Repositories;

use App\Models\Ticket;
use App\Repositories\Resources\ResourceRepository;

class TicketRepository extends ResourceRepository
{

    public function __construct(Ticket $ticket)
    {
        $this->model = $ticket;
    }

}
