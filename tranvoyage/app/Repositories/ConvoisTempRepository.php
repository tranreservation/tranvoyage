<?php

namespace App\Repositories;

use App\Repositories\Resources\ResourceRepository;
use App\Models\ConvoisTemp;

class ConvoisTempRepository extends ResourceRepository
{

    protected $model;

    public function __construct(ConvoisTemp $convoisTemp){
        $this->model = $convoisTemp;
    }
}