<?php

namespace App\Repositories\Resources;
 
 
class ResourceRepository implements ResourceRepositoryInterface
{
    protected $model;

    public function all(){
        return $this->model->all();
    }

    public function create(array $data){
        return $this->model->create($data);
    }

    /*public function save(array $data){
        foreach ($data as $key => $value) {
            $this->model->$key = $value;
        }
        $this->model->save();
        return $this->model;
    }*/

    public function save(array $data, int $id = null){
        $model = null;
        if($id){
            $model = $this->model->find($id);
        }else{
            $model = $this->model;
        }
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        return $model->save();
    }

    public function show($id){
        return $this->model->find($id);
    }

    public function update(array $data,$id){
        return $this->model->find($id)->update($data);
    }

    public function delete($id){
        return $this->model->find($id)->delete();
    }

    public function findOne(int $id){
        return $this->model->find($id);
    }
    
    public function findById(array $data){
        return $this->model->where($data)->get();
    }

    public function where(array $data){
        return $this->model->where($data)->get();
    }

    public function whereG(array $data){
        return $this->model->where($data);
    }

    public function whereFirst(array $data){
        return $this->model->where($data)->first();
    }

    public function updateWhere(array $data, $tab){
        return $this->model->where($tab)->update($data);
    }

    public function paginate($nb){
        return $this->model->paginate($nb);
    }

    public function paginateWhere(array $tab, $nb){
        return $this->model->where($tab)->paginate($nb);
    }

    public function orderByLimit($id, $sens, $limit){
        return $this->model->orderBy($id, $sens)->limit($limit)->get();
    }

    public function orderByLimit2($id, $sens, $limit, $data){
        return $this->model->orderBy($id, $sens)->limit($limit)->where($data)->get();
    }

}