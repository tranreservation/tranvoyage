<?php

namespace App\Repositories\Resources;

interface ResourceRepositoryInterface
{

    public function all();

    public function create(array $data);

    public function findOne(int $id);

    // public function save(array $data);

    public function save(array $data, int $id);

    public function delete($id);

    public function update(array $data, $id);

    public function show($id);
}