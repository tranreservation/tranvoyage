<?php

namespace App\Repositories;
 
use App\Models\Retrait;
use App\Repositories\Resources\ResourceRepository;
 
class RetraitRepository extends ResourceRepository
{

    public function __construct(Retrait $retrait)
    {
        $this->model = $retrait;
    }

}