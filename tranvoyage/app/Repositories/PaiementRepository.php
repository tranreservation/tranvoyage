<?php

namespace App\Repositories;
 
use App\Models\Paiement;
use App\Repositories\Resources\ResourceRepository;
 
class PaiementRepository extends ResourceRepository
{

    public function __construct(Paiement $paiement)
    {
        $this->model = $paiement;
    }

}