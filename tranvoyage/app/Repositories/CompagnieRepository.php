<?php

namespace App\Repositories;
 
use App\Models\Compagnie;
use App\Repositories\Resources\ResourceRepository;
 
class CompagnieRepository extends ResourceRepository
{

    public function __construct(Compagnie $compagnie)
    {
        $this->model = $compagnie;
    }

}