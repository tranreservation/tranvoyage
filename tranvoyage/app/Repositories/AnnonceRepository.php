<?php

namespace App\Repositories;
 
use App\Models\Annonce;
use App\Repositories\Resources\ResourceRepository;
 
class AnnonceRepository extends ResourceRepository
{

    public function __construct(Annonce $annonce)
    {
        $this->model = $annonce;
    }

}