<?php

namespace App\Repositories;
 
use App\Models\Convois;
use App\Repositories\Resources\ResourceRepository;
 
class ConvoisRepository extends ResourceRepository
{

    public function __construct(Convois $convois)
    {
        $this->model = Convois::where('id','!=',0)->orderby('created_at','desc');
    }

}