<?php

namespace App\Repositories;
 
use App\Models\UserStandard;
use App\Repositories\Resources\ResourceRepository;
 
class UserStandardRepository extends ResourceRepository
{

    public function __construct(UserStandard $userStandard)
    {
        $this->model = $userStandard;
    }

}