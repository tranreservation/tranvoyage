<?php

namespace App\Repositories;
 
use App\Models\Gare;
use App\Repositories\Resources\ResourceRepository;
 
class GareRepository extends ResourceRepository
{

    public function __construct(Gare $gare)
    {
        $this->model = $gare;
    }

}