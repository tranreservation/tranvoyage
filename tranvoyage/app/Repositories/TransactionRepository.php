<?php

namespace App\Repositories;
 
use App\Models\Transaction;
use App\Repositories\Resources\ResourceRepository;
 
class TransactionRepository extends ResourceRepository
{

    public function __construct(Transaction $transaction)
    {
        $this->model = $transaction;
    }

}