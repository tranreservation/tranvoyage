<?php

namespace App\Models;

use App\Admin;
use App\User;
use App\Models\Compagnie;
use App\Models\Gare;
use App\Models\Params\ParamLieu;
// use Illuminate\Database\Eloquent\Model;

class Voyage extends Model
{
    protected $fillable = ["admin_id","user_id","compagnie_id","depart_gare_id","arrive_gare_id","lieu_depart_id","lieu_arrive_id",
    "date_depart","date_arrivee","prix","actif","deleted_at"];

    protected $table = "t_voyages";

    protected $dates = ['date_depart', 'date_arrivee'];


    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function compagnie(){
        return $this->belongsTo(Compagnie::class);
    }

    public function depart_gare(){
        return $this->belongsTo(Gare::class, "depart_gare_id");
    }

    public function arrivee_gare(){
        return $this->belongsTo(Gare::class, "arrive_gare_id");
    }

    public function lieu_depart(){
        return $this->belongsTo(ParamLieu::class, "lieu_depart_id");
    }

    public function lieu_arrivee(){
        return $this->belongsTo(ParamLieu::class, "lieu_arrive_id");
    }
}
