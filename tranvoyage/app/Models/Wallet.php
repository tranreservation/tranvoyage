<?php

namespace App\Models;

use App\Admin;
use App\User;
use App\Compagnie;
use App\Gare;
// use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = "t_wallets";
    
    protected $fillable = ["user_id","solde"];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
