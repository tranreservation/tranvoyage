<?php

namespace App\Models;

use App\Admin;
use App\User;
use App\Compagnie;
use App\Gare;
// use Illuminate\Database\Eloquent\Model;

class UserStandard extends Model
{

    protected $table = "t_users_standards";

    protected $fillable = ["telephone","email","actif","deleted_at"];
}
