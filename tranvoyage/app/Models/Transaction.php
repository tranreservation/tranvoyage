<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Admin;

class Transaction extends Model
{

    protected $table = "t_transactions";
    
    protected $fillable = ["user_id","admin_id","montant_transaction","montant_avant_transaction",
    "montant_apres_transaction"];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
}
