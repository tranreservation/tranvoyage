<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;

class Compagnie extends Model
{

    protected $table = "t_compagnies";
    
    protected $fillable = ["nom","email","telephone","actif","deleted_at"];
}
