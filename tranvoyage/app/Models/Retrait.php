<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Params\ParamStatusRetrait;
use App\Models\Transaction;

class Retrait extends Model
{

    protected $table = "t_retraits";
    
    protected $fillable = ["params_status_retrait_id","transaction_id","montant_retrait","commentaire",
    "date_traitement","actif","deleted_at","user_id","code","reference_mooney"];

    public function statusretrait(){
        return $this->belongsTo(ParamStatusRetrait::class, "params_status_retrait_id");
    }

    public function transaction(){
        return $this->belongsTo(Transaction::class);
    }
}
