<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Params\ParamStatusConvois;
use App\User;
use App\Admin;

class Convois extends Model
{
    protected $table = "t_convois";
   
    //Cette propriété permet de Fillé tous les champs excepté les champs created_at et updated_at qui sont timestamps.
    protected $guarded = [];
    
    // protected $fillable = ["params_status_convois_id","user_id","lieu_depart","lieu_arrivee","lieu_rassemblement","code"
    // ,"description","prix","nbre_ticket","ticket_disponible","actif","deleted_at","libelle","date_depart","date_arrivee"];

   // protected $dates = [];

    public function statusConvois(){
        return $this->belongsTo(ParamStatusConvois::class, "params_status_convois_id");
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
}
