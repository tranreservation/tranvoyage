<?php

namespace App\Models\Params;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Model;

class ParamTypeLieu extends Model
{
    protected $fillable = ["libelle", "description", "actif", "deleted_at"];
}
