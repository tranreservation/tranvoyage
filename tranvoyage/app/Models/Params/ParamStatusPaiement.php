<?php

namespace App\Models\Params;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Model;

class ParamStatusPaiement extends Model
{

    protected $table = "t_params_status_paiement";

    protected $fillable = ["libelle", "description", "actif", "deleted_at"];
}
