<?php

namespace App\Models\Params;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Params\ParamTypeLieu;
use App\Models\Model;


class ParamLieu extends Model
{

    protected $table = "t_params_lieu";

    protected $fillable = ["libelle", "description", "actif", "deleted_at","params_type_lieu_id"];

    public function typeLieu()
    {
        return $this->belongsTo(ParamTypeLieu::class);
    }
}