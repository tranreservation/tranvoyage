<?php

namespace App\Models\Params;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Model;

class ParamGroupe extends Model
{
    protected $fillable = ["libelle", "description", "actif", "deleted_at"];
}
