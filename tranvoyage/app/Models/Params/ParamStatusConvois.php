<?php

namespace App\Models\Params;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Model;

class ParamStatusConvois extends Model
{
    protected $table = "t_params_status_convois";
    
    protected $fillable = ["libelle", "description", "actif", "deleted_at"];
}
