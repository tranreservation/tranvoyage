<?php

namespace App\Models\Params;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Model;

class ParamStatusRetrait extends Model
{

    protected $table = "t_params_status_retrait";

    protected $fillable = ["libelle", "description", "actif", "deleted_at"];
}
