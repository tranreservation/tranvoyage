<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Paiement;

class Recu extends Model
{
    protected $fillable = ["paiement_id","date_validation","date_refus","cause_refus_recu",
    "commentaire","code","actif","deleted_at"];

    public function paiement(){
        return $this->belongsTo(Paiement::class);
    }
}
