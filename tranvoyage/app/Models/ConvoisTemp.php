<?php

namespace App\Models;

use App\Models\Model;

class ConvoisTemp extends Model{

    protected $table = "t_convois_temp";

    protected $fillable = ["code","nom_convois","description","prix","nom_prenom_utilisateur","tel","email","email_checked"];
}