<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Params\ParamStatusTicket;
use App\Models\Transaction;

class Ticket extends Model
{
    protected $table = "t_tickets";

    protected $fillable = ["paiement_id","params_status_ticket_id","vendeur_admin_id","vendeur_id","code","actif","deleted_at"];

    public function statusTicket(){
        return $this->belongsTo(ParamStatusTicket::class, "params_status_ticket_id");
    }
}
