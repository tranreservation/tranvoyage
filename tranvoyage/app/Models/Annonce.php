<?php

namespace App\Models;

use App\User;

class Annonce extends Model
{
    protected $fillable = ["user_id","admin_id","libelle","description","actif","deleted_at"];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
}
