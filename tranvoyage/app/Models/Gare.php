<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;


use App\Models\Params\ParamLieu;
use App\Models\Compagnie;

class Gare extends Model
{
    protected $table = "t_gares";

    protected $fillable = ["compagnie_id","libelle","params_lieu_id","actif","deleted_at"];

    public function compagnie(){
        return $this->belongsTo(Compagnie::class);
    }

    public function lieu(){
        return $this->belongsTo(ParamLieu::class);
    }
}
