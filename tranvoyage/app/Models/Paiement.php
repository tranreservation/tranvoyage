<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use App\Models\Voyage;
use App\Models\Convois;
use App\Models\User;
use App\Models\Params\ParamStatusPaiement;
use App\Models\UserStandard;

class Paiement extends Model
{
    protected $table = "t_paiements";

    protected $fillable = ["voyage_id","convois_id","user_id","user_standard_id","id_transaction","params_status_paiement_id",
    "montant_total","montant_ticket_unitaire","code","nombre_ticket","deleted_at","actif","deleted_at"];

    public function voyage(){
        return $this->belongsTo(Voyage::class);
    }

    public function convois(){
        return $this->belongsTo(Convois::class);
    }

    public function statusPaiement(){
        return $this->belongsTo(ParamStatusPaiement::class);
    }

    public function userstandard(){
        return $this->belongsTo(UserStandard::class, 'user_standard_id');
    }
}
