<?php

namespace App\Http\Controllers\Params;

use App\Http\Controllers\Controller;
use App\Services\Params\ParamGroupeService;

class ParamGroupeController extends Controller{

    public function __construct(ParamGroupeService $service)
    {
        $this->service = $service;
    }

    public function index(){
        $groupes = $this->service->all();
        return view('admins.pages.groupe.index', compact('groupes','i'));
    }

}