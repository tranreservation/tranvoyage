<?php

namespace App\Http\Controllers;

use App\Helps\Help;
use App\Repositories\VoyageRepository;
use App\Repositories\GareRepository;
use App\Repositories\Params\ParamLieuRepository;
use Illuminate\Http\Request;
use App\Models\Voyage;
use App\Repositories\ConvoisRepository;
use App\Models\Convois;

class HomeController extends Controller
{
    private $voyageRepository;
    private $gareRepository;
    private $paramLieuRepository;
    private $convoisRepository;


    public function __construct(VoyageRepository  $voyageRepository, GareRepository $gareRepository,
    ParamLieuRepository $paramLieuRepository, ConvoisRepository $convoisRepository)
    {
        $this->voyageRepository = $voyageRepository;
        $this->gareRepository = $gareRepository;
        $this->paramLieuRepository = $paramLieuRepository;
        $this->convoisRepository = $convoisRepository;
        // $this->middleware('auth');
    }

    // public function index(Request $request)
    // {

    //     // Récupération de toutes les gars
    //     $gares = $this->gareRepository->all();
    //     $lieux = $this->paramLieuRepository->where(['params_type_lieu_id' => 2]);
    //     $data = $request->except('date_depart','page');

    //     foreach ($data as $key => $value) {
    //         if(strlen($value) == 0){
    //             unset($data[$key]);
    //         }
    //     }

    //     // dd($data);

    //     $date_jo = gmdate("Y-m-d");
    //     // dd($date_jo);
    //     // dd(Help::convertDateInFrench("2021-07-15"));
    //     // $voyages = $this->voyageRepository->all();
    //     // $voyages = $this->voyageRepository->paginate(6);
    //     // $query = Voyage::where($data);
    //     $query = Voyage::where([]);
    //     if(strlen($request->get('date_depart')) > 0){
    //         $query = $query->whereDate('date_depart', date('Y-m-d', strtotime($request->get('date_depart'))));
    //         $data['date_depart'] = $request->get('date_depart');
    //     }

    //     if(strlen($request->has('lieu_depart_id')) > 0){
    //         $lieux_searchs = $this->paramLieuRepository->where(['sup_params_lieu_id' => $request->get('lieu_depart_id')]);
    //         // foreach ($lieux_searchs as $key => $value) {

    //         // }
    //         $query->where('lieu_depart_id', $request->get('lieu_depart_id'));
    //         $query->orWhere(function($querySub) use($lieux_searchs) {
    //             foreach ($lieux_searchs as $key => $value) {
    //                 $querySub->orWhere('lieu_depart_id', $value->id);
    //             }
    //         });
    //     }

    //     if(strlen($request->get('lieu_arrive_id')) > 0){
    //         $query = $query->where('lieu_arrive_id', $request->get('lieu_arrive_id'));
    //     }
    //     // dd($query);
    //     // $voyages = $this->voyageRepository->paginateWhere($data, 6);
    //     $voyages = $query->paginate(6);


    //     return view('home', compact('voyages','gares','data','lieux'));
    // }

    public function index(Request $request){
        $code = $request->get('code');
       // dd($code);
        if($code && strlen($code) > 0){
           // $convois = $this->convoisRepository->paginateWhere(['params_status_convois_id' => 2, 'code' => $code, 'communaute' => $code], 10);
           $convois = Convois::where('id','!=','0')->Where('params_status_convois_id','=','2')->Where('code','=',$code)->orWhere('communaute','LIKE',"%{$code}%")->orderby('created_at', 'desc')->paginate(10);
        }else{
            //Quand le paramètre de status est {1} le convois est validé et quand c'est {2} c'est en attente
            $convois = $this->convoisRepository->paginateWhere(['params_status_convois_id' => 2], 10);
        }
        return view('user.pages.convois.index', compact('convois','code'));
    }

    public function firstForm(){
        return view('user.pages.user.form_to_end_convois');
    }

    public function dashboard(){
        return view('user.pages.dashboard.dashboard');
    }

}
