<?php

namespace App\Http\Controllers\User;

// require_once  "../../../cinetpay/cinetpay.php";
// require_once  "../../../cinetpay/commande.php";

use App\Http\Controllers\Controller;
use App\Repositories\PaiementRepository;
use App\Repositories\UserStandardRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\VoyageRepository;
use App\Repositories\ConvoisRepository;
use App\Repositories\TicketRepository;
use Illuminate\Http\Request;
// use CinetPay;
// use App\Cinetpay\Commande;
use App\Cinetpay\CinetPay;
use Illuminate\Support\Facades\Log;
use Mail;
use DB;

class ReservationConvoisController extends Controller{

    private $voyageRepository;
    private $userStandardRepository;
    private $paiementRepository;
    private $convoisRepository;
    private $transactionRepository;
    private $ticketRepository;

    public function __construct(VoyageRepository  $voyageRepository, UserStandardRepository $userStandardRepository, PaiementRepository $paiementRepository,
    ConvoisRepository $convoisRepository, TransactionRepository $transactionRepository, TicketRepository $ticketRepository){
        $this->voyageRepository = $voyageRepository;
        $this->userStandardRepository = $userStandardRepository;
        $this->paiementRepository = $paiementRepository;
        $this->convoisRepository = $convoisRepository;
        $this->transactionRepository = $transactionRepository;
        $this->ticketRepository = $ticketRepository;
    }

    public function premiereEtape(Request $request){
        $code = $request->get('code');
        $convois = $this->convoisRepository->whereFirst(['code' => $code]);
        return view('user.pages.convois.reservation.premiere_etape', compact('convois'));
    }

    public function deuxiemeEtape(Request $request){
          // dd($request);
        $convois_id = $request->get('convois_id');
        $convois = $this->voyageRepository->findOne($convois_id);
        $nbre_tickets = $request->get('nbre_tickets');
        $montant_total = $request->get('total_tickets');
        $tel_ticket = $request->get('tel_ticket');
        $email = $request->get('email');

        // Création d'un utilisateur
        $user = $this->createUserStandart($tel_ticket, $email);

        // Création d'un paiement standart
        $paiement = $this->generatePaiement($convois_id, $user->id, $montant_total, $convois->prix, $nbre_tickets);

        return view('user.pages.convois.reservation.deuxieme_etape', compact('convois','nbre_tickets',
            'montant_total','tel_ticket','email', 'paiement'));
    }

    public function successEtape(Request $request){
        $id_transaction = $request->get('id_transaction');
        // dd($id_transaction);
        $paiement = $this->paiementRepository->whereG(['id_transaction' => $id_transaction])->first();
        $statut = $paiement && $paiement->params_status_paiement_id == 3 ? true : false;
        return view('user.pages.convois.reservation.success_etape', compact('paiement','statut'));
    }

    public function show($code){
        $convois = $this->convoisRepository->whereFirst(['code' => $code]);
        return view('user.pages.convois.show', compact('convois'));
    }

    public function callback(Request $request){
        try
        {
            DB::beginTransaction();
            Log::info("--- ReservationController::callback --- START ---");
            $id_transaction = $request->get('cpm_trans_id');
            $code_ticket = "";

            // Initialisation de CinetPay et Identification du paiement
            // $id_transaction = $_POST['cpm_trans_id'];
            //Veuillez entrer votre apiKey et site ID

            /*$apiKey = "2038304885ffddd1d536dc0.56779503";
            //Veuillez entrer votre siteId
            $site_id = "179731"; // à remplacer
            $plateform = "PROD";
            $version = "V1";
            $CinetPay = new CinetPay($site_id, $apiKey, $plateform, $version);
            $CinetPay->setTransId($id_transaction)->getPayStatus();
            $cpm_site_id = $CinetPay->_cpm_site_id;
            $signature = $CinetPay->_signature;
            $cpm_amount = $CinetPay->_cpm_amount;
            $cpm_trans_id = $CinetPay->_cpm_trans_id;
            $cpm_custom = $CinetPay->_cpm_custom;
            $cpm_currency = $CinetPay->_cpm_currency;
            $cpm_payid = $CinetPay->_cpm_payid;
            $cpm_payment_date = $CinetPay->_cpm_payment_date;
            $cpm_payment_time = $CinetPay->_cpm_payment_time;
            $cpm_error_message = $CinetPay->_cpm_error_message;
            $payment_method = $CinetPay->_payment_method;
            $cpm_phone_prefixe = $CinetPay->_cpm_phone_prefixe;
            $cel_phone_num = $CinetPay->_cel_phone_num;
            $cpm_ipn_ack = $CinetPay->_cpm_ipn_ack;
            $created_at = $CinetPay->_created_at;
            $updated_at = $CinetPay->_updated_at;
            $cpm_result = $CinetPay->_cpm_result;
            $cpm_trans_status = $CinetPay->_cpm_trans_status;
            $cpm_designation = $CinetPay->_cpm_designation;
            $buyer_name = $CinetPay->_buyer_name;*/

            // Aucun enregistrement dans la base de donnée ici
            $cpm_result = "00";
            if ($cpm_result == '00')
            {
                // une page HTML de paiement bon
                // echo 'Félicitation, votre paiement a été effectué avec succès';

                // Recupération du paiement
                $paiement = $this->paiementRepository->whereFirst(["id_transaction" => $id_transaction, "params_status_paiement_id" => 1]);

                // On met à jour le statut du paiement
                $this->paiementRepository->update(["params_status_paiement_id" => 3], $paiement->id);
                $id_transaction = $paiement->id_transaction;
                /*$sqlQuery = $con->query("SELECT * FROM paiement WHERE (id_transaction='" . $id_transaction . "')");
                $count = $sqlQuery->rowCount();
                $uti = $sqlQuery->fetch();

                $sqlQuery1 = $con->query("SELECT * FROM compagnie WHERE (id_compagnie='" . $uti['compagnie_paiement_id'] . "')"); //compagnie_paiement_id
                $uti1 = $sqlQuery1->fetch();

                $sqlreq1 = $con->query("SELECT * FROM gare WHERE (id_gare='" . $uti['depart_id'] . "')");
                $arr = $sqlreq1->fetch();

                $sqlreq2 = $con->query("SELECT * FROM gare WHERE (id_gare='" . $uti['arrive_id'] . "')"); //destination_paiement_ticket_id
                $dep = $sqlreq2->fetch();

                $sqlreq3 = $con->query("SELECT * FROM destination WHERE (id_destination='" . $uti['destination_paiement_ticket_id'] . "')"); //destination_paiement_ticket_id
                $dest = $sqlreq3->fetch();
                $tel = $uti['telephone_paiement'];
                $sender = "TRANVOYAGE";
                $code_ticket = $uti['code_ticket'];
                $email = $uti['email_paiement'];*/

                // Envoi de sms
                // sendSmsCodeTicketAfterPayTicket($tel, $code_ticket, $uti, $uti1, $dep['nom_gare'], $arr['nom_gare'], $dest);

                // Generate ticket
                $this->generateTicket($paiement);

                // Envoi de mail
                $this->sendMailAfterPayTicket($paiement);

                // Generate transaction
                $this->updateWallet($paiement->convois->user, $paiement->montant_total);

                Log::info("--- ReservationController::callback --- Le callback a été validé ---");
            }else{
                // Recupération du paiement
                $paiement = $this->paiementRepository->whereFirst(["id_transaction" => $id_transaction]);
                // On met à jour le statut du paiement
                $this->paiementRepository->update(["status_paiement" => 4], $paiement->id);
                Log::info("--- ReservationController::callback --- Souccis de validation du callback ---");
            }
            Log::info("--- ReservationController::callback --- DONE ---");

            DB::commit();

            return redirect()->route('user.convois.reservation.successtape', ['id_transaction='.$id_transaction]);
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            // echo "Erreur :" . $e->getMessage();
            // Une erreur s'est produite
            Log::info("--- ReservationController::callback --- Une erreur est survenue lors de la validation du callback ".$e->getMessage()."---");
            throw new \Exception('Une erreur est survenue !');
        }
    }

    // Generate un paiement
    private function generatePaiement($convois_id, $user_id , $montant_total, $montant_ticket_unitaire, $nombre_ticket){
        // dd($convois_id, $user_id , $montant_total, $montant_ticket_unitaire, $nombre_ticket);
        return $this->paiementRepository->create([
            "convois_id" => $convois_id,
            "user_standard_id" => $user_id,
            "params_status_paiement_id" => 1, // Le paimenent a été initié
            "montant_total" => $montant_total,
            "montant_ticket_unitaire" => $montant_ticket_unitaire,
            "code" => $this->generateCode(),
            "nombre_ticket" => $nombre_ticket,
          //  "id_transaction" => '7607'
           "id_transaction" => $this->getIdTransactionFromCinetPay()
        ]);
    }

    // Create a user
    private function createUserStandart($telephone, $email){
        return $this->userStandardRepository->create([
            "telephone" => $telephone,
            "email" => $email,
            "actif" => 1
        ]);
    }

    private function generateCode(){
        $se = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ@';
        $numero = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $code_ticket = '';
        $num = '';

        for ($i = 0;$i < 7;$i++)
        {
            $code_ticket .= substr($numero, rand() % (strlen($numero)) , 1);
        }
        return $code_ticket;
    }

    // Get id transaction from cineptay
    private function getIdTransactionFromCinetPay(){
       
        return Cinetpay::generateTransId();
      // return "787";
    }

    // Function d'envoi de mail après que le paiement est été effectué
    private function sendMailAfterPayTicket($paiement)
    {
        $email = $paiement->userstandard->email;
        $data['telephone'] = $paiement->userstandard->telephone;
        $data['lieu_depart'] = $paiement->convois->lieu_depart;
        $data['lieu_arrivee'] = $paiement->convois->lieu_arrivee;
        $data['date_depart'] = date("d-m-Y", strtotime($paiement->convois->date_depart));
        $data['heure_depart'] = date("h:i:s", strtotime($paiement->convois->date_depart));
        $data['entreprise'] = "";
        $data['nombre_ticket'] = $paiement->nombre_ticket;
        $data['prix'] = number_format($paiement->convois->prix, 2, '.', ' ');
        $data['montant_total'] = number_format($paiement->montant_total, 2, '.', ' ');
        $data['tickets'] = $this->ticketRepository->where(['paiement_id' => $paiement->id]);
        Mail::send('mails.buy_ticket_convois', $data, function($message) use($email) {
            $message->to($email, 'RESERVATION TICKETS')->subject
                ('Réservation de tickets');
            $message->from('admin@tranvoyage.com','Admin tranvoyage');
        });
    }

    // Update wallet
    private function updateWallet($user, $montant){

        $newSolde = $user->wallet->solde + $montant;

        // Create transaction
        $this->transactionRepository->create([
            "user_id" => $user->id,
            "montant_transaction" => $montant,
            "montant_avant_transaction" => $user->wallet->solde,
            "montant_apres_transaction" => $newSolde
        ]);

        // Update user'wallet
        $wallet = $user->wallet;
        $wallet->solde = $newSolde;
        $wallet->save();
    }

    // Generate ticket
    private function generateTicket($paiement){
        $vendeur_id = $paiement->convois->user ? $paiement->convois->user->id : null;
        $vendeur_admin_id = $paiement->convois->admin ? $paiement->convois->admin->id : null;
        for ($i=0; $i < $paiement->nombre_ticket; $i++) {
            $this->ticketRepository->create([
                "paiement_id" => $paiement->id,
                "params_status_ticket_id" => 1, // Par defaut le ticket est valide
                "vendeur_id" => $vendeur_id,
                "vendeur_admin_id" => $vendeur_admin_id,
                "code" => $this->generateCode(),
                "actif" => 1
            ]);
        }
    }

}
