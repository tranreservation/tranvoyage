<?php

namespace App\Http\Controllers\User\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Wallet;


class HomeController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(){
        $user  = Auth::user();
        $wallet = $user->wallet;
        // $wallet = Wallet::where('user_id', $user->id)->first();
        // dd($wallet);
        return view('user.pages.dashboard.dashboard', compact('wallet'));
    }

}
