<?php

namespace App\Http\Controllers\User\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Mail;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Retrait;
use App\Models\Ticket;

class RetraitController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $user  = Auth::user();
        $wallet = $user->wallet;
        $retraits = Retrait::where(['user_id' => $user->id])->orderBy('created_at', 'desc')->paginate(10);
        $i = 1;
        return view('user.pages.dashboard.retraits.index', compact('wallet', 'retraits', 'i'));
    }

    public function create(){
        $user  = Auth::user();
        $wallet = $user->wallet;
        return view('user.pages.dashboard.retraits.create', compact('wallet'));
    }

    public function store(Request $request){
        $data = $request->except('_token');
        $data['user_id'] = Auth::user()->id;
        $data['params_status_retrait_id'] = 1;
        $data['code'] = $this->generateCode();
        Retrait::create($data);
        return redirect()->route('user.retraits.index');
    }

    private function generateCode($length = 7){
        $se = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ@';
        $numero = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $code_ticket = '';
        $num = '';

        for ($i = 0;$i <= $length;$i++)
        {
            $code_ticket .= substr($numero, rand() % (strlen($numero)) , 1);
        }
        return $code_ticket;
    }
}