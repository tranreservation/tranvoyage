<?php

namespace App\Http\Controllers\User\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Mail;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;
use App\Models\Ticket;

class TicketController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $user  = Auth::user();
        $wallet = $user->wallet;
        $tickets = Ticket::where(['vendeur_id' => $user->id])->orderBy('created_at', 'desc')->paginate(10);
        $i = 1;
        return view('user.pages.dashboard.tickets.index', compact('wallet', 'tickets', 'i'));
    }

    public function edit($code, Request $request){
        $user  = Auth::user();
        $wallet = $user->wallet;
        $ticket = Ticket::where('code', $code)->first();
        $statut = $request->get('statut');
        return view('user.pages.dashboard.tickets.edit', compact('wallet', 'ticket','statut'));
    }

    public function update($code, Request $request){
        Ticket::where('code', $code)->update(['params_status_ticket_id' => $request->get('params_status_ticket_id')]);
        return redirect()->route('user.tickets.index');
    }
}