<?php

namespace App\Http\Controllers\User\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Mail;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;

class TransactionController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $user  = Auth::user();
        $wallet = $user->wallet;
        $transactions = Transaction::where(['user_id' => $user->id])->orderBy('created_at', 'desc')->paginate(10);
        $i = 1;
        return view('user.pages.dashboard.transactions.index', compact('wallet', 'transactions', 'i'));
    }
}