<?php

namespace App\Http\Controllers\User\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Mail;

use App\Http\Controllers\Controller;
use App\Repositories\ConvoisTempRepository;
use App\Repositories\ConvoisRepository;
use App\Repositories\WalletRepository;
use App\Repositories\PaiementRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Models\Convois;

class ConvoisController extends Controller{

    private $convoisTempRepository;
    private $convoisRepository;
    private $paiementRepository;
    private $walletRepository;
    private $wallet;

    public function __construct(ConvoisTempRepository $convoisTempRepository, ConvoisRepository $convoisRepository,
    WalletRepository $walletRepository, PaiementRepository $paiementRepository){
        $this->convoisTempRepository = $convoisTempRepository;
        $this->convoisRepository = $convoisRepository;
        $this->walletRepository = $walletRepository;
        $this->paiementRepository = $paiementRepository;
        $this->middleware('auth');
    }

    public function index(){
        $user  = Auth::user();
        $wallet = $user->wallet;

        $convois = Convois::where(['user_id' => $user->id])->orderBy('created_at', 'desc')->limit(5)->paginate(10);
        $i = 1;
        return view('user.pages.dashboard.convois.index', compact('wallet', 'convois', 'i'));
    }

    public function show($id){
        $user  = Auth::user();
        $wallet = $user->wallet;
        $convois = $this->convoisRepository->findOne($id);
        $paiements = $this->paiementRepository->paginateWhere(['convois_id' => $convois->id,"params_status_paiement_id" => 3], 10);
        return view('user.pages.dashboard.convois.show', compact('wallet', 'convois','paiements'));
    }

    public function create(){
        $user  = Auth::user();
        $wallet = $user->wallet;
        return view('user.pages.dashboard.convois.create', compact('wallet'));
    }

    public function store(Request $request){

        $user  = Auth::user();
        $date_oj = gmdate('Y-m-d');
        $checkConvoisExist = Convois::where('libelle','=', $request->libelle)->where('user_id','=', $user->id)->get();
        $count = Count($checkConvoisExist);
        //dd($count);
        if ($request->date_depart < $date_oj) {
            return back()->with('error','Désolé la date de départ ne doit pas être inférieur à celle du jour !')->withInput();
        }elseif ($request->date_depart > $request->date_arrivee) {
            return back()->with('error','Désolé la date de départ ne doit pas être supérieur à celle d\'arrivée !')->withInput();
        }elseif ($count>0) {
            return back()->with('error','Ce nom de convoit existe déjà .')->withInput();
        } else {
            $data = $request->except('_token');
            //dd($data);
            $data["user_id"] = $user->id;
            $data["params_status_convois_id"] = 1;
            $data["code"] = $this->generateCode();
            if ($request->hasFile('image_file')) {
                //je recupere le nom de l'image du fichier
                  //dd($request->file('image_file'));
                $name = $request->file('image_file')->getClientOriginalName();
                //je crée le nom du dossier
                $pathName = 'photos';
                //je place le fichier dans le dossier que j'ai spécifié
                $request->file('image_file')->move($pathName, $name);
                //j'affecte le chemin du dossier à l'attribut image
                $data["image_file"] = $pathName."/".$name;
            }

           // dd($data);



            //$this->convoisRepository->create($data);
            Convois::create($data);
            return redirect()->route('user.convois.index');
        }

    }

    public function delete($id){
        $user  = Auth::user();
        $wallet = $user->wallet;
        $convois = $this->convoisRepository->findOne($id);
        return view('user.pages.dashboard.convois.delete', compact('wallet', 'convois'));
    }

    public function remove($id){
        $this->convoisRepository->delete($id);
        return redirect()->route('user.convois.index');
    }

    private function generateCode($length = 7){
        $se = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ@';
        $numero = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $code_ticket = '';
        $num = '';

        for ($i = 0;$i <= $length;$i++)
        {
            $code_ticket .= substr($numero, rand() % (strlen($numero)) , 1);
        }
        return $code_ticket;
    }
}
