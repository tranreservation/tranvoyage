<?php

namespace App\Http\Controllers\User\Dashboard;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;


class UserController extends Controller{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(){
        $user  = Auth::user();
        $wallet = $user->wallet;
        return view('user.pages.dashboard.profile.edit', compact('wallet','user'));
    }

    public function update($id, Request $request){
        User::where('id', $id)->update([
            'password' => Hash::make($request->get('password')),
            'telephone' => $request->get('telephone'),
            'name' => $request->get('name')
        ]);
        return redirect()->route('user.profile.edit');
    }

}
