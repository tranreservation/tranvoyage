<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Paiement;
use App\Models\Ticket;

class TicketController extends Controller{

    public function viewRechercherTicket(){
        return view('user.pages.ticket.rechercher_ticket');
    }

    public function rechercherTicket(Request $request){
        $telephone = $request->get('telephone');
        $paiements = Paiement::where('telephone_paiement', $telephone)->get();
        $statutSearch = "QDQSGDSQBEVXTTEHAIXDDTEDOHT0125445";
        foreach ($paiements as $paiement){
            $tickets = Ticket::where('paiement_id', $paiement->id)->get();
            foreach ($tickets as $ticket){
                if($ticket->statusTicket->id == 1){
                    $statutSearch = "QDQSGDSQBEVXTTEHAIXDDTEDOHT012544501";
                }
            }
        }

        // Envoyé les tickets disponibles
        return redirect()->route('user.ticket.rechercher.resultat', ['statut' => $statutSearch]);
    }

    public function resultatRechercherTicket($statut){
        $response = $statut == "QDQSGDSQBEVXTTEHAIXDDTEDOHT012544501" ? true : false;
        return view('user.pages.ticket.resultat_rechercher_ticket', compact('response'));
    }

}
