<?php

namespace App\Http\Controllers\User;

// require_once  "../../../cinetpay/cinetpay.php";
// require_once  "../../../cinetpay/commande.php";

use App\Http\Controllers\Controller;
use App\Repositories\PaiementRepository;
use App\Repositories\UserStandardRepository;
use App\Repositories\VoyageRepository;
use Illuminate\Http\Request;
// use CinetPay;
// use App\Cinetpay\Commande;
use App\Cinetpay\CinetPay;
use Illuminate\Support\Facades\Log;
use Mail;

class ReservationController extends Controller{

    private $voyageRepository;
    private $userStandardRepository;
    private $paiementRepository;

    public function __construct(VoyageRepository  $voyageRepository, UserStandardRepository $userStandardRepository, PaiementRepository $paiementRepository){
        $this->voyageRepository = $voyageRepository;
        $this->userStandardRepository = $userStandardRepository;
        $this->paiementRepository = $paiementRepository;
    }

    public function premiereEtape(Request $request){
        $voyage_id = $request->get('voyage_id');
        $voyage = $this->voyageRepository->findOne($voyage_id);
        return view('user.pages.reservation.premiere_etape', compact('voyage'));
    }

    public function deuxiemeEtape(Request $request){
      //  dd($request);
        $voyage_id = $request->get('voyage_id');
        $voyage = $this->voyageRepository->findOne($voyage_id);
        $nbre_tickets = $request->get('nbre_tickets');
        $montant_total = $request->get('total_tickets');
        $tel_ticket = $request->get('tel_ticket');
        $email = $request->get('email');

        // Création d'un utilisateur
        $user = $this->createUserStandart($tel_ticket, $email);

        // Création d'un paiement standart
        $paiement = $this->generatePaiement($voyage_id, $user->id, $montant_total, $voyage->prix, $nbre_tickets);

        return view('user.pages.reservation.deuxieme_etape', compact('voyage','nbre_tickets',
            'montant_total','tel_ticket','email', 'paiement'));
    }

    public function successEtape(Request $request){
        $id_transaction = $request->get('id_transaction');
        // dd($id_transaction);
        $paiement = $this->paiementRepository->whereG(['id_transaction' => $id_transaction])->first();
        $statut = $paiement && $paiement->params_status_paiement_id == 3 ? true : false;
        return view('user.pages.reservation.success_etape', compact('paiement','statut'));
    }

    public function callback(Request $request){
        try
        {
            Log::info("--- ReservationController::callback --- START ---");
            $id_transaction = $request->get('cpm_trans_id');
            $code_ticket = "";

            // Initialisation de CinetPay et Identification du paiement
            // $id_transaction = $_POST['cpm_trans_id'];
            //Veuillez entrer votre apiKey et site ID

            /*$apiKey = "2038304885ffddd1d536dc0.56779503";
            //Veuillez entrer votre siteId
            $site_id = "179731"; // à remplacer
            $plateform = "PROD";
            $version = "V1";
            $CinetPay = new CinetPay($site_id, $apiKey, $plateform, $version);
            $CinetPay->setTransId($id_transaction)->getPayStatus();
            $cpm_site_id = $CinetPay->_cpm_site_id;
            $signature = $CinetPay->_signature;
            $cpm_amount = $CinetPay->_cpm_amount;
            $cpm_trans_id = $CinetPay->_cpm_trans_id;
            $cpm_custom = $CinetPay->_cpm_custom;
            $cpm_currency = $CinetPay->_cpm_currency;
            $cpm_payid = $CinetPay->_cpm_payid;
            $cpm_payment_date = $CinetPay->_cpm_payment_date;
            $cpm_payment_time = $CinetPay->_cpm_payment_time;
            $cpm_error_message = $CinetPay->_cpm_error_message;
            $payment_method = $CinetPay->_payment_method;
            $cpm_phone_prefixe = $CinetPay->_cpm_phone_prefixe;
            $cel_phone_num = $CinetPay->_cel_phone_num;
            $cpm_ipn_ack = $CinetPay->_cpm_ipn_ack;
            $created_at = $CinetPay->_created_at;
            $updated_at = $CinetPay->_updated_at;
            $cpm_result = $CinetPay->_cpm_result;
            $cpm_trans_status = $CinetPay->_cpm_trans_status;
            $cpm_designation = $CinetPay->_cpm_designation;
            $buyer_name = $CinetPay->_buyer_name;*/

            // Aucun enregistrement dans la base de donnée ici
            $cpm_result = "00";
            if ($cpm_result == '00')
            {
                // une page HTML de paiement bon
                // echo 'Félicitation, votre paiement a été effectué avec succès';

                // Recupération du paiement
                $paiement = $this->paiementRepository->whereFirst(["id_transaction" => $id_transaction]);

                if($paiement){
                    // On met à jour le statut du paiement
                    $this->paiementRepository->update(["params_status_paiement_id" => 3], $paiement->id);
                    $id_transaction = $paiement->id_transaction;
                    /*$sqlQuery = $con->query("SELECT * FROM paiement WHERE (id_transaction='" . $id_transaction . "')");
                    $count = $sqlQuery->rowCount();
                    $uti = $sqlQuery->fetch();

                    $sqlQuery1 = $con->query("SELECT * FROM compagnie WHERE (id_compagnie='" . $uti['compagnie_paiement_id'] . "')"); //compagnie_paiement_id
                    $uti1 = $sqlQuery1->fetch();

                    $sqlreq1 = $con->query("SELECT * FROM gare WHERE (id_gare='" . $uti['depart_id'] . "')");
                    $arr = $sqlreq1->fetch();

                    $sqlreq2 = $con->query("SELECT * FROM gare WHERE (id_gare='" . $uti['arrive_id'] . "')"); //destination_paiement_ticket_id
                    $dep = $sqlreq2->fetch();

                    $sqlreq3 = $con->query("SELECT * FROM destination WHERE (id_destination='" . $uti['destination_paiement_ticket_id'] . "')"); //destination_paiement_ticket_id
                    $dest = $sqlreq3->fetch();
                    $tel = $uti['telephone_paiement'];
                    $sender = "TRANVOYAGE";
                    $code_ticket = $uti['code_ticket'];
                    $email = $uti['email_paiement'];*/

                    // Envoi de sms
                    // sendSmsCodeTicketAfterPayTicket($tel, $code_ticket, $uti, $uti1, $dep['nom_gare'], $arr['nom_gare'], $dest);

                    // Envoi de mail
                    $this->sendMailAfterPayTicket($paiement);
                    Log::info("--- ReservationController::callback --- Le callback a été validé ---");
                }else{
                    Log::info("--- ReservationController::callback --- Impossible de trouvé le numéros de la transaction ---");
                }
            }else{
                // Recupération du paiement
                $paiement = $this->paiementRepository->whereFirst(["id_transaction" => $id_transaction]);
                // On met à jour le statut du paiement
                $this->paiementRepository->update(["status_paiement" => 3], $paiement->id);
                Log::info("--- ReservationController::callback --- Souccis de validation du callback ---");
            }
            Log::info("--- ReservationController::callback --- DONE ---");
            return redirect()->route('user.reserver.successtape', ['id_transaction='.$id_transaction]);
        }
        catch(Exception $e)
        {
            // echo "Erreur :" . $e->getMessage();
            // Une erreur s'est produite
            Log::info("--- ReservationController::callback --- Une erreur est survenue lors de la validation du callback ".$e->getMessage()."---");
        }
    }

    // Generate un paiement
    private function generatePaiement($voyage_id, $user_id , $montant_total, $montant_ticket_unitaire, $nombre_ticket){
        return $this->paiementRepository->create([
            "voyage_id" => $voyage_id,
            "user_standard_id" => $user_id,
            "params_status_paiement_id" => 1, // Le paimenent a été initié
            "montant_total" => $montant_total,
            "montant_ticket_unitaire" => $montant_ticket_unitaire,
            "code" => $this->generateCode(),
            "nombre_ticket" => $nombre_ticket,
            "id_transaction" => $this->getIdTransactionFromCinetPay()
        ]);
    }

    // Create a user
    private function createUserStandart($telephone, $email){
        return $this->userStandardRepository->create([
            "telephone" => $telephone,
            "email" => $email,
            "actif" => 1
        ]);
    }

    private function generateCode(){
        $se = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ@';
        $numero = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $code_ticket = '';
        $num = '';

        for ($i = 0;$i < 7;$i++)
        {
            $code_ticket .= substr($numero, rand() % (strlen($numero)) , 1);
        }
        return $code_ticket;
    }

    // Get id transaction from cineptay
    private function getIdTransactionFromCinetPay(){
        return Cinetpay::generateTransId();
    }

    // Function d'envoi de mail après que le paiement est été effectué
    private function sendMailAfterPayTicket($paiement)
    {
        $email = $paiement->userstandard->email;
        $data['telephone'] = $paiement->userstandard->telephone;
        $data['depart_gare_libelle'] = $paiement->voyage->depart_gare->libelle;
        $data['arrivee_gare_libelle'] = $paiement->voyage->arrivee_gare->libelle;
        $data['date_depart'] = date("d-m-Y", strtotime($paiement->voyage->date_depart));
        $data['heure_depart'] = date("h:i:s", strtotime($paiement->voyage->date_depart));
        $data['compagnie'] = $paiement->voyage->compagnie->nom;
        $data['nombre_ticket'] = $paiement->nombre_ticket;
        $data['prix'] = number_format($paiement->voyage->prix, 2, '.', ' ');
        $data['montant_total'] = number_format($paiement->montant_total, 2, '.', ' ');
        Mail::send('mails.buy_ticket', $data, function($message) use($email) {
            $message->to($email, 'RESERVATION TICKETS')->subject
                ('Réservation de tickets');
            $message->from('admin@tranvoyage.com','Admin tranvoyage');
        });
    }


    private function sendMailAfterPayTicketOlversion($paiement)
    {
        $email = $paiement->userstandard->email;
        if (strlen(trim($email)) > 0)
        {
            $mail = new \PHPMailer();

            $body = "<b style='color:#000;'>Bonjour, </b><br/>";
            $body .= "<br/>Votre réservation de tickets a été éffectué avec succès !";
            // $bom = utf8_decode($titre);
            $body = $body . "<br/>";
            $body .= utf8_decode("<p style='text-align:center;background:#3498DB;padding:5px;color:white;font-size:14px;font-weight:bold;'>Informations sur la reservation</p><br/>");
            // $body .= "<p><b style='color:#2980B9;'>Téléphone:</b> ".$tel_ticket."</p>";
            // $body .= "<p><b style='color:#2980B9;'>Code du ticket: </b>".$code_ticket."</p>";
            // $body .= "<p><b style='color:#2980B9;'>Trajet: </b>".$dep['nom_gare']."-".$arr['nom_gare']."</p>";
            // //$body .= "<p><b style='color:#2980B9;'>Arrivé: </b>".$arr['nom_gare']."</p>";
            // $body .= "<p><b style='color:#2980B9;'>Date de départ: </b>".gmdate("d-m-Y", strtotime($_POST['date_depart']))." à ".$_POST['heure_depart']."</p>";
            // $body .= "<p><b style='color:#2980B9;'>Compagnie: </b>".$uti1['nom_compagnie']."</p>";
            // $body .= "<p><b style='color:#2980B9;'>Nombre de billets: </b>".$uti['nombre_ticket']."</p>";
            // $body .= "<p><b style='color:#2980B9;'>Montant unitaire billets: </b>".number_format($uti['mont_ticket_unitaire'], 2,'.',' ')." XOF</p>";
            // $body .= "<p><b style='color:#2980B9;'>Montant Total: </b> <b>".number_format($uti['montant_total_ticket'], 2,'.',' ')." XOF</b></p><br/>";
            $body .= "<!DOCTYPE html>
                <html>
                <head>
                <style>
                table, th, td {
                    border: 1px solid black;
                    padding:5px;
                }

                td {
                padding:5px;
                text-align:center;
                background:#3498DB;
                color:white;
                }


                </style>
                </head>
                <body><table>
                    <tr>
                    <th>Téléphone</th>
                    <th>Trajet</th>
                    <th>Date de départ</th>
                    <th>Compagnie</th>
                    <th>Nombre de billets</th>
                    <th>Montant unitaire billets</th>
                    <th>Montant Total</th>
                    </tr>
                    <tr>";
            $body .= " <td>" . $paiement->userstandard->telephone . "</td>";
            $body .= "  <td>" . $paiement->voyage->depart_gare->libelle . " -" . $paiement->voyage->arrivee_arrivee->libelle. "</td>";
            $body .= "  <td>" . date("d-m-Y", strtotime($paiement->voyage->date_depart)) . " à " . date("h:i:s", strtotime($paiement->voyage->date_depart)) . "</td>";
            $body .= "  <td>" . $paiement->voyage->compagnie->nom . "</td>";
            $body .= "  <td>" . $paiement->nombre_ticket . "</td>";
            $body .= "  <td>" . number_format($paiement->voyage->prix, 2, '.', ' ') . " XOF</td>";
            $body .= "  <td>" . number_format($paiement->montant_total, 2, '.', ' ') . " XOF</td>
                    </tr>
                </table>
                <br/>
                </table>

                <br/>

                Merci pour votre fidelité, <b>Tranvoyage.com</b> vous souhaite un trè beau voyage.
                </body>
                </html>";
            // $body .= "<p><b>Le code de ticket est requis pour terminer le paiement sur le site. </b></p><br/>";// code (".$code_ticket.")$uti['mont_ticket_unitaire']number_format($row['tarif_prix'], 2,'.',' ')
            // $body .= utf8_decode(<p>Cliquez sur ce lien pour vous connecter pour terminer le paiement:<a href='http://tranvoyage.com/paiement_ticket.php?code_ticket='.$code_ticket.'&montant_total_ticket='.$uti['montant_total_ticket']. '&nombre_ticket='.$uti['nombre_ticket'].''>tranvoyage</a></p><br/><br/>");
            // $body .= utf8_decode("<p>Cliquez sur ce lien pour vous connecter pour terminer le paiement: http://tranvoyage.com/paiement_ticket.php?code_ticket=" . $code_ticket . "&montant_total_ticket=" . $uti['montant_total_ticket'] . "&nombre_ticket=" . $uti['nombre_ticket'] . "</p><br/><br/>");
            $body = utf8_decode($body);
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            ); // ignorer l'erreur de certificat.
            $mail->Host = "mail47.lwspanel.com";
            $mail->Port = 587;

            $mail->Username = "admin@tranvoyage.com";
            $mail->Password = "tran2021@T";
            $mail->From = "admin@tranvoyage.com"; //adresse d’envoi correspondant au login entrée précédement
            $textfrom_ = "RESERVATION TICKETS";
            $textfrom = utf8_decode($textfrom_);
            $mail->FromName = "TRANVOYAGE | " . $textfrom . " "; // nom qui sera affiché


            $tit = "Réservation de tickets";
            $bomo = utf8_decode($tit);
            $mail->Subject = $bomo; // sujet
            $mail->AltBody = "corps du message au format texte"; //Body au format texte
            $mail->WordWrap = 50; // nombre de caractere pour le retour a la ligne automatique
            $mail->MsgHTML($body);
            $admin_mail = "admin@tranvoyage.com";
            $mail->AddReplyTo($admin_mail);
            $mail->AddAddress($email); // here to add the mail of the user
            // $mail->AddAddress('cherubin0225@gmail.com');
            // $mail->AddAddress('tran.reservation@gmail.com'); // here to add the mail of the user
            // $mail->AddAddress('');// here to add the mail of the user
            // $mail->AddAddress('');// here to add the mail of the user
            $mail->IsHTML(true); // envoyer au format html, passer a false si en mode texte
            $mail->Send();
        }
    }
}
