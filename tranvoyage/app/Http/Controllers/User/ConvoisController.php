<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Mail;

use App\Http\Controllers\Controller;
use App\Repositories\ConvoisTempRepository;
use App\Repositories\ConvoisRepository;
use App\Repositories\WalletRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ConvoisController extends Controller{

    private $convoisTempRepository;
    private $convoisRepository;
    private $walletRepository;

    public function __construct(ConvoisTempRepository $convoisTempRepository, ConvoisRepository $convoisRepository,
    WalletRepository $walletRepository){
        $this->convoisTempRepository = $convoisTempRepository;
        $this->convoisRepository = $convoisRepository;
        $this->walletRepository = $walletRepository;
    }

    public function index(){
       
        return view('user.pages.convois.index');
    }

    public function show($code){
        $convois = $this->convoisRepository->whereFirst(['code' => $code]);
        return view('user.pages.convois.show', compact('convois'));
    }

    public function premiereetape(){
        return view('user.pages.convois.creer.premiereetape');
    }

    public function deuxiemeetape(){
        return view('user.pages.convois.creer.deuxiemeetape');
    }

    public function saveDeuxiemeEtape(Request $request){
        $code = $this->generateCode();
        $data = $request->except('EMAIL_');
        $data['code'] = $code;
        $this->convoisTempRepository->create($data);
        return redirect()->route('user.convois.creer.troisiemeetape', ['code='.$code]);
    }

    public function troisiemeetape(Request $request){
        $code = $request->get('code');
        return view('user.pages.convois.creer.troisiemeetape', compact('code'));
    }

    public function saveTroisiemeEtape(Request $request){
        $code = $request->get('code');
        $convois = $this->convoisTempRepository->whereFirst(['code' => $code]);
        $data = $request->all();
        $this->convoisTempRepository->update($data, $convois->id);
        $url = URL::temporarySignedRoute(
            'user.convois.creer.callback', now()->addMinutes(30), ['code='.$code]
        );

        // send mail 
        $this->sendMail($data["email"], $url);

        return redirect()->route('user.convois.creer.quatriemeetape', ['code='.$code]);
    }

    public function quatriemeetape(){
        return view('user.pages.convois.creer.quatriemeetape');
    }

    public function successEtape(Request $request){
        $code = $request->get('code');
        $convois = $this->convoisRepository->whereFirst([
            'code' => $code
        ]);
        $newPassword = null;
        // Generate code and send
        if($convois->user->sended_reset_password_at == null){
            $newPassword = $this->resetPassword($code);   
        }

        // Déconnexion automatique
        Auth::logout();
        
        return view('user.pages.convois.creer.success_etape', compact('convois','newPassword'));
    }

    public function createConvoisCallback(Request $request){
        Log::info("--- ConvoisController::createConvoisCallback --- START ");
        DB::beginTransaction();
        try {
            // Récuperation du convois
            $code = $request->get('code');
            $convoisTemp = $this->convoisTempRepository->whereFirst(['code' => $code]);
            if($convoisTemp && $convoisTemp->email_checked == 1){
                return redirect()->route('user.convois.creer.invalid');
            }

            $data = ["email_checked" => 1];
            // Validation de création de convois
            $this->convoisTempRepository->update($data, $convoisTemp->id);

            // Création d'un accès pour l'utilisateur qui se connectera
            // Si l'utilisateur existe déja par besoin de le créer a nouveau
            $user = User::where('email', $convoisTemp->email)->first();
            if($user){

            }else{
                User::create([
                    'name' => $convoisTemp->nom_prenom_utilisateur,
                    'email' => $convoisTemp->email,
                    'tel' => $convoisTemp->tel,
                    'actif' => 1,
                    'password' => Hash::make("password")
                ]);

                $user = User::where('email', $convoisTemp->email)->first();

                $this->walletRepository->create([
                    'solde' => 0,
                    'user_id' => $user->id
                ]);
            }

            // Création d'un convois
            $dataConvois = [
                "params_status_convois_id" => 1,
                "code" => $convoisTemp->code,
                "libelle" => $convoisTemp->nom_convois,
                "user_id" => $user->id,
                "lieu_depart" => $convoisTemp->lieu_depart,
                "lieu_arrivee"  => $convoisTemp->lieu_arrivee,
                "lieu_rassemblement" => null,
                "date_depart" => $convoisTemp->date_depart,
                "date_arrivee" => $convoisTemp->date_arrivee,
                "description" => $convoisTemp->description,
                "prix" => $convoisTemp->prix,
                "nbre_ticket" => null,
                "ticket_disponible" => null,
                "actif" => 1
            ];
            $convois = $this->convoisRepository->create($dataConvois);

            DB::commit();
            Log::info("--- ConvoisController::createConvoisCallback --- DONE ");
            return redirect()->route('user.convois.creer.successtape', ['code='.$convois->code]);
        } catch (\Exception $e) {
            DB::rollback();
            Log::info("--- ConvoisController::createConvoisCallback --- Exception : ".$e->getMessage());
            return redirect()->route('user.convois.creer.invalid');
        }
        
    }

    private function generateCode($length = 7){
        $se = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ@';
        $numero = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $code_ticket = '';
        $num = '';

        for ($i = 0;$i <= $length;$i++)
        {
            $code_ticket .= substr($numero, rand() % (strlen($numero)) , 1);
        }
        return $code_ticket;
    }

    private function sendMail($email, $url){
        $data = ["url" => $url];
        Mail::send('mails.validate_email_after_create_convois', $data, function($message) use($email) {
            $message->to($email, "Confirmation de l'adresse electronique")->subject
                ("Confirmation de l'adresse electrinique");
            $message->from('admin@tranvoyage.com',"Confirmation de l'adresse electronique");
        });
    }

    private function resetPassword($code){
        $convois = $this->convoisRepository->whereFirst(['code' => $code]);
        $user = $convois->user;
        
        $newPassword = $user->last_login ? null : strtolower($this->generateCode(8));
            
        // Send mail
        $this->sendMailToResetPassword($user->email, $code, $newPassword);
        $user->sended_reset_password_at = date("Y-m-d H:i:s");
        $user->password = Hash::make($newPassword);
        $user->save();

        return $newPassword;
    }

    private function sendMailToResetPassword($email, $code, $newPassword){
        $data = [
            "code" => $code,
            "newPassword" => $newPassword
        ];
        Mail::send('mails.reset_password_after_create_convois', $data, function($message) use($email) {
            $message->to($email, "Création de convois")->subject
                ("Création de convois");
            $message->from('admin@tranvoyage.com',"Création de convois");
        });
    }
}