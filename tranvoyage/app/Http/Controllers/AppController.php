<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
        return view('pages.qui_sommes_nous');
    }

    public function contact()
    {
        return view('pages.contact');
    }
}
