@extends('layouts.app')

@section('content')

    <section class="top_place section_padding" style="margin-bottom:50px;">
        <div class="container">
            <div class="row">
                <div class="offset-md-3 col-md-6">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h3 id="title-reserver">Rechercher un ticket</h3>
                        </div>
                        <div class="col-md-12 text-center" style="margin: 25px 0px;">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur doloribus ducimus
                            eveniet ex quam quibusdam quidem quod temporibus velit voluptates? Ab aliquid ducimus et,
                            maiores modi nam neque quidem ut!
                        </div>
                        <form class="col-md-12" action="{{ route('user.ticket.rechercherticket') }}" method="post">
                            {{ csrf_field() }}
                            <div class="mb-3">
<!--                                <label class="form-label">le numéros de téléphone</label>-->
                                <input type="text" name="telephone" class="form-control" placeholder="Entrer le numéros de téléphone">
                            </div>

                            {{-- <button type="submit" id="envoie_paiement" class="btn btn-md btn-success float-right">Continuer
                                <i id="icon_paiment" class="fa fa-arrow-right"></i></button> --}}
                            <div class="col text-center">
                                <button  type="submit" id="envoie_paiement" class="btn btn-md btn-success">Rechercher</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
