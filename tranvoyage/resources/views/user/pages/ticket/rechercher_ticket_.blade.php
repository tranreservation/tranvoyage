@extends('layouts.app')

@section('content')

    <style>

        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                            <div class="booking_form">
                                <form action="#" class="form-inline row">
                                    <div class="form-group col-md-12" style="margin-top: 70px">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control" placeholder="Saisir le code de ticket ..." id="search-ticket">
                                              <span class="input-group-btn">
                                                <button class="btn btn-search" type="button" id="fa-search"><i class="fa fa-search"></i> Rechercher</button>
                                              </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="container" id="result-search-ticket">
        <div class="row">
            <div class="col-md-12">
                <div id="text-before-search-ticket" class="text-center">
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit voluptatem vero voluptate, accusantium quas architecto quidem tempora 
                    veniam, placeat molestiae cumque ipsa asperiores voluptas unde dolorem consequuntur velit voluptatum dignissimos.
                </div>
            </div>

            <!--<div class="offset-md-3 col-md-6">
                <div class="row">
                    <div class="col-md-12">
                       <h3 id="title-reserver">Reserver un ticket</h3>
                       <div class="row">
                           <div class="col-md pannel" id="pannel-1">Informations tickets</div>
                           <div class="col-md pannel" id="pannel-2">Autres informations</div>
                       </div>
                    </div>
                    <form class="col-md-12" action="#" id="form_compte_ticket">
                        <div class="mb-3">
                            <label class="form-label">Nombre de ticket</label>
                            <input type="text" value="1" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix unitaire</label>
                            <input type="text" id="prix_unitaire" value="10" class="form-control"  readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix total (F.CFA)</label>
                            <input type="text"  value="10" name="total_tickets" id="total_tickets" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Téléphone <i class="fa fa-question-circle" aria-hidden="true" title="Veuillez cliquer sur ce bouton d'information" onclick="info();" style="color:red;cursor:pointer;"></i></label>
                            <input type="tel" name="tel_ticket" id="tel_ticket" value="45678902" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Email (optionnel)</label>
                            <input type="email" name="email" id="email" class="form-control" value="dks@yopmail.com" readonly>
                        </div>

                        <a href="index.php" class="btn btn-md btn-success float-left"><i class="fa fa-arrow-left"></i> Acheter un autre ticket</a>
                    </form>
                </div>
            </div>-->

            <div class="offset-md-3 col-md-6">
                <div class="col-md-12">
                    <h3 id="title-reserver">Détail du ticket</h3>
                 </div>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Informations tickets</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Autres informations</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="mb-3">
                            <label class="form-label">Nombre de ticket</label>
                            <input type="text" value="1" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix unitaire</label>
                            <input type="text" id="prix_unitaire" value="10" class="form-control"  readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix total (F.CFA)</label>
                            <input type="text"  value="10" name="total_tickets" id="total_tickets" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Téléphone <i class="fa fa-question-circle" aria-hidden="true" title="Veuillez cliquer sur ce bouton d'information" onclick="info();" style="color:red;cursor:pointer;"></i></label>
                            <input type="tel" name="tel_ticket" id="tel_ticket" value="45678902" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Email (optionnel)</label>
                            <input type="email" name="email" id="email" class="form-control" value="dks@yopmail.com" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Statut</label>
                            <input type="email" name="email" id="email" class="form-control" value="Ticket validé" readonly>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="mb-3">
                            <label class="form-label">Compagnie</label>
                            <input type="text" value="UTB" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Gare de départ</label>
                            <input type="text" id="prix_unitaire" value="10" class="form-control"  readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Gare de destination</label>
                            <input type="text"  value="10" name="total_tickets" id="total_tickets" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Date du voyage <i class="fa fa-question-circle" aria-hidden="true" title="Veuillez cliquer sur ce bouton d'information" onclick="info();" style="color:red;cursor:pointer;"></i></label>
                            <input type="tel" name="tel_ticket" id="tel_ticket" value="12/02/2021" class="form-control" readonly>
                        </div>
                    </div>
                </div>
                <a href="{{ route('home') }}" class="btn btn-md btn-success float-left"><i class="fa fa-arrow-left"></i> Acheter un autre ticket</a>
            </div>

           
        </div>
    </section>
@endsection
