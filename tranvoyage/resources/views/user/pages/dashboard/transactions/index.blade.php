@extends('layouts.dashboard_user')

@section('content')

    <div class="bg-white shadow-sm rounded py-4 mb-4">
        <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">Liste des transactions</h3>

        <!-- Title
        =============================== -->
        <div class="transaction-title py-2 px-4">
            <div class="row font-weight-00">
                <div class="col col-sm-3">Date</div>
                <div class="col col-sm-9 text-center">Montant</div>
            </div>
        </div>
        <!-- Title End -->

        <!-- Transaction List
        =============================== -->
        <div class="transaction-list">
            @foreach ($transactions as $transaction)
                <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail">
                    <div class="row align-items-center flex-row">
                        <div class="col col-sm-3"> <span class="d-block text-4">{{ $transaction->created_at }}</span> </div>
                        <div class="col col-sm-9 text-center"> <span class="d-block text-4">@if($transaction->montant_transaction > 0) +{{ $transaction->montant_transaction }} F CFA @endif</span> </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- Transaction List End -->

        <div class="row">
            <div class="col text-center">
                {{ $transactions->links() }}
            </div>
        </div>
        <!-- View all Link End -->

    </div>
@endsection

