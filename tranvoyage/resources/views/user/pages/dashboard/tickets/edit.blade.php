@extends('layouts.dashboard_user')

@section('content')
<form action="{{ route('user.tickets.update', ['code' => $ticket->code ]) }}" method="POST" class="text-center">
    {{ csrf_field() }}
    <div class="text-4" style="margin-top: 80px;margin-bottom: 37px;">Voulez vous @if($statut == 2) validé @elseif($statut== 4) annulé @endif ce ticket ? </div>
    <input type="text" name="params_status_ticket_id" value="{{ $statut }}" hidden>
    <div class="col text-center">
        <a href="{{ route('user.tickets.index') }}" class="btn btn-primary">Annuler</a>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
  </form>
@endsection
