@extends('layouts.dashboard_user')

@section('content')

    <div class="bg-white shadow-sm rounded py-4 mb-4">
        <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">Liste des tickets</h3>

        <!-- Title
        =============================== -->
        <div class="transaction-title py-2 px-4">
            <div class="row font-weight-00">
                <div class="col-2 col-sm-1 text-center"><span class="">#</span></div>
                <div class="col col-sm-3">Date de création</div>
                <div class="col col-sm-4">Code</div>
                <div class="col-auto col-sm-2 d-none d-sm-block text-center">Status</div>
                <div class="col-3 col-sm-2 text-right">Actions</div>
            </div>
        </div>
        <!-- Title End -->

        <!-- Transaction List
        =============================== -->
        <div class="transaction-list">
            @foreach ($tickets as $ticket)
                <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail">
                    <div class="row align-items-center flex-row">
                        <div class="col-2 col-sm-1 text-center"> <span class="d-block text-4 font-weight-300">{{ $i++ }}</span></div>
                        <div class="col col-sm-3"> <span class="d-block text-4">{{ $ticket->created_at }}</span> </div>
                        <div class="col col-sm-4"> <span class="d-block text-4">{{ $ticket->code }}</span> </div>
                        <div class="col-auto col-sm-2 d-none d-sm-block text-center text-3">
                            @if($ticket->statusTicket->id == 1)
                                <span class="text-success" data-toggle="tooltip" data-original-title="Valide"><i class="fas fa-ellipsis-h"></i></span>
                            @elseif($ticket->statusTicket->id == 2)
                                <span class="text-danger" data-toggle="tooltip" data-original-title="Utilisé"><i class="fas fa-check-circle"></i></span>
                            @elseif($ticket->statusTicket->id == 3)
                                <span class="text-danger" data-toggle="tooltip" data-original-title="Expiré"><i class="fas fa-times-circle"></i></span>
                            @elseif($ticket->statusTicket->id == 4)
                                <span class="text-danger" data-toggle="tooltip" data-original-title="Annulé"><i class="fas fa-times-circle"></i></span>
                            @endif
                        </div>
                        <div class="col-3 col-sm-2 text-right text-4">
                            <a href="{{ route('user.tickets.edit', ['code' => $ticket->code, 'statut=2']) }}" data-toggle="tooltip" data-original-title="Validé ce ticket"><i class="fas fa-check"></i></a>
                            <a href="{{ route('user.tickets.edit', ['code' => $ticket->code, 'statut=4']) }}" data-toggle="tooltip" data-original-title="Annulé ce ticket"><i class="fas fa-power-off"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- Transaction List End -->

        <!-- View all Link
        =============================== -->
        <div class="row">
            <div class="col text-center">
                {{ $tickets->links() }}
            </div>
        </div>
        <!-- View all Link End -->

    </div>
@endsection

