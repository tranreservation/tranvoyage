@extends('layouts.dashboard_user')

@section('content')

<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Détails</a>
    </li>
    <!--<li class="nav-item">-->
    <!--    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Liste des tickets</a>-->
    <!--</li>-->
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <form action="{{ route('user.convois.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Libelle</label>
                <input type="text" class="form-control" placeholder="Libellé du convois" value="{{ $convois->libelle }}" name="libelle" readonly>
            </div>
            <div class="form-group">
                <label>Lieu de départ</label>
                <input type="text" class="form-control" placeholder="Lieu de départ" name="lieu_depart" value="{{ $convois->lieu_depart }}" readonly>
            </div>
            <div class="form-group">
                <label>Lieu d'arrivée</label>
                <input type="text" class="form-control" placeholder="Lieu d'arrivée" name="lieu_arrivee" value="{{ $convois->lieu_arrivee }}" readonly>
            </div>
            <div class="form-group">
                <label>Lieu de rassemblement</label>
                <textarea name="lieu_rassemblement" class="form-control" cols="30" rows="10" readonly>{{ $convois->lieu_rassemblement }}</textarea>
            </div>
            <div class="form-group">
                <label>Communauté</label>
                <input type="text" class="form-control" name="lieu_depart" value="{{ $convois->communaute }}" readonly>
            </div>
            <div class="form-group">
                <label>Date de départ</label>
                <input type="text" class="form-control" placeholder="Date de départ" name="date_depart" value="{{ date('d/m/Y', strtotime($convois->date_depart)) }}" readonly>
            </div>
            <div class="form-group">
                <label>Date de d'arrivée</label>
                <input type="text" class="form-control" placeholder="Date d'arrivée" name="date_arrivee" value="{{ date('d/m/Y', strtotime($convois->date_arrivee)) }}" readonly>
            </div>
            <div class="form-group">
                <label>Prix</label>
                <input type="number" class="form-control" placeholder="Prix" name="prix" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="{{ $convois->prix }}" readonly>
            </div>
            <div class="form-group">
                <label>Nombre de ticket</label>
                <input type="number" class="form-control" placeholder="Nombre de ticket" name="nbre_ticket" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="{{ $convois->nbre_ticket }}" readonly>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control" cols="30" rows="10" readonly>{{ $convois->description ?? "" }}</textarea>
            </div>
            <div class="form-group"><label>Photo</label> <span class="d-block text-3"> <img src="{{ asset($convoi->image_file ?? 'photos/default-image.png') }}" height="200px" width="200px" alt="photo de convoi"/></span> </div>
<!--            <button type="submit" class="btn btn-primary">Enregistrer</button>-->
        </form>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Code</th>
        <th scope="col">Téléphone</th>
        <th scope="col">Nombre de tickets</th>
      </tr>
    </thead>
    <tbody>
    <?php  ?>
    @if(count($paiements) > 0)
      @foreach ($paiements as $paiement)
        <tr>
            <th scope="row">{{ $i++ }}</th>
            <td>{{ $paiement->code }}</td>
            <td>{{ $paiement->userstandard->telephone }}</td>
            <td>{{ $paiement->nombre_ticket }}</td>
        </tr>
      @endforeach
    @else
        <tr>
            <td colspan="4" class="text-center">Aucun ticket disponible pour ce convois</td>
        </tr>
    @endif
    </tbody>
  </table>

    </div>
    <a href="{{ route('user.convois.index') }}" class="btn btn-primary"><i id="icon_paiment" class="fa fa-arrow-left"></i> Retour</a>
</div>



@endsection
