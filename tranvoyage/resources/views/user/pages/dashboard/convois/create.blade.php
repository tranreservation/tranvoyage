@extends('layouts.dashboard_user')

@section('content')

<form action="{{ route('user.convois.store') }}" method="POST" id="idForm" enctype="multipart/form-data">
    {{ csrf_field() }}

    @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="form-group">
      <label >Libelle</label> <span style="color:red;">*</span>
      <input type="text" class="form-control" maxlength = "200"  required placeholder="Libellé du convois" value="{{old('libelle')}}" name="libelle">
    </div>
    <div class="form-group">
        <label >Lieu de départ</label> <span style="color:red;">*</span>
        <input type="text" class="form-control" maxlength = "200"  required placeholder="Lieu de départ" value="{{old('lieu_depart')}}" name="lieu_depart">
    </div>
    <div class="form-group">
        <label >Lieu d'arrivée</label> <span style="color:red;">*</span>
        <input type="text" class="form-control" maxlength = "200"  required placeholder="Lieu d'arrivée" value="{{old('lieu_arrivee')}}" name="lieu_arrivee">
    </div>
    <div class="form-group">
        <label >Lieu de rassemblement</label> <span style="color:red;">*</span>
        <textarea name="lieu_rassemblement" maxlength = "200" class="form-control" required cols="30" rows="10">{{old('lieu_rassemblement')}}</textarea>
    </div>
    <div class="form-group row">
        <label >Date de départ</label> <span style="color:red;">*</span>
        <input type="date"   class="form-control  @if($message) is-invalid  @endif" required placeholder="Date de départ" name="date_depart" value="{{old('date_depart')}}">
         @if ($message)
            <div class="col-sm-3">
                <small id="passwordHelp" class="text-danger">
                  La date est invalide
                </small>
            </div>
          @endif
    </div>
    <div class="form-group">
        <label >Date de d'arrivée</label> <span style="color:red;">*</span>
        <input type="date" class="form-control @if($message) is-invalid  @endif" required placeholder="Date d'arrivée" name="date_arrivee" value="{{old('date_arrivee')}}">
        @if ($message)
            <div class="col-sm-3">
                <small id="passwordHelp" class="text-danger">
                  La date est invalide
                </small>
            </div>
          @endif
    </div>
    <div class="form-group">
        <label >Prix</label> <span style="color:red;">*</span>
        <input type="number" min="1000" class="form-control" value="{{old('prix')}}" required placeholder="Prix" name="prix" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
    </div>
    <div class="form-group">
        <label >Nombre de ticket</label>
        <input type="number" class="form-control" min="1" required placeholder="Nombre de ticket" value="{{old('nbre_ticket')}}" name="nbre_ticket" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');">
    </div>
    <div class="form-group">
        <label >Nom de la Communauté</label> <span style="color:red;"></span>
        <input type="text" class="form-control" placeholder="Saisir le nom de la communauté" name="communaute" value="{{old('communaute')}}">
    </div>
    <div class="form-group">
        <label >Ajouter une image </label> <span style="color:red;"></span>
        <input type="file" class="form-control" value="{{old('image_file')}}"  name="image_file" accept="image/*" >
    </div>
    <div class="form-group">
        <label >Description</label>
        <textarea name="description" class="form-control"  cols="30" rows="10">{{old('description')}}</textarea>
    </div>
    <a href="{{ route('user.convois.index') }}" class="btn btn-primary">Annuler</a>
    <button type="submit" class="btn btn-primary" id="AddConvois">Enregistrer</button>
  </form>
@endsection


<!-- <script>

    $(function() {

        alert();

            // this is the id of the form
            $("#idForm").submit(function(e) {
                 alert();
                e.preventDefault(); // avoid to execute the actual submit of the form.

                var form = $(this);

                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                    {
                        alert(data); // show response from the php script.
                    }
                    });


                });

    });

</script> -->

