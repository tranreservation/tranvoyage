@extends('layouts.dashboard_user')

@section('content')
<form action="{{ route('user.convois.remove', ['id' => $convois->id ]) }}" method="POST" class="text-center">
    {{ csrf_field() }}
    <div class="text-4" style="margin-top: 80px;margin-bottom: 37px;">Voulez vous supprimer ce convois ? </div>
    <div class="col text-center">
        <a href="{{ route('user.convois.index') }}" class="btn btn-primary">Annuler</a>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </div>
  </form>
@endsection
