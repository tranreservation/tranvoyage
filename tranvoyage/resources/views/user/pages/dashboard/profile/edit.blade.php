@extends('layouts.dashboard_user')

@section('content')
    <form action="{{ route('user.profile.update', ['id' => $user->id]) }}" method="POST" >
        {{ csrf_field() }}
        <div class="form-group">
            <label >Nom et prénoms</label>
            <input type="text" class="form-control" placeholder="Nom et prénom" name="name" value="{{ $user->name }}">
        </div>
        <div class="form-group">
            <label >Téléphone</label>
            <input type="text" class="form-control" placeholder="Téléphone" name="telephone" value="{{ $user->telephone }}">
        </div>
        <div class="form-group">
            <label >Email</label>
            <input type="text" class="form-control" placeholder="Téléphone" name="email" disabled>
        </div>
        <div class="form-group">
            <label >Mot de passe</label>
            <input type="password" class="form-control" placeholder="Mot de passe" name="password">
        </div>
        <div class="form-group">
            <label >Confirmer le mot de passe</label>
            <input type="password" class="form-control" placeholder="Mot de passe" name="password">
        </div>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
    </form>
@endsection
