@extends('layouts.dashboard_user')

@section('content')

    <a href="{{ route('user.retraits.create') }}"  class="btn btn-primary float-right">Créer une demande</a>
    <div class="bg-white shadow-sm rounded py-4 mb-4">
        <h3 class="text-5 font-weight-400 d-flex align-items-center px-4 mb-4">Liste des retraits</h3>

        <!-- Title
        =============================== -->
        <div class="transaction-title py-2 px-4">
            <div class="row font-weight-00">
                <div class="col-2 col-sm-1 text-center"><span class="">#</span></div>
                <div class="col col-sm-3">Date de création</div>
                <div class="col col-sm-3">Code</div>
                <div class="col col-sm-3 text-center">Montant</div>
                <div class="col-auto col-sm-2 d-none d-sm-block text-right">Status</div>
            </div>
        </div>
        <!-- Title End -->

        <!-- Transaction List
        =============================== -->
        <div class="transaction-list">
            @foreach ($retraits as $retrait)
                <div class="transaction-item px-4 py-3" data-toggle="modal" data-target="#transaction-detail">
                    <div class="row align-items-center flex-row">
                        <div class="col-2 col-sm-1 text-center"> <span class="d-block text-4 font-weight-300">{{ $i++ }}</span></div>
                        <div class="col col-sm-3"> <span class="d-block text-4">{{ $retrait->created_at }}</span> </div>
                        <div class="col col-sm-3"> <span class="d-block text-4">{{ $retrait->code }}</span> </div>
                        <div class="col col-sm-3 text-center"> <span class="d-block text-4">{{ $retrait->montant_retrait }}</span> </div>
                        <div class="col-auto col-sm-2 d-none d-sm-block text-right text-3">
                            @if($retrait->statusretrait->id == 1)
                                <span class="text-warning" data-toggle="tooltip" data-original-title="En attente"><i class="fas fa-ellipsis-h"></i></span>
                            @elseif($retrait->statusretrait->id == 2)
                                <span class="text-success" data-toggle="tooltip" data-original-title="Validé"><i class="fas fa-check-circle"></i></span>
                            @elseif($retrait->statusretrait->id == 3)
                                <span class="text-danger" data-toggle="tooltip" data-original-title="Rejeté"><i class="fas fa-times-circle"></i></span>
                            @elseif($retrait->statusretrait->id == 4)
                                <span class="text-danger" data-toggle="tooltip" data-original-title="Annulé"><i class="fas fa-times-circle"></i></span>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- Transaction List End -->


        <!-- View all Link
        =============================== -->
        <div class="row">
            <div class="col text-center">
                {{ $retraits->links() }}
            </div>
        </div>
        <!-- View all Link End -->

    </div>
@endsection

