@extends('layouts.dashboard_user')

@section('content')
<form action="{{ route('user.retraits.store') }}" method="POST" >
    {{ csrf_field() }}
    <div class="form-group">
      <label >Montant du retrait</label>
      <input type="text" class="form-control" placeholder="Montant du retrait" name="montant_retrait">
    </div>
    <a href="{{ route('user.retraits.index') }}" class="btn btn-primary">Annuler</a>
    <button type="submit" class="btn btn-primary">Enregistrer</button>
  </form>
@endsection
