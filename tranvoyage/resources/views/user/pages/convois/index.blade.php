@extends('layouts.app')

@section('content')

    <style>
        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                            <div class="booking_form">
                                <form action="{{ route('home') }}" class="form-inline row">
                                    <div class="form-group col-md-12" style="margin-top: 70px">
                                        <div class="input-group col-md-12">
                                            <input type="text" class="form-control" name="code" value="{{ $code }}" placeholder="Rechercher un convois à partir du code ou du nom de la communauté ..." id="search-ticket">
                                              <span class="input-group-btn">
                                                <button class="btn btn-search" type="submit" id="fa-search"><i class="fa fa-search"></i> Rechercher</button>
                                              </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="top_place section_padding">
        <h3 class="text-9 text-center">Liste des convois</h3>
        <div class="container">
            <div class="row" style="margin-top:-15px;">
                <div class="col-md-12">
                    <div class="row" style="display: flex;"><style>
                        .mobile10{
                            display: none
                        }

                        #logo{
                            /*display: none*/
                        }


                    </style>

                    @inject('helpService', 'App\Services\HelpService')

                    @forelse ($convois as $convoi)
                        <div class="col-md-6 col-gars_affiche">
                            <div class="gars_affiche">
                                <div class="row">
                                    <div class="col-3 col-md-3 col-lg-3" id="col-logo">
                                        <span id="logo-7"><i class="fa fa-bus fa-7x"></i></span>
                                        <span id="logo-6"><i class="fa fa-bus fa-6x"></i></span>
                                    </div>
                                    <div class="col-3 col-md-3 col-lg-3 mobile10">
                                        <i class="fa fa-bus fa-3x" style="padding:10px;color:#fff;"></i>
                                        <span style="color:white;margin-top:-100px;display:none">Jeu 08 Jui 2021</span>
                                        <a href="reservation.php?res=14&amp;1625760302&amp;date=2021-07-08" data-id="14" class="btn" type="button " style="background-color:#03989E;color:#fff;height:30px;margin-top:-30px;margin-left:40px;border-radius:5px;font-size: 12px;font-weight: bold;display:none">Reserver le ticket</a>
                                    </div>

                                    <div id="no-responsive-voyage" class="col-9 col-md-9 col-lg-9">
                                        <ul>
                                            <li class="bull">
                                                <span class="sous-bull">
                                                    <div class="destination">
                                                        <span class="label">{{ $convoi->lieu_depart }} - {{ $convoi->lieu_arrivee }}</span>
                                                    </div>
                                                    <span>|</span>
                                                    <div class="depart">
                                                        <span class="label">Départ:</span>
                                                        <span class="value"><?=date('h:i', strtotime($convoi->date_depart));?></span>
                                                    </div>
                                                </span>
                                            </li>
                                            <li class="bull">
                                                <span class="sous-bull">
                                                    <div class="tarif">
                                                        <span class="label">Tarif:</span>
                                                        <span>{{ $convoi->prix }} XOF</span>
                                                    </div>
                                                </span>
                                            </li>
                                            <li class="bull">
                                                <span class="sous-bull">
                                                    <div class="gare">
                                                        <span class="label">Code : </span><span>{{ $convoi->code }}</span>
                                                    </div>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>

                                    <div id="responsive-voyage" class="col-9 col-md-9 col-lg-9">
                                        <ul>
                                            <li class="bull">
                                                <span class="sous-bull">
                                                    <div class="destination">
                                                        <span class="label">{{ $convoi->lieu_depart }} - {{ $convoi->lieu_arrivee }}</span>
                                                    </div>
                                                    <div class="depart">
                                                        <span class="label">Départ:</span>
                                                        <span class="value"><?=date('h:i', strtotime($convoi->date_depart));?></span>
                                                    </div>
                                                    <div class="tarif">
                                                        <span class="label">Tarif:</span>
                                                        <span>{{ $convoi->prix }} XOF</span>
                                                    </div>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-3 col-md-12 col-lg-3" id="date-voyage">
                                        <span style="">
                                           <?php
                                                $date = '2021-01-01';
                                            ?>
                                            {{ $helpService->convertDateInFrench($convoi->date_depart->format('Y-m-d')) ?? "" }}
                                        </span>
                                    </div>
                                    <div class="col-9 col-md-12 col-lg-9">
                                        <a href="{{ route('user.convois.reservation.show', ['code' => $convoi->code ]) }}" data-id="14" class="btn btn_reserver btn-success" >Détail du convois</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-md-12 py-2">
                            <p class="text-center text-7">
                               Désolé, aucun convoi ne correspond à ce mot-clé !
                            </p>
                        </div>
                    @endforelse
                </div>
            </div>

            <!-- <div class="d-flex justify-content-center flex-pagination">
                <nav aria-label="Page navigation example" class="col-md-12">
                    <ul class="pagination justify-content-center">
                      <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Précédent</a>
                      </li>
                      <li class="page-item"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item">
                        <a class="page-link" href="#">Suivant</a>
                      </li>
                    </ul>
                  </nav>
            </div> -->
            <div class="d-flex justify-content-center flex-pagination">
                {{ $convois->links() }}
            </div>
        </div>
    </section>

@endsection
