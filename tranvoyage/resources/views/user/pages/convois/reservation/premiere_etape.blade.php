@extends('layouts.app')

@section('content')

    <section class="top_place section_padding" style="margin-bottom:50px;">
        <div class="container">
            <div class="row">
                <div class="offset-md-3 col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 id="title-reserver">Reserver un ticket </h3>
                        </div>
                        <form class="col-md-12" action="{{ route('user.convois.reservation.deuxiemetape') }}">
                            <div class="mb-3">
                                <label class="form-label">Choisir le nombre</label>
                                <select name="nbre_tickets" id="nbre_tickets" style="" onchange="

                                var nbs = this.value;
                                
                                var pu = document.getElementById('prix_unitaire').value; 

                                var total = parseInt(nbs) * parseInt(pu); 

                                document.getElementById('total_tickets').value = parseInt(total);

                                " name="nbre_tickets" class="form-control">

                                    <?php for ($i = 1; $i < 11; $i++) { ?> <option
                                        value="<?php echo $i; ?>"><?php echo
                                        $i; ?></option>
                                        <?php } ?>
                                </select>
                            </div>


                            <div class="mb-3">
                                <label class="form-label">Prix unitaire</label>
                                <input type="text" id="prix_unitaire" value="{{ $convois->prix  }}" class="form-control" readonly>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Prix total (F.CFA)</label>
                                <input type="text" value="{{ $convois->prix }}" name="total_tickets" id="total_tickets" class="form-control"
                                    readonly>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Téléphone <i class="fa fa-question-circle" aria-hidden="true"
                                        title="Veuillez cliquer sur ce bouton d'information" onclick="info();"
                                        style="color:red;cursor:pointer;"></i></label>
                                <input type="tel" name="tel_ticket" id="tel_ticket" name="tel_ticket" class="form-control" required>
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Email (optionnel)</label>
                                <input type="email" name="email" id="email" name="email" class="form-control">
                            </div>

                            <input type="text" value="{{ $convois->id }}" name="convois_id" hidden>

                            <a href="{{ route('user.convois.reservation.show', ['code' => $convois->code]) }}" class="btn btn-md btn-success float-left"><i class="fa fa-arrow-left"></i>
                                Retour</a>
                            {{-- <button type="submit" id="envoie_paiement" class="btn btn-md btn-success float-right">Continuer
                                <i id="icon_paiment" class="fa fa-arrow-right"></i></button> --}}
                            <button  type="submit" id="envoie_paiement" class="btn btn-md btn-success float-right">Continuer
                                    <i id="icon_paiment" class="fa fa-arrow-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>

function changeVal() {

alert();
    
   var nb = document.getElementById("nbre_tickets");

    var pu = document.getElementById("prix_unitaire").value();

    var recuptotal = document.getElementById("total_tickets"):

    nb.addEventListener('change', function() {
 
        nbs = this.value;

        alert(nbs);
    
        total = parseInt(nbs) * parseInt(pu); 

        recuptotal.value(recuptotal);

     });
    
}
    </script>



@endsection









