
@extends('layouts.app')

@section('content')

<style>
    .cinetpay-button {
        /*color: #fff !important;
        background-color: #218838 !important;
        border-color: #1e7e34 !important;*/
        float: right;
    }

    .btn-success {
        color: #fff !important;
    }

    #goCinetPay {
        padding: 0px;
    }

    #cinetpay-button {
        padding-top: 12px;
    }

</style>

<section class="top_place" style="margin-top:50px;margin-bottom:50px;">

    <div class="container">
        <div class="row">
            <div class="offset-md-3 col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h3 id="title-reserver">Reserver un ticket</h3>
                    </div>
                    <div class="col-xl-12">



                        <div class="mb-3">
                            <label class="form-label">Le nombre de ticket</label>
                            <input type="text" value="{{ $nbre_tickets }}" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix unitaire</label>
                            <input type="text" class="form-control" value="{{ $convois->prix  }}" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix total (F.CFA)</label>
                            <input type="text" value="{{ $montant_total  }}" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Téléphone </label>
                            <input type="tel" value="{{ $tel_ticket  }}" class="form-control" readonly>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Email</label>
                            <input type="email" value="{{ $email }}" class="form-control" readonly>
                        </div>

                        @inject('paiementService', 'App\Services\PaiementService')
                        {{ $paiementService->genrateButton($paiement->id) }}

                        @php
                            $url = route('user.convois.reservation.premiereetape')
                        @endphp

                        <?php
                                echo"<script language='javascript'>
                                                document.getElementById('goCinetPay').innerHTML += \"<a class='btn btn-md btn-success float-left' href='$url?convois_id=$convois->id' >Retour</a>\";
                                                // document.getElementById('goCinetPay').style.color = 'padding: 0px';
                                                document.getElementById('goCinetPay').classList.add('col-lg-12');
                                                document.getElementById('goCinetPay').classList.remove('cinetpay-button');
                                                /*document.getElementsByClassName('cinetpay-button large').classList.add('btn-success');*/
                                                // console.log('Text ->'+document.getElementsByClassName('cinetpay-button large')[0].className);
                                                document.getElementsByClassName('cinetpay-button large')[0].setAttribute('id', 'cinetpay-button');
                                                // document.getElementById('cinetpay-button').classList.add(' btn btn-md btn-success');
                                                document.getElementsByClassName('cinetpay-button large')[0].className += ' btn btn-md btn-success';
                                                let html = document.getElementsByClassName('btn btn-md btn-success float-left')[0].innerHTML;
                                                html = \"<i class='fa fa-arrow-left'></i> \" + html;
                                                document.getElementsByClassName('btn btn-md btn-success float-left')[0].innerHTML = html;
                                                document.getElementById('cinetpay-button').innerHTML += \"<i class='fas fa-shopping-cart'></i>\";
                                            </script>";
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection
