
@extends('layouts.app')

@section('content')

<style>
    .page-success h1,
    .page-error h1 {
        color: #88B04B;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-weight: 900;
        font-size: 40px;
        margin-bottom: 10px;
    }

    .page-success p,
    .page-error p {
        color: #404F5E;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-size: 20px;
        margin: 0;
    }

    .page-success i,
    .page-error i {
        font-size: 100px;
    }

    .page-success i {
        color: #9ABC66;
    }

    .page-error i {
        color: #B00C2A
    }

    .page-success .card,
    .page-error .card {
        background: white;
        padding: 60px;
        display: inline-block;
        margin: 0 auto;
    }

    .center {
        text-align: center;
    }

    .transaction,
    .recu {
        font-size: 20px;
    }

    .important {
        font-weight: bold;
        font-size: 25px;
    }

</style>


@if($statut == true)
    <div class="container page-success" style="margin-bottom: 50px;padding-top: 72px;">
        <div class="row">
            <div class="col-md-12 center" style="">
                <div style="">
                    <i class="checkmark">✓</i>
                </div>
                <h2>Votre paiement a été effectué avec succès !</h2>
            </div>

            <div class="col-md-12 center transaction">
                Le numéros de la transaction est <span class="important">{{ $paiement->id_transaction}}</span>
            </div>

            <div class="col-md-12 center recu">
                Votre numero de recu est <span class="important">{{ $paiement->code}}</span>
            </div>

            <!-- <div class="col-md-12 center" style="padding-top: 40px;">
                <a href="" class="btn btn-md btn-success">Télécharger le reçu </a>
            </div> -->
        </div>
    </div>
@else

    <div class="container page-error" style="margin-bottom: 50px;">
        <div class="row">
            <div class="col-md-12 center" style="">
                <div style="">
                    <i class="fas fa-exclamation"></i>
                </div>
                <h2>Désolé votre paiement a échoué !</h2>
            </div>

            <div class="col-md-12 center" style="padding-top: 40px;">
                <a href="index.php" class="btn btn-md btn-success">Réserver un autre ticket</a>
            </div>
        </div>
    </div>

@endif

@endsection
