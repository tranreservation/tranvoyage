@extends('layouts.app')

@section('content')

    <style>

        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

    </style>

     @inject('helpService', 'App\Services\HelpService')

    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">

                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="top_place section_padding">
        <div class="container">
            <div class="row">
                 <div class="col-md-4">&nbsp;</div>
                    <div class="col-md-4" style="text-align:center;">
                        <h4 style="color:#2471A3;text-transform:uppercase;border-radius:5px;border-bottom:2px solid #2471A3;">DETAILS D'UN CONVOIS</h4>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                    <div class="col-md-12">&nbsp;</div>
                   {{-- {{dd($convois)}} --}}
                    <div class="row col-md-12 border border-ouset shadow bg-white" style="margin-left:7px !important;">
                           
                                <div class="col-md-6" onmouseover="onItemHover(this)" onmouseout="onItemOut(this)"> 
                                        <div class="row col-md-12">
                                            <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class=" pt-1"><i class="fa fa-bus fa-2x text-primary"></i></span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ $convois->libelle ?? ""}}</span></p></div>
                                            <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class=" pt-1"><i class="fas fa-user fa-2x text-primary"></i></span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">Mr {{ App\User::find($convois->user_id)->name ?? ""}}</span></p></div>
                                            <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class=" pt-1"><i class="fas fa-phone-volume fa-2x text-primary"></i></span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ App\User::find($convois->user_id)->telephone ?? ""}}</span></p></div>  
                                            <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class=" pt-1"><i class="fas fa-calendar-alt fa-2x text-primary"></i></span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ $helpService->convertDateInFrenchComplet($convois->date_depart->format('Y-m-d')) }}</span></p></div> 
                                             <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class=" pt-1"><i class="fas fa-map-marker-alt fa-2x text-primary"></i></span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ $convois->lieu_depart }} - {{ $convois->lieu_arrivee }}</span></p></div>  
                                        </div>        
                                </div>
                                
                                <div class="col-md-6" onmouseover="onItemHover(this)" onmouseout="onItemOut(this)"> 
                                        <div class="row"> 
                                            @if($convois->communaute)  <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class="font-weight-700 pt-1">Communauté: </span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ $convois->communaute ?? " "}}</span></p></div>  @endif
                                            @if($convois->description)  <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class="font-weight-700 pt-1">Lieu de rassemblement: </span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ $convois->lieu_rassemblement ?? " "}}</span></p></div>  @endif
                                            <div class="col-md-12" ><p class="text-uppercase float-left pt-3 pb-0"><span class="font-weight-700 pt-1">Code: </span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ $convois->code }}</span></p></div>  
                                            @if($convois->description)  <div class="col-md-12" style="word-break:break-all;"><p class="font-weight-700 pt-1" style="display: inline-block;">Description: </p>&nbsp;<p class="text-monospace bd-highlight" style="text-transform:capitalize;word-break:break-all;display: inline-block;">{{ $convois->description ?? " "}}</p></div>  @endif
                                            <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class="font-weight-700 pt-1">Tarif: </span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ $convois->prix }} XOF</span></p></div>
                                             <div class="col-md-12"><p class="text-uppercase float-left pt-3 pb-0"><span class="font-weight-700 pt-1 d-flex">Photo du convois:<img src="{{ asset($convoi->image_file ?? 'photos/default-image.png') }}" height="100px" width="100px" alt="photo de convoi"/></div>
                                        </div>        
                                </div>
                         
                                {{-- <div class="col-md-4" onmouseover="onItemHover(this)" onmouseout="onItemOut(this)" style="text-align: center;" onclick="redirect('emailto:{{App\User::find($convois->user_id)->email}}');">
                            
                                            <div class="col-md-12">
                                                <p class="text-uppercase  pt-3 pb-0"><span class="float-center pt-1"><i class="fas fa-user fa-2x text-primary"></i></span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">Mr {{ App\User::find($convois->user_id)->name ?? ""}}</span></p>
                                            </div>
                                
                                        <div class="col-md-4">&nbsp;</div>    

                                </div>

                                <div class="col-md-4" onmouseover="onItemHover(this)" onmouseout="onItemOut(this)" onclick="redirect('tel:{{App\User::find($convois->user_id)->telephone}}');">

                                        <div class="col-md-12">
                                                <p class="text-uppercase  pt-3 pb-0"><span class="float-center pt-1"><i class="fas fa-phone-volume fa-2x text-primary"></i></span>&nbsp;<span class="text-monospace text-nowrap bd-highlight" style="width: 8rem;text-transform:capitalize;">{{ App\User::find($convois->user_id)->telephone ?? ""}}</span></p>
                                        </div>
                                
                                        <div class="col-md-4">&nbsp;</div>   

                                </div> --}}

                    </div>  

                    {{-- <div
                    {{-- <div class="col text-center" style="margin-top: 16px;">
                        <a href="{{ route('user.convois.reservation.premiereetape', ['code='.$convois->code]) }}" class="btn btn-md btn-success">
                            Reserver un ticket</a>
                    </div> --}}
            </div>
        </div>
    </section>

    <script>
          function onItemHover(x) {
                console.log('hover')
                x.style.cursor = 'pointer';
                x.style.backgroundColor = "#EAF2F8";
            }  

          function onItemOut(x) {
              console.log('out')
                x.style.cursor = 'pointer';
                x.style.backgroundColor = "#fff";
            }  

            function redirect(tel){
                window.location.href =tel;
            }
    </script>

@endsection
