
@extends('layouts.app')

@section('content')

<style>
    .page-success h1,
    .page-error h1 {
        color: #88B04B;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-weight: 900;
        font-size: 40px;
        margin-bottom: 10px;
    }

    .page-success p,
    .page-error p {
        color: #404F5E;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-size: 20px;
        margin: 0;
    }

    .page-success i,
    .page-error i {
        font-size: 100px;
    }

    .page-success i {
        color: #9ABC66;
    }

    .page-error i {
        color: #B00C2A
    }

    .page-success .card,
    .page-error .card {
        background: white;
        padding: 60px;
        display: inline-block;
        margin: 0 auto;
    }

    .center {
        text-align: center;
    }

    .transaction,
    .recu {
        font-size: 20px;
    }

    .important {
        font-weight: bold;
        font-size: 25px;
    }

</style>


<div class="container page-success" style="margin-bottom: 50px;">
    <div class="row">
        <div class="col-md-12 center" style="">
            <div style="">
                <i class="checkmark">✓</i>
            </div>
            <h2>Désolé l'url n'est plus disponible !</h2>
        </div>
    </div>
@endsection