@extends('layouts.app')

@section('content')

    <style>

        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

        .page-success h1,
    .page-error h1 {
        color: #88B04B;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-weight: 900;
        font-size: 40px;
        margin-bottom: 10px;
    }

    .page-success p,
    .page-error p {
        color: #404F5E;
        font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
        font-size: 20px;
        margin: 0;
    }

    .page-success i,
    .page-error i {
        font-size: 100px;
    }

    .page-success i {
        color: #9ABC66;
    }

    .page-error i {
        color: #B00C2A
    }

    .page-success .card,
    .page-error .card {
        background: white;
        padding: 60px;
        display: inline-block;
        margin: 0 auto;
    }

    .center {
        text-align: center;
    }

    .transaction,
    .recu {
        font-size: 20px;
    }

    .important {
        font-weight: bold;
        font-size: 25px;
    }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">

                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="container page-success" id="result-search-ticket">
        <div class="row" style="margin: 64px 0px">
            <div class="col-md-12 text-center">
                <div style="">
                    <i class="checkmark">✓</i>
                </div>
                <h2 style="font-weight: normal">Votre convois a été crée avec succès veuillez vérifier votre adresse electronique car
                    un email vous a été envoyé .
                    Connectez vous afin de finaliser la création du convois !</h2>
                <i class="far fa-envelope" style="color: #000;font-weight: normal;font-size: 46px;"></i>
            </div>
        </div>
    </section>

@endsection
