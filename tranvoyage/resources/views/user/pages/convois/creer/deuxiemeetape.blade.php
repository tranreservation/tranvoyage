@extends('layouts.app')

@section('content')

    <style>

        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">

                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="container top_place section_padding">
        <form action="{{ route('user.convois.creer.save.deuxiemeetape') }}" method="POST">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <h3>Informations concernant le convois</h3>
                    <div class="mb-3">
                        <label class="form-label">Nom du convois</label>
                        <input type="text" class="form-control" required name="nom_convois">
                    </div>

                    {{-- <div class="mb-3">
                        <label class="form-label">Lieu de départ du convois</label>
                        <input type="text" class="form-control" required name="lieu_depart">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Lieu d'arrivée du convois</label>
                        <input type="text" class="form-control" required name="lieu_arrivee">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Date de départ du convois</label>
                        <input type="text" class="form-control" required name="lieu_arrivee">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Date d'arrivée du convois</label>
                        <input type="text" class="form-control" required name="lieu_arrivee">
                    </div> --}}

                    <div class="mb-3">
                        <label class="form-label">Description du convois</label>
                        <textarea id="" cols="30" rows="10" class="form-control" required name="description"></textarea>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Prix du convois</label>
                        <input type="text"  value="" id="total_tickets" name="prix" class="form-control" required >
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{ route('user.convois.creer.premiereetape') }}" class="btn btn-primary float-left"><i id="icon_paiment" class="fa fa-arrow-left"></i> Revenir</a>
                    <button type="submit" class="btn btn-md btn-success float-right">
                        Continuer <i id="icon_paiment" class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </form>
    </section>

@endsection
