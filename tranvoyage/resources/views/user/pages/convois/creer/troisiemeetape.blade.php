@extends('layouts.app')

@section('content')

    <style>
        #search-ticket {
            border: 0;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search {
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel {}

        #pannel-1,
        #pannel-2 {
            background: red;
        }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="top_place section_padding">
        <div class="container">
            <form class="row" action="{{ route('user.convois.creer.save.troisiemeetape') }}" method="POST">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <h3>Informations concernant l'utilisateur</h3>
                    <div class="mb-3">
                        <label class="form-label">Nom et prénom<i class="fa fa-question-circle" aria-hidden="true"
                                title="Veuillez cliquer sur ce bouton d'information" onclick="info();"
                                style="color:red;cursor:pointer;"></i></label>
                        <input type="tel" name="nom_prenom_utilisateur" class="form-control">
                    </div>

                    {{-- <div class="mb-3">
                        <label class="form-label">Nom de l'entreprise</label>
                        <input type="email" name="nom_prenom_utilisateur" class="form-control">
                    </div> --}}

                    <div class="mb-3">
                        <label class="form-label">Numéros de téléphone</label>
                        <input type="text" name="tel" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input type="email" name="email" class="form-control">
                    </div>

                    <input type="text" name="code" value="{{ $code }}" hidden>
                </div>
                <div class="col-md-12">
                    <a href="" class="btn btn-primary float-left"><i id="icon_paiment" class="fa fa-arrow-left"></i> Revenir</a>
                    <button type="submit" class="btn btn-md btn-success float-right">
                        Continuer <i
                            class="fa fa-arrow-right"></i>
                    </button>
                </div>

            </form>
    </section>

@endsection
