@extends('layouts.app')

@section('content')

    <style>

        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="container" id="result-search-ticket">
        <div class="row" style="margin: 64px 0px;">
            <div class="col-md-12 text-center" style="margin-top: 30px;margin-bottom: 10px;">
                <div id="text-before-search-ticket" class="text-center text-5 pb-3" style="font-weight: 400;font-size: 25px;line-height: 30px;">
                    Tranvoyage, le Blablacar de l’intérieur est le moyen le plus simple et le plus flexible pour vous de gagner de l'argent supplémentaire ou de couvrir vos frais de voiture pendant votre temps libre.
                    <br> Créer votre convoi et rejoignez le millier de partenaires qui gagnent déjà beaucoup d'argent en aidant notre communauté de passagers à se déplacer à Abidjan.
                </div>
            </div>
            <div class="col-md-12 col text-center" style="margin-bottom: 10px;">
                <a href="{{ route('user.convois.create') }}" class="btn btn-md btn-success">Créer un convois</a>
            </div>
        </div>
    </section>
    {{-- <section class="top_place section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                            <label class="form-label">Nom du convois</label>
                            <input type="text" class="form-control" >
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Description du convois</label>
                            <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Prix du convois</label>
                            <input type="text"  value="" name="total_tickets" id="total_tickets" class="form-control" >
                        </div>

                        <hr/>
                        <div class="mb-3">
                            <label class="form-label">Nom et prénom<i class="fa fa-question-circle" aria-hidden="true" title="Veuillez cliquer sur ce bouton d'information" onclick="info();" style="color:red;cursor:pointer;"></i></label>
                            <input type="tel" name="tel_ticket" id="tel_ticket" value="" class="form-control" >
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Nom de l'entreprise</label>
                            <input type="email" name="email" id="email" class="form-control" value="dks@yopmail.com" >
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Numéros de téléphone</label>
                            <input type="email" name="email" id="email" class="form-control" value="" >
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Email</label>
                            <input type="email" name="email" id="email" class="form-control" value="" >
                        </div>
                </div>
            </div>
            <a href="{{ route('user.convois.reservervation.premiereetape') }}" class="btn btn-md btn-success float-left"><i class="fa fa-arrow-left"></i>
               Créer un convois</a>
        </div>
    </section> --}}

@endsection
