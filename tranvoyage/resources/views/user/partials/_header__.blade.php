 <!-- Navigation -->
 <nav class="navbar navbar-expand-lg static-top">
	<div class="container">
		<a class="navbar-brand" href="{{ route('home') }}">
			{{-- <img src="{{ asset('assets/img/logo.jpg') }}" alt=""> --}}
			<img src="http://placehold.it/150x50?text=Logo" alt="">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" id="navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active">
					<a class="nav-link" href="{{ route('home') }}">Acceuil
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('user.ticket.rechercher') }}">Rechercher un ticket</a>
				</li>
				{{-- <li class="nav-item">
					<a class="nav-link" href="{{ route('user.convois.index') }}">Rechercher un convois</a>
				</li> --}}
				<li class="nav-item">
					<a class="nav-link" href="{{ route('user.convois.creer.premiereetape') }}">Créer un convois</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('app.qui_sommes_nous') }}">Qui sommes-nous ?</a>
				</li>
				@if (Auth::check())
					<li class="nav-item">
						<a class="nav-link" href="{{ route('user.dashboard') }}">Dashboard</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('logout') }}">Se déconnecter</a>
					</li>
				@else
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">Se connecter</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="{{ route('login') }}">S'inscrire</a>
					</li>
				@endif

			</ul>
		</div>
	</div>
</nav>
