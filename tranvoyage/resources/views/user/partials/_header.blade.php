<header id="layout-header">
    <!-- Header ============================================= -->
    <header id="header">
        <div class="container">
            <div class="header-row">
                <div class="header-column justify-content-start header-first">
                    <!-- Logo ============================= -->
                    <div class="logo"> <a class="d-flex" href="{{ route('home') }}" title="Tranvoyage"><img
                                src="{{ asset('assets/img/logo.jpg') }}" width="121" alt="Payyed" style="height: 75px;"/></a>
                    </div>
                    <!-- Logo end -->
                    <!-- Collapse Button ============================== -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav" id="navbarResponsive">
                        <span></span> <span></span> <span></span> </button><!-- Collapse Button end -->

                    <!-- Primary Navigation ============================== -->
                    <nav class="primary-menu navbar navbar-expand-lg">
                        <div id="header-nav" class="collapse navbar-collapse">
                            <ul class="navbar-nav mr-auto text-truncate">
                                <li class=" active "><a href="{{ route('home') }}">Accueil</a></li>
                                <!--<li class=""><a href="{{ route('user.ticket.rechercher') }}">Rechercher un ticket</a></li>-->
                                <li class=""><a href="{{ route('user.convois.creer.premiereetape') }}">Créer un convois</a></li>
                                <li class=""><a href="{{ route('app.qui_sommes_nous') }}">Qui sommes-nous ?</a></li>
                                <li class=""><a href="{{ route('app.contact') }}">Contact</a></li>
                                <!--@if (Auth::check())-->
                                <!--    <li class="li-navbar"><a href="{{ route('user.dashboard') }}">Mon compte</a> </li>-->
                                <!--    <li class="li-navbar"><a href="{{ route('logout') }}">Se déconnecter</a></li>-->
                                <!--@else-->
                                <!--    <li class="li-navbar"><a href="{{ route('login') }}">Se connecter</a> </li>-->
                                <!--    <li class="li-navbar"><a href="{{ route('register') }}">S'inscrire</a></li>-->
                                <!--@endif-->
                            </ul>
                        </div>
                    </nav>
                    <!-- Primary Navigation end -->
                </div>

                <div class="header-column justify-content-end header-two">
                    <!-- Login & Signup Link ============================== -->
                    <nav class="login-signup navbar navbar-expand">
                        <ul class="navbar-nav text-truncate">
                            @if (Auth::check())
                                <li><a href="{{ route('user.dashboard') }}" class="dashboard">Mon compte</a> </li>
                                <li class="align-items-center h-auto ml-sm-3 dashboard"><a class="btn btn-primary d-sm-block" style="color: #fff; cursor: pointer" href="{{ route('logout') }}">Se déconnecter</a></li>
                            @else
                                <li><a href="{{ route('login') }}">Se connecter</a> </li>
                                <li class="align-items-center h-auto ml-sm-3"><a class="btn btn-primary d-sm-block" style="color: #fff; cursor: pointer" href="{{ route('register') }}">S'inscrire</a></li>
                            @endif
                        </ul>
                    </nav>
                    <!-- Login & Signup Link end -->
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->        </header>
