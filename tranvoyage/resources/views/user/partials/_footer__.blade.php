<footer class="footer-area ">
    <div class="container ">
        <div class="row">
            <div class="col-sm-6 col-md-7">
                <div class="single-footer-widget ">
                    <h4>INSCRIPTION À LA NEWSLETTER</h4>
                    <div class="form-wrap " id="mc_embed_signup ">
                        <form action="#" class="form-inline">
                            <input class="form-control " name="EMAIL " placeholder="Votre adresse email"
                                onfocus="this.placeholder='' " onblur="this.placeholder='Votre adresse email ' "
                                required=" " type="email ">
                            <button onclick="newsletter();" class="click-btn btn btn-default text-uppercase ">
                                <i class="far fa-paper-plane "></i>
                            </button>
                            <div style="position: absolute; left: -5000px; ">
                                <input name="" tabindex="-1 " value=" " type="text ">
                            </div>
                            <div class="info "></div>
                        </form>
                    </div>
                    <p>Abonnez-vous à notre newsletter pour recevoir les dernières nouvelles et offres .</p>
                </div>
            </div>
            <div class="offset-md-1 col-sm-6 col-md-3 ">
                <div class="single-footer-widget footer_icon ">
                    <h4>NOUS CONTACTER</h4>
                    <p>4156, Rue des jardins Cocody, Cote d'Ivoire (+225) 07 7948 1690</p>
                    <span><a href="mailto:contact@tranvoyage.com"
                            class="__cf_email__ ">contact@tranvoyage.com</a></span>
                    <div class="social-icons ">
                        <a target="_blank" href="https://www.facebook.com/tranvoyage"><i
                                class="ti-facebook "></i></a>
                        <!--<a href="# "><i class="ti-twitter-alt "></i></a>-->
                        <!--<a href="# "><i class="ti-pinterest "></i></a>-->
                        <a target="_blank" href="https://www.instagrame.com/tranvoyage/"><i
                                class="ti-instagram "></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid ">
        <div class="row justify-content-center ">
            <div class="col-lg-12 ">
                <div class="copyright_part_text text-center ">
                    <p class="footer-text m-0 ">
                        Copyright &copy;
                        <script data-cfasync="false " src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js "></script>
                        <script>
                            document.write(new Date().getFullYear());
                        </script> Tous droits réservés | Développer par l'équipe de <a
                            target="_blank ">Tranvoyage.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>