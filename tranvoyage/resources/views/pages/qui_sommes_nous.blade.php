@extends('layouts.app')

@section('content')

    <style>

        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">

                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="top_place section_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <h3 class="text-10">Qui sommes nous</h3>
                    <p class="text-4">
                        <span class="">TRANVOYAGE</span> ou le ” Blablacar de l’intérieur” est une plateforme communautaire d’annonce de convoi, qui permet à tout voyageur de trouver et de voyager dans n’importe quel convoi.
                        Les conducteurs disposant d’assez de place dans leurs véhicules, partagent aux passagers à travers une annonce sur  <a href="https://www.tranvoyage.com">www.tranvoyage.com</a>  pour des moyennes et longues distances.
                        Tranvoyage est disponible pour des transports partout à Abidjan.
                    </p>
                    {{-- <p class="text-4">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam assumenda eius incidunt
                        possimus quidem quisquam quo quos soluta veritatis voluptatibus. Alias earum id quo ratione
                        repellendus? Adipisci eligendi ipsum odit.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam assumenda eius incidunt
                        possimus quidem quisquam quo quos soluta veritatis voluptatibus. Alias earum id quo ratione
                        repellendus? Adipisci eligendi ipsum odit.
                    </p> --}}
<!--                    <div class="mb-3">
                        <label class="form-label">Nom et prénom</label>
                        <input type="text" class="form-control" >
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input type="text" class="form-control" >
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Message</label>
                        <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                    </div>-->
                </div>
            </div>
<!--            <a href="{{ route('user.convois.creer.troisiemeetape') }}" class="btn btn-md btn-success float-left">
               Envoyer</a>-->
        </div>
    </section>

@endsection
