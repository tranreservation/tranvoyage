
@extends('layouts.app')

@section('content')

    <style>

        #search-ticket{
            border: 0;
                border-top-color: currentcolor;
                border-right-color: currentcolor;
                border-bottom-color: currentcolor;
                border-left-color: currentcolor;
            border-top-color: currentcolor;
            border-right-color: currentcolor;
            border-bottom-color: currentcolor;
            border-left-color: currentcolor;
            display: block;
            padding: 10px 32px;
            font-size: 16px;
            color: #555;
            box-shadow: none;
            outline: 0;
            border-color: #fff;
            border-radius: 3px 0px 0px 3px;
            height: 68px !important;
            box-shadow: 0px 8px 20px 0px rgba(0, 0, 0, 0.15);
            font-size: 20px;
            text-align: center;
        }

        #fa-search{
            white-space: nowrap;
            color: #fff;
            border: 0;
            cursor: pointer;
            background: #63c76a;
            transition: all .2s ease-out, color .2s ease-out;
            height: 68px;
            border-radius: 0px .25rem .25rem 0px;
        }

        .pannel{}

        #pannel-1, #pannel-2{
            background: red;
        }

    </style>
    <section class="banniere">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">

                        </div>
                    </div>
                </div>
            </div>
            </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="input-group-overlay d-none d-lg-flex mx-4"> <input class="form-control appended-form-control"
                type="text" placeholder="Search for products">
            <div class="input-group-append-overlay"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
        </div>
    </section>

    <section class="top_place section_padding">
        <div class="container">
            <div class="row" style="padding-top: 14px;">

                <div class="col-md-4 mb-4">
                    <div class="bg-white shadow-md rounded h-100 p-3">
                        <div class="featured-box text-center">
                            <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-map-marker-alt" aria-hidden="true"></i></div>
                            <h3>Situation géographique</h3>
                            <p>Cocody Angré Château - carrefour Immeuble batime</p>
                        </div>
                    </div>
                </div>
            
                <div class="col-md-4 mb-4">
                    <div class="bg-white shadow-md rounded h-100 p-3">
                        <div class="featured-box text-center">
                            <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-phone" aria-hidden="true"></i> </div>
                            <h3>Téléphone</h3>
                            <p class="mb-0">(+225) 47446373</p>
                            <p class="mb-0">(+225) 46033025</p>
                            <p class="mb-0">(+225) 04755552</p>
                        </div>
                    </div>
                </div>
            
                <div class="col-md-4 mb-4">
                    <div class="bg-white shadow-md rounded h-100 p-3">
                        <div class="featured-box text-center">
                            <div class="featured-box-icon text-primary mt-4"> <i class="fas fa-envelope" aria-hidden="true"></i> </div>
                            <h3>Email</h3>
                            <p>support@tranvoyage.com</p>
                        </div>
                    </div>
                </div>
            
                <div class="offset-md-1 col-md-10">
                    <form id="signupForm" method="post" data-request-flash="" data-request="onSendEmail">
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                        <label for="emailAddress">Nom et prénoms</label>
                        <input type="text" class="form-control" id="emailAddress" required="" placeholder="Entrez votre nom et prénoms" name="nameAndSurname">
                    </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                        <label for="emailAddress">Email</label>
                        <input type="email" class="form-control" id="emailAddress" required="" placeholder="Entrez votre email" name="email">
                    </div>
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Message</label>
                        <textarea class="form-control" id="loginPassword" rows="5" name="content"></textarea>
                    </div>
                    <button class="float-right col-md-3 btn btn-primary btn-block my-4" type="submit">Envoyer</button>
                </form>
                </div>
            
                 <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <iframe src="https://maps.google.com/maps?q=BATIM%2C%20Abidjan&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed" style="border:0" allowfullscreen="" width="100%" height="600" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>

@endsection