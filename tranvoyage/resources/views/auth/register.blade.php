@extends('layouts.app')

@section('content')

<div class="login-signup-page mx-auto my-5">
    <h3 class="font-weight-400 text-center">S'inscrire</h3>
    <p class="lead text-center">Vos informations d'inscription sont en sécurité avec nous.</p>
    <div class="bg-light shadow-md rounded p-4 mx-2">
        <form id="signupForm" method="post" data-request-flash="" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <label for="full">Nom</label>
                <input type="text" class="form-control" id="fullName" required="" placeholder="Entrez votre nom" name="name" @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                @error('name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="emailAddress">Email</label>
                <input type="email" class="form-control" id="emailAddress" required="" placeholder="Entrez votre email" name="email" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
                <div class="form-group">
                <label for="telephone">Telephone</label>
                <input type="text" class="form-control" id="telephone" required="" placeholder="Entrez votre téléphone" name="telephone" @error('telephone') is-invalid @enderror" name="telephone" value="{{ old('telephone') }}" required autocomplete="telephone">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="loginPassword">Mot de passe</label>
                <input type="password" class="form-control" id="loginPassword" required="" placeholder="Entrez votre mot de passe" name="password" @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="loginPassword">Confirmer le mot de passe</label>
                <input type="password" class="form-control" id="loginPassword" required="" placeholder="Retaper le mot de passe" name="password_confirmation">
            </div>
            <button class="btn btn-primary btn-block my-4" type="submit">S'inscrire</button>
        </form>
        <p class="text-3 text-muted text-center mb-0">Avez vous déja un compte ? <a class="btn-link" href="{{ route('login') }}">Se connecter</a></p>
    </div>
</div>
@endsection
