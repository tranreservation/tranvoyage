@extends('layouts.app')

@section('content')

<div class="login-signup-page mx-auto my-5">

    <!--<h3 class="font-weight-400 text-center">Connexion</h3>-->
    <!--<p class="lead text-center">Vos informations d'inscription sont en sécurité avec nous.</p>-->
    <div class="bg-light shadow-md rounded p-4 mx-2">
        <img src="http://placehold.it/150x50?text=Logo" alt="Payyed" width="198" height="100" style="margin-bottom: 32px;margin-left: 28%;margin-top: 1%;">
        <form id="signupForm" method="post"  action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <label for="emailAddress">Email</label>
                <input type="email" class="form-control" id="emailAddress" name="email" required
                       placeholder="Entrez votre email" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="loginPassword">Password</label>
                <input type="password" class="form-control" id="loginPassword" required
                       placeholder="Entrez votre mot de passe" name="password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <button class="btn btn-primary btn-block my-4" type="submit">{{ __('Login') }}</button>
        </form>
        <p class="text-3 text-muted text-center mb-0"><a href="#">Mot de passe oublié ?</a>  /
            <a class="btn-link" href="{{ route('register') }}">S'inscrire</a></p>

    </div>
</div>

@endsection
