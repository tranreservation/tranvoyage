<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/themify-icons.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/flaticon.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/fontawesome/css/all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/gijgo.min.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/nice-select.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/js/build/css/intlTelInput.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/js/build/css/demo.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
    <link href="{{ asset('assets/css/jquery.dataTables.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/js/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.27.0/slimselect.min.css" rel="stylesheet">
    <link id="bsdp-css" href="{{ asset('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}"
        rel="stylesheet">

</head>

<body>
    <div id="app">
       
        @include('user.partials._header')

        <main>
            @yield('content')
        </main>

        @include('user.partials._footer')
        
    </div>

    <!-- <script src="js/French.json"></script> -->
    <script src="{{ asset('assets/js/jquery-1.12.1.min.js') }}"></script>
    <!-- <script src="js/jquery-3.4.1.slim.min.js"></script> -->

    <script src="{{ asset('assets/js/popper.min.js') }}"></script>

    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.magnific-popup.js') }}"></script>

    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('assets/js/masonry.pkgd.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/js/gijgo.min.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.form.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/mail-script.js') }}"></script>
    <script src="{{ asset('assets/js/contact.js') }}"></script>

    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/js/build/js/intlTelInput.js') }}"></script>

    <script src="{{ asset('assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        var input = document.querySelector("#tel_ticket_");
        window.intlTelInput(input, {
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            geoIpLookup: function(callback) {
                $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            // hiddenInput: "full_number",
            // initialCountry: "auto",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            // separateDialCode: true,
            utilsScript: "js/build/js/utils.js",
        });
    </script>

    <script src="{{ asset('assets/js/function.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script src="{{ asset('assets/js/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- <script src="js/toastr/toast.script.js"></script> -->


    <script type="text/javascript">
        $(document).ready(function() {
            // alert();

            $(".chargement").html('<img src="img/loading.gif" style="width:70px;height:70px;" />').show();
            $(".gars_aff").hide();

            setTimeout(function() {
                $(".chargement").html('<img src="img/loading.gif" style="width:70px;height:70px;" />')
                    .hide();
                $(".gars_aff").show();
            }, 1500);


            $('#example').DataTable({
                "scrollY": 200,
                "scrollX": true,
                language: {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    //  zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    // emptyTable:     "Aucune donnée disponible dans le tableau",
                    zeroRecords: "",
                    emptyTable: "",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }
            });

            change_gare('0');

            $("#btn_envoie_rech").on('click', function() {
                // alert();
                change_gare('0');
            })

        });


        //gare
        // function change_gare(page_id_nav) {
        //     /*var gare_dep = $("#gare_dep").val();
        //     var gare_arr = $("#gare_arr").val();
        //     var date_dep = $("#datepicker_2").val();*/
        //     var gare_dep = $("#gare_dep_2").val();
        //     var gare_arr = $("#gare_arr_2").val();
        //     var date_dep = $("#datepicker_2").val();
        //     // alert(date_dep)
        //     // alert(gare_dep);
        //     var dataString = 'page_id=' + page_id_nav + '&gare_dep=' + gare_dep + "&gare_arr=" + gare_arr + "&date_dep=" +
        //         date_dep;
        //     $.ajax({
        //         type: "POST",
        //         url: "template/charge_gare.php",
        //         data: dataString,
        //         cache: false,
        //         success: function(data) {
        //             // alert(data);
        //             // $(".gars_aff").html(data);
        //             var text = $(".gars_aff").text();
        //             text += "text";
        //             $(".gars_aff").html(data);
        //             // $(".gars_aff").insertAfter('ddddd')
        //             console.log("Append html");
        //         }
        //     });
        // }
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
        //  $('#example').DataTable();

        new SlimSelect({
            select: '#gare_dep'
        })

        new SlimSelect({
            select: '#gare_arr'
        })
    </script>
    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function() {
            var options = {
                whatsapp: "+225 07 7948 1690", // WhatsApp number
                call_to_action: "Contactez-nous", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol,
                host = "getbutton.io",
                url = proto + "//static." + host;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function() {
                WhWidgetSendButton.init(host, proto, options);
            };
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /GetButton.io widget -->





</body>

</html>
