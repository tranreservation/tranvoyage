<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <link rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i'
          type='text/css'>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/owl.carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/stylesheet.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/flaticon.css') }}">
    {{-- <script src="https://kit.fontawesome.com/a076d05399.js"></script> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

    <style>
        a:hover, a:focus{
            text-decoration: none !important;
        }

        .bottommargin {
            margin-bottom: 50px !important;
        }

        .entry-image,
        .entry-image .slide a,
        .entry-image img,
        .entry-image>a {
            display: block;
            position: relative;
            width: 100%;
            height: auto;
        }

        .entry-image {
            margin-bottom: 30px;
        }

        .entry-image,
        .entry-image .slide a,
        .entry-image img,
        .entry-image>a {
            display: block;
            position: relative;
            width: 100%;
            height: auto;
        }

        .actualite a {
            text-decoration: none !important;
            color: #1abc9c;
        }

        .ipost .entry-image img {
            border-radius: 0;
        }

        .entry-image,
        .entry-image .slide a,
        .entry-image img,
        .entry-image>a {
            display: block;
            position: relative;
            width: 100%;
            height: auto;
        }

        .ipost .entry-title h3,
        .ipost .entry-title h4 {
            margin: 0;
            font-size: 16px;
            font-weight: 600;
        }

        .ipost .entry-title h3 a,
        .ipost .entry-title h4 a {
            color: #333;
        }

        .ipost .entry-meta {
            margin-right: -10px;
        }

        .entry-meta {
            margin: 10px -10px -15px 0;
            margin-right: -10px;
            list-style: none;
        }

        .ipost .entry-meta li {
            font-size: 13px;
            margin-right: 10px;
        }

        .entry-meta li {
            float: left;
            font-size: 13px;
            line-height: 14px;
            margin: 0 10px 15px 0;
            margin-right: 10px;
            color: #999;
            font-family: crete round, serif;
            font-style: italic;
        }

        .ipost .entry-meta li {
            font-size: 13px;
        }

        .entry-meta li {
            font-size: 13px;
            line-height: 14px;
            color: #999;
            font-family: crete round, serif;
            font-style: italic;
        }

        .entry-meta {
            list-style: none;
        }

        .entry-meta li i {
            position: relative;
            top: 1px;
            font-size: 14px;
            margin-right: 3px;
        }

        .ipost .entry-meta li {
            font-size: 13px;
        }

        .entry-meta li {
            font-size: 13px;
            line-height: 14px;
            color: #999;
            font-family: crete round, serif;
            font-style: italic;
        }

        .entry-meta li::before {
            content: '/';
            display: inline-block;
            margin-right: 10px;
            opacity: .5;
        }

        .entry-meta li a {
            color: #999;
        }

        .entry-meta li i {
            position: relative;
            top: 1px;
            font-size: 14px;
            margin-right: 3px;
        }

        .icon-comments::before {
            content: "\e9b8";
        }

        .ipost .entry-content {
            margin-top: 20px;
        }

        .entry-content {
            position: relative;
        }

        #content p {
            line-height: 1.8;
        }

        .ipost .entry-content p {
            margin-bottom: 0;
        }


        .clearfix::after {

            display: block;
            clear: both;
            content: "";
        }

        a {
            text-decoration: none !important;
            color: #1abc9c;
        }

        .owl-stage-outer.owl-height{
            height: 400px !important;
        }

        .hero-bg{
            height: 400px !important;
        }

        .btn-primary:focus{
            color: #fff;
        }
    </style>

</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div data-loader="dual-ring"></div>
</div>
<!-- Preloader End -->


<!-- Document Wrapper============================================= -->
<div id="main-wrapper">

    <!-- Header -->
@include('user.partials._header')

<!-- Content end -->
@yield('content')
<!-- Content start -->

    <!-- Footer start -->
@include('user.partials._footer')
<!-- Footer end -->
</div>
<!-- Document Wrapper end -->


<!-- Back to Top ============================================= -->
<a id="back-to-top" data-toggle="tooltip" title="Back to Top" href="javascript:void(0)">
    <i class="fa fa-chevron-up"></i>
</a>

<!-- Video Modal ============================================= -->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content bg-transparent border-0">
            <button type="button" class="close text-white opacity-10 ml-auto mr-n3 font-weight-400"
                    data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-body p-0">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" id="video" allow="autoplay"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Video Modal end -->

<!-- Login form Modal ============================================= -->
<div class="modal" tabindex="-1" id="showLoginFormModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Connexion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" data-request-flash data-request="onSignin">
                    <input type="hidden" name="intended" value="true">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="login" class="form-control" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label>Mot de passe</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-primary" style="float: right;">Connexion</button>
                    <p><a href="http://hblessingscare.com/inscription">Voulez vous créez un compte ?</a></p>
                </form>
            </div>
            <!--<div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>
<!-- Login form Modal  end -->

<!-- Script -->
<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
<!--<script src="http://hblessingscare.com/themes/projettopup/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/theme.js') }}"></script>
<script src="{{ asset('assets/js/responsive.js') }}"></script>

<!-- Scripts -->
<!--<script src="http://hblessingscare.com/themes/projettopup/assets/vendor/jquery.js"></script>-->
<!--<script src="http://hblessingscare.com/themes/projettopup/assets/vendor/bootstrap.js"></script>-->
{{-- <script src="{{ asset('assets/js/theme.js') }}"></script> --}}
<script src="{{ asset('assets/javascript/app.js') }}"></script>

<script>
    $(document).ready(function (e) {
        $("input[name='typeuser']").click(function (e) {
            if (e.target.value == 1) {
                $("#agence_first_login").css('display', 'block');
                $("#demarcheur_first_login").css('display', 'none');
            } else {
                $("#agence_first_login").css('display', 'none');
                $("#demarcheur_first_login").css('display', 'block');
            }
        });

        // afficher les informations (mail, tel) du demarcheur ou de l'agence
        let contact = 0;
        $("a[href='#contactPhone'").click(function () {
            $(".text-area").css('display', 'block');
            $("#contactPhone").css('display', 'flex');
            contact += 1;
            displayTextArea(2);
        });
        $(".contactForm").click(function () {
            $(".text-area").css('display', 'block');
            $("#contactForm").css('display', 'flex');
            contact += 1;
            displayTextArea(1);
        });

        let displayTextArea = function (form) {
            if (contact > 1) {
                $(".text-area").removeClass('warning-small').addClass('warning-large');
            } else {
                if (form == 2) {
                    $("#contactPhone").css('float', 'left');
                }
                $(".text-area").addClass('warning-small');
            }
        }

        $("#commandeProduitModal").modal('toggle');
    });
</script>
<!-- disposition maison -->

<script>
    // $("#collapse-navbar").css({'display' :'none !important'});
    // $("#collapse-navbar .navbar-nav").css('display', 'none');

    $("#btnResponsive").click(function (e) {
        if ($("#layout-header").hasClass("responsive")) {
            $("#layout-header").removeClass("responsive");
            // $("#collapse-navbar").removeClass("responsive-collapse");
            $("#collapse-navbar .navbar-nav").css('display', 'none');
        } else {
            $("#layout-header").addClass("responsive");
            // $("#collapse-navbar").addClass("responsive-collapse");
            $("#collapse-navbar .navbar-nav").css('display', 'block');
        }
    });
</script>


<script>
    $(document).ready(function (e) {
        $("input[name='typeuser']").click(function (e) {
            if (e.target.value == 1) {
                $("#agence_first_login").css('display', 'block');
                $("#demarcheur_first_login").css('display', 'none');
            } else {
                $("#agence_first_login").css('display', 'none');
                $("#demarcheur_first_login").css('display', 'block');
            }
        });

        // afficher les informations (mail, tel) du demarcheur ou de l'agence
        let contact = 0;
        $("a[href='#contactPhone'").click(function () {
            $(".text-area").css('display', 'block');
            $("#contactPhone").css('display', 'flex');
            contact += 1;
            displayTextArea(2);
        });
        $(".contactForm").click(function () {
            $(".text-area").css('display', 'block');
            $("#contactForm").css('display', 'flex');
            contact += 1;
            displayTextArea(1);
        });

        let displayTextArea = function (form) {
            if (contact > 1) {
                $(".text-area").removeClass('warning-small').addClass('warning-large');
            } else {
                if (form == 2) {
                    $("#contactPhone").css('float', 'left');
                }
                $(".text-area").addClass('warning-small');
            }
        }

        // chargement des images
        $(".imageminia").click(function (e) {
            e.preventDefault();
            // alert("chargement d'image");
            // console.log(e.target.attr('imgptf'));
            let indeximg = $(this).data('imgptf');
            // console.log($(this).data('imgptf'));
            // console.log($(".imggf-"+indeximg).attr('src'));
            // $(".slide.active img").attr('src', $(".imggf-"+indeximg).attr('src'));
            // console.log();
            $(".slide.active").removeClass("active").css("opacity", 0);
            $(".imggf-" + indeximg).parent().addClass("active").css("opacity", '');
            $(".img-miniature li.active").removeClass("active");
            $(this).parent().addClass('active');
        });

        $("#navbarResponsive").toggle(
            function(){ console.log("open");}
        });

        // $("#navbarResponsive").click(function(){
        //     if($("#navbarResponsive").hasClass("collapsed")){
        //         console.log("collapsed existe");
        //     }else{
        //         console.log("collapsed n'existe pas");
        //     }
        // });

    });
</script>





</body>

</html>
