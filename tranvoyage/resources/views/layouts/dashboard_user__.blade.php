<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <link rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i'
          type='text/css'>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/owl.carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/stylesheet.css') }}">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <style>
        a:hover, a:focus{
            text-decoration: none !important;
        }

        .bottommargin {
            margin-bottom: 50px !important;
        }

        .entry-image,
        .entry-image .slide a,
        .entry-image img,
        .entry-image>a {
            display: block;
            position: relative;
            width: 100%;
            height: auto;
        }

        .entry-image {
            margin-bottom: 30px;
        }

        .entry-image,
        .entry-image .slide a,
        .entry-image img,
        .entry-image>a {
            display: block;
            position: relative;
            width: 100%;
            height: auto;
        }

        .actualite a {
            text-decoration: none !important;
            color: #1abc9c;
        }

        .ipost .entry-image img {
            border-radius: 0;
        }

        .entry-image,
        .entry-image .slide a,
        .entry-image img,
        .entry-image>a {
            display: block;
            position: relative;
            width: 100%;
            height: auto;
        }

        .ipost .entry-title h3,
        .ipost .entry-title h4 {
            margin: 0;
            font-size: 16px;
            font-weight: 600;
        }

        .ipost .entry-title h3 a,
        .ipost .entry-title h4 a {
            color: #333;
        }

        .ipost .entry-meta {
            margin-right: -10px;
        }

        .entry-meta {
            margin: 10px -10px -15px 0;
            margin-right: -10px;
            list-style: none;
        }

        .ipost .entry-meta li {
            font-size: 13px;
            margin-right: 10px;
        }

        .entry-meta li {
            float: left;
            font-size: 13px;
            line-height: 14px;
            margin: 0 10px 15px 0;
            margin-right: 10px;
            color: #999;
            font-family: crete round, serif;
            font-style: italic;
        }

        .ipost .entry-meta li {
            font-size: 13px;
        }

        .entry-meta li {
            font-size: 13px;
            line-height: 14px;
            color: #999;
            font-family: crete round, serif;
            font-style: italic;
        }

        .entry-meta {
            list-style: none;
        }

        .entry-meta li i {
            position: relative;
            top: 1px;
            font-size: 14px;
            margin-right: 3px;
        }

        .ipost .entry-meta li {
            font-size: 13px;
        }

        .entry-meta li {
            font-size: 13px;
            line-height: 14px;
            color: #999;
            font-family: crete round, serif;
            font-style: italic;
        }

        .entry-meta li::before {
            content: '/';
            display: inline-block;
            margin-right: 10px;
            opacity: .5;
        }

        .entry-meta li a {
            color: #999;
        }

        .entry-meta li i {
            position: relative;
            top: 1px;
            font-size: 14px;
            margin-right: 3px;
        }

        .icon-comments::before {
            content: "\e9b8";
        }

        .ipost .entry-content {
            margin-top: 20px;
        }

        .entry-content {
            position: relative;
        }

        #content p {
            line-height: 1.8;
        }

        .ipost .entry-content p {
            margin-bottom: 0;
        }


        .clearfix::after {

            display: block;
            clear: both;
            content: "";
        }

        a {
            text-decoration: none !important;
            color: #1abc9c;
        }

        .owl-stage-outer.owl-height{
            height: 400px !important;
        }

        .hero-bg{
            height: 400px !important;
        }

        .btn-primary:focus{
            color: #fff;
        }
    </style>

</head>

<body>

    <!-- Preloader -->
    <div id="preloader">
        <div data-loader="dual-ring"></div>
    </div>
    <!-- Preloader End -->


    <!-- Document Wrapper============================================= -->
    <div id="main-wrapper">

        <!-- Header -->
        @include('user.partials._header')

        <!-- Content end -->
        @yield('content')
        <!-- Content start -->

        <!-- Footer start -->
        @include('user.partials._footer')
        <!-- Footer end -->
    </div>
    <!-- Document Wrapper end -->

    <div id="app">

        @include('user.partials._header')

        <main>

            <section class="top_place section_padding">
                <div class="container">
                    <div class="row" style="margin-top:-15px;">
                        <div class="col-md-12">
                            <div class="row" style="display: flex;">
                            <div class="col-md-4">
                                <div>
                                    Solde: {{ $wallet->solde }} F CFA
                                </div>
                                <div class="list-group">
                                    <a href="{{ route('user.dashboard') }}" class="list-group-item list-group-item-action active">
                                        Dasboard
                                    </a>
                                    <a href="{{ route('user.convois.index') }}" class="list-group-item list-group-item-action">Convois</a>
                                    <a href="{{ route('user.retraits.index') }}" class="list-group-item list-group-item-action">Retraits</a>
                                    <a href="{{ route('user.transactions.index') }}" class="list-group-item list-group-item-action">Transactions</a>
                                    <a href="{{ route('user.tickets.index') }}" class="list-group-item list-group-item-action">Tickets</a>
                                    <a href="{{ route('user.profile.edit') }}" class="list-group-item list-group-item-action">Mon compte</a>
                                </div>
                            </div>

                            <div class="col-md-8">
                                @yield('content')
                            </div>

                        </div>
                    </div>

                </div>
            </section>
        </main>

        @include('user.partials._footer')


    </div>

    <!-- <script src="js/French.json"></script> -->
    <script src="{{ asset('assets/js/jquery-1.12.1.min.js') }}"></script>
    <!-- <script src="js/jquery-3.4.1.slim.min.js"></script> -->

    <script src="{{ asset('assets/js/popper.min.js') }}"></script>

    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.magnific-popup.js') }}"></script>

    <script src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>

    <script src="{{ asset('assets/js/masonry.pkgd.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('assets/js/gijgo.min.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.form.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/mail-script.js') }}"></script>
    <script src="{{ asset('assets/js/contact.js') }}"></script>

    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.responsive.js') }}"></script>
    <script src="{{ asset('assets/js/build/js/intlTelInput.js') }}"></script>

    <script src="{{ asset('assets/assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        var input = document.querySelector("#tel_ticket_");
        window.intlTelInput(input, {
            // allowDropdown: false,
            // autoHideDialCode: false,
            // autoPlaceholder: "off",
            // dropdownContainer: document.body,
            // excludeCountries: ["us"],
            // formatOnDisplay: false,
            geoIpLookup: function(callback) {
                $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            // hiddenInput: "full_number",
            // initialCountry: "auto",
            // localizedCountries: { 'de': 'Deutschland' },
            // nationalMode: false,
            // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            // placeholderNumberType: "MOBILE",
            // preferredCountries: ['cn', 'jp'],
            // separateDialCode: true,
            utilsScript: "js/build/js/utils.js",
        });
    </script>

    <script src="{{ asset('assets/js/function.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script src="{{ asset('assets/js/sweetalert2/sweetalert2.min.js') }}"></script>
    <!-- <script src="js/toastr/toast.script.js"></script> -->


    <script type="text/javascript">
        $(document).ready(function() {
            // alert();

            $(".chargement").html('<img src="img/loading.gif" style="width:70px;height:70px;" />').show();
            $(".gars_aff").hide();

            setTimeout(function() {
                $(".chargement").html('<img src="img/loading.gif" style="width:70px;height:70px;" />')
                    .hide();
                $(".gars_aff").show();
            }, 1500);


            $('#example').DataTable({
                "scrollY": 200,
                "scrollX": true,
                language: {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    //  zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    // emptyTable:     "Aucune donnée disponible dans le tableau",
                    zeroRecords: "",
                    emptyTable: "",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }
            });

            change_gare('0');

            $("#btn_envoie_rech").on('click', function() {
                // alert();
                change_gare('0');
            })

        });


        //gare
        // function change_gare(page_id_nav) {
        //     /*var gare_dep = $("#gare_dep").val();
        //     var gare_arr = $("#gare_arr").val();
        //     var date_dep = $("#datepicker_2").val();*/
        //     var gare_dep = $("#gare_dep_2").val();
        //     var gare_arr = $("#gare_arr_2").val();
        //     var date_dep = $("#datepicker_2").val();
        //     // alert(date_dep)
        //     // alert(gare_dep);
        //     var dataString = 'page_id=' + page_id_nav + '&gare_dep=' + gare_dep + "&gare_arr=" + gare_arr + "&date_dep=" +
        //         date_dep;
        //     $.ajax({
        //         type: "POST",
        //         url: "template/charge_gare.php",
        //         data: dataString,
        //         cache: false,
        //         success: function(data) {
        //             // alert(data);
        //             // $(".gars_aff").html(data);
        //             var text = $(".gars_aff").text();
        //             text += "text";
        //             $(".gars_aff").html(data);
        //             // $(".gars_aff").insertAfter('ddddd')
        //             console.log("Append html");
        //         }
        //     });
        // }
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
        //  $('#example').DataTable();

        new SlimSelect({
            select: '#gare_dep'
        })

        new SlimSelect({
            select: '#gare_arr'
        })
    </script>
    <!-- GetButton.io widget -->
    <script type="text/javascript">
        (function() {
            var options = {
                whatsapp: "+225 07 7948 1690", // WhatsApp number
                call_to_action: "Contactez-nous", // Call to action
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol,
                host = "getbutton.io",
                url = proto + "//static." + host;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function() {
                WhWidgetSendButton.init(host, proto, options);
            };
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /GetButton.io widget -->





</body>

</html>
