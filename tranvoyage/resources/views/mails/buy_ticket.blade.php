<b style='color:#000;'>Bonjour, </b><br />
<br />Votre réservation de tickets a été éffectué avec succès !
<br />
<p style='text-align:center;background:#3498DB;padding:5px;color:white;font-size:14px;font-weight:bold;'>Informations
    sur la reservation</p><br />

<!DOCTYPE html>
<html>

<head>
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            padding: 5px;
        }

        td {
            padding: 5px;
            text-align: center;
            background: #3498DB;
            color: white;
        }
    </style>
</head>

<body>
    <table>
        <tr>
            <th>Téléphone</th>
            <th>Trajet</th>
            <th>Date de départ</th>
            <th>Compagnie</th>
            <th>Nombre de billets</th>
            <th>Montant unitaire billets</th>
            <th>Montant Total</th>
        </tr>
        <tr>
            <td>{{ $telephone }}</td>
            <td>{{ $depart_gare_libelle }} - {{ $arrivee_gare_libelle }}</td>
            <td>{{ $date_depart }} à {{ $heure_depart }}</td>;
            <td>{{ $compagnie }}</td>;
            <td>{{ $nombre_ticket }}</td>;
            <td>{{ $prix }}XOF</td>;
            <td>{{ $montant_total }} XOF</td>
        </tr>
    </table>
    <br />
    </table>

    <br />

    Merci pour votre fidelité, <b>Tranvoyage.com</b> vous souhaite un trè beau voyage.
</body>

</html>