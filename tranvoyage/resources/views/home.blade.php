@extends('layouts.app')

@section('content')

    <section class="banniere">
        <div class="container">
            <div class="col-md-12">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                        <div class="booking_form">
                            <form action="{{ route('home') }}" class="form-inline row">
                                <div class="form_group col-md-3 col-12">
                                    <select class="" name="lieu_depart_id">
                                        <option value="">Chosir la gare de départ</option>
                                        @foreach ($lieux as $lieu)
                                            <option value="{{ $lieu->id }}" @if(isset($data['lieu_depart_id']) && $data['lieu_depart_id'] == $lieu->id) selected @endif>{{ $lieu->libelle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form_group col-md-3 col-12">
                                    <select class="" name="lieu_arrive_id">
                                        <option value="">Choisir la gare d'arrivée </option>
                                        @foreach ($lieux as $lieu)
                                            <option value="{{ $lieu->id }}" @if(isset($data['lieu_arrive_id']) && $data['lieu_arrive_id'] == $lieu->id) selected @endif>{{ $lieu->libelle }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form_group col-md-3 col-12" >
                                    <input id="datepicker_2" name="date_depart" value="@if(isset($data['date_depart'])) {{ $data['date_depart'] }} @endif" placeholder="Choisir la date de départ" readonly="true">
                                </div>
                                <div class="form_group col-md-3 col-12">
                                        <button type="submit" class="btn_1" id="btn_envoie_rech"
                                        style="background:#7DBF50;"><i class="fa fa-search"></i>Rechercher</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <span class="first-span">LA LISTE DES GARES SELON LES DESTINATIONS</span>
            </div>
            <div class="col-md-12">
                <span class="two-span">Plus de besoin de vous déplacer . Vous</span>
                <span class="three-span">pouvez acheter votre ticket </span>
                <span class="two-span">en toute sécurité </span>
                <span class="first-span">ici.</span>
            </div>
        </div>
    </section>

    <section class="booking_part" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="booking_content">
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                                <div class="booking_form">
                                    <form action="#">
                                        <div class="form-row">
                                            <div class="form_colum">
                                                <select class="" id="gare_dep"
                                                    style="border-radius:5px;border:5px solid #7DBF50;">
                                                    <option value="">choisir une gare de départ</option>
                                                    <option value="">Gare 1</option>
                                                    <option value="">Gare 2</option>
                                                    <option value="">Gare 3</option>
                                                    <option value="">Gare 4</option>
                                                </select>
                                            </div>
                                            <div class="form_colum">
                                                <select class="" id="gare_arr"
                                                    style="border-radius:5px;border:5px solid #7DBF50;">
                                                    <option value="">choisir une gare d'arrivée </option>
                                                    <option value="">Gare 1</option>
                                                    <option value="">Gare 2</option>
                                                    <option value="">Gare 3</option>
                                                    <option value="">Gare 4</option>
                                                </select>
                                            </div>
                                            <div class="form_colum" style="margin-top:-4px;">
                                                <input id="datepicker_2" style="border-radius:5px;border:5px solid #7DBF50;"
                                                    placeholder="Choisir la date de départ" readonly="true">
                                            </div>
                                            <div class="form_btn">
                                                <a href="#" class="btn_1" id="btn_envoie_rech"
                                                    style="background:#7DBF50;">Rechercher <i class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="top_place section_padding">
        <div class="container">
            <div class="row" style="margin-top:-15px;">
                <div class="col-md-12">
                    <div class="row" style="display: flex;">
                        <style>
                            .mobile10 {
                                display: none
                            }

                            #logo {
                                /*display: none*/
                            }

                        </style>

                        @inject('helpService', 'App\Services\HelpService')
                       
                        @foreach ($voyages as $voyage)
                            <form class="col-md-6 col-gars_affiche" action="{{ route('user.convois.reservervation.premiereetape') }}">
                                <div class="gars_affiche">
                                    <div class="row">
                                        <div class="col-3 col-md-3 col-lg-3" id="col-logo">
                                            <span id="logo-7"><i class="fa fa-bus fa-7x"></i></span>
                                            <span id="logo-6"><i class="fa fa-bus fa-6x"></i></span>
                                        </div>
                                        <div class="col-3 col-md-3 col-lg-3 mobile10">
                                            <i class="fa fa-bus fa-3x" style="padding:10px;color:#fff;"></i>
                                            <span style="color:white;margin-top:-100px;display:none">Jeu 08 Jui 2021</span>
                                            <a href="{{ route('user.convois.reservervation.premiereetape') }}" data-id="14" class="btn"
                                                type="button "
                                                style="background-color:#03989E;color:#fff;height:30px;margin-top:-30px;margin-left:40px;border-radius:5px;font-size: 12px;font-weight: bold;display:none">Reserver
                                                le ticket</a>
                                        </div>

                                        {{-- <div id="no-responsive-voyage" class="col-9 col-md-9 col-lg-9">
                                            <ul>
                                                <li class="bull">
                                                    <span class="sous-bull">
                                                        <div class="destination">
                                                            <span class="label">{{ $voyage->lieu_depart->libelle }} - {{ $voyage->lieu_arrivee->libelle }}.</span>
                                                        </div>
                                                        <span>|</span>
                                                        <div class="depart">
                                                            <span class="label">Départ:</span>
                                                            <span class="value"><?=date('d-m-Y', strtotime($voyage->date_depart));?></span>
                                                        </div>
                                                    </span>
                                                </li>
                                                <li class="bull">
                                                    <span class="sous-bull">
                                                        <div class="tarif">
                                                            <span class="label">Tarif:</span>
                                                            <span>{{ $voyage->prix }} XOF</span>
                                                        </div>
                                                        <span>|</span>
                                                        <div class="compagnie">
                                                            <span class="label"> Compagnie: </span>
                                                            <span class="value">{{ $voyage->compagnie->nom }}</span>
                                                        </div>
                                                    </span>
                                                </li>
                                                <li class="bull">
                                                    <span class="sous-bull">
                                                        <div class="gare">
                                                            <span class="label">Gare: </span><span>{{ $voyage->depart_gare->libelle }}</span>
                                                        </div>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div> --}}

                                        {{-- <div id="responsive-voyage" class="col-9 col-md-9 col-lg-9">
                                            <ul>
                                                <li class="bull">
                                                    <span class="sous-bull">
                                                        <div class="destination">
                                                            <span class="label">ABOBO - YAMOU.</span>
                                                        </div>
                                                        <div class="depart">
                                                            <span class="label">Départ:</span>
                                                            <span class="value">10:30</span>
                                                        </div>
                                                        <div class="tarif">
                                                            <span class="label">Tarif:</span>
                                                            <span>5 500.00 XOF</span>
                                                        </div>
                                                        <div class="compagnie">
                                                            <span class="label"> Compagnie: </span>
                                                            <span class="value">LUTB</span>
                                                        </div>
                                                        <div class="gare">
                                                            <span class="label">Gare: </span><span>ABOBO </span>
                                                        </div>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div> --}}

                                    </div>
                                    <div class="row">
                                        <div class="col-3 col-md-12 col-lg-3" id="date-voyage">
                                            <span style="">
                                                <?php
                                                $date = '2021-01-01';
                                                ?>
                                                {{ $helpService->convertDateInFrench($voyage->date_depart->format('Y-m-d')) }}
                                            </span>
                                        </div>
                                        <input type="text" name="voyage_id" value="{{ $voyage->id }}" hidden>
                                        <div class="col-9 col-md-12 col-lg-9">
                                            <button type="submit" class="btn btn_reserver btn-success">Reserver le ticket</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endforeach


                        {{-- <div class="col-12 col-md-12"><div class="row justify-content-md-center"><div class="col-12 col-md-12"><div class="pagination"><span class="disabled">« <span class="precedent">Précédent</span>&nbsp;&nbsp;</span><span class="current">1</span><a href="javascript:void(0);" onclick="change_gare(2);">2</a><a href="javascript:void(0);" onclick="change_gare(3);">3</a><a href="javascript:void(0);" onclick="change_gare(4);">4</a><a href="javascript:void(0);" onclick="change_gare(2);"><span class="suivant">Suivant</span> »</a></div></div></div></div><br><br> --}}


                        <script>
                            $(document).ready(function() {
                                //  alert();
                                $("#mobile1").hide();
                                // $("#mobile10").hide();
                                $(".voir_choix_ticket").hide();

                                if (screen.width > 767) {
                                    $("#mobile1").hide();
                                    $("#desktop1").show();
                                    $("#desktop2").show();
                                } else if (screen.width < 767) {
                                    $("#mobile1").show();
                                    $("#desktop1").hide();
                                    $("#desktop2").hide();
                                }

                                if (screen.width > 767) {
                                    // $(".mobile10").hide();
                                    $(".desktop10").show();
                                    $(".desktop20").show();
                                } else if (screen.width < 767) {
                                    // $(".mobile10").show();
                                    $(".desktop10").hide();
                                    $(".desktop20").hide();
                                }


                                $("#tickets_achat").on('click', function() {
                                    // alert();
                                    $(".cache_choix_ticket").show();
                                    $(".voir_choix_ticket").hide();
                                    $("#tickets_achat").addClass('active');
                                    $("#mes_tickets").removeClass('active');
                                    $("#nbre_tickets").val('');
                                    $("#total_tickets").val('');

                                })

                                $("#mes_tickets").on('click', function() {
                                    //  alert();
                                    $(".cache_choix_ticket").hide();
                                    $(".voir_choix_ticket").show();
                                    $("#tickets_achat").removeClass('active');
                                    $("#mes_tickets").addClass('active');
                                    $("#nbre_tickets").val('');
                                    $("#total_tickets").val('');

                                })

                                // $(".btn_reserver").on('click', function() {
                                //     id_reserve_ref = $(this).val();
                                //       alert(id_reserve_ref);
                                //        $.ajax({
                                //          type:'POST',
                                //          url:'../page_reservation.php',
                                //          data:"ref_reserve=" + id_reserve_ref,
                                //          success: function(resultat) {

                                //          }


                                //        })

                                // })

                                $("#nbre_tickets").on('change', function() {
                                    //alert($(this).val());
                                    var nbre = $(this).val();

                                    var valeur = 2000;

                                    total = parseInt(nbre) * parseInt(valeur);

                                    var total_nbre = $("#total_tickets").val(total);

                                })



                                //mes_tickets
                            })
                        </script>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-center flex-pagination">
                {{ $voyages->links() }}
            </div>

            {{-- <div class="d-flex justify-content-center flex-pagination">
                <nav aria-label="Page navigation example" class="col-md-12">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">Précédent</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#">Suivant</a>
                        </li>
                    </ul>
                </nav>
            </div> --}}

        </div>
    </section>

@endsection
