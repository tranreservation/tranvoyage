<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('/voyages/reservation-callback', 'User\ReservationController@callback')->name('user.reserver.callback');
Route::post('/convois/reservation-callback', 'User\ReservationConvoisController@callback')->name('user.convois.reservation.callback');
Route::get('/convois/creer-convois-callback', 'User\ConvoisController@createConvoisCallback')->name('user.convois.creer.callback')->middleware('signed');
