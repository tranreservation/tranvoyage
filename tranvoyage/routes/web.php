<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', '');

// Auth::routes();

Auth::routes();

Route::get('/logout', function(){
    Auth::logout();
    return view('auth.login');
})->name('logout');

Route::get('/', 'HomeController@index')->name('home');
/*Route::get('/first-form','HomeController@firstForm')->name('firstform');
Route::get('/voyages/reservation-premiere-etape', 'User\ReservationController@premiereEtape')->name('user.reserver.premiereetape');
Route::get('/voyages/reservation-deuxieme-etape', 'User\ReservationController@deuxiemeEtape')->name('user.reserver.deuxiemetape');
Route::get('/voyages/reservation-success-etape', 'User\ReservationController@successEtape')->name('user.reserver.successtape');
// Route::post('/voyages/reservation-callback', 'User\ReservationController@callback')->name('user.reserver.callback');
Route::post('/voyages/reservation-callback', 'User\ReservationController@callback')->name('user.reserver.callback');
Route::get('/tickets/rechercher-ticket', 'User\TicketController@rechercherTicket')->name('user.ticket.rechercher');*/

Route::get('/first-form','HomeController@firstForm')->name('firstform');
Route::get('/convois/reservation-premiere-etape', 'User\ReservationConvoisController@premiereEtape')->name('user.convois.reservation.premiereetape');
Route::get('/convois/reservation-deuxieme-etape', 'User\ReservationConvoisController@deuxiemeEtape')->name('user.convois.reservation.deuxiemetape');
Route::get('/convois/reservation-success-etape', 'User\ReservationConvoisController@successEtape')->name('user.convois.reservation.successtape');
Route::post('/convois/reservation-callback', 'User\ReservationConvoisController@callback')->name('user.convois.reservation.callback');
Route::get('/convois/detail-convois/{code}', 'User\ReservationConvoisController@show')->name('user.convois.reservation.show');

Route::get('/tickets/rechercher-ticket', 'User\TicketController@viewRechercherTicket')->name('user.ticket.rechercher');
Route::post('/tickets/rechercher-ticket', 'User\TicketController@rechercherTicket')->name('user.ticket.rechercherticket');
Route::get('/tickets/rechercher-ticket-resultat/{statut}', 'User\TicketController@resultatRechercherTicket')->name('user.ticket.rechercher.resultat');

Route::get('/convois/rechercher-convois', 'User\ConvoisController@index')->name('user.convois.index');
Route::get('/convois/creer-convois-premiere-etape', 'User\ConvoisController@premiereetape')->name('user.convois.creer.premiereetape');
Route::get('/convois/creer-convois-deuxieme-etape', 'User\ConvoisController@deuxiemeetape')->name('user.convois.creer.deuxiemeetape');
Route::post('/convois/creer-convois-save-deuxieme-etape', 'User\ConvoisController@saveDeuxiemeEtape')->name('user.convois.creer.save.deuxiemeetape');
Route::get('/convois/creer-convois-troisieme-etape', 'User\ConvoisController@troisiemeetape')->name('user.convois.creer.troisiemeetape');
Route::post('/convois/creer-convois-save-troisieme-etape', 'User\ConvoisController@saveTroisiemeEtape')->name('user.convois.creer.save.troisiemeetape');
Route::get('/convois/creer-convois-quatrieme-etape', 'User\ConvoisController@quatriemeetape')->name('user.convois.creer.quatriemeetape');
Route::get('/convois/creer-convois-success-etape', 'User\ConvoisController@successEtape')->name('user.convois.creer.successtape');
Route::get('/convois/creer-convois-invalid-etape', function(){
    return view('user.pages.convois.creer.url_invalid');
})->name('user.convois.creer.invalid');
// Route::get('/discount', function(){
//     return 'some_discount_code_here';
// })->name('discountCode')->middleware('signed');

Route::get('/qui-sommes-nous', 'AppController@index')->name('app.qui_sommes_nous');
Route::get('/contact', 'AppController@contact')->name('app.contact');

Route::group(['middleware'=>'auth'], function () {
    Route::get('/user/dashboard', 'User\Dashboard\HomeController@dashboard')->name('user.dashboard');

    /*Les convois */
    Route::get('/user/convois', 'User\Dashboard\ConvoisController@index')->name('user.convois.index');
    Route::get('/user/convois/create', 'User\Dashboard\ConvoisController@create')->name('user.convois.create');
    Route::post('/user/convois/store', 'User\Dashboard\ConvoisController@store')->name('user.convois.store');
    Route::get('/user/convois/edit/{id}', 'User\Dashboard\ConvoisController@edit')->name('user.convois.edit');
    Route::put('/user/convois/update/{id}', 'User\Dashboard\ConvoisController@update')->name('user.convois.update');
    Route::get('/user/convois/delete/{id}', 'User\Dashboard\ConvoisController@delete')->name('user.convois.delete');
    Route::post('/user/convois/remove/{id}', 'User\Dashboard\ConvoisController@remove')->name('user.convois.remove');
    Route::get('/user/convois/show/{id}', 'User\Dashboard\ConvoisController@show')->name('user.convois.show.dashboard');

    /* Demande de retrait */
    Route::get('/user/retraits', 'User\Dashboard\RetraitController@index')->name('user.retraits.index');
    Route::get('/user/retraits/create', 'User\Dashboard\RetraitController@create')->name('user.retraits.create');
    Route::post('/user/retraits/stotre', 'User\Dashboard\RetraitController@store')->name('user.retraits.store');
    Route::get('/user/retraits/show/{id}', 'User\Dashboard\RetraitController@show')->name('user.retraits.show');

    /*Les transactions */
    Route::get('/user/transactions', 'User\Dashboard\TransactionController@index')->name('user.transactions.index');

    /*Les informations sur l'utilisateur */
    Route::get('/user/profile/edit', 'User\Dashboard\UserController@edit')->name('user.profile.edit');
    Route::post('/user/profile/update/{id}', 'User\Dashboard\UserController@update')->name('user.profile.update');

    Route::get('/user/tickets', 'User\Dashboard\TicketController@index')->name('user.tickets.index');
    Route::get('/user/tickets/edit/{code}', 'User\Dashboard\TicketController@edit')->name('user.tickets.edit');
    Route::post('/user/tickets/update/{code}', 'User\Dashboard\TicketController@update')->name('user.tickets.update');
});

Route::get('send-mail', function () {

   

    $details = [

        'title' => 'Mail from ItSolutionStuff.com',

        'body' => 'This is for testing email using smtp'

    ];

   

    \Mail::to('bootnetcrasher@gmail.com')->send(new \App\Mail\MyTestMail($details));

   

    dd("Email is Sent.");

});
