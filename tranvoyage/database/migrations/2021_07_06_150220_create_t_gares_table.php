<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTGaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_gares', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('compagnie_id');
            $table->unsignedBigInteger('params_lieu_id');
            $table->string('libelle');
            $table->boolean('actif')->default(0);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_gares', function(Blueprint $table) {
            /*$table->foreign('admin_id')->references('id')->on('t_admins')
                ->onDelete('restrict')
                ->onUpdate('restrict');*/
            $table->foreign('compagnie_id')->references('id')->on('t_compagnies')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('params_lieu_id')->references('id')->on('t_params_lieu')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

        Schema::table('t_voyages', function(Blueprint $table) {
            $table->foreign('depart_gare_id')->references('id')->on('t_gares')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('arrive_gare_id')->references('id')->on('t_gares')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_gares');
    }
}
