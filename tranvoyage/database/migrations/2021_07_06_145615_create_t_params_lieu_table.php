<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTParamsLieuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_params_lieu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('params_type_lieu_id');
            $table->unsignedBigInteger('sup_params_lieu_id')->nullable();
            $table->string('libelle');
            $table->string('description')->nullable();
            $table->boolean('actif')->default(0);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_params_lieu', function(Blueprint $table) {
            $table->foreign('params_type_lieu_id')->references('id')->on('t_params_type_lieu')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('sup_params_lieu_id')->references('id')->on('t_params_lieu')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_params_lieu');
    }
}
