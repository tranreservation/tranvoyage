<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_paiements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('voyage_id')->nullable();
            $table->unsignedBigInteger('convois_id')->nullable();
            $table->unsignedBigInteger('user_standard_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('params_status_paiement_id');
            $table->string('id_transaction')->unique();
            $table->integer('montant_total');
            $table->integer('montant_ticket_unitaire');
            $table->string('code');
            $table->string('telephone_paiement')->nullable();
            $table->integer('nombre_ticket');
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_paiements', function(Blueprint $table) {
            $table->foreign('voyage_id')->references('id')->on('t_voyages')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('params_status_paiement_id')->references('id')->on('t_params_status_paiement')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_paiements');
    }
}
