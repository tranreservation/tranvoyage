<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_tickets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('paiement_id');
            $table->unsignedBigInteger('params_status_ticket_id');
            $table->unsignedBigInteger('vendeur_admin_id')->nullable();
            $table->unsignedBigInteger('vendeur_id')->nullable();
            $table->string('code')->unique();
            $table->boolean('actif')->default(0);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_tickets', function(Blueprint $table) {
            $table->foreign('paiement_id')->references('id')->on('t_paiements')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('vendeur_admin_id')->references('id')->on('t_admins')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('vendeur_id')->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_tickets');
    }
}
