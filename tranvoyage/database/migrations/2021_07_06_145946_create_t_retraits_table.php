<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTRetraitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_retraits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('params_status_retrait_id');
            $table->unsignedBigInteger('transaction_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->integer('montant_retrait');
            $table->string('code')->nullable();
            $table->string('reference_mooney')->nullable();
            $table->string('commentaire')->nullable();
            $table->date('date_traitement')->nullable();
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_retraits', function(Blueprint $table) {
            $table->foreign('params_status_retrait_id')->references('id')->on('t_params_status_retrait')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_retraits');
    }
}
