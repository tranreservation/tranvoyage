<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTUsersStandartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_users_standards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('telephone');
            $table->string('email')->nullable();
            $table->boolean('actif')->default(0);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_paiements', function(Blueprint $table) {
            $table->foreign('user_standard_id')->references('id')->on('t_users_standards')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_users_standards');
    }
}
