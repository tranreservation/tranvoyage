<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTConvoisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_convois', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('params_status_convois_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->string('code')->unique();
            $table->string('libelle')->unique();
            $table->string('lieu_depart')->nullable();
            $table->string('lieu_arrivee')->nullable();
            $table->string('lieu_rassemblement')->nullable();
            $table->dateTime('date_depart')->nullable();
            $table->dateTime('date_arrivee')->nullable();
            $table->string('description')->nullable();
            $table->integer('prix');
            $table->integer('nbre_ticket')->nullable();
            $table->integer('ticket_disponible')->nullable();
            $table->boolean('actif')->default(0);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();

            /**
             * Prévoir des reservations vip
             */
        });

        Schema::table('t_convois', function(Blueprint $table) {
            $table->foreign('params_status_convois_id')->references('id')->on('t_params_status_convois')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('admin_id')->references('id')->on('t_admins')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

        Schema::table('t_paiements', function(Blueprint $table) {
            $table->foreign('convois_id')->references('id')->on('t_convois')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_convois');
    }
}
