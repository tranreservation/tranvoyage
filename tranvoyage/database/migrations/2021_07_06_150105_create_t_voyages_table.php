<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTVoyagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_voyages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('compagnie_id');
            $table->unsignedBigInteger('depart_gare_id');
            $table->unsignedBigInteger('arrive_gare_id');
            $table->unsignedBigInteger('lieu_depart_id');
            $table->unsignedBigInteger('lieu_arrive_id');
            $table->dateTime('date_depart');
            $table->dateTime('date_arrivee');
            $table->integer('prix');
            $table->boolean('actif')->default(0);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_voyages', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('admin_id')->references('id')->on('t_admins')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('compagnie_id')->references('id')->on('t_compagnies')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('lieu_depart_id')->references('id')->on('t_params_lieu')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('lieu_arrive_id')->references('id')->on('t_params_lieu')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_voyages');
    }
}
