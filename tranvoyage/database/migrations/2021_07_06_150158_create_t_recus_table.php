<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTRecusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_recus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('paiement_id');
            $table->date('date_validation')->nullable();
            $table->date('date_refus')->nullable();
            $table->string('cause_refus_recu')->nullable(); // Mettre les cause dans une table
            $table->string('commentaire')->nullable();
            $table->string('code');
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();
        });

        Schema::table('t_recus', function(Blueprint $table) {
            $table->foreign('paiement_id')->references('id')->on('t_paiements')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_recus');
    }
}
