<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTConvoisTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_convois_temp', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->string('nom_convois')->nullable();
            $table->string('description')->nullable();
            $table->integer('prix')->nullable();
            $table->string('nom_prenom_utilisateur')->nullable();
            $table->string('tel')->nullable();
            $table->string('email')->nullable();
            $table->string('lieu_depart')->nullable();
            $table->string('lieu_arrivee')->nullable();
            $table->string('date_depart')->nullable();
            $table->string('date_arrivee')->nullable();
            $table->boolean('email_checked')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_convois_temp');
    }
}
