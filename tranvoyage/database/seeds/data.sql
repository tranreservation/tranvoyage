SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

/* t_params_status_convois */
INSERT INTO `t_params_status_convois`(`libelle`, `description`, `actif`, `created_at`, `updated_at`) VALUES
('En attente', "En attente de validation de la part de l' administation", 1, now(),now()),
('Validé', "Validé part l' administation", 1, now(),now()),
('Rejeté', "Annulé par l'utilisateur", 1, now(),now()),
('Expiré', "Rejeté après analyse", 1, now(),now());

/* t_compagnies */
INSERT INTO `t_compagnies` (`nom`, `email`, `telephone`, `created_at`, `updated_at`) VALUES
('CTE TRANSPORT ', 'ctecompagniedetransportexpress@gmail.com', '0505489532', now(), now()),
('SABE TRANSPORT ', 'tran.reservation@gmail.com', '22504375', now(), now()),
('SITTI TRANSPORT ', 'sitti@gmail.com', '0777251106', now(), now()),
('GAMON TRANSPORT ', 'gamon@gmail.com', '0103339813', now(), now()),
('TTF TRANSPORT ', 'ttf@gmail.com', '0709044555', now(), now()),
('COMPAGNIE DE TRANSPORT TCHOLOGO OFFICIEL ', 'ctto@gmail.com', '0575112078/0747707778', now(), now()),
('GTT TRANSPORT ', 'gtt@gmail.com', '0140796023', now(), now()),
('STBA TRANSPORT ', 'stba@gmail.com', '0707673454', now(), now()),
('STC TRANSPORT ', 'stc@gmail.com', '0707853060', now(), now()),
('UTS TRANSPORT ', 'uts@gmail.com', '0777601570', now(), now()),
('TRANSPORT AVS', 'transportavs@gmail.com', '0102956063', now(), now()),
('GDF TRANSPORT ', 'gdftransport@gmail.com', '0779801963', now(), now()),
('TCF EXPRESS MISTRAL ', 'tcf@gmail.com', '0140511652', now(), now()),
('GDF TRANSPORT *', 'gdftransport@cmail.com', '20372037/ 0758609881', now(), now()),
('GANA TRANSPORT CI', 'ganatrans@yahoo.fr', '0777872347', now(), now()),
('ETS SAMA TRANSPORT ', 'samatransport@gmail.com', '0708028942/0574585827', now(), now()),
('TILEMSI TRANSPORT ABIDJAN ', 'tilemstransport@gmail.com', '+22344904212', now(), now()),
('SONEF TRANSPORT VOYAGEURS ', 'sonef_niger@yahoo.fr', '+22720734358/+22790901239/22720752174', now(), now()),
('CHONCO TRANSPORT ', 'dsilue7@gmail.com', '0586909092', now(), now()),
('UTB', 'contacts@utb.ci', '0564616360', now(), now()),
('OUEST TRANSACTIONS CI OT TRANSPORT ', 'ouesttransactionci@gmail.com', '20375186', now(), now());

/* t_params_type_lieu */
INSERT INTO `t_params_type_lieu` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Pays', 'Pays', 1, NULL, now(), now()),
(2, 'Ville', 'Ville', 1, NULL, now(), now()),
(3, 'Commune', 'Commune', 1, NULL, now(), now()),
(4, 'Secteur', NULL, 1, NULL, now(), now());

/* t_params_lieu */
INSERT INTO `t_params_lieu` (`id`, `params_type_lieu_id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, 2, 'Abidjan', NULL, 1, NULL, now(), now()),
(4, 3, 'ADJAME', NULL, 1, NULL, now(), now()),
(5, 3, 'ABOBO ', NULL, 1, NULL, now(), now()),
(6, 2, 'YAMOUSSOUKRO ', NULL, 1, NULL, now(), now()),
(7, 2, 'BOUAKE', NULL, 1, NULL, now(), now()),
(8, 2, 'KOROGHO', NULL, 1, NULL, now(), now()),
(9, 2, 'BEOUMI', NULL, 1, NULL, now(), now()),
(10, 2, 'BONON', NULL, 1, NULL, now(), now()),
(11, 3, 'YOPOUGON', NULL, 1, NULL, now(), now());

/* t_gares */
INSERT INTO `t_gares` (`id`, `compagnie_id`, `params_lieu_id`, `libelle`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 20, 4, "ADJAME", 1, NULL, now(), now()),
(2, 20, 5, "ABOBO", 1, NULL, now(), now()),
(3, 20, 6, "YAMOUSSOUKRO", 1, NULL, now(), now()),
(4, 20, 6, "BOUAKE", 1, NULL, now(), now()),
(5, 20, 6, "BEOUMI", 1, NULL, now(), now());

/* t_voyages */
INSERT INTO `t_voyages` (`id`, `admin_id`, `user_id`, `compagnie_id`, `depart_gare_id`, `arrive_gare_id`, `lieu_depart_id`, `lieu_arrive_id`, `date_depart`, `date_arrivee`, `prix`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(2, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(3, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(4, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(5, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(6, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(7, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(8, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(9, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(10, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(11, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(12, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(14, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now()),
(15, NULL, NULL, 20, 2, 3, 5, 6, '2021-07-15 10:22:28', '2021-07-15 12:22:28', 5500, 1, NULL, now(), now());


/* t_params_status_paiement */
INSERT INTO `t_params_status_paiement` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Initié', 'Le paiement a été initié', 1, NULL, '2021-07-16 14:24:36', '2021-07-16 14:24:36'),
(2, 'En cours', 'En cours de paiement', 0, NULL, '2021-07-16 14:25:30', '2021-07-16 14:25:30'),
(3, 'Finalisé', 'Le paiement a été finalisé', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48'),
(4, 'Annulé', 'Le paiement a été annulé', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48');

/* t_convois */
INSERT INTO `t_convois` (`id`, `params_status_convois_id`, `user_id`, `admin_id`, `code`, `libelle`, `lieu_depart`, `lieu_arrivee`, `lieu_rassemblement`, `date_depart`, `date_arrivee`, `description`, `prix`, `nbre_ticket`, `ticket_disponible`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 1, NULL, 'LQ20BRBR', 'Convois 1', NULL, 'Autem in voluptate c', 'Nam recusandae Culp', '2003-07-08 00:00:00', '1976-01-12 00:00:00', 'Quia doloremque eos', 53, 97, NULL, 0, NULL, '2021-07-20 00:13:22', '2021-07-20 00:13:22'),
(4, 2, 1, NULL, 'V5ZFLYIY', 'Convois 2', NULL, 'Eos nisi dignissimos', 'Magna pariatur Mole', '2020-08-05 00:00:00', '2011-07-27 00:00:00', 'Maiores perspiciatis', 11, 74, NULL, 0, NULL, '2021-07-20 00:36:56', '2021-07-20 00:36:56'),
(5, 2, 1, NULL, 'AYI88PWQ', 'Convois 3', NULL, 'Enim nesciunt ipsam', 'Quia natus natus par', '1992-07-02 00:00:00', '1983-06-09 00:00:00', 'Deserunt ab esse ven', 40, 98, NULL, 0, NULL, '2021-07-20 00:37:04', '2021-07-20 00:37:04'),
(6, 2, 1, NULL, 'B1Y8HX5Y', 'Convois 4', NULL, 'Ad ea qui fuga Repe', 'Vel eum magni irure', '1979-06-29 00:00:00', '2012-05-28 00:00:00', 'Aperiam sed recusand', 48, 56, NULL, 0, NULL, '2021-07-20 00:37:14', '2021-07-20 00:37:14'),
(7, 2, 1, NULL, 'PKJZRT6C', 'Convois 5', NULL, 'Aute cum sint et max', 'Quam in ex iste et N', '2015-07-21 00:00:00', '1978-08-23 00:00:00', 'Quis nisi sapiente v', 25, 82, NULL, 0, NULL, '2021-07-20 11:42:30', '2021-07-20 11:42:30'),
(8, 1, 1, NULL, 'Q2ZGLFRN', 'Convois 6', NULL, 'Mollitia corrupti q', 'Quis rerum enim expl', '1998-03-06 00:00:00', '1970-06-20 00:00:00', 'Excepturi quo commod', 71, 52, NULL, 0, NULL, '2021-07-20 11:49:41', '2021-07-20 11:49:41');

/* t_params_status_paiement */
INSERT INTO `t_params_status_tickets` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Valide', 'Le ticket est encore valide', 1, NULL, '2021-07-16 14:24:36', '2021-07-16 14:24:36'),
(2, 'Utilisé', 'Le ticket est déja utilisé', 0, NULL, '2021-07-16 14:25:30', '2021-07-16 14:25:30'),
(3, 'Expiré', 'Le ticket a expiré', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48'),
(4, 'Annulé', 'Le ticket est annulé', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48');

/* t_params_status_retraits */
INSERT INTO `t_params_status_retrait` (`id`, `libelle`, `description`, `actif`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'En attente', 'La demande est en attente de validation', 1, NULL, '2021-07-16 14:24:36', '2021-07-16 14:24:36'),
(2, 'Validé', 'La demande de retrait a été validé et transféré', 0, NULL, '2021-07-16 14:25:30', '2021-07-16 14:25:30'),
(3, 'Rejeté', 'La demande de retrait a été rejeté', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48'),
(4, 'Annulé', 'La demande de retrait a été annulé ', 0, NULL, '2021-07-16 14:26:48', '2021-07-16 14:26:48');
COMMIT;
