<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Models\Wallet;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Create a user
        $user = new User;
        $user->name = "john doe";
        $user->email  = "johndoe@yopmail.com";
        $user->telephone = "0745858995";
        $user->actif = 1;
        $user->password = Hash::make('123456789');
        $user->save();

        // Create a wallet for a user
        $wallet = new Wallet;
        $wallet->solde = 0;
        $wallet->user_id = $user->id;
        $wallet->save();
    }
}
