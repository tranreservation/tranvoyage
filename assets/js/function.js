$(document).ready(function() {
    //alert();
    // $("#mobile1").hide();
    // $("#mobile10").hide();
    /*$(".voir_choix_ticket").hide();
    $(".vu_code_validation").hide();
    $("#load_avant_ticket").hide();
    $("#load_avant_tab").hide();
    $(".voir_ticket_verif").hide();
    $('#load_avant_tab').hide();*/
    // if (screen.width > 767) {
    //     $("#mobile1").hide();
    //     $("#desktop1").show();
    //     $("#desktop2").show();
    // } else if (screen.width < 767) {
    //     $("#mobile1").show();
    //     $("#desktop1").hide();
    //     $("#desktop2").hide();
    // }

    // if (screen.width > 767) {
    //     $("#mobile10").hide();
    //     $("#desktop10").show();
    //     $("#desktop20").show();
    // } else if (screen.width < 767) {
    //     $("#mobile10").show();
    //     $("#desktop10").hide();
    //     $("#desktop20").hide();
    // }


    $("#tickets_achat").on('click', function() {
        // alert();
        $(".cache_choix_ticket").show();
        $(".voir_choix_ticket").hide();
        $("#tickets_achat").addClass('active');
        $("#mes_tickets").removeClass('active');
        // $("#nbre_tickets").val('');
        // $("#total_tickets").val('');

    })

    $("#mes_tickets").on('click', function() {
        //  alert();
        $(".cache_choix_ticket").hide();
        $(".voir_choix_ticket").show();
        $("#tickets_achat").removeClass('active');
        $("#mes_tickets").addClass('active');
        //  $("#nbre_tickets").val('');
        //  $("#total_tickets").val('');

    })

    $("#nbre_tickets").on('change', function() {
        // alert($(this).val());
        //   $("#total_tickets").val("");
        var total = 0;

        var nbre = $(this).val();

        var valeur = $("#prix_unitaire").val();

        total = parseInt(nbre) * parseInt(valeur);

        $("#total_tickets").val(total);

    })

    $('#form_compte_ticket').on('submit', function(e) {
        e.preventDefault();
        // alert();
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: "template/compte_ticket.php",
            data: new FormData(this),
            beforeSend: function() {
                $('#icon_paiment').addClass("fas fa-sync fa-spin").show();
            },
            success: function(msg) {
                //  alert(msg);
                if (msg == 1) {
                    // var title = "Paiement",
                    //     message = "Enregistrement éffectué, votre compte de paiement vient d'être créé afin de pouvoir valider le paiement",
                    //     type = "success";
                    alert("Erreur !")
                        //  addtoast(title, message, type);
                } else {

                    // alert(msg);
                    //  var title = "Paiement",
                    // var message = "Enregistrement éffectué, un code de validation vient de vous êtes envoyé par sms sur ce numéro",
                    //     type = "success";
                    // addtoastpaie(message, type);
                    addtoastclient();
                    setTimeout(function() {
                        $('#icon_paiment').removeClass("fas fa-sync fa-spin").show();
                    }, 2500);
                    $("#email").val('');
                    $("#tel_ticket").val('');

                    $("#load_avant_ticket").show();
                    setTimeout(function() {
                        $("#load_avant_ticket").hide();
                        // $(".vu_code_validation").show();
                    }, 4000);

                    msg = JSON.parse(msg);
                    let code_ticket = msg.code_ticket;
                    let montant_total_ticket = msg.montant_total_ticket;
                    let nombre_ticket = msg.nombre_ticket;
                    let mont_ticket_unitaire = msg.mont_ticket_unitaire;
                    let telephone = msg.telephone;
                    let email = msg.email;
                    $(location).attr("href", "paiement_ticket.php?code_ticket=" + code_ticket + "&montant_total_ticket=" + montant_total_ticket + "&nombre_ticket=" + nombre_ticket + "&mont_ticket_unitaire=" + mont_ticket_unitaire + "&telephone=" + telephone + "&email=" + email);


                    // addtoast(title, message, type); tel_ticket

                }
            }
        });
        // }
        return false;

    });



    $("#form_verif_ticket").on('submit', function(e) {
        e.preventDefault();
        // alert();
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: "template/verif_ticket.php",
            data: new FormData(this),
            beforeSend: function() {
                $('#load_avant_tab').show();
            },
            success: function(msg) {
                //  alert(msg);
                $('#load_avant_tab').show();
                setTimeout(function() {
                    $('#load_avant_tab').hide();
                }, 2000);
                $(".voir_ticket_verif").html(msg).show();
            }
        });
    });



    $("#envoie_code_valide").on('click', function(e) {
        e.preventDefault();
        // alert($("#code_validation").val());
        $.ajax({
            type: "POST",
            url: "template/confirme_ticket.php",
            data: "code_validation=" + $("#code_validation").val(),
            dataType: "json",
            beforeSend: function() {
                $('#icon_paiment_validation').addClass("fas fa-circle-notch fa-spin");
            },
            success: function(msg) {
                if (msg == 1) {
                    erreurverifticket();
                    $('#icon_paiment_validation').removeClass("fas fa-circle-notch fa-spin");
                } else {
                    console.log(msg);
                    console.log(msg.code_ticket);
                    code_ticket = msg.code_ticket; //
                    montant_total_ticket = msg.montant_total_ticket;
                    nombre_ticket = msg.nombre_ticket;
                    $(location).attr("href", "paiement_ticket.php?code_ticket=" + code_ticket + "&montant_total_ticket=" + montant_total_ticket + "&nombre_ticket=" + nombre_ticket);
                    return;
                }
            }
        });
    });




    // info();  <i class="fas fa-circle-notch fa-spin"></i>

    //mes_tickets
})


function info() {

    Swal.fire(
        'Information',
        'Le numéro de téléphone que vous saisisez vous ouvres un compte afin de confirmer le paiement du ticket, il doit être préfixé du 225',
    )
}

function newsletter() {

    Swal.fire(
        'Newsletter',
        'Abonnement éffectué avec succès',
    )
    setTimeout(function() {
        location.reload();
    }, 2000);
}


function addtoastpaie(message, type) {

    Swal.fire({
            position: 'top-end',
            icon: type,
            title: message,
            showConfirmButton: false,
            timer: 1500
        })
        //location.reload();
}

function addtoastclient() {
    var notify = $.notify('<strong>Chargement</strong> En attente d\'envoie...', {
        allow_dismiss: false,
        showProgressbar: true
    });

    setTimeout(function() {
        notify.update({ 'type': 'success', 'message': '<strong>Succès</strong> Un sms de validation vous a été envoyé !', 'progress': 25 });
    }, 4500);

}

function erreurverifticket() {
    $.notify({
        title: '<strong>Désolé !</strong>',
        message: 'Votre code de validation est incorrecte ou introuvable dans notre base.'
    }, {
        type: 'danger'
    });

}
